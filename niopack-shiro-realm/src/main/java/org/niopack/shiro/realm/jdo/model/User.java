package org.niopack.shiro.realm.jdo.model;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.base.MoreObjects;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.jdo.annotations.Element;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.Join;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import javax.jdo.annotations.Unique;
import javax.validation.constraints.NotNull;

/**
 * The persistent class for application users.
 */
@PersistenceCapable(detachable = "true", identityType = IdentityType.APPLICATION)
@Unique(name = "USER_NAME_IDX", members = {"userName"})
public class User {

  @PrimaryKey
  @Persistent(column = "user_id", valueStrategy = IdGeneratorStrategy.IDENTITY)
  private long userId;

  @Persistent(column = "user_name")
  @NotNull
  private String userName;

  @Persistent(column = "credential")
  @NotNull
  private String credential;

  @Persistent(table = "users_roles")
  @Join(column = "user_id")
  @Element(column = "role_id")
  private Set<Role> roles = new HashSet<>();

  public long getUserId() {
    return userId;
  }

  void setUserId(long userId) {
    this.userId = userId;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = checkNotNull(userName, "userName");
  }

  public String getCredential() {
    return credential;
  }

  public void setCredential(String credential) {
    this.credential = checkNotNull(credential, "credential");
  }

  public Set<Role> getRoles() {
    return roles;
  }

  public void setRoles(Set<Role> roles) {
    this.roles = roles;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.userId);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof User)) {
      return false;
    }
    User that = (User) obj;
    return Objects.equals(this.userId, that.userId);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("userId", this.userId)
        .add("userName", this.userName)
        .toString();
  }
}
