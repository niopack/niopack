package org.niopack.shiro.realm.jdo.model;

import static com.google.common.base.Preconditions.checkNotNull;
import static javax.jdo.annotations.IdGeneratorStrategy.IDENTITY;
import static javax.jdo.annotations.IdentityType.APPLICATION;

import com.google.common.base.MoreObjects;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.jdo.annotations.Element;
import javax.jdo.annotations.Join;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import javax.validation.constraints.NotNull;

/**
 * The persistent class for roles.
 */
@PersistenceCapable(detachable = "true", identityType = APPLICATION)
// TODO(codefx): Add <userId, roleName> unique index.
public class Role {

  @PrimaryKey
  @Persistent(column = "role_id", valueStrategy = IDENTITY)
  private long roleId;

  @Persistent(column = "role_name")
  @NotNull
  private String roleName;

  @Persistent(table = "roles_permissions")
  @Join(column = "role_id")
  @Element(column = "permission_id")
  private Set<Permission> permissions = new HashSet<>();

  @Persistent(mappedBy = "roles")
  private Set<User> users = new HashSet<>();

  public long getRoleId() {
    return roleId;
  }

  void setRoleId(long roleId) {
    this.roleId = roleId;
  }

  public String getRoleName() {
    return roleName;
  }

  public void setRoleName(String roleName) {
    this.roleName = checkNotNull(roleName, "roleName");
  }

  public Set<Permission> getPermissions() {
    return permissions;
  }

  public void setPermissions(Set<Permission> permissions) {
    this.permissions = permissions;
  }

  public Set<User> getUsers() {
    return users;
  }

  public void setUsers(Set<User> users) {
    this.users = users;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.roleId);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Role)) {
      return false;
    }
    Role that = (Role) obj;
    return Objects.equals(this.roleId, that.roleId);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("roleId", this.roleId)
        .add("roleName", this.roleName)
        .toString();
  }
}
