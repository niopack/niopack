package org.niopack.shiro.realm.jdo.dao;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.jdo.PersistenceManager;
import org.datanucleus.api.jdo.JDOPersistenceManager;
import org.datanucleus.query.typesafe.BooleanExpression;
import org.datanucleus.query.typesafe.TypesafeQuery;
import org.niopack.shiro.realm.jdo.model.Permission;
import org.niopack.shiro.realm.jdo.model.QPermission;
import org.niopack.shiro.realm.jdo.model.QRole;

public class PermissionDao {

  public Permission getByPermissionId(PersistenceManager pm, long permissionId) {
    checkNotNull(pm, "pm");

    return pm.getObjectById(Permission.class, permissionId);
  }

  public Permission getByPermissionName(JDOPersistenceManager jdopm, String permissionName) {
    checkNotNull(jdopm, "jdopm");
    checkNotNull(permissionName, "permissionName");

    TypesafeQuery<Permission> tq = jdopm.newTypesafeQuery(Permission.class);
    QPermission cand = QPermission.candidate();
    return tq.filter(cand.permissionName.eq(permissionName)).executeUnique();
  }

  public List<Permission> findByRoleNames(JDOPersistenceManager jdopm, Set<String> roleNames) {
    checkNotNull(jdopm, "jdopm");
    checkNotNull(roleNames, "roleNames");

    if (roleNames.isEmpty()) {
      return new ArrayList<>();
    }
    TypesafeQuery<Permission> tq = jdopm.newTypesafeQuery(Permission.class);
    QPermission cand = QPermission.candidate();
    QRole var = QRole.variable("var");
    int index = 0;
    BooleanExpression filter = null;
    for (String roleName : roleNames) {
      BooleanExpression expr = var.roleName.eq(tq.stringParameter("rn_" + index));
      filter = filter == null ? expr : filter.or(expr);
      tq.setParameter("rn_" + index, roleName);
      ++index;
    }
    return tq.filter(cand.roles.contains(var).and(filter))
        .executeResultList(Permission.class, true);
  }
}
