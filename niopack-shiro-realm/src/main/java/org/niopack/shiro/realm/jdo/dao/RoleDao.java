package org.niopack.shiro.realm.jdo.dao;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.jdo.PersistenceManager;
import org.datanucleus.api.jdo.JDOPersistenceManager;
import org.datanucleus.query.typesafe.TypesafeQuery;
import org.niopack.shiro.realm.jdo.model.QRole;
import org.niopack.shiro.realm.jdo.model.Role;

public class RoleDao {

  public Role getByRoleId(PersistenceManager pm, long roleId) {
    checkNotNull(pm, "pm");

    return pm.getObjectById(Role.class, roleId);
  }

  public Role getByRoleName(JDOPersistenceManager jdopm, String roleName) {
    checkNotNull(jdopm, "jdopm");
    checkNotNull(roleName, "roleName");

    TypesafeQuery<Role> tq = jdopm.newTypesafeQuery(Role.class);
    QRole cand = QRole.candidate();
    return tq.filter(cand.roleName.eq(roleName)).executeUnique();
  }
}
