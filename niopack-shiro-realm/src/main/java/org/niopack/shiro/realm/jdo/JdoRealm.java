package org.niopack.shiro.realm.jdo;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.jdo.FetchGroup;
import org.apache.shiro.authc.AccountException;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAccount;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.niopack.db.jdo.Transactor;
import org.niopack.logging.ContextualLogger;
import org.niopack.shiro.realm.jdo.dao.PermissionDao;
import org.niopack.shiro.realm.jdo.dao.UserDao;
import org.niopack.shiro.realm.jdo.model.User;
import org.slf4j.Logger;

/**
 * Realm that allows authentication and authorization via JDBC calls. The default queries suggest a
 * potential schema for retrieving the user's password for authentication, and querying for a user's
 * roles and permissions. The default queries can be overridden by setting the query properties of
 * the realm.
 * <p/>
 * If the default implementation of authentication and authorization cannot handle your schema, this
 * class can be subclassed and the appropriate methods overridden. (usually
 * {@link #doGetAuthenticationInfo(org.apache.shiro.authc.AuthenticationToken)},
 * {@link #getRoleNamesForUser(java.sql.Connection,String)}, and/or
 * {@link #getPermissions(java.sql.Connection,String,java.util.Collection)}
 * <p/>
 * This realm supports caching by extending from {@link org.apache.shiro.realm.AuthorizingRealm}.
 *
 * @since 0.2
 */
public class JdoRealm extends AuthorizingRealm {

  private static final Logger log = ContextualLogger.create();

  protected UserDao userDao = new UserDao();
  protected PermissionDao permissionDao = new PermissionDao();
  protected boolean permissionsLookupEnabled = false;

  protected final Transactor transactor;

  @Inject
  public JdoRealm(Transactor transactor) {
    this.transactor = checkNotNull(transactor, "transactor");
  }

  /**
   * Enables lookup of permissions during authorization. The default is "false" - meaning that only
   * roles are associated with a user. Set this to true in order to lookup roles <b>and</b>
   * permissions.
   *
   * @param permissionsLookupEnabled true if permissions should be looked up during authorization,
   *        or false if only roles should be looked up.
   */
  public void setPermissionsLookupEnabled(boolean permissionsLookupEnabled) {
    this.permissionsLookupEnabled = permissionsLookupEnabled;
  }

  @Override
  protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token)
      throws AuthenticationException {
    UsernamePasswordToken upToken = (UsernamePasswordToken) token;
    final String username = upToken.getUsername();

    // Null username is invalid
    if (username == null) {
      throw new AccountException("Null usernames are not allowed by this realm.");
    }

    SimpleAccount info = null;
    try {
      User user = transactor.retrieve(jdopm -> userDao.getByUserName(jdopm, username));
      if (user == null) {
        throw new UnknownAccountException("No account found for user [" + username + "]");
      }
      info = new SimpleAccount(username, user.getCredential(), getName());
    } catch (Exception e) {
      final String message = "There was a SQL error while authenticating user [" + username + "]";
      if (log.isErrorEnabled()) {
        log.error(message, e);
      }
      // Rethrow any SQL errors as an authentication exception
      throw new AuthenticationException(message, e);
    }
    return info;
  }

  /**
   * This implementation of the interface expects the principals collection to return a String
   * username keyed off of this realm's {@link #getName() name}
   *
   * @see #getAuthorizationInfo(org.apache.shiro.subject.PrincipalCollection)
   */
  @Override
  protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
    // null usernames are invalid
    if (principals == null) {
      throw new AuthorizationException("PrincipalCollection method argument cannot be null.");
    }

    String username = (String) getAvailablePrincipal(principals);
    try {
      // Retrieve roles and permissions from database
      User user = transactor.retrieve(pm -> {
        pm.getFetchPlan().setGroup(FetchGroup.ALL);
        return userDao.getByUserName(pm, username);
      });
      Set<String> roleNames = user.getRoles()
          .stream()
          .map(r -> r.getRoleName())
          .collect(Collectors.toSet());

      Set<String> permissions = null;
      if (permissionsLookupEnabled) {
        permissions = transactor.retrieve((pm) -> permissionDao.findByRoleNames(pm, roleNames)
            .stream()
            .map(p -> p.getPermissionName())
            .collect(Collectors.toSet()));
      }

      SimpleAuthorizationInfo info = new SimpleAuthorizationInfo(roleNames);
      info.setStringPermissions(permissions);
      return info;
    } catch (Exception e) {
      final String message = "There was a SQL error while authorizing user [" + username + "]";
      if (log.isErrorEnabled()) {
        log.error(message, e);
      }
      // Rethrow any SQL errors as an authorization exception
      throw new AuthorizationException(message, e);
    }
  }
}
