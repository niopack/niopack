package org.niopack.shiro.realm.jdo.dao;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.jdo.PersistenceManager;
import org.datanucleus.api.jdo.JDOPersistenceManager;
import org.datanucleus.query.typesafe.TypesafeQuery;
import org.niopack.shiro.realm.jdo.model.QUser;
import org.niopack.shiro.realm.jdo.model.User;

public class UserDao {

  public User getByUserId(PersistenceManager pm, long userId) {
    checkNotNull(pm, "pm");

    return pm.getObjectById(User.class, userId);
  }

  public User getByUserName(JDOPersistenceManager jdopm, String userName) {
    checkNotNull(jdopm, "jdopm");
    checkNotNull(userName, "userName");

    TypesafeQuery<User> tq = jdopm.newTypesafeQuery(User.class);
    QUser cand = QUser.candidate();
    return tq.filter(cand.userName.eq(userName)).executeUnique();
  }
}
