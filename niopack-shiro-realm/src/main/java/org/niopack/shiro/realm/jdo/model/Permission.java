package org.niopack.shiro.realm.jdo.model;

import static com.google.common.base.Preconditions.checkNotNull;
import static javax.jdo.annotations.IdGeneratorStrategy.IDENTITY;
import static javax.jdo.annotations.IdentityType.APPLICATION;

import com.google.common.base.MoreObjects;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import javax.validation.constraints.NotNull;

/**
 * The persistent class for permissions.
 */
@PersistenceCapable(detachable = "true", identityType = APPLICATION)
public class Permission {

  @PrimaryKey
  @Persistent(column = "permission_id", valueStrategy = IDENTITY)
  private long permissionId;

  @Persistent(column = "permission_name")
  @NotNull
  private String permissionName;

  @Persistent(mappedBy = "permissions")
  private Set<Role> roles = new HashSet<>();

  public long getPermissionId() {
    return permissionId;
  }

  void setPermissionId(long permissionId) {
    this.permissionId = permissionId;
  }

  public String getPermissionName() {
    return permissionName;
  }

  public void setPermissionName(String permissionName) {
    this.permissionName = checkNotNull(permissionName, "permissionName");
  }

  public Set<Role> getRoles() {
    return roles;
  }

  public void setRoles(Set<Role> roles) {
    this.roles = roles;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.permissionId);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Permission)) {
      return false;
    }
    Permission that = (Permission) obj;
    return Objects.equals(this.permissionId, that.permissionId);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("permissionId", this.permissionId)
        .add("permissionName", this.permissionName)
        .toString();
  }
}
