package org.niopack.shiro.module;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import javax.inject.Named;
import org.apache.shiro.authc.credential.PasswordMatcher;
import org.niopack.db.jdo.Transactor;
import org.niopack.shiro.realm.jdo.JdoRealm;

public class RealmModule extends AbstractModule {

  @Override
  protected void configure() {
    install(new PasswordServiceModule());
  }

  @Provides
  private JdoRealm provideRealm(Transactor transactor, PasswordMatcher passwordMatcher,
      @Named("shiro.permissionsLookupEnabled") boolean permissionsLookupEnabled) {
    JdoRealm realm = new JdoRealm(transactor);
    realm.setCredentialsMatcher(passwordMatcher);
    realm.setPermissionsLookupEnabled(permissionsLookupEnabled);
    return realm;
  }
}
