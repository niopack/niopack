package org.niopack.shiro.realm.jdo.model;

import com.google.common.testing.EqualsTester;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class PermissionTest {

  @Test
  public void testEquality() {
    Permission p0a = new Permission();
    Permission p0b = new Permission();

    Permission p1a = new Permission();
    p1a.setPermissionId(1L);
    p1a.setPermissionName("p1a");

    Permission p1b = new Permission();
    p1b.setPermissionId(1L);
    p1b.setPermissionName("p1b");

    Permission p2 = new Permission();
    p2.setPermissionId(2L);

    new EqualsTester()
        .addEqualityGroup(p0a, p0b)
        .addEqualityGroup(p1a, p1b)
        .addEqualityGroup(p2)
        .testEquals();
  }
}
