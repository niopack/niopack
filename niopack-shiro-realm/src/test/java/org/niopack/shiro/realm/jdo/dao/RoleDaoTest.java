package org.niopack.shiro.realm.jdo.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.niopack.db.testing.MockDatabase;
import org.niopack.shiro.realm.jdo.model.Role;

@RunWith(JUnit4.class)
public class RoleDaoTest {

  @Rule public MockDatabase mockDb = new MockDatabase();

  @Test
  public void shouldInsertSuccessfully() {
    Role role = new Role();
    role.setRoleName("admin");

    mockDb.getTransactor().run(pm -> {
      pm.makePersistent(role);
      assertNotNull(pm.getObjectById(Role.class, role.getRoleId()));
    });
  }

  @Test
  public void shouldGetByIdSuccessfully() {
    Role role = new Role();
    role.setRoleName("admin");

    mockDb.getTransactor().run(pm -> {
      RoleDao roleDao = new RoleDao();
      pm.makePersistent(role);
      assertEquals(role, roleDao.getByRoleId(pm, role.getRoleId()));
    });
  }

  @Test
  public void shouldGetByNameSuccessfully() {
    Role role = new Role();
    role.setRoleName("admin");

    mockDb.getTransactor().run(pm -> {
      RoleDao roleDao = new RoleDao();
      assertNull(roleDao.getByRoleName(pm, role.getRoleName()));
      pm.makePersistent(role);
      assertEquals(role, roleDao.getByRoleName(pm, role.getRoleName()));
    });
  }
}
