package org.niopack.shiro.realm.jdo;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.name.Names;
import javax.inject.Inject;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.PasswordService;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ThreadContext;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.niopack.db.jdo.Transactor;
import org.niopack.db.testing.MockDatabase;
import org.niopack.shiro.module.RealmModule;
import org.niopack.shiro.realm.jdo.model.Permission;
import org.niopack.shiro.realm.jdo.model.Role;
import org.niopack.shiro.realm.jdo.model.User;

@RunWith(JUnit4.class)
public class JdoRealmTest {

  private static final String USER_NAME = "testUser";
  private static final String PLAIN_TEXT_PASSWORD = "testPassword";
  private static final String TEST_ROLE = "testRole";
  private static final String TEST_PERMISSION = "testDomain:testTarget:testAction";

  @Rule public MockDatabase mockDb = new MockDatabase();
  @Rule public ExpectedException thrown = ExpectedException.none();

  @Inject private PasswordService passwordService;
  @Inject private JdoRealm realm;
  private DefaultSecurityManager securityManager;

  @Before
  public void setUp() {
    ThreadContext.remove();
    Injector injector = Guice.createInjector(new RealmModule(), new AbstractModule() {
      @Override protected void configure() {
        bind(Transactor.class).toInstance(mockDb.getTransactor());
        bind(Realm.class).to(JdoRealm.class);
        bindConstant().annotatedWith(Names.named("shiro.permissionsLookupEnabled")).to(true);
      }
    });
    injector.injectMembers(this);
    securityManager = new DefaultSecurityManager(realm);
    SecurityUtils.setSecurityManager(securityManager);
    createSchema();
  }

  @After
  public void tearDown() {
    SecurityUtils.setSecurityManager(null);
    securityManager.destroy();
    ThreadContext.remove();
  }

  @Test
  public void testLoginSuccess() throws Exception {
    Subject.Builder builder = new Subject.Builder(securityManager);
    Subject currentUser = builder.buildSubject();
    UsernamePasswordToken token = new UsernamePasswordToken(USER_NAME, PLAIN_TEXT_PASSWORD);
    currentUser.login(token);
    currentUser.logout();
  }

  @Test
  public void testWrongPassword() throws Exception {
    Subject.Builder builder = new Subject.Builder(securityManager);
    Subject currentUser = builder.buildSubject();
    UsernamePasswordToken token = new UsernamePasswordToken(USER_NAME, "passwrd");
    thrown.expect(IncorrectCredentialsException.class);
    currentUser.login(token);
  }

  @Test
  public void testRolePresent() throws Exception {
    Subject.Builder builder = new Subject.Builder(securityManager);
    Subject currentUser = builder.buildSubject();
    UsernamePasswordToken token = new UsernamePasswordToken(USER_NAME, PLAIN_TEXT_PASSWORD);
    currentUser.login(token);
    assertTrue(currentUser.hasRole(TEST_ROLE));
  }

  @Test
  public void testRoleNotPresent() throws Exception {
    Subject.Builder builder = new Subject.Builder(securityManager);
    Subject currentUser = builder.buildSubject();
    UsernamePasswordToken token = new UsernamePasswordToken(USER_NAME, PLAIN_TEXT_PASSWORD);
    currentUser.login(token);
    assertFalse(currentUser.hasRole("Game Overall Director"));
  }

  @Test
  public void testPermissionPresent() throws Exception {
    realm.setPermissionsLookupEnabled(true);

    Subject.Builder builder = new Subject.Builder(securityManager);
    Subject currentUser = builder.buildSubject();
    UsernamePasswordToken token = new UsernamePasswordToken(USER_NAME, PLAIN_TEXT_PASSWORD);
    currentUser.login(token);
    assertTrue(currentUser.isPermitted(TEST_PERMISSION));
  }

  @Test
  public void testPermissionNotPresent() throws Exception {
    realm.setPermissionsLookupEnabled(true);

    Subject.Builder builder = new Subject.Builder(securityManager);
    Subject currentUser = builder.buildSubject();
    UsernamePasswordToken token = new UsernamePasswordToken(USER_NAME, PLAIN_TEXT_PASSWORD);
    currentUser.login(token);
    assertFalse(currentUser.isPermitted("testDomain:testTarget:specialAction"));
  }

  private void createSchema() {
    User user = new User();
    user.setUserName(USER_NAME);
    user.setCredential(passwordService.encryptPassword(PLAIN_TEXT_PASSWORD));

    Role role = new Role();
    role.setRoleName(TEST_ROLE);

    Permission permission = new Permission();
    permission.setPermissionName(TEST_PERMISSION);

    user.getRoles().add(role);
    role.getUsers().add(user);

    role.getPermissions().add(permission);
    permission.getRoles().add(role);

    mockDb.getTransactor().run(pm -> {
      pm.makePersistent(user);
      pm.makePersistent(role);
      pm.makePersistent(permission);
    });
  }
}
