package org.niopack.shiro.realm.jdo.dao;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.niopack.db.testing.MockDatabase;
import org.niopack.shiro.realm.jdo.model.Permission;
import org.niopack.shiro.realm.jdo.model.Role;

@RunWith(JUnit4.class)
public class PermissionDaoTest {

  @Rule public MockDatabase mockDb = new MockDatabase();

  @Test
  public void shouldUpsertSuccessfully() {
    Permission permission = new Permission();
    permission.setPermissionName("can_add");

    mockDb.getTransactor().run(pm -> {
      pm.makePersistent(permission);
      assertNotNull(pm.getObjectById(Permission.class, permission.getPermissionId()));
    });
  }

  @Test
  public void shouldGetByIdSuccessfully() {
    Permission permission = new Permission();
    permission.setPermissionName("can_add");

    mockDb.getTransactor().run(pm -> {
      PermissionDao permissionDao = new PermissionDao();
      pm.makePersistent(permission);
      assertEquals(permission, permissionDao.getByPermissionId(pm, permission.getPermissionId()));
    });
  }

  @Test
  public void shouldGetByNameSuccessfully() {
    Permission permission = new Permission();
    permission.setPermissionName("can_add");

    mockDb.getTransactor().run(pm -> {
      PermissionDao permissionDao = new PermissionDao();
      assertNull(permissionDao.getByPermissionName(pm, permission.getPermissionName()));
      pm.makePersistent(permission);
      assertEquals(permission,
          permissionDao.getByPermissionName(pm, permission.getPermissionName()));
    });
  }

  @Test
  public void shouldFindNoRolesForNonexistentRoleName() {
    mockDb.getTransactor().run(pm -> {
      PermissionDao permissionDao = new PermissionDao();
      Set<String> roleNames = new HashSet<>(Arrays.asList("r1"));
      assertTrue(permissionDao.findByRoleNames(pm, roleNames).isEmpty());
    });
  }

  @Test
  public void shouldFindNoRolesForEmptyRoleNames() {
    mockDb.getTransactor().run(pm -> {
      PermissionDao permissionDao = new PermissionDao();
      assertTrue(permissionDao.findByRoleNames(pm, new HashSet<>()).isEmpty());
    });
  }

  @Test
  public void shouldFindRolesForExistingRoleName() {
    mockDb.getTransactor().run(pm -> {
      Role r1 = new Role();
      r1.setRoleName("r1");

      Role r2 = new Role();
      r2.setRoleName("r2");

      Permission p1 = new Permission();
      p1.setPermissionName("p1");
      p1.getRoles().add(r1);

      Permission p2 = new Permission();
      p2.setPermissionName("p2");
      p2.getRoles().add(r2);

      Permission p3 = new Permission();
      p3.setPermissionName("p3");
      p3.getRoles().add(r1);
      p3.getRoles().add(r2);

      r1.getPermissions().add(p1);
      r1.getPermissions().add(p3);

      r2.getPermissions().add(p2);
      r2.getPermissions().add(p3);

      pm.makePersistent(r1);
      pm.makePersistent(r2);

      pm.makePersistent(p1);
      pm.makePersistent(p2);
      pm.makePersistent(p3);

      Set<String> roleNames = new HashSet<>(Arrays.asList("r1", "r2"));
      List<Permission> result = new PermissionDao().findByRoleNames(pm, roleNames);
      assertThat(result).containsExactly(p1, p2, p3);
    });
  }
}
