package org.niopack.shiro.realm.jdo.model;

import com.google.common.testing.EqualsTester;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class RoleTest {

  @Test
  public void testEquality() {
    Role r0a = new Role();
    Role r0b = new Role();

    Role r1a = new Role();
    r1a.setRoleId(1L);
    r1a.setRoleName("r1a");

    Role r1b = new Role();
    r1b.setRoleId(1L);
    r1b.setRoleName("r1b");

    Role r2 = new Role();
    r2.setRoleId(2L);

    new EqualsTester()
        .addEqualityGroup(r0a, r0b)
        .addEqualityGroup(r1a, r1b)
        .addEqualityGroup(r2)
        .testEquals();
  }
}
