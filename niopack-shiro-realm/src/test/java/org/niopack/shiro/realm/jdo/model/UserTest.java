package org.niopack.shiro.realm.jdo.model;

import com.google.common.testing.EqualsTester;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class UserTest {

  @Test
  public void testEquality() {
    User u0a = new User();
    User u0b = new User();

    User u1a = new User();
    u1a.setUserId(1L);
    u1a.setCredential("p1a");

    User u1b = new User();
    u1b.setUserId(1L);
    u1b.setCredential("p1b");

    User u2 = new User();
    u2.setUserId(2L);

    new EqualsTester()
        .addEqualityGroup(u0a, u0b)
        .addEqualityGroup(u1a, u1b)
        .addEqualityGroup(u2)
        .testEquals();
  }
}
