package org.niopack.shiro.realm.jdo.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.niopack.db.testing.MockDatabase;
import org.niopack.shiro.realm.jdo.model.User;

@RunWith(JUnit4.class)
public class UserDaoTest {

  @Rule public MockDatabase mockDb = new MockDatabase();

  @Test
  public void shouldInsertSuccessfully() {
    User user = new User();
    user.setUserName("john");
    user.setCredential("p@ssw0rd");

    mockDb.getTransactor().run(pm -> {
      pm.makePersistent(user);
      assertNotNull(pm.getObjectById(User.class, user.getUserId()));
    });
  }

  @Test
  public void shouldGetByIdSuccessfully() {
    User user = new User();
    user.setUserName("john");
    user.setCredential("p@ssw0rd");

    mockDb.getTransactor().run(pm -> {
      UserDao userDao = new UserDao();
      pm.makePersistent(user);
      assertEquals(user, userDao.getByUserId(pm, user.getUserId()));
    });
  }

  @Test
  public void shouldGetByNameSuccessfully() {
    User user = new User();
    user.setUserName("john");
    user.setCredential("p@ssw0rd");

    mockDb.getTransactor().run(pm -> {
      UserDao userDao = new UserDao();
      assertNull(userDao.getByUserName(pm, user.getUserName()));
      pm.makePersistent(user);
      assertEquals(user, userDao.getByUserName(pm, user.getUserName()));
    });
  }
}
