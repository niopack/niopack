package org.niopack.validation;

import static com.google.common.truth.Truth.assertThat;

import com.google.common.collect.ImmutableList;
import javax.validation.Valid;
import javax.validation.Validation;
import javax.validation.Validator;
import org.junit.Test;

@SuppressWarnings({"FieldMayBeFinal","MethodMayBeStatic","UnusedDeclaration"})
public class MethodValidatorTest {
  public static class SubExample {
    @ValidationMethod(message = "also needs something special")
    public boolean isOK() {
      return false;
    }
  }

  public static class Example {
    @Valid
    private SubExample subExample = new SubExample();

    @ValidationMethod(message = "must have a false thing")
    public boolean isFalse() {
      return false;
    }

    @ValidationMethod(message = "must have a true thing")
    public boolean isTrue() {
      return true;
    }
  }

  private final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

  @Test
  public void complainsAboutMethodsWhichReturnFalse() throws Exception {
    ImmutableList<String> errors = ConstraintViolations.format(validator.validate(new Example()));

    assertThat(errors)
        .containsExactly("must have a false thing", "subExample also needs something special");
  }
}
