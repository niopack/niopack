package org.niopack.queue.module;

import static org.niopack.queue.common.QuartzFunction.REGISTER_TRIGGER;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.net.HostAndPort;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.multibindings.MapBinder;
import java.util.Map;
import java.util.Map.Entry;
import javax.inject.Singleton;
import org.gearman.Gearman;
import org.gearman.GearmanFunction;
import org.gearman.GearmanWorker;
import org.niopack.jackson.ObjectMappers;
import org.niopack.queue.common.JobFunction;
import org.niopack.queue.gearman.RegisterTriggerFn;
import org.niopack.queue.quartz.GuicyJobFactory;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;

/**
 * Guice module for Gearman backed task queue.
 */
public class TaskQueueModule extends AbstractModule {

  @Override
  protected void configure() {
    install(new GearmanClientModule());
    bind(GuicyJobFactory.class).in(Singleton.class);
    bind(ObjectMapper.class).toInstance(ObjectMappers.forJson());

    MapBinder<JobFunction, GearmanFunction> mapbinder =
        MapBinder.newMapBinder(binder(), JobFunction.class, GearmanFunction.class);
    mapbinder.addBinding(REGISTER_TRIGGER).to(RegisterTriggerFn.class);
  }

  @Provides
  @Singleton
  private Scheduler provideScheduler(GuicyJobFactory jobFactory) throws SchedulerException {
    Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();
    scheduler.setJobFactory(jobFactory);
    return scheduler;
  }

  @Provides
  private GearmanWorker provideGearmanWorker(Gearman gearman,
      Map<JobFunction, GearmanFunction> functions) {
    GearmanWorker worker = gearman.createGearmanWorker();
    for (Entry<JobFunction, GearmanFunction> entry : functions.entrySet()) {
      worker.addFunction(entry.getKey().name(), entry.getValue());
    }
    for (HostAndPort address : GearmanClientModule.gearmanServer) {
      worker.addServer(gearman.createGearmanServer(address.getHostText(), address.getPort()));
    }
    return worker;
  }
}
