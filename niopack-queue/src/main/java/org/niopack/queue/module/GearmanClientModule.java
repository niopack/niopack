package org.niopack.queue.module;

import com.google.common.collect.ImmutableList;
import com.google.common.net.HostAndPort;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import java.util.List;
import javax.inject.Singleton;
import org.gearman.Gearman;
import org.gearman.GearmanClient;

/**
 * Guice module for Gearman Job Server.
 */
public class GearmanClientModule extends AbstractModule {

  public static final List<HostAndPort> gearmanServer = ImmutableList.of(
    HostAndPort.fromString("localhost:4730")
  );

  @Override
  protected void configure() {
  }

  @Provides
  @Singleton
  private Gearman provideGearman() {
    return Gearman.createGearman();
  }

  @Provides
  private GearmanClient provideGearmanClient(Gearman gearman) {
    GearmanClient client = gearman.createGearmanClient();
    for (HostAndPort address : gearmanServer) {
      client.addServer(gearman.createGearmanServer(address.getHostText(), address.getPort()));
    }
    return client;
  }
}
