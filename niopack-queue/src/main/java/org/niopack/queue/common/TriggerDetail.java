package org.niopack.queue.common;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.base.MoreObjects;
import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * POJO representing schedule of a future Gearman Job invocation.
 */
public class TriggerDetail implements Serializable {

  private static final long serialVersionUID = 1L;

  private String userId;
  private Instant triggerInstant;
  private GearmanJobArgs args;

  @SuppressWarnings("unused")
  private TriggerDetail() {
    // Jackson serialization
  }

  public TriggerDetail(String userId, Instant triggerInstant, GearmanJobArgs args) {
    this.userId = checkNotNull(userId, "userId");
    this.triggerInstant = checkNotNull(triggerInstant, "triggerInstant");
    this.args = checkNotNull(args, "args");
  }

  public String getUserId() {
    return userId;
  }

  public Instant getTriggerInstant() {
    return triggerInstant;
  }

  public GearmanJobArgs getArgs() {
    return args;
  }

  @Override
  public int hashCode() {
    return Objects.hash(triggerInstant, args);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof TriggerDetail)) {
      return false;
    }
    TriggerDetail that = (TriggerDetail) obj;
    return Objects.equals(this.triggerInstant, that.triggerInstant)
        && Objects.equals(this.args, that.args);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("triggerInstant", triggerInstant)
        .add("args", args)
        .toString();
  }
}
