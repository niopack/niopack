package org.niopack.queue.common;

/**
 * Enumeration of Gearman functions supported for scheduled execution.
 */
public enum QuartzFunction implements JobFunction {
  REGISTER_TRIGGER
}
