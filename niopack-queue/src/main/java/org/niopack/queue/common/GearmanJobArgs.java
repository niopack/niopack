package org.niopack.queue.common;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.base.MoreObjects;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Objects;
import org.gearman.GearmanJobPriority;

/**
 * POJO representation of arguments of a Gearman Job.
 */
public final class GearmanJobArgs implements Serializable {

  private static final long serialVersionUID = 1L;

  private String functionName;
  private byte[] data;
  private GearmanJobPriority priority;

  @SuppressWarnings("unused")
  private GearmanJobArgs() {
    // Jackson serialization
  }

  public GearmanJobArgs(String functionName, byte[] data, GearmanJobPriority priority) {
    this.functionName = checkNotNull(functionName, "functionName");
    checkNotNull(data, "data");
    this.data = Arrays.copyOf(data, data.length);
    this.priority = checkNotNull(priority, "priority");
  }

  public String getFunctionName() {
    return functionName;
  }

  public byte[] getData() {
    return Arrays.copyOf(data, data.length);
  }

  public GearmanJobPriority getPriority() {
    return priority;
  }

  @Override
  public int hashCode() {
    return Objects.hash(functionName, data, priority);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof GearmanJobArgs)) {
      return false;
    }
    GearmanJobArgs that = (GearmanJobArgs) obj;
    return Objects.equals(this.functionName, that.functionName)
        && Arrays.equals(this.data, that.data)
        && Objects.equals(this.priority, that.priority);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("functionName", functionName)
        .add("data", data)
        .add("priority", priority)
        .toString();
  }
}
