package org.niopack.queue.common;

public interface JobFunction {
  String name();
}
