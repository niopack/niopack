package org.niopack.queue.gearman;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.niopack.queue.common.QuartzFunction.REGISTER_TRIGGER;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import javax.inject.Inject;
import org.gearman.GearmanFunction;
import org.gearman.GearmanFunctionCallback;
import org.niopack.queue.common.TriggerDetail;
import org.niopack.queue.quartz.GearmanJobDispatcher;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.Trigger;

/**
 * Gearman function for registering trigger for a scheduled execution.
 */
public class RegisterTriggerFn implements GearmanFunction {

  public static final String GROUP = "quartz";

  private final ObjectMapper mapper;
  private final Scheduler scheduler;

  @Inject
  public RegisterTriggerFn(ObjectMapper mapper, Scheduler scheduler) {
    this.mapper = checkNotNull(mapper, "mapper");
    this.scheduler = checkNotNull(scheduler, "scheduler");
  }

  @Override
  public byte[] work(String function, byte[] data, GearmanFunctionCallback callback)
      throws Exception {
    checkArgument(REGISTER_TRIGGER.name().equals(function),
        "Expected function %s and found %s", REGISTER_TRIGGER, function);

    TriggerDetail detail = mapper.readValue(data, TriggerDetail.class);
    String args = mapper.writeValueAsString(detail.getArgs());
    String userId = detail.getUserId();

    // Define the job and tie it to our GearmanJobDispatcher class
    JobDetail job = newJob(GearmanJobDispatcher.class)
        .withIdentity(String.valueOf(userId), GROUP)
        .usingJobData("args", args)
        .withDescription("Register Trigger for " + userId)
        .requestRecovery()
        .build();

    // Trigger the job to run now, and then repeat every 40 seconds
    Trigger trigger = newTrigger()
        .withIdentity(String.valueOf(userId), "quartz")
        .startAt(new Date(detail.getTriggerInstant().toEpochMilli()))
        .withSchedule(simpleSchedule().withRepeatCount(0)).build();

    // Tell quartz to schedule the job using our trigger
    scheduler.scheduleJob(job, trigger);

    return userId.getBytes(StandardCharsets.UTF_8);
  }
}
