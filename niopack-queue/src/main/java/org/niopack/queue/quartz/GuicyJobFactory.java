package org.niopack.queue.quartz;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.inject.Injector;
import com.google.inject.ProvisionException;
import javax.inject.Inject;
import javax.inject.Singleton;
import org.quartz.Job;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.spi.JobFactory;
import org.quartz.spi.TriggerFiredBundle;

/**
 * Quartz {@link JobFactory} that uses Guice to create new {@link Job} instances.
 */
@Singleton
public class GuicyJobFactory implements JobFactory {

  private final Injector injector;

  @Inject
  public GuicyJobFactory(Injector injector) {
    this.injector = checkNotNull(injector, "injector");
  }

  @Override
  public Job newJob(TriggerFiredBundle tfb, Scheduler schdlr) throws SchedulerException {
    try {
      return injector.getInstance(tfb.getJobDetail().getJobClass());
    } catch (ProvisionException e) {
      throw new SchedulerException(e);
    }
  }
}
