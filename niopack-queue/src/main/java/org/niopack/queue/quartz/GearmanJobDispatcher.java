package org.niopack.queue.quartz;

import static com.google.common.base.Preconditions.checkNotNull;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Throwables;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import javax.inject.Inject;
import org.gearman.GearmanClient;
import org.gearman.GearmanJobEvent;
import org.niopack.logging.ContextualLogger;
import org.niopack.queue.common.GearmanJobArgs;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.slf4j.Logger;

/**
 * Quartz job for dispatching a Gearman Job at the scheduled instant.
 */
@DisallowConcurrentExecution
public class GearmanJobDispatcher implements Job {

  private static final Logger logger = ContextualLogger.create();

  private final GearmanClient client;
  private final ObjectMapper mapper;

  @Inject
  public GearmanJobDispatcher(GearmanClient client, ObjectMapper mapper) {
    this.client = checkNotNull(client, "client");
    this.mapper = checkNotNull(mapper, "mapper");
  }

  @Override
  public void execute(JobExecutionContext jec) throws JobExecutionException {
    try {
      String strArgs = jec.getJobDetail().getJobDataMap().getString("args");
      GearmanJobArgs args = mapper.readValue(strArgs, GearmanJobArgs.class);
      // TODO(codefx): Add retry?
      client.submitBackgroundJob(
          args.getFunctionName(),
          args.getData(),
          args.getPriority(),
          jec.getJobDetail().getKey(),
          (JobKey attachment, GearmanJobEvent event) -> {
            String result = new String(event.getData(), StandardCharsets.UTF_8);
            switch (event.getEventType()) {
              case GEARMAN_JOB_SUCCESS:
                logger.info("Job = %s succeeded.", attachment);
                break;
              case GEARMAN_SUBMIT_FAIL:
              case GEARMAN_JOB_FAIL:
                logger.info("Job = %s failed as %s.", attachment, event.getEventType());
                break;
              default:
                logger.info("Job = %s status %s.", attachment, event.getEventType());
                break;
            }
      });
    } catch (IOException e) {
      Throwables.propagateIfInstanceOf(e, JobExecutionException.class);
      throw new JobExecutionException(e);
    }
  }
}
