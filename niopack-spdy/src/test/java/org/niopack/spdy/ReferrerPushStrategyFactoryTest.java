package org.niopack.spdy;

import static com.google.common.truth.Truth.assertThat;

import org.junit.Test;
import org.niopack.jackson.DiscoverableSubtypeResolver;

public class ReferrerPushStrategyFactoryTest {
  @Test
  public void isDiscoverable() throws Exception {
    assertThat(new DiscoverableSubtypeResolver().getDiscoveredSubtypes())
        .contains(ReferrerPushStrategyFactory.class);
  }
}
