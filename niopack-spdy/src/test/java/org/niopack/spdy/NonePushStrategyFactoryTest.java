package org.niopack.spdy;

import static com.google.common.truth.Truth.assertThat;

import org.eclipse.jetty.spdy.server.http.PushStrategy;
import org.junit.Test;
import org.niopack.jackson.DiscoverableSubtypeResolver;

public class NonePushStrategyFactoryTest {
  private final NonePushStrategyFactory factory = new NonePushStrategyFactory();

  @Test
  public void returnsAPushStrategyWhichNeverPushesAnything() throws Exception {
    assertThat(factory.build())
        .isInstanceOf(PushStrategy.None.class);
  }

  @Test
  public void isDiscoverable() throws Exception {
    assertThat(new DiscoverableSubtypeResolver().getDiscoveredSubtypes())
        .contains(NonePushStrategyFactory.class);
  }
}
