package org.niopack.spdy;

import static com.google.common.truth.Truth.assertThat;

import org.junit.Test;
import org.niopack.jackson.DiscoverableSubtypeResolver;

public class Spdy3ConnectorFactoryTest {
  @Test
  public void isDiscoverable() throws Exception {
    assertThat(new DiscoverableSubtypeResolver().getDiscoveredSubtypes())
        .contains(Spdy3ConnectorFactory.class);
  }
}
