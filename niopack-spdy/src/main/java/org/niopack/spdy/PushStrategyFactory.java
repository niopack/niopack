package org.niopack.spdy;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import org.eclipse.jetty.spdy.server.http.PushStrategy;
import org.niopack.jackson.Discoverable;

/**
 * Builds {@link PushStrategy} instances for SPDY connectors.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
public interface PushStrategyFactory extends Discoverable {
  PushStrategy build();
}
