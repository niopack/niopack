package org.niopack.lifecycle;

import java.util.EventListener;
import org.eclipse.jetty.server.Server;

public interface ServerLifecycleListener extends EventListener {
  void serverStarted(Server server);
}
