package com.example.helloworld;

import com.google.inject.Provides;
import javax.servlet.ServletContext;
import org.apache.shiro.config.Ini;
import org.apache.shiro.guice.web.ShiroWebModule;
import org.apache.shiro.realm.text.IniRealm;

public class MyShiroWebModule extends ShiroWebModule {

  public MyShiroWebModule(ServletContext servletContext) {
    super(servletContext);
  }

  @SuppressWarnings("unchecked")
  @Override
  protected void configureShiroWeb() {
    try {
      bindRealm().toConstructor(IniRealm.class.getConstructor(Ini.class));
    } catch (NoSuchMethodException | SecurityException e) {
      addError(e);
    }

    addFilterChain("/admin/**", AUTHC_BASIC);
    addFilterChain("/tasks/**", AUTHC_BASIC);
  }

  @Provides
  Ini loadShiroIni() {
    return Ini.fromResourcePath("classpath:shiro.ini");
  }
}
