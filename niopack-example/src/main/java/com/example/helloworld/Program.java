package com.example.helloworld;

import org.niopack.Application;

public class Program {

  public static void main(String[] args) throws Exception {
    new Application().run(TestContextListener.class);
  }
}
