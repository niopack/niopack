package com.example.helloworld;

import com.codahale.metrics.health.HealthCheck;
import com.example.helloworld.health.TemplateHealthCheck;
import com.example.helloworld.resources.HelloWorldResource;
import com.google.common.collect.ImmutableList;
import com.google.inject.Module;
import com.google.inject.Provides;
import com.google.inject.multibindings.MapBinder;
import org.apache.shiro.guice.web.ShiroWebModule;
import org.niopack.configuration.Configuration;
import org.niopack.guice.ResteasyGuiceServletContextListener;
import org.niopack.guice.ResteasyServletModule;

public class TestContextListener extends ResteasyGuiceServletContextListener {

  @Override
  public Iterable<Module> getModules() {
    return ImmutableList.of(new MyShiroWebModule(getServletContext()),
        ShiroWebModule.guiceFilterModule(),

        new ResteasyServletModule() {

          @Override
          protected void configureResources() {
            MapBinder<String, HealthCheck> healthChecksBinder =
                MapBinder.newMapBinder(binder(), String.class, HealthCheck.class);
            healthChecksBinder.addBinding("myapp").to(TemplateHealthCheck.class);
          }

          @Provides
          public HelloWorldResource provideHelloWorldResource(Configuration configuration) {
            return new HelloWorldResource("Hello, %s!", "Stranger");
          }
        });
  }
}
