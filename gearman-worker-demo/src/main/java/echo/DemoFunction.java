package echo;

import org.niopack.queue.common.JobFunction;

/**
 * Enumeration of workers functions.
 */
public enum DemoFunction implements JobFunction {
  ECHO
}
