package echo;

import static echo.DemoFunction.ECHO;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.MapBinder;
import org.gearman.GearmanFunction;
import org.niopack.queue.common.JobFunction;
import org.niopack.queue.module.TaskQueueModule;

/**
 * Guice module for Gearman Job Server.
 */
public class JobServerModule extends AbstractModule {

  @Override
  protected void configure() {
    install(new TaskQueueModule());

    MapBinder<JobFunction, GearmanFunction> mapbinder =
        MapBinder.newMapBinder(binder(), JobFunction.class, GearmanFunction.class);
    mapbinder.addBinding(ECHO).to(EchoWorker.class);
  }
}
