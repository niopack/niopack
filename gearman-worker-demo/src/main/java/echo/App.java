package echo;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.gearman.GearmanWorker;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;

class App {

  public static void main(String[] args) throws SchedulerException {
    Injector injector = Guice.createInjector(new JobServerModule());

    // Start three Gearman workers.
    injector.getInstance(GearmanWorker.class);
    injector.getInstance(GearmanWorker.class);
    injector.getInstance(GearmanWorker.class);

    // Start Quartz scheduler
    Scheduler scheduler = injector.getInstance(Scheduler.class);
    scheduler.start();
  }
}
