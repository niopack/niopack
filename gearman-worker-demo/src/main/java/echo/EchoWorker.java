package echo;

import org.gearman.GearmanFunction;
import org.gearman.GearmanFunctionCallback;

/**
 * The echo worker echos data sent by client.
 */
public class EchoWorker implements GearmanFunction {

  @Override
  public byte[] work(String function, byte[] data, GearmanFunctionCallback callback)
      throws Exception {
    /*
     * The work method performs the gearman function. In this case, the echo function simply returns
     * the data it received
     */
    return data;
  }
}
