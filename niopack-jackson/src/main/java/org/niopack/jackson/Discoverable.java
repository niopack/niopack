package org.niopack.jackson;

/**
 * A tag interface which allows Niopack to load Jackson subtypes at runtime, which enables polymorphic
 * configurations.
 */
public interface Discoverable {
}
