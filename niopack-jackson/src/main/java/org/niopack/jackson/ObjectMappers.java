package org.niopack.jackson;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.datatype.guava.GuavaModule;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;
import com.fasterxml.jackson.module.afterburner.AfterburnerModule;
import org.niopack.jackson.datatype.jdk8.Jdk8ExtrasModule;

public class ObjectMappers {
  private ObjectMappers() {
  }

  private static class JsonObjectMapperHolder {
    private static final ObjectMapper INSTANCE = new JsonMapper();
  }

  private static class XmlObjectMapperHolder {
    private static final ObjectMapper INSTANCE = new XmlMapper();
  }

  private static class YamlObjectMapperHolder {
    private static final ObjectMapper INSTANCE = new ObjectMapper(new YAMLFactory());
  }

  public static ObjectMapper forJson() {
    return JsonObjectMapperHolder.INSTANCE;
  }

  public static ObjectMapper forXml() {
    return XmlObjectMapperHolder.INSTANCE;
  }

  public static ObjectMapper forYaml() {
    return YamlObjectMapperHolder.INSTANCE;
  }

  /**
   * Json {@link ObjectMapper} with support for Java8 Optional, Java8 Time, Guava,
   * Logback and {@link JsonSnakeCase}. It also includes all {@link Discoverable} interface
   * implementations.
   */
  private static class JsonMapper extends ObjectMapper {
    private JsonMapper() {
      // TODO(tamal): Should we try to auto lookup modules?
      // mapper.findAndRegisterModules();
      registerModule(new GuavaModule());
      registerModule(new LogbackModule());
      registerModule(new Jdk8ExtrasModule());
      registerModule(new GuavaExtrasModule());
      registerModule(new JSR310Module());
      registerModule(new AfterburnerModule());
      registerModule(new FuzzyEnumModule());
      setPropertyNamingStrategy(new AnnotationSensitivePropertyNamingStrategy());
      setSubtypeResolver(new DiscoverableSubtypeResolver());
    }
  }
}
