package org.niopack.jackson.datatype.jdk8;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.BeanDescription;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.deser.Deserializers;
import com.fasterxml.jackson.databind.jsontype.TypeDeserializer;
import com.fasterxml.jackson.databind.ser.Serializers;
import com.fasterxml.jackson.databind.type.TypeFactory;
import java.util.Optional;
import org.niopack.jackson.datatype.jdk8.deser.JdkOptionalDeserializer;
import org.niopack.jackson.datatype.jdk8.ser.JdkBeanSerializerModifier;
import org.niopack.jackson.datatype.jdk8.ser.JdkOptionalSerializer;

public class Jdk8ExtrasModule extends Module {

  private static class Jdk8ExtrasSerializers extends Serializers.Base {
    @Override
    public JsonSerializer<?> findSerializer(SerializationConfig config, JavaType type,
        BeanDescription beanDesc) {
      Class<?> raw = type.getRawClass();
      if (Optional.class.isAssignableFrom(raw)) {
        return new JdkOptionalSerializer(type);
      }
      return super.findSerializer(config, type, beanDesc);
    }
  }

  private static class Jdk8ExtrasDeserializers extends Deserializers.Base {
    @Override
    public JsonDeserializer<?> findBeanDeserializer(JavaType type, DeserializationConfig config,
        BeanDescription beanDesc) throws JsonMappingException {
      Class<?> raw = type.getRawClass();
      if (raw == Optional.class) {
        JavaType[] types = config.getTypeFactory().findTypeParameters(type, Optional.class);
        JavaType refType = (types == null) ? TypeFactory.unknownType() : types[0];
        JsonDeserializer<?> valueDeser = type.getValueHandler();
        TypeDeserializer typeDeser = type.getTypeHandler();
        // [Issue#42]: Polymorphic types need type deserializer
        if (typeDeser == null) {
          typeDeser = config.findTypeDeserializer(refType);
        }
        return new JdkOptionalDeserializer(type, refType, typeDeser, valueDeser);
      }
      return super.findBeanDeserializer(type, config, beanDesc);
    }
  }

  @Override
  public String getModuleName() {
    return "jdk8-extras";
  }

  @Override
  public Version version() {
    return Version.unknownVersion();
  }

  @Override
  public void setupModule(SetupContext context) {
    context.addDeserializers(new Jdk8ExtrasDeserializers());
    context.addSerializers(new Jdk8ExtrasSerializers());
    context.addBeanSerializerModifier(new JdkBeanSerializerModifier());
  }

  @Override
  public int hashCode() {
    return Jdk8ExtrasModule.class.hashCode();
  }

  @Override
  public boolean equals(Object o) {
    return this == o;
  }
}
