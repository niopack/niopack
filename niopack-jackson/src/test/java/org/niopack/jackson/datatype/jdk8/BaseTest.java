package org.niopack.jackson.datatype.jdk8;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Arrays;

public abstract class BaseTest extends junit.framework.TestCase {
  protected BaseTest() {}

  protected ObjectMapper mapperWithModule() {
    ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(new Jdk8ExtrasModule());
    return mapper;
  }

  protected String aposToQuotes(String json) {
    return json.replace("'", "\"");
  }

  public String quote(String str) {
    return '"' + str + '"';
  }

  protected void verifyException(Throwable e, String... matches) {
    String msg = e.getMessage();
    String lmsg = (msg == null) ? "" : msg.toLowerCase();
    for (String match : matches) {
      String lmatch = match.toLowerCase();
      if (lmsg.indexOf(lmatch) >= 0) {
        return;
      }
    }
    fail("Expected an exception with one of substrings (" + Arrays.asList(matches)
        + "): got one with message \"" + msg + "\"");
  }
}
