package org.niopack.db.module;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.TypeLiteral;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;
import javax.inject.Singleton;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManagerFactory;
import org.niopack.db.jdo.ConsistentTransactorFactory;
import org.niopack.db.jdo.TransactorFactory;

public abstract class AbstractShardedDatabaseModule extends AbstractDatabaseModule {

  private final List<String> connectionUrls;

  protected AbstractShardedDatabaseModule(Class<? extends Annotation> annotation,
      List<String> connectionUrls,
      Map<String, String> persistenceProperties) {
    super(annotation, persistenceProperties);
    this.connectionUrls = ImmutableList.copyOf(checkNotNull(connectionUrls, "connectionUrls"));
  }

  @Override
  protected void bindDbConnection() {
    bind(new TypeLiteral<List<String>>() {})
        .annotatedWith(annotation)
        .toInstance(connectionUrls);
    expose(Key.get(new TypeLiteral<List<String>>() {}, annotation));

    Key<List<PersistenceManagerFactory>> annotatedPmfKey =
        Key.get(new TypeLiteral<List<PersistenceManagerFactory>>() {}, annotation);
    bind(annotatedPmfKey).to(new TypeLiteral<List<PersistenceManagerFactory>>() {});
    expose(annotatedPmfKey);
  }

  @Override
  protected void bindTransactor() {
    bind(TransactorFactory.class)
        .annotatedWith(annotation)
        .to(ConsistentTransactorFactory.class)
        .in(Singleton.class);
    expose(Key.get(TransactorFactory.class, annotation));
  }

  @Provides
  @Singleton
  private List<PersistenceManagerFactory> providePersistenceManagerFactories() {
    ImmutableList.Builder<PersistenceManagerFactory> pmfs = ImmutableList.builder();
    for (String connectionUrl : connectionUrls) {
      Map<String, String> props = ImmutableMap.<String, String>builder()
          .putAll(persistenceProperties)
          .put("javax.jdo.option.ConnectionURL", connectionUrl)
          .build();
      pmfs.add(JDOHelper.getPersistenceManagerFactory(props));
    }
    return pmfs.build();
  }
}
