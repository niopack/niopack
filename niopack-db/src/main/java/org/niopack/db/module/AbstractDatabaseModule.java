package org.niopack.db.module;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Key;
import com.google.inject.PrivateModule;
import com.google.inject.TypeLiteral;
import java.lang.annotation.Annotation;
import java.util.Map;

public abstract class AbstractDatabaseModule extends PrivateModule {

  /*
  <!-- .put("datanucleus.identifier.case", "LowerCase") -->
  <!-- .put("datanucleus.mapping.Schema", "shiro") -->
  */
  public static final Map<String, String> DEFAULT_PGSQL_PROPERTIES =
      ImmutableMap.<String, String>builder()
          .put("javax.jdo.PersistenceManagerFactoryClass",
              "org.datanucleus.api.jdo.JDOPersistenceManagerFactory")
          .put("javax.jdo.option.ConnectionDriverName", "org.postgresql.Driver")
          .put("javax.jdo.option.ConnectionUserName", "vagrant")
          .put("javax.jdo.option.ConnectionPassword", "vagrant")
          .put("javax.jdo.option.Mapping", "pgsql")
          .put("datanucleus.connectionPoolingType", "HikariCP")
          .put("datanucleus.connectionPool.maxPoolSize", "2")
          .put("datanucleus.schema.autoCreateAll", "true")
          .put("datanucleus.schema.validateTables", "false")
          .put("datanucleus.schema.validateConstraints", "false")
          // TODO(tamal): https://trello.com/c/gd3JMONP
          .put("datanucleus.DetachAllOnCommit", "true")
          .put("datanucleus.CopyOnAttach", "false")
          .put("datanucleus.maxFetchDepth", "-1")
          .put("datanucleus.detachedState", "all")
          .build();

  protected final Class<? extends Annotation> annotation;
  protected final Map<String, String> persistenceProperties;

  protected AbstractDatabaseModule(Class<? extends Annotation> annotation,
      Map<String, String> persistenceProperties) {
    this.annotation = checkNotNull(annotation, "annotation");
    this.persistenceProperties =
        ImmutableMap.copyOf(checkNotNull(persistenceProperties, "persistenceProperties"));
  }

  @Override
  protected void configure() {
    bindDbConnection();
    bind(new TypeLiteral<Map<String, String>>() {})
        .annotatedWith(annotation)
        .toInstance(persistenceProperties);
    expose(Key.get(new TypeLiteral<Map<String, String>>() {}, annotation));
    bindTransactor();
  }

  protected abstract void bindDbConnection();

  protected abstract void bindTransactor();
}
