package org.niopack.db.module;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.TypeLiteral;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Map;
import javax.inject.Singleton;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManagerFactory;
import org.niopack.db.jdo.Transactor;
import org.niopack.db.jdo.TransactorImpl;

public abstract class AbstractUnshardedDatabaseModule extends AbstractDatabaseModule {

  private final String connectionUrl;

  protected AbstractUnshardedDatabaseModule(Class<? extends Annotation> annotation,
      String connectionUrl,
      Map<String, String> persistenceProperties) {
    super(annotation, persistenceProperties);
    this.connectionUrl = checkNotNull(connectionUrl, "connectionUrl");
  }

  @Override
  protected void bindDbConnection() {
    bindConstant().annotatedWith(annotation).to(connectionUrl);
    expose(Key.get(String.class, annotation));

    Key<PersistenceManagerFactory> annotatedPmfKey =
        Key.get(PersistenceManagerFactory.class, annotation);
    bind(annotatedPmfKey).to(PersistenceManagerFactory.class);
    expose(annotatedPmfKey);
  }

  @Override
  protected void bindTransactor() {
    bind(Transactor.class)
        .annotatedWith(annotation)
        .to(TransactorImpl.class)
        .in(Singleton.class);
    expose(Key.get(Transactor.class, annotation));
  }

  @Provides
  @Singleton
  private PersistenceManagerFactory providePersistenceManagerFactory() {
      Map<String, String> props = ImmutableMap.<String, String>builder()
          .putAll(persistenceProperties)
          .put("javax.jdo.option.ConnectionURL", connectionUrl)
          .build();
      return JDOHelper.getPersistenceManagerFactory(props);
  }
}
