package org.niopack.db;

import org.niopack.configuration.Configuration;

public interface DatabaseConfiguration<T extends Configuration> {
  DataSourceFactory getDataSourceFactory(T configuration);
}
