package org.niopack.db.jdo;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.function.Consumer;
import java.util.function.Function;
import javax.annotation.concurrent.ThreadSafe;
import javax.inject.Inject;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Transaction;
import org.datanucleus.api.jdo.JDOPersistenceManager;

@ThreadSafe
public final class TransactorImpl implements Transactor {

  private final PersistenceManagerFactory pmf;

  @Inject
  public TransactorImpl(PersistenceManagerFactory pmf) {
    this.pmf = checkNotNull(pmf, "pmf");
  }

  @Override
  public void run(Consumer<JDOPersistenceManager> task) {
    checkNotNull(task, "task");
    JDOPersistenceManager jdopm = (JDOPersistenceManager) pmf.getPersistenceManager();
    Transaction tx = jdopm.currentTransaction();
    try {
      tx.begin();
      task.accept(jdopm);
      tx.commit();
    } finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      jdopm.close();
    }
  }

  @Override
  public <R> R retrieve(Function<JDOPersistenceManager, R> task) {
    checkNotNull(task, "task");
    JDOPersistenceManager jdopm = (JDOPersistenceManager) pmf.getPersistenceManager();
    Transaction tx = jdopm.currentTransaction();
    R result;
    try {
      tx.begin();
      result = task.apply(jdopm);
      tx.commit();
    } finally {
      if (tx.isActive()) {
        tx.rollback();
      }
      jdopm.close();
    }
    return result;
  }
}
