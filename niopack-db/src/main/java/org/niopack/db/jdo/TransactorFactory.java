package org.niopack.db.jdo;

/**
 * Sharding aware {@code Transactor} factory.
 */
public interface TransactorFactory {
  Transactor get(String tenantId);
}
