package org.niopack.db.jdo;

import java.util.function.Consumer;
import java.util.function.Function;
import org.datanucleus.api.jdo.JDOPersistenceManager;

public interface Transactor {

  void run(Consumer<JDOPersistenceManager> task);

  <R> R retrieve(Function<JDOPersistenceManager, R> task);
}
