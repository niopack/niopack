package org.niopack.db.jdo;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
import java.nio.charset.StandardCharsets;
import java.util.List;
import javax.inject.Inject;
import javax.jdo.PersistenceManagerFactory;

public class ConsistentTransactorFactory implements TransactorFactory {

  private static final HashFunction MD5 = Hashing.md5();

  private final List<PersistenceManagerFactory> pmfs;

  @Inject
  public ConsistentTransactorFactory(List<PersistenceManagerFactory> pmfs) {
    this.pmfs = checkNotNull(pmfs, "pmfs");
  }

  @Override
  public Transactor get(String tenantId) {
    HashCode tenantHash = MD5.hashString(tenantId, StandardCharsets.UTF_8);
    int index = Hashing.consistentHash(tenantHash, pmfs.size());
    return new TransactorImpl(pmfs.get(index));
  }
}
