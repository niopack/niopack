package org.niopack.db.testing;

import com.google.common.testing.NullPointerTester;
import org.datanucleus.enhancer.Persistable;
import org.datanucleus.state.StateManager;

public final class DataNucleusPojoNullPointerTester {

  public static <T> NullPointerTester create(Class<T> klass) throws NoSuchMethodException, SecurityException {
    return new NullPointerTester()
        .ignore(klass.getMethod("dnCopyFields", Object.class, int[].class))
        .ignore(klass.getMethod(
            "dnCopyKeyFieldsFromObjectId", Persistable.ObjectIdFieldConsumer.class, Object.class))
        .ignore(klass.getMethod("dnCopyKeyFieldsToObjectId", Object.class))
        .ignore(klass.getMethod(
            "dnCopyKeyFieldsToObjectId", Persistable.ObjectIdFieldSupplier.class, Object.class))
        .ignore(klass.getMethod("dnMakeDirty", String.class))
        .ignore(klass.getMethod("dnNewInstance", StateManager.class))
        .ignore(klass.getMethod("dnNewInstance", StateManager.class, Object.class))
        .ignore(klass.getMethod("dnNewObjectIdInstance", Object.class))
        .ignore(klass.getMethod("dnProvideFields", int[].class))
        .ignore(klass.getMethod("dnReplaceFields", int[].class));
  }
}
