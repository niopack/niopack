package org.niopack.db.testing;

import java.util.Properties;
import java.util.UUID;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManagerFactory;
import org.junit.rules.ExternalResource;
import org.niopack.db.jdo.Transactor;
import org.niopack.db.jdo.TransactorImpl;

/**
 * Creates a test database with PostgreSQL compatibility mode. To prevent concurrency problems when
 * tests are executed in multithreaded mode, each test method gets its own database.
 */
public class MockDatabase extends ExternalResource {

  private PersistenceManagerFactory pmf;
  private Transactor transactor;

  @Override
  protected void before() throws Throwable {
    Properties props = new Properties();
    String connectionUrl = String.format("jdbc:h2:mem:%s;MODE=PostgreSQL", UUID.randomUUID());
    props.put("javax.jdo.option.ConnectionURL", connectionUrl);
    props.put("javax.jdo.option.ConnectionDriverName", "org.h2.Driver");
    props.put("javax.jdo.option.ConnectionUserName", "");
    props.put("javax.jdo.option.ConnectionPassword", "");
    props.put("javax.jdo.option.Mapping", "pgsql");
    props.put("datanucleus.identifier.case", "LowerCase");
    props.put("datanucleus.schema.autoCreateAll", "true");
    props.put("datanucleus.schema.validateTables", "false");
    props.put("datanucleus.schema.validateConstraints", "false");
    props.put("datanucleus.DetachAllOnCommit", "true");
    props.put("datanucleus.CopyOnAttach", "false");
    props.put("datanucleus.maxFetchDepth", "-1");
    props.put("datanucleus.detachedState", "all");

    pmf = JDOHelper.getPersistenceManagerFactory(props);
    transactor = new TransactorImpl(pmf);
  }

  @Override
  protected void after() {
    pmf.close();
  }

  public PersistenceManagerFactory getPersistenceManagerFactory() {
    return pmf;
  }

  public Transactor getTransactor() {
    return transactor;
  }
}
