package org.niopack.db;

import javax.sql.DataSource;
import org.niopack.lifecycle.Managed;

public interface ManagedDataSource extends DataSource, Managed {

}
