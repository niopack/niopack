package org.niopack.db;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.Assert.fail;

import com.codahale.metrics.MetricRegistry;
import java.sql.SQLFeatureNotSupportedException;
import org.apache.tomcat.jdbc.pool.PoolProperties;
import org.junit.Test;

public class ManagedPooledDataSourceTest {
  private final PoolProperties config = new PoolProperties();
  private final MetricRegistry metricRegistry = new MetricRegistry();
  private final ManagedPooledDataSource dataSource = new ManagedPooledDataSource(config, metricRegistry);

  @Test
  public void hasNoParentLogger() throws Exception {
    try {
      dataSource.getParentLogger();
      fail(SQLFeatureNotSupportedException.class.getName());
    } catch (SQLFeatureNotSupportedException e) {
      assertThat((Object) e).isInstanceOf(SQLFeatureNotSupportedException.class);
    }
  }
}
