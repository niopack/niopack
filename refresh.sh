#!/bin/bash
set -x

git pull origin master
gradle clean install
cd nio-compiler
gradle installDist
sudo ln -sf $PWD/build/install/nio-compiler/bin/nio-compiler /usr/local/bin/niopacker
