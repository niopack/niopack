package org.niopack.flag;

import static com.google.common.truth.Truth.assertThat;

import com.google.common.base.Function;
import com.google.common.net.HostAndPort;
import java.util.Arrays;
import java.util.function.Predicate;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.niopack.flag.Flag.ParserType;

@RunWith(JUnit4.class)
public class FlagTest {

  @Rule
  public ExpectedException thrown = ExpectedException.none();
  @Rule
  public FlagTestRule flagTest = new FlagTestRule();

  private static class DefaultParsableFlags {

    @Flag(help = "Byte valued flag.")
    private static final Byte byteFlag = 0x1;

    @Flag(help = "Short valued flag.")
    private static final Short shortFlag = 2;

    @Flag(help = "Integer valued flag.", validator = PositiveInteger.class)
    private static final Integer intFlag = 3;

    @Flag(help = "Long valued flag.")
    private static final Long longFlag = 4L;

    @Flag(help = "Float valued flag.")
    private static final Float floatFlag = 5.0f;

    @Flag(help = "Double valued flag.")
    private static final Double doubleFlag = 6.0;

    @Flag(help = "Character valued flag.")
    private static final Character charFlag = '\n';

    @Flag(help = "Boolean flag initialized to false.")
    private static final Boolean falseFlag = Boolean.FALSE;

    @Flag(help = "Boolean flag initialized to true.")
    private static final Boolean trueFlag = Boolean.TRUE;

    @Flag(help = "String valued flag.")
    private static final String stringFlag = new String("7");

    @Flag(help = "Flag type with fromString() method.")
    private static final HostAndPort ipAddress = HostAndPort.fromParts("10.11.12.13", 55); //NOPMD
  }

  private static class ShortNameFlags1 {

    @Flag(help = "Flag with user provided short name", name = "my_flag")
    private static final String userGivenName = new String("1");

    @Flag(help = "Flag with no short name")
    private static final String noShortName = new String("2");
  }

  private static class ShortNameFlags2 {

    @Flag(help = "Flag with user provided short name")
    private static final String userGivenName = new String("3");

    @Flag(help = "Flag with no short name")
    private static final String noShortName = new String("4");
  }

  @Test
  public void shouldSetPrimitiveFlagsFromConstantValue() {
    Flags.parse(new String[] {});

    assertThat(DefaultParsableFlags.byteFlag).isEqualTo((byte) 1);
    assertThat(DefaultParsableFlags.shortFlag).isEqualTo((short) 2);
    assertThat(DefaultParsableFlags.intFlag).isEqualTo(3);
    assertThat(DefaultParsableFlags.longFlag).isEqualTo(4L);
    assertThat(DefaultParsableFlags.floatFlag).isEqualTo(5.0f);
    assertThat(DefaultParsableFlags.doubleFlag).isEqualTo(6.0);
    assertThat(DefaultParsableFlags.charFlag).isEqualTo('\n');
    assertThat(DefaultParsableFlags.falseFlag).isEqualTo(Boolean.FALSE);
    assertThat(DefaultParsableFlags.trueFlag).isEqualTo(Boolean.TRUE);
    assertThat(DefaultParsableFlags.stringFlag).isEqualTo("7");
    assertThat(DefaultParsableFlags.ipAddress.toString()).isEqualTo("10.11.12.13:55"); //NOPMD
  }

  @Test
  public void shouldSetPrimitiveFlagsFromCommandLine() {
    Flags.parse(new String[] {
        "--byteFlag=2",
        "--shortFlag=20",
        "--intFlag=30",
        "--longFlag=40",
        "--floatFlag=50.0",
        "--doubleFlag=60.0",
        "--charFlag=" + '\t',
        "--falseFlag=true",
        "--trueFlag=false",
        "--stringFlag=70",
        "--ipAddress=10.11.12.13:60",
        "--unusedFlag=f1",
        "unusedCommand"});
    assertThat(DefaultParsableFlags.byteFlag).isEqualTo((byte) 2);
    assertThat(DefaultParsableFlags.shortFlag).isEqualTo((short) 20);
    assertThat(DefaultParsableFlags.intFlag).isEqualTo(30);
    assertThat(DefaultParsableFlags.longFlag).isEqualTo(40L);
    assertThat(DefaultParsableFlags.floatFlag).isEqualTo(50.0f);
    assertThat(DefaultParsableFlags.doubleFlag).isEqualTo(60.0);
    assertThat(DefaultParsableFlags.charFlag).isEqualTo('\t');
    assertThat(DefaultParsableFlags.falseFlag).isEqualTo(Boolean.TRUE);
    assertThat(DefaultParsableFlags.trueFlag).isEqualTo(Boolean.FALSE);
    assertThat(DefaultParsableFlags.stringFlag).isEqualTo("70");
    assertThat(DefaultParsableFlags.ipAddress.toString()).isEqualTo("10.11.12.13:60"); //NOPMD
  }

  @Test
  public void testFlagDump() {
    Flags.parse(new String[] {});
    String[] flags = Flags.dump();
    assertThat(flags).isNotEmpty();
  }

  @Test
  public void testResetAllFlags() {
    Flags.parse(new String[] {
        "--byteFlag=2",
        "--shortFlag=20",
        "--intFlag=30",
        "--longFlag=40",
        "--floatFlag=50.0",
        "--doubleFlag=60.0",
        "--charFlag=" + '\t',
        "--falseFlag=true",
        "--trueFlag=false",
        "--stringFlag=70",
        "--ipAddress=10.11.12.13:60",
        "--unusedFlag=f1",
        "unusedCommand"});
    assertThat(DefaultParsableFlags.byteFlag).isEqualTo((byte) 2);
    assertThat(DefaultParsableFlags.shortFlag).isEqualTo((short) 20);
    assertThat(DefaultParsableFlags.intFlag).isEqualTo(30);
    assertThat(DefaultParsableFlags.longFlag).isEqualTo(40L);
    assertThat(DefaultParsableFlags.floatFlag).isEqualTo(50.0f);
    assertThat(DefaultParsableFlags.doubleFlag).isEqualTo(60.0);
    assertThat(DefaultParsableFlags.charFlag).isEqualTo('\t');
    assertThat(DefaultParsableFlags.falseFlag).isEqualTo(Boolean.TRUE);
    assertThat(DefaultParsableFlags.trueFlag).isEqualTo(Boolean.FALSE);
    assertThat(DefaultParsableFlags.stringFlag).isEqualTo("70");
    assertThat(DefaultParsableFlags.ipAddress.toString()).isEqualTo("10.11.12.13:60"); //NOPMD

    Flags.resetAllForTest();
    assertThat(DefaultParsableFlags.byteFlag).isEqualTo((byte) 1);
    assertThat(DefaultParsableFlags.shortFlag).isEqualTo((short) 2);
    assertThat(DefaultParsableFlags.intFlag).isEqualTo(3);
    assertThat(DefaultParsableFlags.longFlag).isEqualTo(4L);
    assertThat(DefaultParsableFlags.floatFlag).isEqualTo(5.0f);
    assertThat(DefaultParsableFlags.doubleFlag).isEqualTo(6.0);
    assertThat(DefaultParsableFlags.charFlag).isEqualTo('\n');
    assertThat(DefaultParsableFlags.falseFlag).isEqualTo(Boolean.FALSE);
    assertThat(DefaultParsableFlags.trueFlag).isEqualTo(Boolean.TRUE);
    assertThat(DefaultParsableFlags.stringFlag).isEqualTo("7");
    assertThat(DefaultParsableFlags.ipAddress.toString()).isEqualTo("10.11.12.13:55"); //NOPMD
  }

  @Test
  public void shouldResetFlagSelectively() {
    Flags.parse(new String[] {
        "--my_flag=10",
        "--userGivenName=30"
    });
    assertThat(ShortNameFlags1.userGivenName).isEqualTo("10");
    assertThat(ShortNameFlags2.userGivenName).isEqualTo("30");

    Flags.resetForTest("my_flag");
    assertThat(ShortNameFlags1.userGivenName).isEqualTo("1");
    assertThat(ShortNameFlags2.userGivenName).isEqualTo("30");
  }

  @Test
  public void shouldUserGivenNameAndGeneratedShortNameCoexist() {
    Flags.parse(new String[] {
        "--my_flag=10",
        "--userGivenName=30"
    });
    assertThat(ShortNameFlags1.userGivenName).isEqualTo("10");
    assertThat(ShortNameFlags2.userGivenName).isEqualTo("30");
  }

  @Test
  public void shouldBeNoGeneratedShortName() {
    String[] unusedFlags = Flags.parse(new String[] {
        "--noShortName=unused",
        "--" + ShortNameFlags1.class.getCanonicalName() + ".noShortName=20",
        "--" + ShortNameFlags2.class.getCanonicalName() + ".noShortName=40"
    });
    assertThat(ShortNameFlags1.noShortName).isEqualTo("20");
    assertThat(ShortNameFlags2.noShortName).isEqualTo("40");
    assertThat(Arrays.asList(unusedFlags))
        .containsExactly("--noShortName=unused")
        .inOrder();
  }

  @Test
  public void shouldReturnUnusedFlags() {
    String[] unusedFlags = Flags.parse(new String[] {
        "--noShortName=unused",
        "--unusedFlag=f1",
        "unusedCommand"
    });
    assertThat(Arrays.asList(unusedFlags))
        .containsExactly("--noShortName=unused", "--unusedFlag=f1", "unusedCommand")
        .inOrder();
  }

  @Test
  public void testShortFormAssignmentOfBooleanFlags() {
    Flags.parse(new String[] {
        "--falseFlag",
        "--notrueFlag"
    });
    assertThat(DefaultParsableFlags.falseFlag).isEqualTo(Boolean.TRUE);
    assertThat(DefaultParsableFlags.trueFlag).isEqualTo(Boolean.FALSE);
  }

  @Test
  public void ShouldFailValidation() {
    thrown.expect(IllegalArgumentException.class);
    thrown.expectMessage("Invalid value -1 assigned to flag --intFlag=-1");
    Flags.parse(new String[] {"--intFlag=-1"});
  }

  static class PositiveInteger implements Predicate<Object> {
    @Override
    public boolean test(Object input) {
      return ((Integer) input) > 0;
    }
  }

  @Flag(help = "Flag with json value.", parserType = ParserType.JSON)
  private static final ComplexFlag jsonFlag = new ComplexFlag(1, "2");

  @Test
  public void shouldSetJsonFlag() {
    Flags.parse(new String[] {"--jsonFlag={\"id\":10,\"name\":\"20\"}"});

    assertThat(jsonFlag.id).isEqualTo(10);
    assertThat(jsonFlag.name).isEqualTo("20");
  }

  @Flag(help = "Flag with yaml value.", parserType = ParserType.XML)
  private static final ComplexFlag xmlFlag = new ComplexFlag(1, "2");

  @Test
  public void shouldSetXmlFlag() {
    Flags.parse(new String[] {"--xmlFlag=<ComplexFlag><id>10</id><name>20</name></ComplexFlag>"});

    assertThat(xmlFlag.id).isEqualTo(10);
    assertThat(xmlFlag.name).isEqualTo("20");
  }

  @Flag(help = "Flag with yaml value.", parserType = ParserType.YAML)
  private static final ComplexFlag yamlFlag = new ComplexFlag(1, "2");

  @Test
  public void shouldSetYamlFlag() {
    Flags.parse(new String[] {"--yamlFlag=---\n" + "  id: 10\n" + "  name: \"20\""});

    assertThat(yamlFlag.id).isEqualTo(10);
    assertThat(yamlFlag.name).isEqualTo("20");
  }

  @Flag(help = "Flag with custom parser.",
      parserType = ParserType.CUSTOM,
      parser = ComplexFlagParser.class)
  private static final ComplexFlag customFlag = new ComplexFlag(1, "2");

  @Test
  public void shouldSetCustomFlag() {
    Flags.parse(new String[] {"--customFlag=10,20"});

    assertThat(customFlag.id).isEqualTo(10);
    assertThat(customFlag.name).isEqualTo("20");
  }

  public static class ComplexFlag {
    public int id;
    public String name;

    public ComplexFlag() {
    }

    public ComplexFlag(int id, String name) {
      this.id = id;
      this.name = name;
    }

    @Override
    public String toString() {
      return id + "," + name;
    }
  }

  static class ComplexFlagParser implements Function<String, Object> {
    @Override
    public Object apply(String input) {
      String[] parts = input.split(",");
      return new ComplexFlag(Integer.valueOf(parts[0]), parts[1]);
    }
  }
}
