package org.niopack.flag;

import static com.google.common.truth.Truth.assertThat;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class FlagTestRuleTest {

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @Flag(help = "Integer valued flag.")
  private static final Integer aflag = 1;

  @Test
  public void testResetAllFlags() {
    Flags.parse(new String[] {"--aflag=30",});
    assertThat(aflag).isEqualTo(30);

    thrown.expect(NullPointerException.class);
    thrown.expectMessage("Must be called from a test method with @Rule FlagTestRule enabled.");
    Flags.resetAllForTest();
  }

  @Test
  public void shouldResetFlagSelectively() {
    Flags.parse(new String[] {"--aflag=30",});
    assertThat(aflag).isEqualTo(30);

    thrown.expect(NullPointerException.class);
    thrown.expectMessage("Must be called from a test method with @Rule FlagTestRule enabled.");
    Flags.resetForTest("aflag");
  }
}
