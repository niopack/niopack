package org.niopack.flag;

import static com.google.common.base.Preconditions.checkState;

import java.util.HashMap;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

public class FlagTestRule implements TestRule {

  @Override
  public Statement apply(Statement statement, Description description) {
    return new Statement() {
      @Override
      public void evaluate() throws Throwable {
        checkState(Flags.flagValues == null, "A test is already running.");
        Flags.flagValues = new HashMap<>();
        try {
          statement.evaluate();
        } finally {
          Flags.resetAllForTest();
          Flags.flagValues = null;
        }
      }
    };
  }

}
