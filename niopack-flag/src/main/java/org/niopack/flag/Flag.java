package org.niopack.flag;

import com.google.common.base.Function;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.function.Predicate;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface Flag {

  String name() default "";

  String help();

  ParserType parserType() default ParserType.DEFAULT;

  Class<? extends Function<String, Object>> parser() default UnusedParser.class;

  Class<? extends Predicate<Object>> validator() default AlwaysTrue.class;

  public static enum ParserType {
    DEFAULT,
    CUSTOM,
    JSON,
    XML,
    YAML
  }

  /**
   * This parser is not used. This is rather a placeholder so that api users can avoid
   * setting parser for ParserType.DEFAULT.
   */
  public static class UnusedParser implements Function<String, Object> {
    @Override
    public Object apply(String input) {
      return input;
    }
  }

  public static class AlwaysTrue implements Predicate<Object> {
    @Override
    public boolean test(Object input) {
      return true;
    }
  }
}
