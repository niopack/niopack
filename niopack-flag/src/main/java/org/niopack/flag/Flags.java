package org.niopack.flag;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static org.niopack.flag.FlagProcessor.FLAGS_FILE_NAME;
import static org.niopack.flag.FlagProcessor.PACKAGE_NAME;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Throwables;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import org.niopack.flag.Flag.ParserType;
import org.niopack.jackson.ObjectMappers;
import org.niopack.logging.ContextualLogger;
import org.slf4j.Logger;

public final class Flags {

  private static final Logger logger = ContextualLogger.create();

  static Map<Field, Object> flagValues;
  static Map<String, Field> fieldsByFlagName;

  public static String[] parse(String[] args) {
    loadFieldsByFlagName();
    List<String> unknownFlags = new ArrayList<>();
    for (String argument : args) {
      logger.info("Parsing flag = " + argument);
      if (argument.startsWith("--")) {
        String[] parts = argument.substring(2).split("=");
        Field field;
        String value;
        if (parts.length == 2) {
          field = fieldsByFlagName.get(parts[0]);
          value = parts[1];
        } else {
          if (parts[0].startsWith("no")) {
            field = fieldsByFlagName.get(parts[0].substring("no".length()));
            value = Boolean.FALSE.toString();
          } else {
            field = fieldsByFlagName.get(parts[0]);
            value = Boolean.TRUE.toString();
          }
          if (field != null) {
            checkArgument(field.getType() == Boolean.class,
                "%s form can only used with boolean flags", argument);
          }
        }
        if (field != null) {
          Class<?> fieldType = field.getType();
          Flag flag = field.getAnnotation(Flag.class);
          try {
            Object parsedValue;
            switch (flag.parserType()) {
              case JSON:
                parsedValue = ObjectMappers.forJson().readValue(value, fieldType);
                break;
              case XML:
                parsedValue = ObjectMappers.forXml().readValue(value, fieldType);
                break;
              case YAML:
                parsedValue = ObjectMappers.forYaml().readValue(value, fieldType);
                break;
              case CUSTOM:
                parsedValue = flag.parser().newInstance().apply(value);
                break;
              case DEFAULT:
              default:
                if (fieldType == Character.class) {
                  checkArgument(value.length() == 1, "%s is not a character", argument);
                  parsedValue = value.charAt(0);
                } else {
                  try {
                    Constructor<?> stringConstructor = fieldType.getConstructor(String.class);
                    parsedValue = stringConstructor.newInstance(value);
                  } catch (NoSuchMethodException e) {
                    Method parserMethod = findStaticMethod(fieldType, "valueOf", String.class);
                    if (parserMethod == null) {
                      parserMethod = findStaticMethod(fieldType, "fromString", String.class);
                    }
                    checkState(parserMethod != null,
                        "%s has no string constructor and no static parserMethod named 'valueOf'"
                            + " or 'fromString' that accepts a single String argument.",
                        fieldType);
                    parsedValue = parserMethod.invoke(null, value);
                  }
                }
                break;
            }
            checkArgument(flag.validator().newInstance().test(parsedValue),
                "Invalid value %s assigned to flag %s", parsedValue, argument);

            // relax modifiers
            boolean accessible = field.isAccessible();
            if (!accessible) {
              field.setAccessible(true);
            }
            Field modifiers = field.getClass().getDeclaredField("modifiers");
            modifiers.setAccessible(true);
            modifiers.setInt(field, field.getModifiers() & ~Modifier.FINAL);

            // set value
            if (flagValues != null) {
              flagValues.put(field, field.get(null));
            }
            field.set(null, parsedValue);

            // reset modifiers
            modifiers.setInt(field, field.getModifiers() & Modifier.FINAL);
            modifiers.setAccessible(accessible);
            field.setAccessible(accessible);
          } catch (InstantiationException
              | IllegalAccessException
              | IOException
              | NoSuchFieldException
              | InvocationTargetException e) {
            throw Throwables.propagate(e);
          }
        } else {
          logger.warn("Unknown flag = {0}.", argument);
          unknownFlags.add(argument);
        }
      } else {
        logger.warn("Unknown flag = {0}.", argument);
        unknownFlags.add(argument);
      }
    }
    return unknownFlags.toArray(new String[] {});
  }

  private static void loadFieldsByFlagName() {
    if (fieldsByFlagName != null) {
      return;
    }
    try {
      fieldsByFlagName = new HashMap<>();
      for (Definition definition : getDefinitions(ObjectMappers.forJson())) {
        Class<?> klass = Class.forName(definition.getBinaryClassName());
        Field field = klass.getDeclaredField(definition.getFieldName());
        fieldsByFlagName.put(definition.getCanonicalFlagName(), field);
        if (definition.getShortName() != null) {
          fieldsByFlagName.put(definition.getShortName(), field);
        }
      }
    } catch (ClassNotFoundException | IOException | NoSuchFieldException | SecurityException e) {
      throw Throwables.propagate(e);
    }
  }

  public static String[] dump() {
    ObjectMapper mapper = ObjectMappers.forJson();
    Set<String> flags = new TreeSet<>();
    try {
      for (Definition definition : getDefinitions(mapper)) {
        Class<?> klass = Class.forName(definition.getBinaryClassName());
        Field field = klass.getDeclaredField(definition.getFieldName());
        Flag flag = field.getAnnotation(Flag.class);

        // relax modifiers
        boolean accessible = field.isAccessible();
        if (!accessible) {
          field.setAccessible(true);
        }
        Field modifiers = field.getClass().getDeclaredField("modifiers");
        modifiers.setAccessible(true);
        modifiers.setInt(field, field.getModifiers() & ~Modifier.FINAL);

        // set value
        final String value;
        if (flag.parserType() == ParserType.JSON) {
          value = mapper.writeValueAsString(field.get(null));
        } else {
          value = field.get(null).toString();
        }

        // reset modifiers
        modifiers.setInt(field, field.getModifiers() & Modifier.FINAL);
        modifiers.setAccessible(accessible);
        field.setAccessible(accessible);

        flags.add("--" + definition.getCanonicalFlagName() + "=" + value);
        if (definition.getShortName() != null) {
          flags.add("--" + definition.getShortName() + "=" + value);
        }
      }
    } catch (ClassNotFoundException
        | IllegalAccessException
        | IOException
        | NoSuchFieldException e) {
      throw Throwables.propagate(e);
    }
    return flags.toArray(new String[] {});
  }

  public static void resetForTest(String... flagNames) {
    checkNotNull(flagValues, "Must be called from a test method with @Rule FlagTestRule enabled.");
    loadFieldsByFlagName();
    try {
      for (String flagName : flagNames) {
        Field field = fieldsByFlagName.get(flagName);
        checkNotNull(field, "Invalid flag name = %s.", flagName);
        checkArgument(flagValues.containsKey(field), "Flag = %s was not set for test.", flagName);
        resetField(field);
      }
    } catch (IllegalAccessException | NoSuchFieldException e) {
      throw Throwables.propagate(e);
    }
  }

  public static void resetAllForTest() {
    checkNotNull(flagValues, "Must be called from a test method with @Rule FlagTestRule enabled.");
    try {
      for (Field field : new HashSet<>(flagValues.keySet())) {
        resetField(field);
      }
    } catch (IllegalAccessException | NoSuchFieldException e) {
      throw Throwables.propagate(e);
    }
  }

  private static List<Definition> getDefinitions(ObjectMapper mapper) throws IOException {
    Enumeration<URL> systemResources = Thread.currentThread().getContextClassLoader()
        .getResources(PACKAGE_NAME.replace('.', '/') + "/" + FLAGS_FILE_NAME);

    List<Definition> definitions = new ArrayList<>();
    while (systemResources.hasMoreElements()) {
      Scanner scanner = new Scanner(systemResources.nextElement().openStream()).useDelimiter("\\A");
      if (scanner.hasNext()) {
        definitions.addAll(Arrays.asList(mapper.readValue(scanner.next(), Definition[].class)));
      }
    }
    return definitions;
  }

  private static Method findStaticMethod(Class<?> type, String name, Class<?>... parameterTypes) {
    try {
      Method method = type.getMethod(name, parameterTypes);
      return Modifier.isStatic(method.getModifiers()) ? method : null;
    } catch (NoSuchMethodException e1) {
      return null;
    }
  }

  private static void resetField(Field field) throws NoSuchFieldException, IllegalAccessException {
    // relax modifiers
    boolean accessible = field.isAccessible();
    if (!accessible) {
      field.setAccessible(true);
    }
    Field modifiers = field.getClass().getDeclaredField("modifiers");
    modifiers.setAccessible(true);
    modifiers.setInt(field, field.getModifiers() & ~Modifier.FINAL);

    // reset value
    field.set(null, flagValues.remove(field));

    // reset modifiers
    modifiers.setInt(field, field.getModifiers() & Modifier.FINAL);
    modifiers.setAccessible(accessible);
    field.setAccessible(accessible);
  }

  private Flags() {
  }
}
