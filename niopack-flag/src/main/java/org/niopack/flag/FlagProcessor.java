package org.niopack.flag;

import static javax.tools.Diagnostic.Kind.ERROR;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.common.base.Strings;
import com.google.common.base.Throwables;
import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Multiset;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.util.Elements;
import javax.lang.model.util.SimpleElementVisitor8;
import javax.tools.StandardLocation;
import org.niopack.jackson.ObjectMappers;
import org.niopack.logging.ContextualLogger;
import org.slf4j.Logger;

@SupportedAnnotationTypes(FlagProcessor.FLAG_CLASS_NAME)
@SupportedSourceVersion(SourceVersion.RELEASE_8)
public class FlagProcessor extends AbstractProcessor {

  private static final Logger logger = ContextualLogger.create();

  private static final Set<String> PRIMITIVE_TYPES = ImmutableSet.<String>builder()
      .add("byte")
      .add("short")
      .add("int")
      .add("long")
      .add("float")
      .add("double")
      .add("char")
      .add("boolean")
      .build();

  static final String PACKAGE_NAME = "org.niopack.flag";
  static final String FLAG_CLASS_NAME = PACKAGE_NAME + ".Flag";
  static final String FLAGS_FILE_NAME = "flags.json";

  private final Set<String> flagNames = new HashSet<>();
  private final Map<Element, Definition> definitionsByElement = new HashMap<>();
  private final Multiset<String> shortNames = HashMultiset.create();
  private Writer writer;

  @Override
  public synchronized void init(ProcessingEnvironment processingEnv) {
    super.init(processingEnv);
    try {
      writer = processingEnv.getFiler()
          .createResource(StandardLocation.CLASS_OUTPUT, PACKAGE_NAME, FLAGS_FILE_NAME)
          .openWriter();
    } catch (IOException e) {
      throw Throwables.propagate(e);
    }
  }

  @Override
  public boolean process(Set<? extends TypeElement> annotations,
      RoundEnvironment roundEnvironment) {
    if (roundEnvironment.processingOver()) {
      for (Definition definition : definitionsByElement.values()) {
        if (definition.getGivenName() != null) {
          definition.setShortName(definition.getGivenName());
        } else {
          if (shortNames.count(definition.getFieldName()) == 1) {
            definition.setShortName(definition.getFieldName());
          }
        }
      }
      try {
        ObjectMapper mapper = ObjectMappers.forJson();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.writeValue(writer, definitionsByElement.values());
        writer.close();
      } catch (IOException e) {
        throw Throwables.propagate(e);
      }
      return true;
    }

    for (Element element : roundEnvironment.getElementsAnnotatedWith(Flag.class)) {
      Flag flag = element.getAnnotation(Flag.class);
      if (element.getKind() != ElementKind.FIELD) {
        error("@Flag applied to a non-field: %s.", element);
      }
      if (!element.getModifiers().contains(Modifier.STATIC) && !element.getModifiers()
          .contains(Modifier.FINAL)) {
        error("@Flag applied to a non-constant field: %s.", element);
      }
      if (PRIMITIVE_TYPES.contains(element.asType().toString())) {
        error("@Flag applied to a primitive type field: %s.", element.asType().toString());
      }
      boolean usesConstantValue = element.accept(new ConstantValueDetector(), null);
      if (usesConstantValue) {
        error("Constant expression used to initialize flag %s.", element);
      }

      Elements elements = processingEnv.getElementUtils();
      TypeElement typeElement = getEnclosingTypeElement(element);
      if (typeElement == null) {
        error("No enclosing type found for %s", element);
      }
      if (typeElement.getQualifiedName().toString().isEmpty()) {
        error("Can't define flags inside local and anonymous classes.");
      }

      Definition definition = new Definition();
      definition.setCanonicalClassName(typeElement.getQualifiedName().toString());
      definition.setBinaryClassName(elements.getBinaryName(typeElement).toString());
      definition.setFieldName(element.getSimpleName().toString());
      definition.setGivenName(Strings.emptyToNull(flag.name().trim()));
      definition.setHelp(flag.help().trim());

      definitionsByElement.put(element, definition);

      if (flagNames.contains(definition.getCanonicalFlagName())) {
        error("Canonical flag name %s already exists.", definition.getCanonicalFlagName());
      }
      flagNames.add(definition.getCanonicalFlagName());

      if (definition.getGivenName() != null) {
        if (flagNames.contains(definition.getGivenName())) {
          error("User provided short name %s already exists.", definition.getGivenName());
        }
        flagNames.add(definition.getGivenName());
        shortNames.add(definition.getGivenName());
      } else {
        shortNames.add(definition.getFieldName());
      }
    }
    return true;
  }

  private TypeElement getEnclosingTypeElement(Element element) {
    Element current = element;
    while (current != null) {
      if (current instanceof TypeElement) {
        return ((TypeElement) current);
      }
      current = current.getEnclosingElement();
    }
    return null;
  }

  private void error(String message, Object... arguments) {
    processingEnv.getMessager().printMessage(ERROR, String.format(message, arguments));
  }

  private static class ConstantValueDetector extends SimpleElementVisitor8<Boolean, Void> {
    @Override
    public Boolean visitVariable(VariableElement e, Void aVoid) {
      super.visitVariable(e, aVoid);
      return e.getConstantValue() != null;
    }
  }
}
