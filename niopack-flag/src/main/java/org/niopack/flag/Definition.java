package org.niopack.flag;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.google.common.base.MoreObjects;

@JsonInclude(Include.NON_NULL)
final class Definition {

  private String canonicalClassName;
  private String binaryClassName;
  private String fieldName;
  private String givenName;
  private String shortName;
  private String help;

  public String getCanonicalClassName() {
    return canonicalClassName;
  }

  public void setCanonicalClassName(String canonicalClassName) {
    this.canonicalClassName = canonicalClassName;
  }

  public String getBinaryClassName() {
    return binaryClassName;
  }

  public void setBinaryClassName(String binaryClassName) {
    this.binaryClassName = binaryClassName;
  }

  public String getFieldName() {
    return fieldName;
  }

  public void setFieldName(String fieldName) {
    this.fieldName = fieldName;
  }

  public String getGivenName() {
    return givenName;
  }

  public void setGivenName(String givenName) {
    this.givenName = givenName;
  }

  public String getShortName() {
    return shortName;
  }

  public void setShortName(String shortName) {
    this.shortName = shortName;
  }

  public String getHelp() {
    return help;
  }

  public void setHelp(String help) {
    this.help = help;
  }

  @JsonIgnore
  public String getCanonicalFlagName() {
    return canonicalClassName + "." + fieldName;
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("canonicalClassName", canonicalClassName)
        .add("binaryClassName", binaryClassName)
        .add("fieldName", fieldName)
        .add("givenName", givenName)
        .add("shortName", shortName)
        .add("help", help)
        .toString();
  }
}
