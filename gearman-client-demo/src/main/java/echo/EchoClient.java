package echo;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.gearman.Gearman;
import org.gearman.GearmanClient;
import org.gearman.GearmanJobEvent;
import org.gearman.GearmanJobReturn;
import org.niopack.queue.module.GearmanClientModule;

/**
 * The echo client submits an "echo" job to a job server and prints the final result. It's the
 * "Hello World" of the java-geraman-service
 * 
 * The echo example illustrates how send a single job and get the result
 */
public class EchoClient {

  public static void main(String... args) throws InterruptedException {
    Injector injector = Guice.createInjector(new GearmanClientModule());
    Gearman gearman = injector.getInstance(Gearman.class);
    GearmanClient client = injector.getInstance(GearmanClient.class);

    /*
     * Submit a job to a job server.
     * 
     * Parameter 1: the gearman function name Parameter 2: the data passed to the server and worker
     * 
     * The GearmanJobReturn is used to poll the job's result
     */
    GearmanJobReturn jobReturn =
        client.submitJob(DemoFunction.ECHO.name(), "Hello World".getBytes());

    /*
     * Iterate through the job events until we hit the end-of-file
     */
    while (!jobReturn.isEOF()) {
      // Poll the next job event (blocking operation)
      GearmanJobEvent event = jobReturn.poll();
      switch (event.getEventType()) {
      // success
        case GEARMAN_JOB_SUCCESS: // Job completed successfully
          // print the result
          System.out.println(new String(event.getData()));
          break;

        // failure
        case GEARMAN_SUBMIT_FAIL: // The job submit operation failed
        case GEARMAN_JOB_FAIL: // The job's execution failed
          System.err.println(event.getEventType() + ": " + new String(event.getData()));
      }
    }

    /*
     * Close the gearman service after it's no longer needed. (closes all sub-services, such as the
     * client)
     * 
     * It's suggested that you reuse Gearman and GearmanClient instances rather recreating and
     * closing new ones between submissions
     */
    gearman.shutdown();
  }
}
