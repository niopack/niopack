package echo;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.gearman.Gearman;
import org.gearman.GearmanClient;
import org.gearman.GearmanJobEvent;
import org.gearman.GearmanJobEventCallback;
import org.gearman.GearmanJoin;
import org.niopack.queue.module.GearmanClientModule;

/**
 * The echo client submits an "echo" job to a job server and prints the final result. It's the
 * "Hello World" of the java-geraman-service
 * 
 * The echo example illustrates how send a single job and get the result
 */
public class EchoClientAsynchronous implements GearmanJobEventCallback<String> {

  public static void main(String... args) throws InterruptedException {
    Injector injector = Guice.createInjector(new GearmanClientModule());
    Gearman gearman = injector.getInstance(Gearman.class);
    GearmanClient client = injector.getInstance(GearmanClient.class);

    /*
     * Submit a job to a job server. This submit method uses an asynchronous callback object to
     * process the job's result
     * 
     * Parameter 1: the gearman function name Parameter 2: the data passed to the server and worker
     * Parameter 3: an attachment returned through the callback Parameter 4: the callback used to
     * process the job events
     * 
     * The GearmanJoin object is used to block the current thread until the end-of-file has been
     * reached.
     */
    GearmanJoin<String> join =
        client.submitJob(DemoFunction.ECHO.name(), "Hello World".getBytes(),
            DemoFunction.ECHO.name(), new EchoClientAsynchronous());

    /*
     * Block the current thread until all events have been processed.
     */
    join.join();


    /*
     * After the job has been completely processed. We close the service
     * 
     * It's suggested that you reuse Gearman and GearmanClient instances rather recreating and
     * closing new ones between submissions
     */
    gearman.shutdown();
  }

  public void onEvent(String attachment, GearmanJobEvent event) {
    /*
     * This method is called by the client when an event is received
     */
    switch (event.getEventType()) {
      case GEARMAN_JOB_SUCCESS: // Job completed successfully
        System.out.println(new String(event.getData()));
        break;
      case GEARMAN_SUBMIT_FAIL: // The job submit operation failed
      case GEARMAN_JOB_FAIL: // The job's execution failed
        System.err.println(event.getEventType() + ": " + new String(event.getData()));
    }

  }
}
