package org.niopack.servlets.tasks;

import static com.codahale.metrics.MetricRegistry.name;
import static com.google.common.base.Preconditions.checkNotNull;

import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.codahale.metrics.annotation.ExceptionMetered;
import com.codahale.metrics.annotation.Metered;
import com.codahale.metrics.annotation.Timed;
import com.codahale.metrics.health.HealthCheckRegistry;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.net.MediaType;
import com.google.inject.Inject;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A servlet which provides access to administrative {@link Task}s. It only responds to {@code POST}
 * requests, since most {@link Task}s aren't side-effect free, and passes along the query string
 * parameters of the request to the task as a multimap.
 *
 * @see Task
 */
public class TaskServlet extends HttpServlet {
  private static final long serialVersionUID = 7404713218661358124L;
  private static final Logger logger = LoggerFactory.getLogger(TaskServlet.class);
  private final ImmutableMap<String, Task> tasks;
  private final ImmutableMap<Task, TaskExecutor> taskExecutors;

  private final MetricRegistry metricRegistry;
  private final HealthCheckRegistry healthCheckRegistry;

  @Inject
  public TaskServlet(Set<Task> taskSet,
      MetricRegistry metricRegistry,
      HealthCheckRegistry healthCheckRegistry) {
    this.metricRegistry = checkNotNull(metricRegistry, "metricRegistry");
    this.healthCheckRegistry = checkNotNull(healthCheckRegistry, "healthCheckRegistry");
    ImmutableMap.Builder<String, Task> tasksBuilder = ImmutableMap.builder();
    ImmutableMap.Builder<Task, TaskExecutor> taskExecutorsBuilder = ImmutableMap.builder();
    for (Task task : taskSet) {
      add(task, tasksBuilder, taskExecutorsBuilder);
    }
    this.tasks = tasksBuilder.build();
    this.taskExecutors = taskExecutorsBuilder.build();
  }

  private void add(Task task, ImmutableMap.Builder<String, Task> tasksBuilder,
      ImmutableMap.Builder<Task, TaskExecutor> taskExecutorsBuilder) {
    tasksBuilder.put('/' + task.getName(), task);

    TaskExecutor taskExecutor = new TaskExecutor(task);
    try {
      Method executeMethod =
          task.getClass().getMethod("execute", ImmutableMultimap.class, PrintWriter.class);

      if (executeMethod.isAnnotationPresent(Timed.class)) {
        Timed annotation = executeMethod.getAnnotation(Timed.class);
        String name = chooseName(annotation.name(), annotation.absolute(), task);
        Timer timer = metricRegistry.timer(name);
        taskExecutor = new TimedTask(taskExecutor, timer);
      }

      if (executeMethod.isAnnotationPresent(Metered.class)) {
        Metered annotation = executeMethod.getAnnotation(Metered.class);
        String name = chooseName(annotation.name(), annotation.absolute(), task);
        Meter meter = metricRegistry.meter(name);
        taskExecutor = new MeteredTask(taskExecutor, meter);
      }

      if (executeMethod.isAnnotationPresent(ExceptionMetered.class)) {
        ExceptionMetered annotation = executeMethod.getAnnotation(ExceptionMetered.class);
        String name = chooseName(annotation.name(), annotation.absolute(), task,
            ExceptionMetered.DEFAULT_NAME_SUFFIX);
        Meter exceptionMeter = metricRegistry.meter(name);
        taskExecutor = new ExceptionMeteredTask(taskExecutor, exceptionMeter, annotation.cause());
      }
    } catch (NoSuchMethodException e) {
      logger.warn(e.getMessage(), e);
    }

    taskExecutorsBuilder.put(task, taskExecutor);
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException,
      IOException {
    final Task task = tasks.get(req.getPathInfo());
    if (task != null) {
      resp.setContentType(MediaType.PLAIN_TEXT_UTF_8.toString());
      final PrintWriter output = resp.getWriter();
      try {
        TaskExecutor taskExecutor = taskExecutors.get(task);
        taskExecutor.executeTask(getParams(req), output);
      } catch (Exception e) {
        logger.error("Error running {}", task.getName(), e);
        resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        output.println();
        output.println(e.getMessage());
        e.printStackTrace(output);
      } finally {
        output.close();
      }
    } else {
      resp.sendError(HttpServletResponse.SC_NOT_FOUND);
    }
  }

  private static ImmutableMultimap<String, String> getParams(HttpServletRequest req) {
    final ImmutableMultimap.Builder<String, String> results = ImmutableMultimap.builder();
    final Enumeration<String> names = req.getParameterNames();
    while (names.hasMoreElements()) {
      final String name = names.nextElement();
      final String[] values = req.getParameterValues(name);
      results.putAll(name, values);
    }
    return results.build();
  }

  public Collection<Task> getTasks() {
    return tasks.values();
  }

  private String chooseName(String explicitName, boolean absolute, Task task, String... suffixes) {
    if (explicitName != null && !explicitName.isEmpty()) {
      if (absolute) {
        return explicitName;
      }
      return name(task.getClass(), explicitName);
    }

    return name(task.getClass(), suffixes);
  }
  
  private void logTasks() {
    final StringBuilder stringBuilder = new StringBuilder(1024).append(String.format("%n%n"));

    for (Task task : tasks.values()) {
      stringBuilder.append(String.format("  %-7s /tasks/%s (%s)%n", "POST", task.getName(), task
          .getClass().getCanonicalName()));
    }

    logger.info("tasks = {}", stringBuilder.toString());
  }

  private void logHealthChecks() {
    if (healthCheckRegistry.getNames().size() <= 1) {
      logger.warn(String.format("%n"
          + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%n"
          + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%n"
          + "!  THIS APPLICATION HAS NO HEALTHCHECKS. THIS MEANS YOU WILL NEVER KNOW    !%n"
          + "!   IF IT DIES IN PRODUCTION, WHICH MEANS YOU WILL NEVER KNOW IF YOU'RE    !%n"
          + "!  LETTING YOUR USERS DOWN. YOU SHOULD ADD A HEALTHCHECK FOR EACH OF YOUR  !%n"
          + "!     APPLICATION'S DEPENDENCIES WHICH FULLY (BUT LIGHTLY) TESTS IT.     !%n"
          + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%n"
          + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"));
    }
    logger.debug("health checks = {}", healthCheckRegistry.getNames());
  }

  private static class TaskExecutor {
    private final Task task;

    private TaskExecutor(Task task) {
      this.task = task;
    }

    public void executeTask(ImmutableMultimap<String, String> params, PrintWriter output)
        throws Exception {
      try {
        task.execute(params, output);
      } catch (Exception e) {
        throw e;
      }
    }
  }

  private static class TimedTask extends TaskExecutor {
    private TaskExecutor underlying;
    private final Timer timer;

    private TimedTask(TaskExecutor underlying, Timer timer) {
      super(underlying.task);
      this.underlying = underlying;
      this.timer = timer;
    }

    @Override
    public void executeTask(ImmutableMultimap<String, String> params, PrintWriter output)
        throws Exception {
      final Timer.Context context = timer.time();
      try {
        underlying.executeTask(params, output);
      } finally {
        context.stop();
      }
    }
  }

  private static class MeteredTask extends TaskExecutor {
    private TaskExecutor underlying;
    private final Meter meter;

    private MeteredTask(TaskExecutor underlying, Meter meter) {
      super(underlying.task);
      this.meter = meter;
      this.underlying = underlying;
    }

    @Override
    public void executeTask(ImmutableMultimap<String, String> params, PrintWriter output)
        throws Exception {
      meter.mark();
      underlying.executeTask(params, output);
    }
  }

  private static class ExceptionMeteredTask extends TaskExecutor {
    private TaskExecutor underlying;
    private final Meter exceptionMeter;
    private final Class<?> exceptionClass;

    private ExceptionMeteredTask(TaskExecutor underlying, Meter exceptionMeter,
        Class<? extends Throwable> exceptionClass) {
      super(underlying.task);
      this.underlying = underlying;
      this.exceptionMeter = exceptionMeter;
      this.exceptionClass = exceptionClass;
    }

    @Override
    public void executeTask(ImmutableMultimap<String, String> params, PrintWriter output)
        throws Exception {
      try {
        underlying.executeTask(params, output);
      } catch (Exception e) {
        if (exceptionMeter != null && exceptionClass.isAssignableFrom(e.getClass())
            || (e.getCause() != null && exceptionClass.isAssignableFrom(e.getCause().getClass()))) { // NOPMD
          exceptionMeter.mark();
        }
        throw e;
      }
    }
  }
}
