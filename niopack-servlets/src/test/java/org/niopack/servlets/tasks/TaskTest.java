package org.niopack.servlets.tasks;

import static com.google.common.truth.Truth.assertThat;

import com.google.common.collect.ImmutableMultimap;
import java.io.PrintWriter;
import org.junit.Test;

public class TaskTest {
  private final Task task = new Task("test") {
    @Override
    public void execute(ImmutableMultimap<String, String> parameters,
              PrintWriter output) throws Exception {

    }
  };

  @Test
  public void hasAName() throws Exception {
    assertThat(task.getName())
        .isEqualTo("test");
  }
}
