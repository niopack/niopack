```
#!shell

$ http -f POST 'http://localhost:8080/api/books' name='War and Peace'

$ http -f GET 'http://localhost:8080/api/books'

$ http -f GET 'http://localhost:8080/api/books/<book_id>'
```
