package org.niopack.demo.bookstore.api.definition.book;

import com.fasterxml.jackson.annotation.JsonTypeName;
import javax.ws.rs.FormParam;
import org.niopack.demo.bookstore.model.service.Book;

public final class CreateMethod {

  public static final class Request {
    @FormParam("name") private String name;

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }
  }

  @JsonTypeName("response")
  public static final class Response {
    private Book book;

    public Book getBook() {
      return book;
    }

    public void setBook(Book book) {
      this.book = book;
    }
  }
}
