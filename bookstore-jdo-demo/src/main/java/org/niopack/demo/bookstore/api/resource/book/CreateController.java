package org.niopack.demo.bookstore.api.resource.book;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.niopack.resteasy.errors.ClientErrorChecks.checkParameter;
import static org.niopack.resteasy.errors.ErrorCode.code;

import javax.inject.Inject;
import org.niopack.db.jdo.Transactor;
import org.niopack.demo.bookstore.api.definition.book.CreateMethod.Request;
import org.niopack.demo.bookstore.api.definition.book.CreateMethod.Response;
import org.niopack.demo.bookstore.common.BookConverter;
import org.niopack.demo.bookstore.model.store.Book;

public class CreateController {

  private static final BookConverter BOOK_CONVERTER = new BookConverter();

  private final Transactor transactor;

  @Inject
  public CreateController(Transactor transactor) {
    this.transactor = checkNotNull(transactor, "transactor");
  }

  Response execute(Request request) {
    checkParameter(request, code("request"));

    Book book = new Book();
    book.setName(request.getName());
    transactor.run(pm -> pm.makePersistent(book));

    Response response = new Response();
    response.setBook(BOOK_CONVERTER.convert(book));
    return response;
  }
}
