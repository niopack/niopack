package org.niopack.demo.bookstore.model.service;

import java.util.Objects;
import javax.annotation.Nullable;

public class Book {

  private long id;
  private String name;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(@Nullable String name) {
    this.name = name;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.id);
  }

  @Override
  public boolean equals(@Nullable Object obj) {
    if (!(obj instanceof Book)) {
      return false;
    }
    Book that = (Book) obj;
    return Objects.equals(this.id, that.id);
  }

  @Override
  public String toString() {
    return id + ":" + name;
  }
}
