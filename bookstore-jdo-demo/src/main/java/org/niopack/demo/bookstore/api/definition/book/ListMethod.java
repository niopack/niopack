package org.niopack.demo.bookstore.api.definition.book;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.ArrayList;
import java.util.List;
import org.niopack.demo.bookstore.model.service.Book;

public final class ListMethod {

  public static final class Request {
  }

  @JsonTypeName("response")
  public static final class Response {
    private List<Book> books = new ArrayList<>();

    public List<Book> getBooks() {
      return books;
    }

    public void setBooks(List<Book> books) {
      this.books = books;
    }
  }
}
