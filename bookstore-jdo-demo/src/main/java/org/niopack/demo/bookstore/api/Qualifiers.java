package org.niopack.demo.bookstore.api;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import javax.inject.Qualifier;

public final class Qualifiers {

  @Documented
  @Qualifier
  @Retention(RUNTIME)
  public static @interface MaxListLength {
  }

  private Qualifiers() {}
}
