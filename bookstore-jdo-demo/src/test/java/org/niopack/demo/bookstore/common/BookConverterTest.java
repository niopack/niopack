package org.niopack.demo.bookstore.common;

import static com.google.common.truth.Truth.assertThat;

import com.google.common.testing.ArbitraryInstances;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class BookConverterTest {

  @Test
  public void testForwardConvertDefaultInstance() {
    org.niopack.demo.bookstore.model.store.Book storeBook =
        new org.niopack.demo.bookstore.model.store.Book();
    org.niopack.demo.bookstore.model.service.Book serviceBook =
        new BookConverter().convert(storeBook);
    assertThat(serviceBook.getId()).isEqualTo(storeBook.getId());
    assertThat(serviceBook.getName()).isEqualTo(storeBook.getName());
  }

  @Test
  public void testBackwardConvertDefaultInstance() {
    org.niopack.demo.bookstore.model.service.Book serviceBook =
        new org.niopack.demo.bookstore.model.service.Book();
    org.niopack.demo.bookstore.model.store.Book storeBook =
        new BookConverter().reverse().convert(serviceBook);
    assertThat(storeBook.getId()).isEqualTo(serviceBook.getId());
    assertThat(storeBook.getName()).isEqualTo(serviceBook.getName());
  }

  @Test
  public void testForwardConvert() {
    org.niopack.demo.bookstore.model.store.Book storeBook =
        ArbitraryInstances.get(org.niopack.demo.bookstore.model.store.Book.class);
    org.niopack.demo.bookstore.model.service.Book serviceBook =
        new BookConverter().convert(storeBook);
    assertThat(serviceBook.getId()).isEqualTo(storeBook.getId());
    assertThat(serviceBook.getName()).isEqualTo(storeBook.getName());
  }

  @Test
  public void testBackwardConvert() {
    org.niopack.demo.bookstore.model.service.Book serviceBook =
        ArbitraryInstances.get(org.niopack.demo.bookstore.model.service.Book.class);
    org.niopack.demo.bookstore.model.store.Book storeBook =
        new BookConverter().reverse().convert(serviceBook);
    assertThat(storeBook.getId()).isEqualTo(serviceBook.getId());
    assertThat(storeBook.getName()).isEqualTo(serviceBook.getName());
  }
}
