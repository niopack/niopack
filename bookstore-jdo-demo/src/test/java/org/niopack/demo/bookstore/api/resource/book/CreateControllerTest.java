package org.niopack.demo.bookstore.api.resource.book;

import static com.google.common.truth.Truth.assertThat;

import com.google.common.testing.NullPointerTester;
import java.time.Clock;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.niopack.db.testing.MockDatabase;
import org.niopack.demo.bookstore.api.definition.book.CreateMethod.Request;
import org.niopack.demo.bookstore.api.definition.book.CreateMethod.Response;
import org.niopack.demo.bookstore.common.BookConverter;
import org.niopack.demo.bookstore.dao.BookDao;
import org.niopack.demo.bookstore.model.service.Book;

@RunWith(JUnit4.class)
public class CreateControllerTest {

  @Rule public MockDatabase mockDb = new MockDatabase();

  @Test
  public void testAllPublicConstructors() {
    new NullPointerTester()
        .setDefault(Clock.class, Clock.systemUTC())
        .testAllPublicConstructors(CreateController.class);
  }

  @Test
  public void testAllPublicInstanceMethods() {
    CreateController controller = createController();
    new NullPointerTester()
        .setDefault(Clock.class, Clock.systemUTC())
        .testAllPublicInstanceMethods(controller);
  }

  @Test
  public void shouldCreateNewBook() {
    CreateController controller = createController();

    Request request = new Request();
    request.setName("War & Peace");
    Response response = controller.execute(request);
    Book book = response.getBook();
    BookConverter converter = new BookConverter();
    assertThat(book).isEqualTo(converter.convert(
        mockDb.getTransactor().retrieve(pm -> new BookDao().get(pm, book.getId()))));
  }

  private CreateController createController() {
    return new CreateController(mockDb.getTransactor());
  }
}
