package org.niopack.demo.bookstore.api.resource.book;

import static com.google.common.truth.Truth.assertThat;

import java.net.URISyntaxException;
import javax.inject.Singleton;
import javax.jdo.PersistenceManagerFactory;
import javax.servlet.http.HttpServletResponse;
import org.jboss.resteasy.mock.MockHttpRequest;
import org.jboss.resteasy.mock.MockHttpResponse;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.niopack.db.jdo.Transactor;
import org.niopack.db.jdo.TransactorImpl;
import org.niopack.db.testing.MockDatabase;
import org.niopack.demo.bookstore.api.definition.book.BookService;
import org.niopack.demo.bookstore.model.store.Book;
import org.niopack.guice.ResteasyServletModule;
import org.niopack.testing.MockServer;

@RunWith(JUnit4.class)
public class BookServiceImplTest {

  @Rule public MockDatabase mockDb = new MockDatabase();

  @Test
  public void testCreate() throws URISyntaxException {
    MockServer server = createServer();

    MockHttpRequest request = MockHttpRequest.post("/api/books");
    request.addFormHeader("name", "War & Peace");
    MockHttpResponse response = new MockHttpResponse();
    server.invoke(request, response);
    assertThat(response.getStatus()).isEqualTo(HttpServletResponse.SC_OK);
    assertThat(response.getContentAsString()).isEqualTo(
        "{\"book\":{\"id\":1,\"name\":\"War & Peace\"}}");
  }

  @Test
  public void testList() throws URISyntaxException {
    MockServer server = createServer();

    MockHttpRequest request = MockHttpRequest.get("/api/books");
    MockHttpResponse response = new MockHttpResponse();
    server.invoke(request, response);
    assertThat(response.getStatus()).isEqualTo(HttpServletResponse.SC_OK);
    assertThat(response.getContentAsString()).isEqualTo("{\"books\":[]}");
  }

  @Test
  public void testGet() throws URISyntaxException {
    MockServer server = createServer();

    Book book = new Book();
    book.setName("War & Peace");
    mockDb.getTransactor().run(pm -> pm.makePersistent(book));

    MockHttpRequest request = MockHttpRequest.get("/api/books/" + book.getId());
    MockHttpResponse response = new MockHttpResponse();
    server.invoke(request, response);
    assertThat(response.getStatus()).isEqualTo(HttpServletResponse.SC_OK);
    assertThat(response.getContentAsString())
        .isEqualTo("{\"book\":{\"id\":1,\"name\":\"War & Peace\"}}");
  }

  private MockServer createServer() {
    return MockServer.create(new ResteasyServletModule() {
      @Override
      protected void configureResources() {
        bind(PersistenceManagerFactory.class).toInstance(mockDb.getPersistenceManagerFactory());
        bind(Transactor.class).to(TransactorImpl.class).in(Singleton.class);
        // Resources
        bind(BookService.class).to(BookServiceImpl.class);
      }
    });
  }
}
