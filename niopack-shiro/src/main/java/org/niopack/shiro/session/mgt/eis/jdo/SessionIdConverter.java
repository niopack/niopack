package org.niopack.shiro.session.mgt.eis.jdo;

import com.google.common.base.Converter;
import java.io.Serializable;
import org.niopack.io.CompressionStrategy;
import org.niopack.io.SerDerUtils;

public class SessionIdConverter extends Converter<String, Serializable> {

  @Override
  protected Serializable doForward(String in) {
    return (Serializable) SerDerUtils.deserialize(in.getBytes(), CompressionStrategy.NONE);
  }

  @Override
  protected String doBackward(Serializable in) {
    return new String(SerDerUtils.serialize(in, CompressionStrategy.NONE));
  }
}
