package org.niopack.shiro.session.mgt.eis.jdo;

import com.google.common.base.Converter;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.SimpleSession;
import org.niopack.io.CompressionStrategy;
import org.niopack.io.SerDerUtils;
import org.niopack.shiro.session.mgt.eis.jdo.model.ShiroSession;

public class SessionConverter extends Converter<ShiroSession, Session> {
  
  private static final SessionIdConverter ID_CONVERTER = new SessionIdConverter();

  @Override
  protected Session doForward(ShiroSession in) {
    return (Session) SerDerUtils.deserialize(in.getSerializedValue(), CompressionStrategy.NONE);
  }

  @Override
  protected ShiroSession doBackward(Session in) {
    ShiroSession out = new ShiroSession();
    out.setHost(in.getHost());
    out.setLastAccessTimestamp(in.getLastAccessTime());
    out.setSerializedValue(SerDerUtils.serialize(in, CompressionStrategy.NONE));
    String shiroSessionId = ID_CONVERTER.reverse().convert(in.getId());
    out.setSessionId(shiroSessionId);
    out.setStartTimestamp(in.getStartTimestamp());
    if (in instanceof SimpleSession) {
      SimpleSession simpleSession = (SimpleSession) in;
      out.setExpired(simpleSession.isExpired());
      out.setStopTimestamp(simpleSession.getStopTimestamp());
    }
    out.setTimeout(in.getTimeout());
    return out;
  }
}
