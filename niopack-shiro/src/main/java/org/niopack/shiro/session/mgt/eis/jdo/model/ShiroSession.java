package org.niopack.shiro.session.mgt.eis.jdo.model;

import com.google.common.base.MoreObjects;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

/**
 * @see <a
 *      href="http://shiro-user.582556.n2.nabble.com/Storing-sessins-in-an-RDBMS-instead-of-an-enterprise-cache-td5645213.html">Storing
 *      sessins in an RDBMS instead of an enterprise cache?</a>
 *
 * @see <a
 *      href="http://www.slideshare.net/planetcassandra/infinite-sessionclusteringwithapacheshiro9x16-updated-1-1-1">Infinite
 *      Session Clustering with Apache Shiro & Cassandra </a>
 */
@PersistenceCapable(table = "SESSION", detachable = "true", identityType = IdentityType.UNSPECIFIED)
public class ShiroSession implements Serializable {

  private static final long serialVersionUID = 1L;

  @PrimaryKey
  @Persistent(column = "session_id", valueStrategy = IdGeneratorStrategy.UNSPECIFIED)
  private String sessionId;

  @Persistent(column = "start_timestamp")
  private Date startTimestamp;

  @Persistent(column = "stop_timestamp")
  private Date stopTimestamp;

  @Persistent(column = "last_access_timestamp")
  private Date lastAccessTimestamp;

  @Persistent(column = "timeout")
  private long timeout;

  @Persistent(column = "expired")
  private boolean expired;

  @Persistent(column = "host")
  private String host;

  @Persistent(column = "serialized_value", defaultFetchGroup = "true")
  private byte[] serializedValue;

  public String getSessionId() {
    return sessionId;
  }

  public void setSessionId(String sessionId) {
    this.sessionId = sessionId;
  }

  public Date getStartTimestamp() {
    return startTimestamp;
  }

  public void setStartTimestamp(Date startTimestamp) {
    this.startTimestamp = startTimestamp;
  }

  public Date getStopTimestamp() {
    return stopTimestamp;
  }

  public void setStopTimestamp(Date stopTimestamp) {
    this.stopTimestamp = stopTimestamp;
  }

  public Date getLastAccessTimestamp() {
    return lastAccessTimestamp;
  }

  public void setLastAccessTimestamp(Date lastAccessTimestamp) {
    this.lastAccessTimestamp = lastAccessTimestamp;
  }

  public long getTimeout() {
    return timeout;
  }

  public void setTimeout(long timeout) {
    this.timeout = timeout;
  }

  public boolean isExpired() {
    return expired;
  }

  public void setExpired(boolean expired) {
    this.expired = expired;
  }

  public String getHost() {
    return host;
  }

  public void setHost(String host) {
    this.host = host;
  }

  public byte[] getSerializedValue() {
    return serializedValue;
  }

  public void setSerializedValue(byte[] serializedValue) {
    this.serializedValue = serializedValue;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(this.sessionId);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof ShiroSession)) {
      return false;
    }
    ShiroSession that = (ShiroSession) obj;
    return Objects.equals(this.sessionId, that.sessionId);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("sessionId", this.sessionId)
        .add("startTimestamp", this.startTimestamp)
        .add("stopTimestamp", this.stopTimestamp)
        .add("lastAccessTimestamp", this.lastAccessTimestamp)
        .add("timeout", this.timeout)
        .add("expired", this.expired)
        .add("host", this.host)
        .toString();
  }
}
