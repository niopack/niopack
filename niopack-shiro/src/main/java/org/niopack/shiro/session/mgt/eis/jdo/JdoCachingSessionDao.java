package org.niopack.shiro.session.mgt.eis.jdo;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.collect.ImmutableList;
import java.io.Serializable;
import java.time.Clock;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import org.apache.shiro.cache.MemoryConstrainedCacheManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.AbstractSessionManager;
import org.apache.shiro.session.mgt.SimpleSession;
import org.apache.shiro.session.mgt.eis.CachingSessionDAO;
import org.datanucleus.query.typesafe.TypesafeQuery;
import org.niopack.db.jdo.Transactor;
import org.niopack.shiro.session.mgt.eis.jdo.model.QShiroSession;
import org.niopack.shiro.session.mgt.eis.jdo.model.ShiroSession;

public class JdoCachingSessionDao extends CachingSessionDAO {

  private static final SessionConverter SESSION_CONVERTER = new SessionConverter();
  private static final SessionIdConverter ID_CONVERTER = new SessionIdConverter();

  private final Transactor transactor;
  private final Clock clock;

  @Inject
  public JdoCachingSessionDao(Transactor transactor, Clock clock) {
    this.transactor = checkNotNull(transactor, "transactor");
    this.clock = checkNotNull(clock, "clock");
    setCacheManager(new MemoryConstrainedCacheManager());
  }

  @Override
  protected Serializable doCreate(Session session) {
    Serializable sessionId = generateSessionId(session);
    assignSessionId(session, sessionId);
    ShiroSession shiroSession = SESSION_CONVERTER.reverse().convert(session);
    transactor.run(pm -> pm.makePersistent(shiroSession));
    return sessionId;
  }

  /**
   * @see <a
   *      href="https://shiro.apache.org/static/current/apidocs/org/apache/shiro/session/mgt/eis/SessionDAO.html#getActiveSessions()">SessionDAO#getActiveSessions</a>
   */
  @Override
  public Collection<Session> getActiveSessions() {
    // TODO(codefx): Inject correct DEFAULT_GLOBAL_SESSION_TIMEOUT
    Date oldestActiveDate = new Date(clock.instant().toEpochMilli()
        - AbstractSessionManager.DEFAULT_GLOBAL_SESSION_TIMEOUT);
    @SuppressWarnings("unchecked")
    List<ShiroSession> activeShiroSessions = transactor.retrieve(pm -> {
        TypesafeQuery<ShiroSession> tq = pm.newTypesafeQuery(ShiroSession.class);
        QShiroSession cand = QShiroSession.candidate();
        return tq.filter(cand.lastAccessTimestamp.lt(oldestActiveDate)
                .and(cand.stopTimestamp.eq((Date)null)))
            .executeList();
      });
    ImmutableList.Builder<Session> activeSessions = ImmutableList.builder();
    for (ShiroSession activeShiroSession : activeShiroSessions) {
      activeSessions.add(SESSION_CONVERTER.convert(activeShiroSession));
    }
    return activeSessions.build();
  }

  @Override
  protected Session doReadSession(Serializable sessionId) {
    String shiroSessionId = ID_CONVERTER.reverse().convert(sessionId);
    ShiroSession shiroSession = transactor.retrieve(
        pm -> pm.getObjectById(ShiroSession.class, shiroSessionId));
    Session session = SESSION_CONVERTER.convert(shiroSession);
    // TODO(codefx): Reassign sessionId since SerDer process does not preserver sessionId.
    assignSessionId(session, sessionId);
    return session;
  }

  @Override
  protected void doUpdate(Session session) {
    ShiroSession updatedSession = SESSION_CONVERTER.reverse().convert(session);
    transactor.run(pm -> {
      ShiroSession storeSession =
          pm.getObjectById(ShiroSession.class, updatedSession.getSessionId());
      refresh(storeSession, updatedSession);
      pm.makePersistent(storeSession);
    });
  }

  @Override
  protected void doDelete(Session session) {
    ShiroSession updatedSession = SESSION_CONVERTER.reverse().convert(session);
    // TODO(codefx): Confirm expired is set.
    updatedSession.setExpired(true);
    if (!(session instanceof SimpleSession)) {
      updatedSession.setStopTimestamp(new Date(clock.millis()));
    }
    transactor.run(pm -> {
      ShiroSession storeSession =
          pm.getObjectById(ShiroSession.class, updatedSession.getSessionId());
      refresh(storeSession, updatedSession);
      pm.makePersistent(storeSession);
    });
  }

  private ShiroSession refresh(ShiroSession pcSession, ShiroSession updatedSession) {
    pcSession.setHost(updatedSession.getHost());
    pcSession.setLastAccessTimestamp(updatedSession.getLastAccessTimestamp());
    pcSession.setSerializedValue(updatedSession.getSerializedValue());
    pcSession.setStartTimestamp(updatedSession.getStartTimestamp());
    pcSession.setExpired(updatedSession.isExpired());
    pcSession.setStopTimestamp(updatedSession.getStopTimestamp());
    return pcSession;
  }
}
