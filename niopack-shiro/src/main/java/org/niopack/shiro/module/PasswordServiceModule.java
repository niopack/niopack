package org.niopack.shiro.module;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import java.security.SecureRandom;
import org.apache.shiro.authc.credential.DefaultPasswordService;
import org.apache.shiro.authc.credential.PasswordMatcher;
import org.apache.shiro.authc.credential.PasswordService;
import org.apache.shiro.crypto.RandomNumberGenerator;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.DefaultHashService;
import org.apache.shiro.crypto.hash.HashService;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.apache.shiro.crypto.hash.format.DefaultHashFormatFactory;
import org.apache.shiro.crypto.hash.format.Shiro1CryptFormat;
import org.apache.shiro.util.ByteSource;

public class PasswordServiceModule extends AbstractModule {

  // TODO(codefx): Switch to flags
  private static final String HASH_ALGORITHM_NAME = Sha256Hash.ALGORITHM_NAME;
  // TODO(codefx): Increase # of iterations to at leaset 100_000 in prod.
  private static final int HASH_ITERATIONS = 1_000;

  @Override
  protected void configure() {
  }

  @Provides
  private RandomNumberGenerator provideRandomNumberGenerator(SecureRandom random) {
    SecureRandomNumberGenerator randomNumberGenerator = new SecureRandomNumberGenerator();
    randomNumberGenerator.setDefaultNextBytesSize(256); // same length as hash alg: SHA-256
    randomNumberGenerator.setSecureRandom(random);
    return randomNumberGenerator;
  }

  @Provides
  private HashService provideHashService(RandomNumberGenerator randomNumberGenerator) {
    DefaultHashService hashService = new DefaultHashService();
    hashService.setGeneratePublicSalt(true);
    hashService.setHashIterations(HASH_ITERATIONS);
    hashService.setHashAlgorithmName(HASH_ALGORITHM_NAME);
    hashService.setPrivateSalt(ByteSource.Util.bytes("MyVerySecretSalt"));
    hashService.setRandomNumberGenerator(randomNumberGenerator);
    return hashService;
  }

  @Provides
  private PasswordService providePasswordService(HashService hashService) {
    DefaultPasswordService passwordService = new DefaultPasswordService();
    // Shiro1CryptFormat is used to store password hash and salt together with alg name
    passwordService.setHashFormat(new Shiro1CryptFormat());
    passwordService.setHashFormatFactory(new DefaultHashFormatFactory());
    passwordService.setHashService(hashService);
    return passwordService;
  }

  @Provides
  private PasswordMatcher providePasswordMatcher(PasswordService passwordService) {
    PasswordMatcher passwordMatcher = new PasswordMatcher();
    passwordMatcher.setPasswordService(passwordService);
    return passwordMatcher;
  }
}
