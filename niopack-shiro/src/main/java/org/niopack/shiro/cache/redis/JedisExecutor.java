package org.niopack.shiro.cache.redis;

import com.google.common.base.Throwables;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.exceptions.JedisConnectionException;

final class JedisExecutor {

  private final JedisPool pool;

  JedisExecutor(String connectionUrl) {
    this.pool = new JedisPool(new JedisPoolConfig(), connectionUrl);
    // TODO(codefx): support timeout
  }

  <R> R execute(Callable<R> callable) {
    Jedis client = pool.getResource();
    try {
      return callable.call(client);
    } catch (JedisConnectionException e) {
      // returnBrokenResource when the state of the object is unrecoverable
      if (client != null) {
        pool.returnBrokenResource(client);
        client = null;
      }
      throw Throwables.propagate(e);
    } catch (Exception e) {
      throw Throwables.propagate(e);
    } finally {
      if (client != null) {
        pool.returnResource(client);
      }
    }
  }

  void destroy() {
    pool.destroy();
  }

  static interface Callable<R> {
    R call(Jedis client) throws Exception;
  }
}
