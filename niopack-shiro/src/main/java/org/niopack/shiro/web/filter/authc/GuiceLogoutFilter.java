package org.niopack.shiro.web.filter.authc;

import javax.inject.Inject;
import javax.inject.Named;
import org.apache.shiro.web.filter.authc.LogoutFilter;

public class GuiceLogoutFilter extends LogoutFilter {

  @Inject @Override
  public void setRedirectUrl(@Named("logout.redirectUrl") String redirectUrl) {
    super.setRedirectUrl(redirectUrl);
  }
}
