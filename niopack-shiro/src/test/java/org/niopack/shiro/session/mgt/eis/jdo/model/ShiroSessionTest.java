package org.niopack.shiro.session.mgt.eis.jdo.model;

import com.google.common.testing.EqualsTester;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class ShiroSessionTest {

  @Test
  public void testEquality() {
    ShiroSession s0a = new ShiroSession();
    ShiroSession s0b = new ShiroSession();

    ShiroSession s1a = new ShiroSession();
    s1a.setSessionId("sid_1");
    s1a.setHost("localhost");

    ShiroSession s1b = new ShiroSession();
    s1b.setSessionId("sid_1");
    s1b.setHost("b3");

    ShiroSession s2 = new ShiroSession();
    s2.setSessionId("sid_2");

    new EqualsTester()
        .addEqualityGroup(s0a, s0b)
        .addEqualityGroup(s1a, s1b)
        .addEqualityGroup(s2)
        .testEquals();
  }
}
