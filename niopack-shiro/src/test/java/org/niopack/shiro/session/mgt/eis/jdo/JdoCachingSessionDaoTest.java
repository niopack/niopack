package org.niopack.shiro.session.mgt.eis.jdo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.name.Names;
import java.io.Serializable;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Date;
import javax.inject.Inject;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.SimpleSession;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.niopack.db.jdo.Transactor;
import org.niopack.db.testing.MockDatabase;
import org.niopack.shiro.module.PasswordServiceModule;
import org.niopack.shiro.session.mgt.eis.jdo.model.ShiroSession;

@RunWith(JUnit4.class)
public class JdoCachingSessionDaoTest {

  private static final SessionConverter SESSION_CONVERTER = new SessionConverter();
  private static final SessionIdConverter ID_CONVERTER = new SessionIdConverter();
  private static final ShiroSessionEquivalence EQIVALENCE_CHECKER = new ShiroSessionEquivalence();

  private static final Serializable SESSION_ID = "SID";
  private static final Clock start = Clock.fixed(Instant.now(), ZoneId.systemDefault());
  private static final Clock update = Clock.tick(start, Duration.ofSeconds(100));
  private static final Clock delete = Clock.tick(update, Duration.ofSeconds(100));

  @Rule public MockDatabase testDb = new MockDatabase();
  @Rule public ExpectedException thrown = ExpectedException.none();

  @Inject private JdoCachingSessionDao dao;

  @Before
  public void setUp() {
    Injector injector = Guice.createInjector(new PasswordServiceModule(), new AbstractModule() {
      @Override protected void configure() {
        bind(Transactor.class).toInstance(testDb.getTransactor());
        bind(Clock.class).toInstance(Clock.systemDefaultZone());
        bind(JdoCachingSessionDao.class);
        bindConstant().annotatedWith(Names.named("shiro.permissionsLookupEnabled")).to(true);
      }
    });
    injector.injectMembers(this);
  }

  @Test
  public void shouldCeateSession() throws Exception {
    SimpleSession session = new SimpleSession();
    session.setExpired(false);
    session.setHost("request_host");
    session.setLastAccessTime(new Date(start.millis()));
    session.setTimeout(100);
    session.setStartTimestamp(new Date(start.millis()));
    Serializable sessionId = dao.doCreate(session);
    ShiroSession shiroSession = SESSION_CONVERTER.reverse().convert(session);

    String shiroSessionId = ID_CONVERTER.reverse().convert(sessionId);
    ShiroSession storedSession =
        testDb.getTransactor().retrieve(pm -> pm.getObjectById(ShiroSession.class, shiroSessionId));
    assertTrue(EQIVALENCE_CHECKER.equivalent(shiroSession, storedSession));
  }

  @Test
  public void shouldReadSession() throws Exception {
    SimpleSession session = new SimpleSession();
    session.setId(SESSION_ID);
    session.setExpired(false);
    session.setHost("request_host");
    session.setLastAccessTime(new Date(start.millis()));
    session.setTimeout(100);
    session.setStartTimestamp(new Date(start.millis()));
    testDb.getTransactor().run(
        pm -> pm.makePersistent(SESSION_CONVERTER.reverse().convert(session)));

    Session storedSession = dao.doReadSession(session.getId());
    assertEquals(session, (SimpleSession) storedSession);
  }

  @Test
  public void shouldUpdateSession() throws Exception {
    SimpleSession session = new SimpleSession();
    session.setId(SESSION_ID);
    session.setExpired(false);
    session.setHost("request_host");
    session.setLastAccessTime(new Date(start.millis()));
    session.setTimeout(100);
    session.setStartTimestamp(new Date(start.millis()));
    testDb.getTransactor().run(
        pm -> pm.makePersistent(SESSION_CONVERTER.reverse().convert(session)));

    session.setLastAccessTime(new Date(update.millis()));
    dao.doUpdate(session);

    String shiroSessionId = ID_CONVERTER.reverse().convert(SESSION_ID);
    ShiroSession storedSession =
        testDb.getTransactor().retrieve(pm -> pm.getObjectById(ShiroSession.class, shiroSessionId));
    ShiroSession expectedSession = SESSION_CONVERTER.reverse().convert(session);
    assertTrue(EQIVALENCE_CHECKER.equivalent(expectedSession, storedSession));
  }

  @Test
  public void shouldDeleteSession() throws Exception {
    SimpleSession session = new SimpleSession();
    session.setId(SESSION_ID);
    session.setExpired(false);
    session.setHost("request_host");
    session.setLastAccessTime(new Date(start.millis()));
    session.setTimeout(100);
    session.setStartTimestamp(new Date(start.millis()));
    testDb.getTransactor().run(
        pm -> pm.makePersistent(SESSION_CONVERTER.reverse().convert(session)));

    session.setExpired(true);
    session.setStopTimestamp(new Date(delete.millis()));
    dao.doDelete(session);

    String shiroSessionId = ID_CONVERTER.reverse().convert(SESSION_ID);
    ShiroSession storedSession =
        testDb.getTransactor().retrieve(pm -> pm.getObjectById(ShiroSession.class, shiroSessionId));
    ShiroSession expectedSession = SESSION_CONVERTER.reverse().convert(session);
    assertTrue(EQIVALENCE_CHECKER.equivalent(expectedSession, storedSession));
  }

  @Test
  public void shouldGetActiveSessions() throws Exception {
    dao.getActiveSessions();
  }
}
