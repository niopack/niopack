package org.niopack.shiro.session.mgt.eis.jdo;

import com.google.common.base.Equivalence;
import java.util.Arrays;
import java.util.Objects;
import org.niopack.shiro.session.mgt.eis.jdo.model.ShiroSession;

public class ShiroSessionEquivalence extends Equivalence<ShiroSession> {

  @Override
  protected boolean doEquivalent(ShiroSession a, ShiroSession b) {
    return Objects.equals(a.getSessionId(), b.getSessionId())
        && Objects.equals(a.getStartTimestamp(), b.getStartTimestamp())
        && Objects.equals(a.getStopTimestamp(), b.getStopTimestamp())
        && Objects.equals(a.getLastAccessTimestamp(), b.getLastAccessTimestamp())
        && Objects.equals(a.getTimeout(), b.getTimeout())
        && Objects.equals(a.isExpired(), b.isExpired())
        && Objects.equals(a.getHost(), b.getHost())
        && Arrays.equals(a.getSerializedValue(), b.getSerializedValue());
  }

  @Override
  protected int doHash(ShiroSession shiroSession) {
    return shiroSession.hashCode();
  }
}
