package org.niopack.demo.bookstore.api.resource.book;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.inject.Inject;
import org.niopack.demo.bookstore.api.definition.book.BookService;
import org.niopack.demo.bookstore.api.definition.book.CreateMethod;
import org.niopack.demo.bookstore.api.definition.book.GetMethod;
import org.niopack.demo.bookstore.api.definition.book.ListMethod;

public class BookServiceImpl implements BookService {

  private final CreateController create;
  private final ListController list;
  private final GetController get;

  @Inject
  public BookServiceImpl(CreateController create, ListController list, GetController get) {
    this.create = checkNotNull(create, "create");
    this.list = checkNotNull(list, "list");
    this.get = checkNotNull(get, "get");
  }

  @Override
  public CreateMethod.Response create(CreateMethod.Request request) {
    return create.execute(request);
  }

  @Override
  public ListMethod.Response list(ListMethod.Request request) {
    return list.execute(request);
  }

  @Override
  public GetMethod.Response get(GetMethod.Request request) {
    return get.execute(request);
  }
}
