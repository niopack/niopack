package org.niopack.demo.bookstore.api.resource.book;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.niopack.resteasy.errors.ClientErrorChecks.checkParameter;
import static org.niopack.resteasy.errors.ErrorCode.code;

import java.time.Clock;
import javax.inject.Inject;
import org.niopack.demo.bookstore.api.definition.book.CreateMethod.Request;
import org.niopack.demo.bookstore.api.definition.book.CreateMethod.Response;
import org.niopack.demo.bookstore.common.BookConverter;
import org.niopack.demo.bookstore.common.BookStore;
import org.niopack.demo.bookstore.model.store.Book;
import org.niopack.resteasy.errors.ErrorCode;

public class CreateController {

  private static final BookConverter BOOK_CONVERTER = new BookConverter();
  private final BookStore store;
  private final Clock clock;

  @Inject
  public CreateController(BookStore store, Clock clock) {
    this.store = checkNotNull(store, "store");
    this.clock = checkNotNull(clock, "clock");
  }

  Response execute(Request request) {
    checkParameter(request, code("request"));

    Book book = new Book();
    book.setId(clock.instant().toEpochMilli()); // use current timestamp as id
    book.setName(request.getName());
    store.getBooks().put(book.getId(), book);

    Response response = new Response();
    response.setBook(BOOK_CONVERTER.convert(book));
    return response;
  }
}
