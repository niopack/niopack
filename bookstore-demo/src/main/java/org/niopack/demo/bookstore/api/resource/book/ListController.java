package org.niopack.demo.bookstore.api.resource.book;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.niopack.resteasy.errors.ClientErrorChecks.checkParameter;
import static org.niopack.resteasy.errors.ErrorCode.code;

import java.util.List;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.niopack.demo.bookstore.api.definition.book.ListMethod.Request;
import org.niopack.demo.bookstore.api.definition.book.ListMethod.Response;
import org.niopack.demo.bookstore.common.BookConverter;
import org.niopack.demo.bookstore.common.BookStore;
import org.niopack.demo.bookstore.model.service.Book;

public class ListController {

  private static final BookConverter BOOK_CONVERTER = new BookConverter();
  private final BookStore store;

  @Inject
  public ListController(BookStore store) {
    this.store = checkNotNull(store, "store");
  }

  Response execute(Request request) {
    checkParameter(request, code("request"));

    Response response = new Response();
    List<Book> books = store.getBooks().values()
        .stream()
        .map(BOOK_CONVERTER)
        .collect(Collectors.toList());
    response.setBooks(books);
    return response;
  }
}
