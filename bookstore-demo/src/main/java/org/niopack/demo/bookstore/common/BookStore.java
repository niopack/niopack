package org.niopack.demo.bookstore.common;

import java.util.HashMap;
import java.util.Map;
import org.niopack.demo.bookstore.model.store.Book;

public class BookStore {

  private final Map<Long, Book> books = new HashMap<>();

  public Map<Long, Book> getBooks() {
    return books;
  }
}
