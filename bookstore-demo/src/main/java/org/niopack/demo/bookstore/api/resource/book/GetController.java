package org.niopack.demo.bookstore.api.resource.book;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.niopack.resteasy.errors.ClientErrorChecks.checkFound;
import static org.niopack.resteasy.errors.ClientErrorChecks.checkParameter;
import static org.niopack.resteasy.errors.ErrorCode.code;

import javax.inject.Inject;
import org.niopack.demo.bookstore.api.definition.book.GetMethod.Request;
import org.niopack.demo.bookstore.api.definition.book.GetMethod.Response;
import org.niopack.demo.bookstore.common.BookConverter;
import org.niopack.demo.bookstore.common.BookStore;
import org.niopack.demo.bookstore.model.store.Book;
import org.niopack.resteasy.errors.ErrorCode;

public class GetController {

  private static final BookConverter BOOK_CONVERTER = new BookConverter();
  private final BookStore store;

  @Inject
  public GetController(BookStore store) {
    this.store = checkNotNull(store, "store");
  }

  Response execute(Request request) {
    checkParameter(request, code("request"));

    Book book = store.getBooks().get(request.getId());
    checkFound(book, code("BOOK_NOT_FOUND"), "Invalid book id: %s.", request.getId());

    Response response = new Response();
    response.setBook(BOOK_CONVERTER.convert(book));
    return response;
  }
}
