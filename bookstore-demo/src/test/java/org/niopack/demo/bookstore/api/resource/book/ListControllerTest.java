package org.niopack.demo.bookstore.api.resource.book;

import static com.google.common.truth.Truth.assertThat;

import com.google.common.testing.ArbitraryInstances;
import com.google.common.testing.NullPointerTester;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.niopack.demo.bookstore.api.definition.book.ListMethod.Request;
import org.niopack.demo.bookstore.api.definition.book.ListMethod.Response;
import org.niopack.demo.bookstore.common.BookConverter;
import org.niopack.demo.bookstore.common.BookStore;
import org.niopack.demo.bookstore.model.store.Book;

@RunWith(JUnit4.class)
public class ListControllerTest {

  @Test
  public void testAllPublicConstructors() {
    new NullPointerTester().testAllPublicConstructors(ListController.class);
  }

  @Test
  public void testAllPublicInstanceMethods() {
    ListController controller = new ListController(new BookStore());
    new NullPointerTester().testAllPublicInstanceMethods(controller);
  }

  @Test
  public void shouldReturnEmptyResponse() {
    BookStore store = new BookStore();
    ListController controller = new ListController(store);

    Response response = controller.execute(new Request());
    assertThat(response.getBooks()).isEmpty();
  }

  @Test
  public void shouldReturnAllBooks() {
    BookStore store = new BookStore();

    Book b1 = new Book();
    b1.setId(1L);
    b1.setName("b1");
    store.getBooks().put(b1.getId(), b1);

    Book b2 = ArbitraryInstances.get(Book.class);
    b2.setId(2L);
    b2.setName("b2");
    store.getBooks().put(b2.getId(), b2);

    ListController controller = new ListController(store);
    Response response = controller.execute(new Request());
    BookConverter converter = new BookConverter();
    assertThat(response.getBooks()).containsExactly(converter.convert(b1), converter.convert(b2));
  }
}
