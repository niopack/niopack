package org.niopack.demo.bookstore.api.resource.book;

import static com.google.common.truth.Truth.assertThat;

import java.net.URISyntaxException;
import java.time.Clock;
import java.time.Instant;
import java.time.ZoneOffset;
import javax.servlet.http.HttpServletResponse;
import org.jboss.resteasy.mock.MockHttpRequest;
import org.jboss.resteasy.mock.MockHttpResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.niopack.demo.bookstore.api.definition.book.BookService;
import org.niopack.demo.bookstore.common.BookStore;
import org.niopack.demo.bookstore.model.store.Book;
import org.niopack.guice.ResteasyServletModule;
import org.niopack.testing.MockServer;

@RunWith(JUnit4.class)
public class BookServiceImplTest {

  private static final long TIMESTAMP = 1000L;
  private static final ResteasyServletModule RESOURCE_MODULE = new ResteasyServletModule() {
    @Override
    protected void configureResources() {
      bind(BookStore.class).toInstance(new BookStore());
      bind(Clock.class).toInstance(Clock.fixed(Instant.ofEpochMilli(TIMESTAMP), ZoneOffset.UTC));
      // Resources
      bind(BookService.class).to(BookServiceImpl.class);
    }
  };

  @Test
  public void testCreate() throws URISyntaxException {
    MockServer server = MockServer.create(RESOURCE_MODULE);

    MockHttpRequest request = MockHttpRequest.post("/api/books");
    request.addFormHeader("name", "War & Peace");
    MockHttpResponse response = new MockHttpResponse();
    server.invoke(request, response);
    assertThat(response.getStatus()).isEqualTo(HttpServletResponse.SC_OK);
    assertThat(response.getContentAsString()).isEqualTo(
        "{\"book\":{\"id\":1000,\"name\":\"War & Peace\"}}");
  }

  @Test
  public void testList() throws URISyntaxException {
    MockServer server = MockServer.create(RESOURCE_MODULE);

    MockHttpRequest request = MockHttpRequest.get("/api/books");
    MockHttpResponse response = new MockHttpResponse();
    server.invoke(request, response);
    assertThat(response.getStatus()).isEqualTo(HttpServletResponse.SC_OK);
    assertThat(response.getContentAsString()).isEqualTo("{\"books\":[]}");
  }

  @Test
  public void testGet() throws URISyntaxException {
    MockServer server = MockServer.create(RESOURCE_MODULE);
    
    Book book = new Book();
    book.setId(TIMESTAMP);
    book.setName("War & Peace");
    BookStore store = server.getInjector().getInstance(BookStore.class);
    store.getBooks().put(TIMESTAMP, book);

    MockHttpRequest request = MockHttpRequest.get("/api/books/" + TIMESTAMP);
    MockHttpResponse response = new MockHttpResponse();
    server.invoke(request, response);
    assertThat(response.getStatus()).isEqualTo(HttpServletResponse.SC_OK);
    assertThat(response.getContentAsString())
        .isEqualTo("{\"book\":{\"id\":1000,\"name\":\"War & Peace\"}}");
  }
}
