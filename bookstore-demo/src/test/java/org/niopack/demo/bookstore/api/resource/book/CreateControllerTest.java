package org.niopack.demo.bookstore.api.resource.book;

import static com.google.common.truth.Truth.assertThat;

import com.google.common.testing.NullPointerTester;
import java.time.Clock;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.niopack.demo.bookstore.api.definition.book.CreateMethod.Request;
import org.niopack.demo.bookstore.api.definition.book.CreateMethod.Response;
import org.niopack.demo.bookstore.common.BookConverter;
import org.niopack.demo.bookstore.common.BookStore;
import org.niopack.demo.bookstore.model.service.Book;

@RunWith(JUnit4.class)
public class CreateControllerTest {

  @Test
  public void testAllPublicConstructors() {
    new NullPointerTester()
        .setDefault(Clock.class, Clock.systemUTC())
        .testAllPublicConstructors(CreateController.class);
  }

  @Test
  public void testAllPublicInstanceMethods() {
    CreateController controller = new CreateController(new BookStore(), Clock.systemUTC());
    new NullPointerTester()
        .setDefault(Clock.class, Clock.systemUTC())
        .testAllPublicInstanceMethods(controller);
  }

  @Test
  public void shouldCreateNewBook() {
    BookStore store = new BookStore();
    CreateController controller = new CreateController(store, Clock.systemUTC());

    Request request = new Request();
    request.setName("War & Peace");
    Response response = controller.execute(request);
    Book book = response.getBook();
    assertThat(book).isEqualTo(new BookConverter().convert(store.getBooks().get(book.getId())));
  }
}
