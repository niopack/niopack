package org.niopack.demo.bookstore.model.store;

import static com.google.common.truth.Truth.assertThat;

import com.google.common.testing.ArbitraryInstances;
import com.google.common.testing.EqualsTester;
import com.google.common.testing.NullPointerTester;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class BookTest {

  @Test
  public void testEquality() {
    Book b11 = new Book();
    b11.setId(1L);
    Book b12 = new Book();
    b12.setId(1L);

    Book b2 = new Book();
    b2.setId(2L);

    new EqualsTester()
        .addEqualityGroup(b11, b12)
        .addEqualityGroup(b2)
        .addEqualityGroup(new Book())
        .testEquals();
  }

  @Test
  public void testAllPublicConstructors() {
    new NullPointerTester().testAllPublicConstructors(Book.class);
  }

  @Test
  public void testAllPublicInstanceMethods() {
    Book book = ArbitraryInstances.get(Book.class);
    new NullPointerTester().testAllPublicInstanceMethods(book);
  }

  @Test
  public void testProperties() {
    long bookId = 1L;
    String bookName = "War & Peace";

    Book book = new Book();
    book.setId(bookId);
    book.setName(bookName);

    assertThat(book.getId()).isEqualTo(bookId);
    assertThat(book.getName()).isEqualTo(bookName);
  }
}
