package org.niopack.demo.bookstore.api.resource.book;

import static com.google.common.truth.Truth.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;

import com.google.common.testing.ArbitraryInstances;
import com.google.common.testing.NullPointerTester;
import javax.ws.rs.core.Response.Status;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.niopack.demo.bookstore.api.definition.book.GetMethod.Request;
import org.niopack.demo.bookstore.api.definition.book.GetMethod.Response;
import org.niopack.demo.bookstore.common.BookConverter;
import org.niopack.demo.bookstore.common.BookStore;
import org.niopack.demo.bookstore.model.store.Book;
import org.niopack.resteasy.errors.ApplicationErrorException;

@RunWith(JUnit4.class)
public class GetControllerTest {

  @Rule public ExpectedException thrown= ExpectedException.none();

  @Test
  public void testAllPublicConstructors() {
    new NullPointerTester().testAllPublicConstructors(GetController.class);
  }

  @Test
  public void testAllPublicInstanceMethods() {
    GetController controller = new GetController(new BookStore());
    new NullPointerTester().testAllPublicInstanceMethods(controller);
  }

  @Test
  public void shouldFailOnAbsentBook() {
    BookStore store = new BookStore();
    GetController controller = new GetController(store);

    Request request = new Request();
    request.setId(1L);
    thrown.expect(ApplicationErrorException.class);
    thrown.expect(hasProperty("status", is(Status.NOT_FOUND)));
    controller.execute(request);
  }

  @Test
  public void shouldGetBook() {
    BookStore store = new BookStore();

    Book b1 = new Book();
    b1.setId(1L);
    b1.setName("b1");
    store.getBooks().put(b1.getId(), b1);

    Book b2 = ArbitraryInstances.get(Book.class);
    b2.setId(2L);
    b2.setName("b2");
    store.getBooks().put(b2.getId(), b2);

    Request request = new Request();
    request.setId(1L);
    GetController controller = new GetController(store);
    Response response = controller.execute(request);
    BookConverter converter = new BookConverter();
    assertThat(response.getBook()).isEqualTo(converter.convert(store.getBooks().get(1L)));
  }
}
