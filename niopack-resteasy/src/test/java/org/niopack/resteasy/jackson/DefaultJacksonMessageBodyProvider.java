package org.niopack.resteasy.jackson;

import javax.validation.Validation;
import javax.ws.rs.ext.Provider;
import org.niopack.jackson.ObjectMappers;

@Provider
public class DefaultJacksonMessageBodyProvider extends JacksonMessageBodyProvider {
  public DefaultJacksonMessageBodyProvider() {
    super(ObjectMappers.forJson(),
        Validation.buildDefaultValidatorFactory().getValidator());
  }
}
