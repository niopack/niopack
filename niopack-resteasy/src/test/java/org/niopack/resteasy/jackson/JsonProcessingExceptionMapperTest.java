package org.niopack.resteasy.jackson;

import org.junit.Test;
import org.niopack.logging.LoggingFactory;

public class JsonProcessingExceptionMapperTest {
  static {
    LoggingFactory.bootstrap();
  }
  
  @Test
  public void empty() {
    // TODO(codefx): Fix test
  }

  /*
  @Override
  protected AppDescriptor configure() {
    return new WebAppDescriptor.Builder("org.niopack.resteasy.jackson").build();
  }

  @Test
  public void returnsA500ForNonDeserializableRepresentationClasses() throws Exception {
    try {
      resource().path("/json/broken")
            .type(MediaType.APPLICATION_JSON)
            .post(new BrokenRepresentation(ImmutableList.of("whee")));
      failBecauseExceptionWasNotThrown(UniformInterfaceException.class);
    } catch (UniformInterfaceException e) {
      assertThat(e.getResponse().getStatus())
          .isEqualTo(500);
    }
  }

  @Test
  public void returnsA400ForNonDeserializableRequestEntities() throws Exception {
    try {
      resource().path("/json/ok")
            .type(MediaType.APPLICATION_JSON)
            .post("{\"bork\":100}");
      failBecauseExceptionWasNotThrown(UniformInterfaceException.class);
    } catch (UniformInterfaceException e) {
      assertThat(e.getResponse().getStatus())
          .isEqualTo(400);
    }
  }
  */
}
