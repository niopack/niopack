package org.niopack.resteasy.validation;

import javax.validation.Validation;
import javax.ws.rs.ext.Provider;
import org.niopack.jackson.ObjectMappers;
import org.niopack.resteasy.jackson.JacksonMessageBodyProvider;

@Provider
public class DefaultJacksonMessageBodyProvider extends JacksonMessageBodyProvider {
  public DefaultJacksonMessageBodyProvider() {
    super(ObjectMappers.forJson(), Validation.buildDefaultValidatorFactory().getValidator());
  }
}

