package org.niopack.resteasy.validation;

import org.junit.Test;
import org.niopack.logging.LoggingFactory;

public class ConstraintViolationExceptionMapperTest {
  static {
    LoggingFactory.bootstrap();
  }
  
  @Test
  public void empty() {
    // TODO(codefx): Fix test
  }

  /*
  @Override
  protected AppDescriptor configure() {
    return new WebAppDescriptor.Builder("org.niopack.resteasy.validation").build();
  }

  @Test
  public void returnsAnErrorMessage() throws Exception {
    assumeThat(Locale.getDefault().getLanguage(), is("en"));

    try {
      resource().path("/valid/").type(MediaType.APPLICATION_JSON).post("{}");
      failBecauseExceptionWasNotThrown(UniformInterfaceException.class);
    } catch (UniformInterfaceException e) {
      assertThat(e.getResponse().getStatus())
          .isEqualTo(422);

      assertThat(e.getResponse().getEntity(String.class))
          .isEqualTo("{\"errors\":[\"name may not be empty (was null)\"]}");
    }
  }
  */
}
