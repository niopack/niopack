package org.niopack.resteasy.errors;

import org.junit.Test;
import org.niopack.logging.LoggingFactory;

public class LoggingExceptionMapperTest {
  static {
    LoggingFactory.bootstrap();
  }
  
  @Test
  public void empty() {
    // TODO(codefx): Fix test
  }

  /*
  @Override
  protected AppDescriptor configure() {
    return new WebAppDescriptor.Builder("org.niopack.resteasy.errors").build();
  }

  @Test
  public void returnsAnErrorMessage() throws Exception {
    try {
      resource().path("/exception/").type(MediaType.APPLICATION_JSON).get(String.class);
      failBecauseExceptionWasNotThrown(UniformInterfaceException.class);
    } catch (UniformInterfaceException e) {
      assertThat(e.getResponse().getStatus())
          .isEqualTo(500);

      assertThat(e.getResponse().getEntity(String.class))
          .startsWith("{\"message\":\"There was an error processing your request. It has been logged (ID ");
    }
  }
  */
}
