package org.niopack.resteasy.errors;

import java.io.IOException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/exception/")
@Produces(MediaType.APPLICATION_JSON)
public class ExceptionResource {
  @GET
  public String show() throws IOException {
    throw new IOException("WHAT");
  }
}
