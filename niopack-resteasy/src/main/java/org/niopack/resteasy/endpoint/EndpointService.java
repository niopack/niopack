package org.niopack.resteasy.endpoint;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import org.jboss.resteasy.core.Dispatcher;
import org.jboss.resteasy.core.ResourceInvoker;
import org.jboss.resteasy.core.ResourceMethodInvoker;
import org.jboss.resteasy.core.ResourceMethodRegistry;

/**
 * A resource that displays a listSources of available endpoints.
 */
@Path("/api/endpoints")
public final class EndpointService {

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public List<ResourceDescription> getEndpoints(@Context Dispatcher dispatcher) {
    ResourceMethodRegistry registry = (ResourceMethodRegistry) dispatcher.getRegistry();
    return ResourceDescription.fromBoundResourceInvokers(registry.getBounded().entrySet());
  }

  static final class ResourceDescription {
    private String basePath;
    private List<MethodDescription> methods;

    public ResourceDescription(String basePath) {
      this.basePath = basePath;
      this.methods = Lists.newArrayList();
    }

    public String getBasePath() {
      return basePath;
    }

    public List<MethodDescription> getMethods() {
      return methods;
    }

    public void addMethod(String path, ResourceMethodInvoker method) {
      String produces = getFirstOrEmpty(method.getProduces());
      String consumes = getFirstOrEmpty(method.getConsumes());
      for (String verb : method.getHttpMethods()) {
        methods.add(new MethodDescription(verb, path, produces, consumes));
      }
    }

    private String getFirstOrEmpty(MediaType[] mediaTypes) {
      if (mediaTypes == null || mediaTypes.length == 0) {
        return "";
      }
      return mediaTypes[0].toString();
    }

    public static List<ResourceDescription> fromBoundResourceInvokers(
        Set<Map.Entry<String, List<ResourceInvoker>>> bound) {
      Map<String, ResourceDescription> descriptions = Maps.newHashMap();

      for (Map.Entry<String, List<ResourceInvoker>> entry : bound) {
        ResourceMethodInvoker aMethod = (ResourceMethodInvoker) entry.getValue().get(0);
        String basePath = aMethod.getMethod().getDeclaringClass().getAnnotation(Path.class).value();

        if (!descriptions.containsKey(basePath)) {
          descriptions.put(basePath, new ResourceDescription(basePath));
        }

        for (ResourceInvoker invoker : entry.getValue()) {
          ResourceMethodInvoker method = (ResourceMethodInvoker) invoker;
          descriptions.get(basePath).addMethod(basePath, method);
        }
      }
      return ImmutableList.copyOf(descriptions.values());
    }
  }

  static final class MethodDescription {
    private String method;
    private String fullPath;
    private String produces;
    private String consumes;

    public MethodDescription(String method, String fullPath, String produces, String consumes) {
      this.method = method;
      this.fullPath = fullPath;
      this.produces = produces;
      this.consumes = consumes;
    }

    public String getMethod() {
      return method;
    }

    public String getFullPath() {
      return fullPath;
    }

    public String getProduces() {
      return produces;
    }

    public String getConsumes() {
      return consumes;
    }
  }
}
