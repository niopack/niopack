package org.niopack.resteasy.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;
import java.util.Set;
import javax.validation.ConstraintViolation;
import org.niopack.validation.ConstraintViolations;

public class ValidationErrorMessage {
  private final ImmutableList<String> errors;

  public ValidationErrorMessage(Set<ConstraintViolation<?>> errors) {
    this.errors = ConstraintViolations.formatUntyped(errors);
  }

  @JsonProperty
  public ImmutableList<String> getErrors() {
    return errors;
  }
}
