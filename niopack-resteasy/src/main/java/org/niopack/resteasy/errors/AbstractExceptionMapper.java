package org.niopack.resteasy.errors;

import com.google.common.base.Throwables;
import java.util.concurrent.ThreadLocalRandom;
import javax.ws.rs.ext.ExceptionMapper;
import org.niopack.logging.ContextualLogger;
import org.niopack.nio.core.Error;
import org.slf4j.Logger;

public abstract class AbstractExceptionMapper<T extends Throwable> implements ExceptionMapper<T> {
  protected static final Logger logger = ContextualLogger.create();

  protected String createLogId() {
    return String.format("%016x", ThreadLocalRandom.current().nextLong());
  }

  protected void logException(String logId, T throwable) {
    logger.error(formatLogMessage(logId, throwable), throwable);
  }

  protected String formatLogMessage(String logId, T throwable) {
    return String.format("Error handling a request: %s", logId);
  }

  protected Error createError(String logId, T throwable) {
    Error error = new Error();
    error.setLogId(logId);
    if (throwable.getMessage() != null) {
      error.setDebugMessage(throwable.getMessage());
    } else {
      error.setDebugMessage(throwable.getClass().getName());
    }
    error.setStackTrace(Throwables.getStackTraceAsString(throwable));
    return error;
  }
}
