package org.niopack.resteasy.errors;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.ws.rs.core.Response.Status;
import org.niopack.nio.core.Error;

public final class ApplicationErrorException extends RuntimeException {

  private final Status status;
  private final Error error;

  public ApplicationErrorException(Status status, Error error) {
    this.status = checkNotNull(status, "status");
    this.error = checkNotNull(error, "error");
  }

  public Status getStatus() {
    return status;
  }

  public Error getError() {
    return error;
  }
}
