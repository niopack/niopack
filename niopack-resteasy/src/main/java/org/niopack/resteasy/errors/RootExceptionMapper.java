package org.niopack.resteasy.errors;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import org.niopack.nio.core.Error;

@Provider
public final class RootExceptionMapper extends AbstractExceptionMapper<Throwable> {

  @Override
  public Response toResponse(Throwable throwable) {
    String logId = createLogId();
    logException(logId, throwable);

    if (throwable instanceof ApplicationErrorException) {
      ApplicationErrorException e = (ApplicationErrorException) throwable;
      e.getError().setLogId(logId);
      return Response.status(e.getStatus())
          .type(MediaType.APPLICATION_JSON)
          .entity(e.getError())
          .build();
    } else if (throwable instanceof WebApplicationException) {
      Response response = ((WebApplicationException) throwable).getResponse();
      Object entity = response.getEntity();
      if (entity != null && entity instanceof Error) {
        ((Error) entity).setLogId(logId);
      }
      return response;
    }

    return Response.serverError()
        .type(MediaType.APPLICATION_JSON)
        .entity(createError(logId, throwable))
        .build();
  }
}
