package org.niopack.resteasy.errors;

import static com.google.common.base.Preconditions.checkNotNull;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static org.niopack.resteasy.errors.FluentErrorBuilder.doThrow;

import javax.annotation.Nullable;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;

public final class ClientErrorChecks {

  private ClientErrorChecks() {}

  /**
   * Ensures that an object reference passed as a request parameter is not null.
   *
   * @param reference an object reference
   * @return the non-null reference that was validated
   * @throws BadRequestException if {@code reference} is null
   */
  public static <T> T checkParameter(T reference) {
    return checkParameter(reference, BAD_REQUEST);
  }

  /**
   * Ensures that an object reference passed as a request parameter is not null.
   *
   * @param reference an object reference
   * @param errorCode an application specific error code
   * @return the non-null reference that was validated
   * @throws BadRequestException if {@code reference} is null
   */
  public static <T, E extends Enum<E>> T checkParameter(T reference, E errorCode) {
    return checkParameter(reference, errorCode.name());
  }

  /**
   * Ensures that an object reference passed as a request parameter is not null.
   *
   * @param reference an object reference
   * @param errorCode an application specific error code
   * @return the non-null reference that was validated
   * @throws BadRequestException if {@code reference} is null
   */
  public static <T> T checkParameter(T reference, ErrorCode errorCode) {
    return checkParameter(reference, errorCode.getValue());
  }

  private static <T> T checkParameter(T reference, String errorCode) {
    doThrow(BAD_REQUEST)
        .withErrorCode(checkNotNull(errorCode, "errorCode"))
        .unless(reference)
        .isNotNull();
    return reference;
  }

  /**
   * Ensures that an object reference passed as a request parameter is not null.
   *
   * @param reference an object reference
   * @param errorCode an application specific error code
   * @param errorMessage the exception message to use if the check fails; will be converted to a
   *     string using {@link String#valueOf(Object)}
   * @return the non-null reference that was validated
   * @throws BadRequestException if {@code reference} is null
   */
  public static <T, E extends Enum<E>> T checkParameter(T reference,
      E errorCode,
      @Nullable Object errorMessage) {
    return checkParameter(reference, errorCode.name(), errorMessage);
  }

  /**
   * Ensures that an object reference passed as a request parameter is not null.
   *
   * @param reference an object reference
   * @param errorCode an application specific error code
   * @param errorMessage the exception message to use if the check fails; will be converted to a
   *     string using {@link String#valueOf(Object)}
   * @return the non-null reference that was validated
   * @throws BadRequestException if {@code reference} is null
   */
  public static <T> T checkParameter(T reference,
      ErrorCode errorCode,
      @Nullable Object errorMessage) {
    return checkParameter(reference, errorCode.getValue(), errorMessage);
  }

  private static <T> T checkParameter(T reference,
      String errorCode,
      @Nullable Object errorMessage) {
    doThrow(BAD_REQUEST)
        .withErrorCode(checkNotNull(errorCode, "errorCode"))
        .withMessage(String.valueOf(errorMessage))
        .unless(reference)
        .isNotNull();
    return reference;
  }

  /**
   * Ensures that an object reference passed as a request parameter is not null.
   *
   * @param reference an object reference
   * @param errorCode an application specific error code
   * @param errorMessageTemplate a template for the exception message should the check fail. The
   *     message is formed by replacing each {@code %s} placeholder in the template with an
   *     argument. These are matched by position - the first {@code %s} gets {@code
   *     errorMessageArgs[0]}, etc.  Unmatched arguments will be appended to the formatted message
   *     in square braces. Unmatched placeholders will be left as-is.
   * @param errorMessageArgs the arguments to be substituted into the message template. Arguments
   *     are converted to strings using {@link String#valueOf(Object)}.
   * @return the non-null reference that was validated
   * @throws BadRequestException if {@code reference} is null
   */
  public static <T, E extends Enum<E>> T checkParameter(T reference,
      E errorCode,
      @Nullable String errorMessageTemplate,
      @Nullable Object... errorMessageArgs) {
    return checkParameter(reference, errorCode.name(), errorMessageTemplate, errorMessageArgs);
  }

  /**
   * Ensures that an object reference passed as a request parameter is not null.
   *
   * @param reference an object reference
   * @param errorCode an application specific error code
   * @param errorMessageTemplate a template for the exception message should the check fail. The
   *     message is formed by replacing each {@code %s} placeholder in the template with an
   *     argument. These are matched by position - the first {@code %s} gets {@code
   *     errorMessageArgs[0]}, etc.  Unmatched arguments will be appended to the formatted message
   *     in square braces. Unmatched placeholders will be left as-is.
   * @param errorMessageArgs the arguments to be substituted into the message template. Arguments
   *     are converted to strings using {@link String#valueOf(Object)}.
   * @return the non-null reference that was validated
   * @throws BadRequestException if {@code reference} is null
   */
  public static <T> T checkParameter(T reference,
      ErrorCode errorCode,
      @Nullable String errorMessageTemplate,
      @Nullable Object... errorMessageArgs) {
    return checkParameter(reference, errorCode.getValue(), errorMessageTemplate, errorMessageArgs);
  }

  private static <T> T checkParameter(T reference,
      String errorCode,
      @Nullable String errorMessageTemplate,
      @Nullable Object... errorMessageArgs) {
    doThrow(BAD_REQUEST)
        .withErrorCode(checkNotNull(errorCode, "errorCode"))
        .withMessage(String.format(errorMessageTemplate, errorMessageArgs))
        .unless(reference)
        .isNotNull();
    return reference;
  }

  /**
   * Ensures the truth of an expression involving one or more request parameters.
   *
   * @param expression a boolean expression
   * @throws BadRequestException if {@code expression} is false
   */
  public static void checkRequest(boolean expression) {
    checkRequest(expression, BAD_REQUEST);
  }

  /**
   * Ensures the truth of an expression involving one or more request parameters.
   *
   * @param expression a boolean expression
   * @param errorCode an application specific error code
   * @throws BadRequestException if {@code expression} is false
   */
  public static <E extends Enum<E>> void checkRequest(boolean expression, E errorCode) {
    checkRequest(expression, errorCode.name());
  }

  /**
   * Ensures the truth of an expression involving one or more request parameters.
   *
   * @param expression a boolean expression
   * @param errorCode an application specific error code
   * @throws BadRequestException if {@code expression} is false
   */
  public static void checkRequest(boolean expression, ErrorCode errorCode) {
    checkRequest(expression, errorCode.getValue());
  }

  private static void checkRequest(boolean expression, String errorCode) {
    doThrow(BAD_REQUEST)
        .withErrorCode(checkNotNull(errorCode, "errorCode"))
        .unless(expression)
        .isTrue();
  }

  /**
   * Ensures the truth of an expression involving one or more request parameters.
   *
   * @param expression a boolean expression
   * @param errorCode an application specific error code
   * @param errorMessage the exception message to use if the check fails; will be converted to a
   *     string using {@link String#valueOf(Object)}
   * @throws BadRequestException if {@code expression} is false
   */
  public static <E extends Enum<E>> void checkRequest(boolean expression,
      E errorCode,
      @Nullable Object errorMessage) {
    checkRequest(expression, errorCode.name(), errorMessage);
  }

  /**
   * Ensures the truth of an expression involving one or more request parameters.
   *
   * @param expression a boolean expression
   * @param errorCode an application specific error code
   * @param errorMessage the exception message to use if the check fails; will be converted to a
   *     string using {@link String#valueOf(Object)}
   * @throws BadRequestException if {@code expression} is false
   */
  public static void checkRequest(boolean expression,
      ErrorCode errorCode,
      @Nullable Object errorMessage) {
    checkRequest(expression, errorCode.getValue(), errorMessage);
  }

  private static void checkRequest(boolean expression,
      String errorCode,
      @Nullable Object errorMessage) {
    doThrow(BAD_REQUEST)
        .withErrorCode(checkNotNull(errorCode, "errorCode"))
        .withMessage(String.valueOf(errorMessage))
        .unless(expression)
        .isTrue();
  }

  /**
   * Ensures the truth of an expression involving one or more request parameters.
   *
   * @param expression a boolean expression
   * @param errorCode an application specific error code
   * @param errorMessageTemplate a template for the exception message should the check fail. The
   *     message is formed by replacing each {@code %s} placeholder in the template with an
   *     argument. These are matched by position - the first {@code %s} gets {@code
   *     errorMessageArgs[0]}, etc.  Unmatched arguments will be appended to the formatted message
   *     in square braces. Unmatched placeholders will be left as-is.
   * @param errorMessageArgs the arguments to be substituted into the message template. Arguments
   *     are converted to strings using {@link String#valueOf(Object)}.
   * @throws BadRequestException if {@code expression} is false
   * @throws NotFoundException if the check fails and either {@code errorMessageTemplate} or
   *     {@code errorMessageArgs} is null (don't let this happen)
   */
  public static <E extends Enum<E>> void checkRequest(boolean expression,
      E errorCode,
      @Nullable String errorMessageTemplate,
      @Nullable Object... errorMessageArgs) {
    checkRequest(expression, errorCode.name(), errorMessageTemplate, errorMessageArgs);
  }

  /**
   * Ensures the truth of an expression involving one or more request parameters.
   *
   * @param expression a boolean expression
   * @param errorCode an application specific error code
   * @param errorMessageTemplate a template for the exception message should the check fail. The
   *     message is formed by replacing each {@code %s} placeholder in the template with an
   *     argument. These are matched by position - the first {@code %s} gets {@code
   *     errorMessageArgs[0]}, etc.  Unmatched arguments will be appended to the formatted message
   *     in square braces. Unmatched placeholders will be left as-is.
   * @param errorMessageArgs the arguments to be substituted into the message template. Arguments
   *     are converted to strings using {@link String#valueOf(Object)}.
   * @throws BadRequestException if {@code expression} is false
   * @throws NotFoundException if the check fails and either {@code errorMessageTemplate} or
   *     {@code errorMessageArgs} is null (don't let this happen)
   */
  public static void checkRequest(boolean expression,
      ErrorCode errorCode,
      @Nullable String errorMessageTemplate,
      @Nullable Object... errorMessageArgs) {
    checkRequest(expression, errorCode.getValue(), errorMessageTemplate, errorMessageArgs);
  }

  private static void checkRequest(boolean expression,
      String errorCode,
      @Nullable String errorMessageTemplate,
      @Nullable Object... errorMessageArgs) {
    doThrow(BAD_REQUEST)
        .withErrorCode(checkNotNull(errorCode, "errorCode"))
        .withMessage(String.format(errorMessageTemplate, errorMessageArgs))
        .unless(expression)
        .isTrue();
  }

  /**
   * Ensures that an object reference passed as a parameter to the calling method is not null.
   *
   * @param reference an object reference
   * @return the non-null reference that was validated
   * @throws NotFoundException if {@code reference} is null
   */
  public static <T> T checkFound(T reference) {
    return checkFound(reference, NOT_FOUND);
  }

  /**
   * Ensures that an object reference passed as a parameter to the calling method is not null.
   *
   * @param reference an object reference
   * @param errorCode an application specific error code
   * @return the non-null reference that was validated
   * @throws NotFoundException if {@code reference} is null
   */
  public static <T, E extends Enum<E>> T checkFound(T reference, E errorCode) {
    return checkFound(reference, errorCode.name());
  }

  /**
   * Ensures that an object reference passed as a parameter to the calling method is not null.
   *
   * @param reference an object reference
   * @param errorCode an application specific error code
   * @return the non-null reference that was validated
   * @throws NotFoundException if {@code reference} is null
   */
  public static <T> T checkFound(T reference, ErrorCode errorCode) {
    return checkFound(reference, errorCode.getValue());
  }

  private static <T> T checkFound(T reference, String errorCode) {
    doThrow(NOT_FOUND)
        .withErrorCode(checkNotNull(errorCode, "errorCode"))
        .unless(reference)
        .isNotNull();
    return reference;
  }

  /**
   * Ensures that an object reference passed as a parameter to the calling method is not null.
   *
   * @param reference an object reference
   * @param errorCode an application specific error code
   * @param errorMessage the exception message to use if the check fails; will be converted to a
   *     string using {@link String#valueOf(Object)}
   * @return the non-null reference that was validated
   * @throws NotFoundException if {@code reference} is null
   */
  public static <T, E extends Enum<E>> T checkFound(T reference,
      E errorCode,
      @Nullable Object errorMessage) {
    return checkFound(reference, errorCode.name(), errorMessage);
  }

  /**
   * Ensures that an object reference passed as a parameter to the calling method is not null.
   *
   * @param reference an object reference
   * @param errorCode an application specific error code
   * @param errorMessage the exception message to use if the check fails; will be converted to a
   *     string using {@link String#valueOf(Object)}
   * @return the non-null reference that was validated
   * @throws NotFoundException if {@code reference} is null
   */
  public static <T> T checkFound(T reference,
      ErrorCode errorCode,
      @Nullable Object errorMessage) {
    return checkFound(reference, errorCode.getValue(), errorMessage);
  }

  private static <T> T checkFound(T reference,
      String errorCode,
      @Nullable Object errorMessage) {
    doThrow(NOT_FOUND)
        .withErrorCode(checkNotNull(errorCode, "errorCode"))
        .withMessage(String.valueOf(errorMessage))
        .unless(reference)
        .isNotNull();
    return reference;
  }

  /**
   * Ensures that an object reference passed as a parameter to the calling method is not null.
   *
   * @param reference an object reference
   * @param errorCode an application specific error code
   * @param errorMessageTemplate a template for the exception message should the check fail. The
   *     message is formed by replacing each {@code %s} placeholder in the template with an
   *     argument. These are matched by position - the first {@code %s} gets {@code
   *     errorMessageArgs[0]}, etc.  Unmatched arguments will be appended to the formatted message
   *     in square braces. Unmatched placeholders will be left as-is.
   * @param errorMessageArgs the arguments to be substituted into the message template. Arguments
   *     are converted to strings using {@link String#valueOf(Object)}.
   * @return the non-null reference that was validated
   * @throws NotFoundException if {@code reference} is null
   */
  public static <T, E extends Enum<E>> T checkFound(T reference,
      E errorCode,
      @Nullable String errorMessageTemplate,
      @Nullable Object... errorMessageArgs) {
    return checkFound(reference, errorCode.name(), errorMessageTemplate, errorMessageArgs);
  }

  /**
   * Ensures that an object reference passed as a parameter to the calling method is not null.
   *
   * @param reference an object reference
   * @param errorCode an application specific error code
   * @param errorMessageTemplate a template for the exception message should the check fail. The
   *     message is formed by replacing each {@code %s} placeholder in the template with an
   *     argument. These are matched by position - the first {@code %s} gets {@code
   *     errorMessageArgs[0]}, etc.  Unmatched arguments will be appended to the formatted message
   *     in square braces. Unmatched placeholders will be left as-is.
   * @param errorMessageArgs the arguments to be substituted into the message template. Arguments
   *     are converted to strings using {@link String#valueOf(Object)}.
   * @return the non-null reference that was validated
   * @throws NotFoundException if {@code reference} is null
   */
  public static <T> T checkFound(T reference,
      ErrorCode errorCode,
      @Nullable String errorMessageTemplate,
      @Nullable Object... errorMessageArgs) {
    return checkFound(reference, errorCode.getValue(), errorMessageTemplate, errorMessageArgs);
  }

  private static <T> T checkFound(T reference,
      String errorCode,
      @Nullable String errorMessageTemplate,
      @Nullable Object... errorMessageArgs) {
    doThrow(NOT_FOUND)
        .withErrorCode(checkNotNull(errorCode, "errorCode"))
        .withMessage(String.format(errorMessageTemplate, errorMessageArgs))
        .unless(reference)
        .isNotNull();
    return reference;
  }
}
