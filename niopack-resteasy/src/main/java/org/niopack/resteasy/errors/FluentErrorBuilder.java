package org.niopack.resteasy.errors;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.annotations.GwtIncompatible;
import com.google.common.base.Optional;
import com.google.common.base.Strings;
import com.google.common.base.Throwables;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multiset;
import com.google.common.collect.SetMultimap;
import com.google.common.collect.Table;
import com.google.common.truth.AbstractVerb.DelegatedVerb;
import com.google.common.truth.BooleanSubject;
import com.google.common.truth.ClassSubject;
import com.google.common.truth.ComparableSubject;
import com.google.common.truth.DefaultSubject;
import com.google.common.truth.FailureStrategy;
import com.google.common.truth.IntegerSubject;
import com.google.common.truth.IterableSubject;
import com.google.common.truth.ListMultimapSubject;
import com.google.common.truth.ListSubject;
import com.google.common.truth.LongSubject;
import com.google.common.truth.MapSubject;
import com.google.common.truth.MultimapSubject;
import com.google.common.truth.MultisetSubject;
import com.google.common.truth.ObjectArraySubject;
import com.google.common.truth.OptionalSubject;
import com.google.common.truth.PrimitiveBooleanArraySubject;
import com.google.common.truth.PrimitiveByteArraySubject;
import com.google.common.truth.PrimitiveCharArraySubject;
import com.google.common.truth.PrimitiveDoubleArraySubject;
import com.google.common.truth.PrimitiveFloatArraySubject;
import com.google.common.truth.PrimitiveIntArraySubject;
import com.google.common.truth.PrimitiveLongArraySubject;
import com.google.common.truth.SetMultimapSubject;
import com.google.common.truth.StringSubject;
import com.google.common.truth.Subject;
import com.google.common.truth.SubjectFactory;
import com.google.common.truth.TableSubject;
import com.google.common.truth.TestVerb;
import com.google.common.truth.ThrowableSubject;
import java.util.List;
import java.util.Map;
import javax.annotation.CheckReturnValue;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import org.niopack.nio.core.Error;

/**
 * A Google Truth based proposition framework for responding to RESTful apis, supporting assertion
 * and assumption semantics in a fluent style.
 */
public final class FluentErrorBuilder {

  private Response.Status status = Response.Status.INTERNAL_SERVER_ERROR;
  private String errorCode = null;
  private String messageToPrepend = null;

  private FluentErrorBuilder() {
  }

  public static FluentErrorBuilder doThrow(Response.Status status) {
    FluentErrorBuilder builder = new FluentErrorBuilder();
    builder.status = checkNotNull(status, "status");
    return builder;
  }

  public <E extends Enum<E>> FluentErrorBuilder withErrorCode(E errorCode) {
    this.errorCode = checkNotNull(errorCode, "errorCode").name();
    return this;
  }

  public FluentErrorBuilder withErrorCode(ErrorCode errorCode) {
    this.errorCode = checkNotNull(errorCode, "errorCode").getValue();
    return this;
  }

  public FluentErrorBuilder withErrorCode(String errorCode) {
    this.errorCode = checkNotNull(errorCode, "errorCode");
    return this;
  }

  public FluentErrorBuilder withMessage(String messageToPrepend) {
    this.messageToPrepend = messageToPrepend;
    return this;
  }

  public void always() {
    Error error = new Error();
    error.setErrorCode(errorCode);
    error.setDebugMessage(Strings.nullToEmpty(messageToPrepend));
    ApplicationErrorException e = new ApplicationErrorException(status, error);
    error.setStackTrace(Throwables.getStackTraceAsString(e));
    throw e;
  }

  public void causedBy(Throwable cause) {
    checkNotNull(cause, "cause");
    if (cause instanceof WebApplicationException) {
      Response response = ((WebApplicationException) cause).getResponse();
      checkArgument(status != null && status.getStatusCode() == response.getStatus(),
          "Invalid response status code. Expected [%d], was [%d].", status.getStatusCode(),
          response.getStatus());
      throw (WebApplicationException) cause;
    }
    Error error = new Error();
    error.setErrorCode(errorCode);
    if (cause.getMessage() != null) {
      error.setDebugMessage(Strings.nullToEmpty(messageToPrepend) + cause.getMessage());
    } else {
      error.setDebugMessage(Strings.nullToEmpty(messageToPrepend) + cause.getClass().getName());
    }
    error.setStackTrace(Throwables.getStackTraceAsString(cause));
    throw new ApplicationErrorException(status, error);
  }

  private TestVerb test() {
    return new TestVerb(new FailureStrategy() {
      @Override
      public void fail(String message, Throwable cause) {
        Error error = new Error();
        error.setErrorCode(errorCode);
        error.setDebugMessage(Strings.nullToEmpty(messageToPrepend) + message);
        ApplicationErrorException e = new ApplicationErrorException(status, error);
        error.setStackTrace(Throwables.getStackTraceAsString(e));
        throw e;
      }
    }, messageToPrepend);
  }

  /**
   * The recommended method of extension of Truth to new types.
   *
   * @param factory a SubjectFactory<S, T> implementation
   * @returns A custom verb for the type returned by the SubjectFactory
   */
  public <S extends Subject<S, T>, T, SF extends SubjectFactory<S, T>> DelegatedVerb<S, T> unlessAbout(
      SF factory) {
    return test().about(factory);
  }

  @CheckReturnValue
  public <T extends Comparable<?>> ComparableSubject<?, T> unless(T target) {
    return test().that(target);
  }

  @CheckReturnValue
  public Subject<DefaultSubject, Object> unless(Object target) {
    return test().that(target);
  }

  @CheckReturnValue
  @GwtIncompatible("ClassSubject.java")
  public ClassSubject unless(Class<?> target) {
    return test().that(target);
  }

  @CheckReturnValue
  public ThrowableSubject unless(Throwable target) {
    return test().that(target);
  }

  @CheckReturnValue
  public LongSubject unless(Long target) {
    return test().that(target);
  }

  @CheckReturnValue
  public IntegerSubject unless(Integer target) {
    return test().that(target);
  }

  @CheckReturnValue
  public BooleanSubject unless(Boolean target) {
    return test().that(target);
  }

  @CheckReturnValue
  public StringSubject unless(String target) {
    return test().that(target);
  }

  @CheckReturnValue
  public <T, C extends Iterable<T>> IterableSubject<? extends IterableSubject<?, T, C>, T, C> unless(
      Iterable<T> target) {
    return test().that(target);
  }

  @CheckReturnValue
  public <T, C extends List<T>> ListSubject<? extends ListSubject<?, T, C>, T, C> unless(List<T> target) {
    return test().that(target);
  }

  @CheckReturnValue
  public <T> ObjectArraySubject<T> unless(T[] target) {
    return test().that(target);
  }

  @CheckReturnValue
  public PrimitiveBooleanArraySubject unless(boolean[] target) {
    return test().that(target);
  }

  @CheckReturnValue
  public PrimitiveIntArraySubject unless(int[] target) {
    return test().that(target);
  }

  @CheckReturnValue
  public PrimitiveLongArraySubject unless(long[] target) {
    return test().that(target);
  }

  @CheckReturnValue
  public PrimitiveByteArraySubject unless(byte[] target) {
    return test().that(target);
  }

  @CheckReturnValue
  public PrimitiveCharArraySubject unless(char[] target) {
    return test().that(target);
  }

  @CheckReturnValue
  public PrimitiveFloatArraySubject unless(float[] target) {
    return test().that(target);
  }

  @CheckReturnValue
  public PrimitiveDoubleArraySubject unless(double[] target) {
    return test().that(target);
  }

  @CheckReturnValue
  public <T> OptionalSubject<T> unless(Optional<T> target) {
    return test().that(target);
  }

  @CheckReturnValue
  public <K, V, M extends Map<K, V>> MapSubject<? extends MapSubject<?, K, V, M>, K, V, M> unless(
      Map<K, V> target) {
    return test().that(target);
  }

  @CheckReturnValue
  public <K, V, M extends Multimap<K, V>> MultimapSubject<? extends MultimapSubject<?, K, V, M>, K, V, M> unless(
      Multimap<K, V> target) {
    return test().that(target);
  }

  @CheckReturnValue
  public <K, V, M extends ListMultimap<K, V>> ListMultimapSubject<? extends ListMultimapSubject<?, K, V, M>, K, V, M> unless(
      ListMultimap<K, V> target) {
    return test().that(target);
  }

  @CheckReturnValue
  public <K, V, M extends SetMultimap<K, V>> SetMultimapSubject<? extends SetMultimapSubject<?, K, V, M>, K, V, M> unless(
      SetMultimap<K, V> target) {
    return test().that(target);
  }

  @CheckReturnValue
  public <E, M extends Multiset<E>> MultisetSubject<? extends MultisetSubject<?, E, M>, E, M> unless(
      Multiset<E> target) {
    return test().that(target);
  }

  @CheckReturnValue
  public <R, C, V, M extends Table<R, C, V>> TableSubject<? extends TableSubject<?, R, C, V, M>, R, C, V, M> unless(
      Table<R, C, V> target) {
    return test().that(target);
  }
}
