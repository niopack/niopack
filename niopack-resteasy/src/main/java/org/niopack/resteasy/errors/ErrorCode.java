package org.niopack.resteasy.errors;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public final class ErrorCode {

  private final String value;

  private ErrorCode(String value) {
    this.value = checkNotNull(value, "value");
  }

  public static final ErrorCode code(String value) {
    return new ErrorCode(value);
  }

  public String getValue() {
    return value;
  }

  @Override
  public int hashCode() {
    return value.hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof ErrorCode)) {
      return false;
    }
    ErrorCode that = (ErrorCode) obj;
    return Objects.equal(this.value, that.value);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("value", value)
        .toString();
  }
}
