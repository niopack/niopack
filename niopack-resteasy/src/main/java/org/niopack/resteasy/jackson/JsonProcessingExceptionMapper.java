package org.niopack.resteasy.jackson;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import org.niopack.resteasy.errors.AbstractExceptionMapper;

@Provider
public class JsonProcessingExceptionMapper extends AbstractExceptionMapper<JsonProcessingException> {

  @Override
  public Response toResponse(JsonProcessingException exception) {
    String logId = createLogId();
    /*
     * If the error is in the JSON generation, it's a server error.
     */
    if (exception instanceof JsonGenerationException) {
      logger.warn(formatLogMessage(logId, exception), exception);
      return Response.serverError()
          .type(MediaType.APPLICATION_JSON)
          .entity(createError(logId, exception))
          .build();
    }

    final String message = exception.getOriginalMessage();

    /*
     * If we can't deserialize the JSON because someone forgot a no-arg constructor, it's a
     * server error and we should inform the developer.
     */
    if (message.startsWith("No suitable constructor found")) {
      logger.error(formatLogMessage(logId, exception), exception);
      return Response.serverError()
          .type(MediaType.APPLICATION_JSON)
          .entity(createError(logId, exception))
          .build();
    }

    /*
     * Otherwise, it's those pesky users.
     */
    logger.debug("Unable to process JSON", exception);
    return Response.status(Response.Status.BAD_REQUEST)
        .type(MediaType.APPLICATION_JSON)
        .entity(createError(logId, exception))
        .build();
  }
}
