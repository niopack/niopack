package org.niopack.resteasy.params;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.time.OffsetDateTime;
import javax.ws.rs.ext.ParamConverter;
import javax.ws.rs.ext.ParamConverterProvider;
import javax.ws.rs.ext.Provider;

@Provider
public class OffsetDateTimeParamConverterProvider implements ParamConverterProvider {

  @Override
  public <T> ParamConverter<T> getConverter(final Class<T> rawType, final Type genericType,
      final Annotation[] annotations) {
    if (rawType.equals(OffsetDateTime.class)) {
      return (ParamConverter) new OffsetDateTimeParamConverter();
    } else {
      return null;
    }
  }

  /**
   * A parameter encapsulating date/time values. All values returned are in UTC.
   */
  private static class OffsetDateTimeParamConverter implements ParamConverter<OffsetDateTime> {

    @Override
    public OffsetDateTime fromString(String value) {
      return OffsetDateTime.parse(value);
    }

    @Override
    public String toString(OffsetDateTime value) {
      return value.toString();
    }
  }
}
