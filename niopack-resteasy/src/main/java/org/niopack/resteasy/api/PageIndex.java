package org.niopack.resteasy.api;

import static com.google.common.base.Preconditions.checkArgument;

public class PageIndex {

  private final long fromIncluded;
  private final long toExcluded;

  public PageIndex(long fromIncluded, long toExcluded, long maxPageLength) {
    checkArgument(maxPageLength >= 0, "maxPageLength must be non-negative.");
    this.fromIncluded = Math.max(fromIncluded, 0);
    this.toExcluded =
        Math.min(toExcluded <= 0 ? Long.MAX_VALUE : toExcluded, fromIncluded + maxPageLength);
  }

  public long getFromIncluded() {
    return fromIncluded;
  }

  public long getToExcluded() {
    return toExcluded;
  }
}
