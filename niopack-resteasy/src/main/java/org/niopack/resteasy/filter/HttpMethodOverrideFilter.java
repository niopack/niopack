package org.niopack.resteasy.filter;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;

/**
 * @see <a
 *      href="http://www.hanselman.com/blog/HTTPPUTOrDELETENotAllowedUseXHTTPMethodOverrideForYourRESTServiceWithASPNETWebAPI.aspx">
 *      Use X-HTTP-Method-Override for your REST Service with ASP.NET Web API </a>
 * @see <a href="http://java.dzone.com/articles/whats-new-jax-rs-20">What's New in JAX-RS 2.0</a>
 */
@Provider
public class HttpMethodOverrideFilter implements ContainerRequestFilter {

  @Override
  public void filter(ContainerRequestContext ctx) {
    String method = ctx.getHeaderString("X-Http-Method-Override");
    if (method != null)
      ctx.setMethod(method);
  }
}
