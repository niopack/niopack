package org.niopack.nio.gradle

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.junit.Test

class NioPluginTest {
    @Test
    void pluginIsApplied() {
        Project project = ProjectBuilder.builder().build()
        project.apply plugin: 'org.niopack.nio.gradle'

        def task = project.tasks.findByName('niopacker')
        assert task instanceof NiopackerTask
        assert task.source == project.file("src/main/nio")
        assert task.output == project.file("$project.buildDir/gen/main/java")
    }
}
