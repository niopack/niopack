<#if packageName?has_content>
package ${packageName};

</#if>
<#list staticImports as staticImport>
import static ${staticImport};
<#if !staticImport_has_next>

</#if>
</#list>
<#list imports as import>
import ${import};
<#if !import_has_next>

</#if>
</#list>
public final class ${typeName} {
  <#if fields?has_content>
  <#list fields as field>
  <#if (field.restParamType)?has_content>@${field.restParamType}("${field.restParamName}") </#if>private ${field.javaType} ${field.name}<#if field.defaultValue??> = ${field.defaultValue}</#if>;
  </#list>

  <#list fields as field>
  public ${field.javaType} <#if field.javaType = 'boolean'>is<#else>get</#if>${field.name?cap_first}() {
    return ${field.name};
  }

  public void set${field.name?cap_first}(${field.javaType} ${field.name}) {
    this.${field.name} = ${field.name};
  }

  </#list>
  @Override
  public int hashCode() {
    <#if fields?size < 5>
    return Objects.hashCode(<#list fields as field>${field.name}<#if field_has_next>, </#if></#list>);
    <#else>
    return Objects.hashCode(<#list fields as field>
        ${field.name}<#if field_has_next>, </#if></#list>);
    </#if>
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof ${typeName})) {
      return false;
    }
    ${typeName} that = (${typeName}) obj;
    return <#list fields as field>Objects.equal(this.${field.name}, that.${field.name})<#if field_has_next>
        && <#else>;</#if></#list>
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        <#list fields as field>
        .add("${field.name}", ${field.name})
        </#list>
        .toString();
  }
  </#if>
}
