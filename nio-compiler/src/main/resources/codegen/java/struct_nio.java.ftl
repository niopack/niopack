<#if packageName?has_content>
package ${packageName};

</#if>
<#list staticImports as staticImport>
import static ${staticImport};
<#if !staticImport_has_next>

</#if>
</#list>
<#list imports as import>
import ${import};
<#if !import_has_next>

</#if>
</#list>
public final class ${typeName}_Nio {
  <#list fields as field>
  public static final String ${field.nameConstant} = "${field.name}";
  </#list>

  public static final List<String> _FIELDS;
  public static final Map<Class<?>, Object> _TYPE_ANNOTATIONS;
  public static final Table<String, Class<?>, Object> _FIELD_ANNOTATIONS;

  static {
    <#if fields?has_content>
    ImmutableList.Builder<String> fields = ImmutableList.<String>builder();
    <#list fields as field>
    fields.add(${field.nameConstant});
    </#list>
    _FIELDS = fields.build();
    <#else>
    _FIELDS = ImmutableList.<String>of();
    </#if>

    <#if typeAnnotations?has_content>
    ImmutableMap.Builder<Class<?>, Object> typeAnnotations = ImmutableMap.<Class<?>, Object>builder();
    <#list typeAnnotations as typeAnnotation>
<#if (typeAnnotation.instantiation)?has_content>${typeAnnotation.instantiation}</#if>
    typeAnnotations.put(${typeAnnotation.klass}.class, ${typeAnnotation.variable});
    </#list>
    _TYPE_ANNOTATIONS = typeAnnotations.build();
    <#else>
    _TYPE_ANNOTATIONS = ImmutableMap.<Class<?>, Object>of();
    </#if>

    <#if fieldAnnotations?has_content>
    ImmutableTable.Builder<String, Class<?>, Object> fieldAnnotations =
        ImmutableTable.<String, Class<?>, Object>builder();
    <#list fieldAnnotations as fieldAnnotation>
<#if (fieldAnnotation.instantiation)?has_content>${fieldAnnotation.instantiation}</#if>
    fieldAnnotations.put(${fieldAnnotation.nameConstant}, ${fieldAnnotation.klass}.class, ${fieldAnnotation.variable});
    </#list>
    _FIELD_ANNOTATIONS = fieldAnnotations.build();
    <#else>
    _FIELD_ANNOTATIONS = ImmutableTable.<String, Class<?>, Object>of();
    </#if>
  }

  private ${typeName}_Nio() {}
}
