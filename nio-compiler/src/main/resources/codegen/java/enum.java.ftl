<#if packageName?has_content>
package ${packageName};

</#if>
public enum ${typeName} {
  <#list fields as field>
  ${field.name}(${field.value})<#if field_has_next>,<#else>;</#if>
  </#list>

  private final int value;

  private ${typeName}(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }
}
