<#if packageName?has_content>
package ${packageName};

</#if>
<#list staticImports as staticImport>
import static ${staticImport};
<#if !staticImport_has_next>

</#if>
</#list>
<#list imports as import>
import ${import};
<#if !import_has_next>

</#if>
</#list>
<#if restService.path?has_content>@Path("${restService.path}")</#if>
<#if restService.produces?has_content>@Produces(${restService.produces})</#if>
public interface ${typeName} {
  <#list methods as method>

  <#if method.verb?has_content>@${method.verb}
  </#if><#if method.path?has_content>@Path("${method.path}")
  </#if>${method.responseType} ${method.name}(@BeanParam ${method.requestType} request);
  </#list>

  <#if restService.path?has_content>@Path("${restService.path}")</#if>
  <#if restService.produces?has_content>@Produces(${restService.produces})</#if>
  interface Raw {
    <#list methods as method>

    <#if method.verb?has_content>@${method.verb}
    </#if><#if method.path?has_content>@Path("${method.path}")
    </#if>Response ${method.name}(@BeanParam ${method.requestType} request);
    </#list>
  }

  @Documented
  @Qualifier
  @Retention(RUNTIME)
  @interface Client {
  }

  @Documented
  @Qualifier
  @Retention(RUNTIME)
  @interface Target {
  }
}
