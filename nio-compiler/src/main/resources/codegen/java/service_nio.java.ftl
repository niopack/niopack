<#if packageName?has_content>
package ${packageName};

</#if>
<#list staticImports as staticImport>
import static ${staticImport};
<#if !staticImport_has_next>

</#if>
</#list>
<#list imports as import>
import ${import};
<#if !import_has_next>

</#if>
</#list>
public final class ${typeName}_Nio {
  <#list methods as method>
  public static final String ${method.nameConstant} = "${method.name}";
  </#list>

  public static final List<String> _METHODS;
  public static final Map<Class<?>, Object> _TYPE_ANNOTATIONS;
  public static final Table<String, Class<?>, Object> _METHOD_ANNOTATIONS;

  static {
    <#if methods?has_content>
    ImmutableList.Builder<String> methods = ImmutableList.<String>builder();
    <#list methods as method>
    methods.add(${method.nameConstant});
    </#list>
    _METHODS = methods.build();
    <#else>
    _METHODS = ImmutableList.<String>of();
    </#if>

    <#if typeAnnotations?has_content>
    ImmutableMap.Builder<Class<?>, Object> typeAnnotations = ImmutableMap.<Class<?>, Object>builder();
    <#list typeAnnotations as typeAnnotation>
<#if (typeAnnotation.instantiation)?has_content>${typeAnnotation.instantiation}</#if>
    typeAnnotations.put(${typeAnnotation.klass}.class, ${typeAnnotation.variable});
    </#list>
    _TYPE_ANNOTATIONS = typeAnnotations.build();
    <#else>
    _TYPE_ANNOTATIONS = ImmutableMap.<Class<?>, Object>of();
    </#if>

    <#if methodAnnotations?has_content>
    ImmutableTable.Builder<String, Class<?>, Object> methodAnnotations =
        ImmutableTable.<String, Class<?>, Object>builder();
    <#list methodAnnotations as methodAnnotation>
<#if (methodAnnotation.instantiation)?has_content>${methodAnnotation.instantiation}</#if>
    methodAnnotations.put(${methodAnnotation.nameConstant}, ${methodAnnotation.klass}.class, ${methodAnnotation.variable});
    </#list>
    _METHOD_ANNOTATIONS = methodAnnotations.build();
    <#else>
    _METHOD_ANNOTATIONS = ImmutableTable.<String, Class<?>, Object>of();
    </#if>
  }

  private ${typeName}_Nio() {}
}
