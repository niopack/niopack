<?php<#if namespace?has_content> namespace ${namespace};</#if>

<#list imports as import>
use ${import?substring(1)};
<#if !import_has_next>

</#if>
</#list>
final class ${typeName} implements JsonSerializable
{
    <#list fields as field>
    public $${field.name}<#if field.typeInfo.array> = []</#if>;
    </#list>

    /**
     * @param array $json
     */
    public function __construct(array $json = [])
    {
        <#list fields as field>
        <#if !field.typeInfo.array>
        if (isset($json['${field.name}'])) {
            <#if field.typeInfo.canonicalName = ''>
            $this->${field.name} = $json['${field.name}'];
            <#else>
            $this->${field.name} = new ${field.typeInfo.typeName}($json['${field.name}']);
            </#if>
        }
        <#else>
        if (isset($json['${field.name}'])) {
            $this->${field.name} = self::parse($json['${field.name}'], '${field.typeInfo.canonicalName}');
        }
        </#if>
        </#list>
    }

    <#if arrayField>
    private static function parse($json, $class)
    {
        $result = [];
        foreach ($json as $key => $value) {
            if ($class === "") {
                $result[$key] = $value;
            } else {
                $result[$key] = new $class($value);
            }
        }
        return $result;
    }

    </#if>
    /**
     * @return array
     */
    public function jsonSerialize()
    {
      <#if fields?has_content>
      $json = [];
      <#list fields as field>
      if (isset($this->${field.name})) {
          $json['${field.name}'] = $this->${field.name};
      }
      </#list>
      return $json;
      <#else>
      return [];
      </#if>
    }
}
