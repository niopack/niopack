<?php<#if namespace?has_content> namespace ${namespace};</#if>

<#list imports as import>
use ${import?substring(1)};
<#if !import_has_next>

</#if>
</#list>
final class ${typeName}Client
{
    private $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    <#list methods as method>
    public function ${method.name}(${method.requestType} $request, array $options = [])
    {
        try {
            <#list method.requestFields as field>
            <#assign stringify>
            <#if field.typeInfo.canonicalName != ''><#t>->jsonSerialize()
            <#elseif field.typeInfo.typeName = ":bool"><#t> ? 'true' : 'false'
            <#else><#t>
            </#if>
            </#assign>
            <#if field.restParamType == "PATH">
            $${field.restParamName} = $request->${field.name}${stringify};
            </#if>
            </#list>
            $url = "${method.path}";
            <#list method.requestFields as field>
            <#assign stringify>
            <#if field.typeInfo.canonicalName != ''><#t>->jsonSerialize()
            <#elseif field.typeInfo.typeName = ":bool"><#t> ? 'true' : 'false'
            <#else><#t>
            </#if>
            </#assign>
            <#if field.restParamType == "FORM">
            if (isset($request->${field.name})) {
                $options['body']['${field.restParamName}'] = $request->${field.name}${stringify};
            }
            </#if>
            <#if field.restParamType == "QUERY">
            if (isset($request->${field.name})) {
                $options['query']['${field.restParamName}'] = $request->${field.name}${stringify};
            }
            </#if>
            </#list>
            // Always use synchronous requests
            unset($options['future']);
            $req = $this->client->createRequest('${method.verb?upper_case}', $url, $options);
            <#list method.requestFields as field>
            <#if field.restParamType == "FORM" && field.typeInfo.array>
            $req->getBody()->setAggregator(Query::duplicateAggregator());
            <#break>
            </#if>
            </#list>
            <#list method.requestFields as field>
            <#if field.restParamType == "QUERY" && field.typeInfo.array>
            $req->getQuery()->setAggregator(Query::duplicateAggregator());
            <#break>
            </#if>
            </#list>
            return Response::of($this->client->send($req), '${method.responseType}');
        } catch (\Exception $e) {
            return Response::fromException($e);
        }
    }

    </#list>
}
