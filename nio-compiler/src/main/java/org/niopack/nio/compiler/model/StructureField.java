package org.niopack.nio.compiler.model;

import org.niopack.nio.compiler.model.type.TypeLiteral;
import org.niopack.nio.compiler.model.value.Value;

public final class StructureField extends AbstractAnnotatedIdentifier {

  private Value value;

  public StructureField(String name) {
    super(name);
  }

  public StructureField(String name, TypeLiteral type) {
    super(name, type);
  }

  @Override
  public Category getCategory() {
    return Category.FIELD_STRUCT;
  }

  public Value getValue() {
    return value;
  }

  public void setValue(Value value) {
    this.value = value;
  }

  @Override
  public String toString() {
    return getName() + "::" + getType() + "::" + getAnnotations() + "::" + value;
  }
}
