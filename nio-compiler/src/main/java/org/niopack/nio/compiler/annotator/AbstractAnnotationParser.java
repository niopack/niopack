package org.niopack.nio.compiler.annotator;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import java.util.IllegalFormatException;
import java.util.Set;
import javax.annotation.Nullable;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.model.AnnotatedIdentifier;
import org.niopack.nio.compiler.model.Identifier.Category;
import org.niopack.nio.compiler.model.value.StructureValue;

abstract class AbstractAnnotationParser<A> implements AnnotationParser<A> {

  protected final AnnotatedIdentifier identifier;
  protected final SymbolTable table;

  public AbstractAnnotationParser(AnnotatedIdentifier identifier, SymbolTable table) {
    this.identifier = checkNotNull(identifier, "identifier");
    this.table = checkNotNull(table, "table");
    checkArgument(getSupportedCategories().contains(identifier.getCategory()),
        "Unsupported category = %s.", identifier.getCategory());
  }

  protected abstract Set<Category> getSupportedCategories();

  /**
   * Finds the structure value defining the annotation
   */
  protected StructureValue find() {
    // canonicalName needs to be checked first to avoid naming collision.
    // TODO(tamal): SymbolTable can be consulted to verify that simpleName can be used.
    StructureValue value = (StructureValue) identifier.getAnnotations().get(getCanonicalName());
    if (value == null) {
      value = (StructureValue) identifier.getAnnotations().get(getSimpleName());
      checkState(value == null || table.find(getSimpleName()) != null);
    }
    return value;
  }

  protected boolean isAnnotated() {
    return find() != null;
  }

  @Override
  @Nullable
  public A getAnnotation() {
    StructureValue value = find();
    if (value == null) {
      return null;
    }
    return parse(value);
  }

  protected abstract A parse(StructureValue value) throws IllegalFormatException;
}
