package org.niopack.nio.compiler.model.type;

public final class MapTypeLiteral implements TypeLiteral {
  private ScalarTypeLiteral keyType;
  private ScalarTypeLiteral valueType;

  public ScalarTypeLiteral getKeyType() {
    return keyType;
  }

  public void setKeyType(ScalarTypeLiteral keyType) {
    this.keyType = keyType;
  }

  public ScalarTypeLiteral getValueType() {
    return valueType;
  }

  public void setValueType(ScalarTypeLiteral valueType) {
    this.valueType = valueType;
  }

  @Override
  public Type getType() {
    return Type.MAP;
  }

  @Override
  public String toString() {
    return "<" + getKeyType() + "," + getValueType() + ">";
  }
}
