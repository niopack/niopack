package org.niopack.nio.compiler.model;

import org.niopack.nio.compiler.model.type.TypeLiteral;

public interface Identifier {

  String getName();

  Category getCategory();

  TypeLiteral getType();

  public static enum Category {
    BUILTIN,
    PACKAGE,
    SERVICE,
    STRUCT,
    ENUM,
    FIELD_ENUM,
    FIELD_STRUCT,
    METHOD_SERVICE;
  }
}
