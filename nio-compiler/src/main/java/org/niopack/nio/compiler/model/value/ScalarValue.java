package org.niopack.nio.compiler.model.value;

import static com.google.common.base.Preconditions.checkState;

import java.util.Base64;

public final class ScalarValue implements Value {

  private final String value;
  private final Type type;

  public ScalarValue(String value, Type type) {
    this.value = value;
    this.type = type;
  }

  public String getValue() {
    return value;
  }

  public boolean toBool() {
    checkState(type == Type.BOOL, "Can't convet from %s type", type);
    return Boolean.valueOf(value);
  }

  public byte[] toBytes() {
    checkState(type == Type.STRING, "Can't convet from %s type", type);
    return Base64.getDecoder().decode(value);
  }

  public char toChar() {
    checkState(type == Type.CHAR, "Can't convet from %s type", type);
    // Praser ensures that this is a single char string.
    return value.charAt(0);
  }

  public <E extends Enum<E>> E toEnum(Class<E> klass) {
    return Enum.valueOf(klass, value);
  }

  @Override
  public String toString() {
    // can't check that the original value was a string
    return value;
  }

  public float toFloat() {
    checkState(type == Type.FLOAT, "Can't convet from %s type", type);
    return Float.valueOf(value);
  }

  public double toDouble() {
    checkState(type == Type.FLOAT, "Can't convet from %s type", type);
    return Double.valueOf(value);
  }

  public byte toByte() {
    checkState(type == Type.INT, "Can't convet from %s type", type);
    return Byte.valueOf(value);
  }

  public short toShort() {
    checkState(type == Type.INT, "Can't convet from %s type", type);
    return Short.valueOf(value);
  }

  public int toInteger() {
    checkState(type == Type.INT, "Can't convet from %s type", type);
    return Integer.valueOf(value);
  }

  public long toLong() {
    checkState(type == Type.INT, "Can't convet from %s type", type);
    return Long.valueOf(value);
  }

  public long toUsec() {
    // TODO(tamal): Support iso encoded timestamp
    return toLong();
  }

  public long toMsec() {
    // TODO(tamal): Support iso encoded timestamp
    return toLong();
  }

  public Type getType() {
    return type;
  }
}
