package org.niopack.nio.compiler.model;

import org.niopack.nio.compiler.model.type.ScalarTypeLiteral;
import org.niopack.nio.compiler.model.type.TypeLiteral;
import org.niopack.nio.compiler.model.type.TypeLiteral.Type;

public final class ServiceMethod extends AbstractAnnotatedIdentifier {

  private TypeLiteral requestType;
  private TypeLiteral responseType;

  public ServiceMethod(String name) {
    super(name, new ScalarTypeLiteral(Type.NONE, name));
  }

  @Override
  public Category getCategory() {
    return Category.METHOD_SERVICE;
  }

  public TypeLiteral getRequestType() {
    return requestType;
  }

  public void setRequestType(TypeLiteral requestType) {
    this.requestType = requestType;
  }

  public TypeLiteral getResponseType() {
    return responseType;
  }

  public void setResponseType(TypeLiteral responseType) {
    this.responseType = responseType;
  }

  @Override
  public String toString() {
    return getName() + "::" + getCategory() + "::" + getAnnotations() + "::REQ=" + requestType
        + "::RESP=" + responseType;
  }
}
