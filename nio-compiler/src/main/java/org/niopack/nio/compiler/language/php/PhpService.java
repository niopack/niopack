package org.niopack.nio.compiler.language.php;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.collect.Iterables;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.niopack.nio.compiler.annotator.RestMethodParser;
import org.niopack.nio.compiler.annotator.RestServiceParser;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.language.php.model.PhpRestMethod;
import org.niopack.nio.compiler.language.php.model.PhpStructureField;
import org.niopack.nio.compiler.language.php.model.TypeInfo;
import org.niopack.nio.compiler.model.Identifier.Category;
import org.niopack.nio.compiler.model.Service;
import org.niopack.nio.compiler.model.Symbol;
import org.niopack.nio.compiler.model.type.ScalarTypeLiteral;
import org.niopack.nio.core.RestMethod;
import org.niopack.nio.core.RestService;

final class PhpService extends AbstractPhpSymbol {

  private final Symbol symbol;
  private final SymbolTable table;
  private final Service definition;

  public PhpService(Symbol symbol, SymbolTable table) {
    Category type = Iterables.getLast(symbol.getIdentifiers()).getCategory();
    checkArgument(type == Category.SERVICE, "%s is not a service.", symbol);
    this.symbol = checkNotNull(symbol, "symbol");
    this.table = checkNotNull(table, "table");
    this.definition = table.getDefinition(symbol);
  }

  @Override public Symbol getSymbol() {
    return symbol;
  }

  @Override public SymbolTable getTable() {
    return table;
  }

  @Override @SuppressWarnings("unchecked")
  public <T> T getDefinition() {
    return (T) definition;
  }

  @Override
  public Category getCategory() {
    return Category.SERVICE;
  }

  @Override public void write(Configuration cfg, Writer writer)
      throws TemplateException, IOException {
    Template template = cfg.getTemplate("php/service_client.php.ftl");
    template.process(getDataModel(), writer);
  }

  @Override
  public Map<String, Object> getDataModel() {
    Map<String, Object> dataModel = new HashMap<>();
    dataModel.put("namespace", getNamespace());
    dataModel.put("typeName", getSimpleName());

    // Simple class name -> Fully qualified class name
    Map<String, String> imports = new HashMap<>();
    imports.put("Response", "\\Niopack\\Response");
    imports.put("Client", "\\GuzzleHttp\\Client");
    imports.put("Query", "\\GuzzleHttp\\Query");
    // imports.put("RequestException", "\\GuzzleHttp\\Exception\\RequestException");

    // TODO(tamal): verify that request has fields annotated with a query param
    TypeResolver resolver = new TypeResolver(table, getNamespace(), imports);

    RestService restService = new RestServiceParser(definition, table).getAnnotation();
    checkNotNull(restService, "%s is missing @RestService", definition.getName());
    List<PhpRestMethod> methods = definition.getMethods().values()
        .stream()
        .map(method -> {
          String name = method.getName();
          RestMethod restMethod = new RestMethodParser(method, table).getAnnotation();
          checkNotNull(restMethod, "%s is missing @RestMethod", method.getName());
          String pathTemplate = "/" + StringUtils.strip(restService.getPath(), "/");
          if (restMethod.getPath() != null) {
            pathTemplate += "/";
            pathTemplate += StringUtils.strip(restMethod.getPath(), "/");
          }
          String path = pathTemplate.replace("{", "${");
          TypeInfo requestType = resolver.resolve(method.getRequestType());
          Symbol requestSymbol =
              table.find(((ScalarTypeLiteral) method.getRequestType()).getTypeName());
          PhpSymbol phpRequestSymbol = PhpSymbolFactory.create(requestSymbol, table);
          TypeInfo responseType = resolver.resolve(method.getResponseType());
          return new PhpRestMethod(name,
              path,
              restMethod.getVerb(),
              requestType.getTypeName(),
              (List<PhpStructureField>) phpRequestSymbol.getDataModel().get("fields"),
              responseType.getCanonicalName());
        }).collect(Collectors.toList());
    dataModel.put("methods", methods);
    dataModel.put("imports", new TreeSet<>(imports.values()));
    return dataModel;
  }
}
