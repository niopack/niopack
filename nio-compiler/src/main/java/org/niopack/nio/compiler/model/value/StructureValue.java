package org.niopack.nio.compiler.model.value;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

import com.google.common.collect.Iterables;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.common.SymbolTable.PostGenerationHandler;
import org.niopack.nio.compiler.model.Structure;

public class StructureValue implements Value, PostGenerationHandler  {

  private String name;
  private Map<String, Value> valueByField = new LinkedHashMap<>();
  private Value onlyFieldValue = null;

  public String getName() {
    return name;
  }

  @Override
  public Type getType() {
    return Type.STRUCT;
  }

  public void setName(String name) {
    this.name = name;
  }

  private void checkIsClean() {
    checkArgument(onlyFieldValue == null, "Can't access fields until cleanup is done.");
  }

  public Set<String> getFields() {
    checkIsClean();
    return valueByField.keySet();
  }

  public boolean hasValue(String field) {
    checkIsClean();
    return valueByField.containsKey(field);
  }

  public Value getValue(String field) {
    checkIsClean();
    return valueByField.get(field);
  }

  public ScalarValue scalarValue(String field) throws ClassCastException {
    return (ScalarValue) getValue(field);
  }

  public void addValueByField(String field, Value value) {
    checkIsClean();
    this.valueByField.put(field, value);
  }

  public void setOnlyFieldValue(Value value, SymbolTable table) {
    this.onlyFieldValue = value;
    table.registerPostGenerationHandler(this);
  }

  @Override
  public void resolveDefaultField(SymbolTable table) {
    Structure structure = table.getDefinition(table.find(name));
    if (onlyFieldValue != null) {
      checkState(structure.getFieldNames().size() == 1,
          "Can't use shorthand notaiton to initialize annotation for structure %s with %s fields.",
          structure.getName(),
          structure.getFieldNames().size());
      valueByField.put(Iterables.getOnlyElement(structure.getFieldNames()), onlyFieldValue);
      onlyFieldValue = null;
    }
  }

  @Override
  public String toString() {
    return name + "::" + valueByField.toString();
  }
}
