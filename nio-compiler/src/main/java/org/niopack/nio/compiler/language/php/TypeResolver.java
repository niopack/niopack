package org.niopack.nio.compiler.language.php;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.niopack.nio.compiler.language.php.PhpSymbol.BS;

import com.google.common.collect.Iterables;
import java.util.Map;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.language.php.model.TypeInfo;
import org.niopack.nio.compiler.model.Identifier.Category;
import org.niopack.nio.compiler.model.Symbol;
import org.niopack.nio.compiler.model.type.ListTypeLiteral;
import org.niopack.nio.compiler.model.type.MapTypeLiteral;
import org.niopack.nio.compiler.model.type.ScalarTypeLiteral;
import org.niopack.nio.compiler.model.type.TypeLiteral;

final class TypeResolver {

  private final SymbolTable table;
  private final String phpNamespace;
  // SimpleName -> CanonicalName
  private final Map<String, String> imports;

  public TypeResolver(SymbolTable table, String phpNamespace, Map<String, String> imports) {
    this.table = checkNotNull(table, "table");
    this.phpNamespace = checkNotNull(phpNamespace, "phpNamespace");
    this.imports = checkNotNull(imports, "imports");
  }

  public SymbolTable getTable() {
    return table;
  }

  public Map<String, String> getImports() {
    return imports;
  }

  public TypeInfo resolve(TypeLiteral type) {
    TypeInfo result = new TypeInfo();
    resolve(type, result);
    return result;
  }

  private void resolve(TypeLiteral type, TypeInfo typeInfo) {
    if (type instanceof ScalarTypeLiteral) {
      String typeName = ((ScalarTypeLiteral) type).getTypeName();
      Symbol symbol = table.find(typeName);
      checkNotNull(symbol, "can't find symbol %s.", typeName);
      Category category = Iterables.getLast(symbol.getIdentifiers()).getCategory();
      if(category == Category.BUILTIN || category == Category.ENUM) {
        typeInfo.setTypeName(":" + typeName);
        typeInfo.setCanonicalName("");
      } else {
        PhpSymbol phpSymbol = PhpSymbolFactory.create(symbol, table);
        if (useSimpleName(phpSymbol.getSimpleName(), phpSymbol.getCanonicalName())) {
          addImport(phpSymbol.getSimpleName(), phpSymbol.getCanonicalName());
          typeInfo.setTypeName(phpSymbol.getSimpleName());
          typeInfo.setCanonicalName(phpSymbol.getCanonicalName());
        } else {
          typeInfo.setTypeName(phpSymbol.getCanonicalName());
          typeInfo.setCanonicalName(phpSymbol.getCanonicalName());
        }
      }
    } else if (type instanceof ListTypeLiteral) {
      typeInfo.setArray(true);
      resolve(((ListTypeLiteral) type).getElementType(), typeInfo);
    } else if (type instanceof MapTypeLiteral) {
      typeInfo.setArray(true);
      resolve(((MapTypeLiteral) type).getValueType(), typeInfo);
    } else {
      throw new IllegalStateException("Unknown typeLiteral = " + type.getClass());
    }
  }

  private boolean useSimpleName(String simpleName, String canonicalName) {
    String existingCanonicalName = imports.get(simpleName);
    if ("void".equals(simpleName)) {
      return false; // so that void is not imported.
    } else if (existingCanonicalName == null) {
      return true;
    } else if (canonicalName.equals(existingCanonicalName)) {
      return true;
    } else {
      // another class with SimpleName List has already been imported,
      // so we need to use canonical name
      return false;
    }
  }

  /**
   * Returns true if import was added.
   */
  public boolean addImport(String simpleName, String canonicalName) {
    checkArgument(canonicalName.endsWith(simpleName),
        "%s is not a simple name for %s", simpleName, canonicalName);
    // canonicalName = BS + containerName + BS + simpleName
    String containerName = canonicalName.equals(BS + simpleName)
        ? ""
        : canonicalName.substring(1, canonicalName.length() - simpleName.length() - 1);
    if (!phpNamespace.equals(containerName)) {
      imports.put(simpleName, canonicalName);
      return true;
    }
    return false;
  }
}
