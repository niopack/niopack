package org.niopack.nio.compiler.language.php.model;

import java.util.List;
import org.niopack.nio.core.HttpVerb;

public final class PhpRestMethod {
  private final String name;
  private final String path;
  private final HttpVerb verb;
  private final String requestType;
  private final List<PhpStructureField> requestFields;
  private final String responseType;

  public PhpRestMethod(String name, String path, HttpVerb verb, String requestType,
      List<PhpStructureField> requestFields, String responseType) {
    this.name = name;
    this.path = path;
    this.verb = verb;
    this.requestType = requestType;
    this.requestFields = requestFields;
    this.responseType = responseType;
  }

  public String getName() {
    return name;
  }

  public String getPath() {
    return path;
  }

  public HttpVerb getVerb() {
    return verb;
  }

  public String getRequestType() {
    return requestType;
  }

  public List<PhpStructureField> getRequestFields() {
    return requestFields;
  }

  public String getResponseType() {
    return responseType;
  }
}
