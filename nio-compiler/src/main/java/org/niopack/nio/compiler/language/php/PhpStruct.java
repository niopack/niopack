package org.niopack.nio.compiler.language.php;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.collect.Iterables;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import org.niopack.nio.compiler.annotator.FieldParser;
import org.niopack.nio.compiler.annotator.ProvidedParser;
import org.niopack.nio.compiler.annotator.RestParamParser;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.language.php.model.PhpStructureField;
import org.niopack.nio.compiler.language.php.model.TypeInfo;
import org.niopack.nio.compiler.model.Identifier.Category;
import org.niopack.nio.compiler.model.Structure;
import org.niopack.nio.compiler.model.Symbol;
import org.niopack.nio.core.Field;
import org.niopack.nio.core.FieldStatus;
import org.niopack.nio.core.ParamType;
import org.niopack.nio.core.Provided;
import org.niopack.nio.core.RestParam;

final class PhpStruct extends AbstractPhpSymbol {

  private final Symbol symbol;
  private final SymbolTable table;
  private final Structure definition;

  public PhpStruct(Symbol symbol, SymbolTable table) {
    Category type = Iterables.getLast(symbol.getIdentifiers()).getCategory();
    checkArgument(type == Category.STRUCT, "%s is not a service.", symbol);
    this.symbol = checkNotNull(symbol, "symbol");
    this.table = checkNotNull(table, "table");
    this.definition = table.getDefinition(symbol);
  }

  @Override
  public Symbol getSymbol() {
    return symbol;
  }

  @Override
  public SymbolTable getTable() {
    return table;
  }

  @Override @SuppressWarnings("unchecked")
  public <T> T getDefinition() {
    return (T) definition;
  }

  @Override
  public Category getCategory() {
    return Category.STRUCT;
  }

  @Override
  public boolean isProvided() {
    Provided annotation = new ProvidedParser(definition, table).getAnnotation();
    return annotation != null && annotation.isProvided();
  }

  @Override
  public void write(Configuration cfg, Writer writer) throws TemplateException, IOException {
    if (isProvided()) {
      return;
    }
    Template template = cfg.getTemplate("php/struct.php.ftl");
    template.process(getDataModel(), writer);
  }

  @Override
  public Map<String, Object> getDataModel() {
    Map<String, Object> dataModel = new HashMap<>();
    dataModel.put("namespace", getNamespace());
    dataModel.put("typeName", getSimpleName());

    // Simple class name -> Fully qualified class name
    Map<String, String> imports = new HashMap<>();
    imports.put("JsonSerializable", "\\JsonSerializable");

    TypeResolver resolver = new TypeResolver(table, getNamespace(), imports);
    List<PhpStructureField> fields = definition.getFields().values().stream()
        .filter(field -> {
          FieldStatus status = FieldStatus.ACTIVE;
          Field annotation = new FieldParser(field, table).getAnnotation();
          if (annotation != null && annotation.getStatus() != null) {
            status = annotation.getStatus();
          }
          return status != FieldStatus.RETIRED;
        })
        .map(field -> {
          String fieldName = field.getName();
          TypeInfo typeInfo = resolver.resolve(field.getType());
          RestParam restParam = new RestParamParser(field, table).getAnnotation();
          ParamType restParamType = null;
          String restParamName = null;
          if (restParam != null && restParam.getType() != null){
            restParamType = restParam.getType();
            restParamName = restParam.getName() != null ? restParam.getName() : fieldName;
          }
          return new PhpStructureField(fieldName, typeInfo, restParamType, restParamName);
        })
        .collect(Collectors.toList());
    dataModel.put("fields", fields);
    dataModel.put("arrayField", fields.stream().anyMatch(field -> field.getTypeInfo().isArray()));
    Set<ParamType> restParamTypes = fields.stream()
        .filter(field -> field.getRestParamType() != null)
        .map(field -> field.getRestParamType())
        .collect(Collectors.toSet());
    dataModel.put("restParamTypes", restParamTypes);
    dataModel.put("imports", new TreeSet<>(imports.values()));
    return dataModel;
  }
}
