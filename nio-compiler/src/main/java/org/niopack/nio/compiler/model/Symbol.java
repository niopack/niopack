package org.niopack.nio.compiler.model;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableList;
import java.util.List;

/**
 * Representation of a Nio symbol.
 */
public final class Symbol {

  public static final String DOT = ".";

  private final ImmutableList<Identifier> identifiers;
  //TODO(tamal): WARN! output is not used in hashcode calculation or for equality checks.
  private final boolean output;

  public Symbol(Identifier identifier, boolean output) {
    this.identifiers = ImmutableList.of(identifier);
    this.output = output;
  }

  public Symbol(Iterable<Identifier> identifiers, boolean output) {
    this.identifiers = ImmutableList.copyOf(checkNotNull(identifiers, "identifiers"));
    this.output = output;
  }

  public String getCanonicalName() {
    StringBuilder sb = new StringBuilder();
    for (Identifier identifier : identifiers) {
      sb.append(DOT);
      sb.append(identifier.getName());
    }
    return sb.toString();
  }

  public List<Identifier> getIdentifiers() {
    return identifiers;
  }

  public boolean isOutput() {
    return output;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(identifiers);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Symbol)) {
      return false;
    }
    Symbol that = (Symbol) obj;
    return Objects.equal(this.identifiers, that.identifiers);
  }

  @Override
  public String toString() {
    return getCanonicalName();
  }
}
