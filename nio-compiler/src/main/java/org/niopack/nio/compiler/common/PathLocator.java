package org.niopack.nio.compiler.common;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.base.Throwables;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.FileSystem;
import java.nio.file.FileSystemAlreadyExistsException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;

public final class PathLocator {

  private final Path rootDirectory;
  private final Path currentDirectory;

  public PathLocator(Path rootDirectory, Path currentDirectory) {
    this.rootDirectory = checkNotNull(rootDirectory, "rootDirectory");
    this.currentDirectory = checkNotNull(currentDirectory, "currentDirectory");
  }

  public Path getRootDirectory() {
    return rootDirectory;
  }

  public Path getCurrentDirectory() {
    return currentDirectory;
  }

  public Path find(String location) {
    final Path path;
    if (location.startsWith("/org/niopack/nio")) {
      URL url = Thread.currentThread().getContextClassLoader().getResource(location.substring(1));
      checkNotNull(url, "Cannot find %s\n.", location);
      try {
        URI uri = url.toURI();
        String[] parts = url.toURI().toString().split("!");
        FileSystem fs = null;
        try {
          fs = FileSystems.newFileSystem(URI.create(parts[0]), new HashMap<>());
        } catch (FileSystemAlreadyExistsException e) {
          fs = FileSystems.getFileSystem(URI.create(parts[0]));
        } catch (IOException e) {
          throw Throwables.propagate(e);
        }
        checkNotNull(fs, "FileSystem can't be null");
        path = fs.getPath(parts[1]);
      } catch (URISyntaxException e) {
        throw Throwables.propagate(e);
      }
    } else if (location.startsWith("/")) {
      path = rootDirectory.resolve(location.substring(1));
    } else {
      path = currentDirectory.resolve(location);
    }
    checkArgument(Files.isRegularFile(path) & Files.isReadable(path),
        "%s does not exist", path.toString());
    System.out.println("\n>>>>> Path: " + path);
    return path;
  }
}
