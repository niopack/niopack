package org.niopack.nio.compiler.common;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.base.Objects;
import org.niopack.nio.compiler.model.AnnotatedIdentifier;

public final class IdentifierEntry {

  private final AnnotatedIdentifier identifier;
  private final int depth;

  public IdentifierEntry(AnnotatedIdentifier identifier, int depth) {
    this.identifier = checkNotNull(identifier, "identifier");
    this.depth = depth;
  }

  public AnnotatedIdentifier getIdentifier() {
    return identifier;
  }

  public int getDepth() {
    return depth;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(identifier, depth);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof IdentifierEntry)) {
      return false;
    }
    IdentifierEntry that = (IdentifierEntry) obj;
    return Objects.equal(this.identifier, that.identifier)
        && Objects.equal(this.depth, that.depth);
  }

  @Override
  public String toString() {
    return identifier.toString();
  }
}
