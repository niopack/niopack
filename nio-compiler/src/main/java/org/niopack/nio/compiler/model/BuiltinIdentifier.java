package org.niopack.nio.compiler.model;

import org.niopack.nio.compiler.model.type.ScalarTypeLiteral;
import org.niopack.nio.compiler.model.type.TypeLiteral;

public final class BuiltinIdentifier implements Identifier {

  private final String name;
  private final TypeLiteral type;

  public BuiltinIdentifier(TypeLiteral.Type type) {
    this.name = type.getConcreteType();
    this.type = new ScalarTypeLiteral(type);
  }

  public String getName() {
    return name;
  }

  @Override
  public TypeLiteral getType() {
    return type;
  }

  public Category getCategory() {
    return Category.BUILTIN;
  }

  @Override
  public String toString() {
    return getName() + "::" + getCategory();
  }
}
