package org.niopack.nio.compiler.tools;

import static com.google.common.base.Preconditions.checkArgument;
import static java.nio.file.FileVisitResult.CONTINUE;

import com.google.common.base.Throwables;
import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.Version;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Locale;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.BailErrorStrategy;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.niopack.nio.compiler.antlr4.NioBaseListener;
import org.niopack.nio.compiler.antlr4.NioLexer;
import org.niopack.nio.compiler.antlr4.NioParser;
import org.niopack.nio.compiler.common.PathLocator;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.language.java.JavaSymbol;
import org.niopack.nio.compiler.language.java.JavaSymbolFactory;
import org.niopack.nio.compiler.language.php.PhpSymbol;
import org.niopack.nio.compiler.language.php.PhpSymbolFactory;
import org.niopack.nio.compiler.language.swift.SwiftSymbol;
import org.niopack.nio.compiler.language.swift.SwiftSymbolFactory;
import org.niopack.nio.compiler.model.Identifier.Category;
import org.niopack.nio.compiler.model.Symbol;

public final class Tool {

  private static void walkParseTree(InputStream nioStream, NioBaseListener listener) {
    try {
      ANTLRInputStream input = new ANTLRInputStream(nioStream);
      NioLexer lexer = new NioLexer(input);
      CommonTokenStream tokens = new CommonTokenStream(lexer);
      NioParser parser = new NioParser(tokens);
      parser.setErrorHandler(new BailErrorStrategy());

      ParseTree tree = parser.init(); // begin parsing at init rule
      ParseTreeWalker walker = new ParseTreeWalker();
      walker.walk(listener, tree);
    } catch (IOException e) {
      Throwables.propagateIfPossible(e);
    }
  }

  public static SymbolTable loadSymbols(Path rootDirectory, Path nioFile, boolean output) {
    PathLocator pathLocator = new PathLocator(rootDirectory, nioFile.getParent());
    SymbolTableGenerator generator = new SymbolTableGenerator(pathLocator, output);
    try {
      walkParseTree(nioFile.toUri().toURL().openStream(), generator);
    } catch (IOException e) {
      Throwables.propagateIfPossible(e);
    }
    return generator.getSymbolTable();
  }

  public static void main(String[] args) throws IOException, TemplateException {
    Configuration cfg = new Configuration(Configuration.VERSION_2_3_21);
    cfg.setIncompatibleImprovements(new Version(2, 3, 20));
    cfg.setDefaultEncoding("UTF-8");
    cfg.setLocale(Locale.US);
    cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
    cfg.setClassForTemplateLoading(Tool.class, "/codegen/");

    /*
    * args[0] = Language
    * args[1] = root directory for all nio files
    * args[2] = nio folder/file for which generate source code
    * args[3] = output directory to persist generated code.
    */
    final Language language;
    final Path rootDirectory;
    final Path nioPath;
    final Path outDirectory;
    if (args.length == 4) {
      language = Language.valueOf(args[0].toUpperCase());
      rootDirectory = Paths.get(args[1]);
      nioPath = Paths.get(args[2]);
      outDirectory = Paths.get(args[3]);
    } else if (args.length == 1) {
      language = Language.valueOf(args[0].toUpperCase());
      String pwd = System.getProperty("user.dir");
      rootDirectory = Paths.get(pwd).resolve("src/main/nio");
      nioPath = Paths.get(pwd).resolve("src/main/nio");
      outDirectory = Paths.get(pwd).resolve("gen_nio/main/" + language.name().toLowerCase());
    } else {
      throw new IllegalArgumentException("Invalid number of arguments: " + args.length);
    }

    System.out.println("--------------------------------");
    System.out.println("language = " + language);
    System.out.println("rootDirectory = " + rootDirectory);
    System.out.println("nioPath = " + nioPath);
    System.out.println("outDirectory = " + outDirectory);
    System.out.println("--------------------------------");

    deleteDirectoryIfExists(outDirectory);
    createDirectory(outDirectory);
    if (Files.isDirectory(nioPath)) {
      compileFolder(cfg, language, rootDirectory, nioPath, outDirectory);
    } else {
      compile(cfg, language, rootDirectory, nioPath, outDirectory);
    }
    System.exit(0);
  }

  private static void compileFolder(Configuration cfg, Language language, Path rootDirectory,
      Path nioPath, Path outDirectory) throws IOException {
    PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:*.nio");
    Files.walkFileTree(nioPath, new SimpleFileVisitor<Path>() {
      @Override
      public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
        Path name = file.getFileName();
        if (name != null && matcher.matches(name)) {
          try {
            compile(cfg, language, rootDirectory, file, outDirectory);
          } catch (IOException | TemplateException e) {
            throw Throwables.propagate(e);
          }
        }
        return CONTINUE;
      }

      @Override
      public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
        return CONTINUE;
      }

      @Override
      public FileVisitResult visitFileFailed(Path file, IOException exc) {
        System.err.println(exc);
        return CONTINUE;
      }
    });
  }

  private static void deleteDirectoryIfExists(Path outDirectory) throws IOException {
    if (Files.notExists(outDirectory)) {
      return;
    }
    Files.walkFileTree(outDirectory, new SimpleFileVisitor<Path>() {
      @Override
      public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
          throws IOException {
        Files.delete(file);
        return FileVisitResult.CONTINUE;
      }

      @Override
      public FileVisitResult postVisitDirectory(Path dir, IOException exc)
          throws IOException {
        Files.delete(dir);
        return FileVisitResult.CONTINUE;
      }
    });
  }

  private static void createDirectory(Path outDirectory) throws IOException {
    try {
      Files.createDirectory(outDirectory);
    } catch (NoSuchFileException e) {
      Files.createDirectories(outDirectory);
    }
  }

  private static void compile(Configuration cfg, Language language, Path rootDirectory,
      Path nioFile, Path outDirectory) throws IOException, TemplateException {
    checkArgument(Files.isDirectory(rootDirectory));
    checkArgument(Files.isRegularFile(nioFile));
    checkArgument(Files.isDirectory(outDirectory));

    System.out.println("\n>>>>> NIO: " + nioFile);

    SymbolTable table = loadSymbols(rootDirectory, nioFile, true);
    for (Symbol symbol : table.outputSymbols().keySet()) {
      switch (language) {
        case JAVA:
          JavaSymbol javaSymbol = JavaSymbolFactory.create(symbol, table);
          javaSymbol.write(cfg, outDirectory);
          break;
        case PHP:
          PhpSymbol phpSymbol = PhpSymbolFactory.create(symbol, table);
          phpSymbol.write(cfg, outDirectory);
          break;
        case SWIFT:
          SwiftSymbol swiftSymbol = SwiftSymbolFactory.create(symbol, table);
          if (swiftSymbol.getCategory() != Category.SERVICE) {
            swiftSymbol.write(cfg, outDirectory);
          }
          break;
      }
    }
  }

  enum Language {
    JAVA,
    PHP,
    SWIFT
  }

  private Tool() {
  }
}
