package org.niopack.nio.compiler.language.java;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.base.Throwables;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;
import org.niopack.nio.compiler.common.Formatter;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.language.java.model.Annotation;
import org.niopack.nio.compiler.model.Structure;
import org.niopack.nio.compiler.model.StructureField;
import org.niopack.nio.compiler.model.Symbol;
import org.niopack.nio.compiler.model.type.ScalarTypeLiteral;
import org.niopack.nio.compiler.model.type.TypeLiteral;
import org.niopack.nio.compiler.model.type.TypeLiteral.Type;
import org.niopack.nio.compiler.model.value.ScalarValue;
import org.niopack.nio.compiler.model.value.StructureValue;
import org.niopack.nio.compiler.model.value.Value;

public class StructureInstantiator {

  private final TypeResolver resolver;
  private final NameGenerator nameGenerator;

  public StructureInstantiator(TypeResolver resolver, NameGenerator nameGenerator) {
    this.resolver = checkNotNull(resolver, "resolver");
    this.nameGenerator = checkNotNull(nameGenerator, "nameGenerator");
  }

  public Annotation codegen(Configuration cfg, String variableNameSeed, StructureValue structValue) {
    try {
      StringBuilder sb = new StringBuilder();
      String variable = codegenInner(cfg, sb, variableNameSeed, structValue);

      SymbolTable table = resolver.getTable();
      Symbol structSymbol = table.find(structValue.getName());
      checkNotNull(structSymbol, "Can't find symbol for %s", structValue.getName());
      JavaSymbol javaStruct = JavaSymbolFactory.create(structSymbol, table);

      return new Annotation(javaStruct.getSimpleName(), variable, sb.toString());
    } catch (TemplateException | IOException e) {
      throw Throwables.propagate(e);
    }
  }

  /**
   * return variable name
   * @param sb the generated code
   */
  public String codegenInner(Configuration cfg, StringBuilder sb, String variableNameSeed,
      StructureValue structValue) throws TemplateException, IOException {
    checkNotNull(variableNameSeed, "Missing variable name seed");
    checkNotNull(structValue, "Missing structure value.");
    // value.name will be null for inner struct values.
    checkNotNull(structValue.getName(), "Missing structure type name.");

    SymbolTable table = resolver.getTable();
    Symbol structSymbol = table.find(structValue.getName());
    checkNotNull(structSymbol, "Can't find symbol for %s", structValue.getName());
    JavaSymbol javaStruct = JavaSymbolFactory.create(structSymbol, table);

    String name = resolver.resolve(new ScalarTypeLiteral(Type.USER_DEFINED, structValue.getName()));
    String variableName = nameGenerator.getName(variableNameSeed);
    Map<String, String> assignments = new HashMap<>();
    for (String fieldName : structValue.getFields()) {
      Structure definition = javaStruct.getDefinition();
      StructureField field = definition.getField(fieldName);

      Value sv = structValue.getValue(fieldName);
      compatibleTypes(field.getType().getType(), sv.getType());

      if (sv instanceof StructureValue) {
        // call recursively
        ((StructureValue) sv).setName(((ScalarTypeLiteral) field.getType()).getTypeName());
        codegenInner(cfg, sb, fieldName, structValue);
      } else {
        ScalarValue scalarValue = (ScalarValue) sv;
        switch (sv.getType()) {
          case BOOL:
          case CHAR:
          case FLOAT:
          case INT:
            assignments.put(Formatter.toUpperCamel(fieldName), scalarValue.toString());
            break;
          case STRING:
            assignments
                .put(Formatter.toUpperCamel(fieldName), "\"" + scalarValue.toString() + "\"");
            break;
          case ENUM:
            assignments.put(
                Formatter.toUpperCamel(fieldName),
                resolver.resolve(field.getType()) + Symbol.DOT + scalarValue.toString());
            break;
          default:
            throw new IllegalStateException("Must not arrive here.");
        }
      }
    }

    // generate java code
    Map<String, Object> dataModel = new HashMap<>();
    dataModel.put("name", name);
    dataModel.put("variableName", variableName);
    dataModel.put("assignments", assignments);

    Writer writer = new StringWriter();
    Template template = cfg.getTemplate("java/struct_instantiation.java.ftl");
    template.process(dataModel, writer);
    sb.append(writer.toString());

    return variableName;
  }

  private boolean compatibleTypes(TypeLiteral.Type typeLiteral, Value.Type valueType) {
    switch (typeLiteral) {
      case BOOL:
        return valueType == Value.Type.BOOL;
      case CHAR:
        return valueType == Value.Type.CHAR;
      case BYTES:
      case STRING:
        return valueType == Value.Type.STRING;
      case FLOAT:
      case DOUBLE:
        return valueType == Value.Type.FLOAT;
      case INT8:
      case INT16:
      case INT32:
      case INT64:
      case USEC:
      case MSEC:
        return valueType == Value.Type.INT;
      case USER_DEFINED:
        return valueType == Value.Type.ENUM || valueType == Value.Type.STRUCT;
      default:
        throw new IllegalStateException(String.format(
            "Value of type %s is incompatible with type literal %s.", valueType, typeLiteral));
    }
  }
}
