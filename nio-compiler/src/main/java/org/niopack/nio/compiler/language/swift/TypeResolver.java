package org.niopack.nio.compiler.language.swift;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.collect.ImmutableSet;
import java.util.Set;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.language.swift.model.TypeInfo;
import org.niopack.nio.compiler.model.Symbol;
import org.niopack.nio.compiler.model.type.ListTypeLiteral;
import org.niopack.nio.compiler.model.type.MapTypeLiteral;
import org.niopack.nio.compiler.model.type.ScalarTypeLiteral;
import org.niopack.nio.compiler.model.type.TypeLiteral;
import org.niopack.nio.compiler.model.type.TypeLiteral.Type;

final class TypeResolver {

  public static final Set<String> PREDEFINED_MODULES = ImmutableSet.of("Foundation");

  private final SymbolTable table;
  private final String module;

  public TypeResolver(SymbolTable table, String module) {
    this.table = checkNotNull(table, "table");
    this.module = checkNotNull(module, "module");
  }

  public SymbolTable getTable() {
    return table;
  }

  public String getModule() {
    return module;
  }

  public TypeInfo resolve(TypeLiteral type) {
    if (type instanceof ScalarTypeLiteral) {
      return resolve((ScalarTypeLiteral) type);
    } else if (type instanceof ListTypeLiteral) {
      return resolve((ListTypeLiteral) type);
    } else if (type instanceof MapTypeLiteral) {
      return resolve((MapTypeLiteral) type);
    } else {
      throw new IllegalStateException("Unknown typeLiteral = " + type.getClass());
    }
  }

  private TypeInfo resolve(ScalarTypeLiteral type) {
    TypeInfo typeInfo = new TypeInfo();
    typeInfo.setFieldType(getType(type));
    typeInfo.setFieldTypeName(getTypeName(type));
    return typeInfo;
  }

  private TypeInfo resolve(ListTypeLiteral type) {
    TypeInfo typeInfo = new TypeInfo();
    typeInfo.setFieldType(Type.LIST);
    typeInfo.setKeyType(getType(type.getElementType()));
    typeInfo.setKeyTypeName(getTypeName(type.getElementType()));
    return typeInfo;
  }

  private TypeInfo resolve(MapTypeLiteral type) {
    TypeInfo typeInfo = new TypeInfo();
    typeInfo.setFieldType(Type.MAP);
    typeInfo.setKeyType(getType(type.getKeyType()));
    typeInfo.setKeyTypeName(getTypeName(type.getKeyType()));
    typeInfo.setValueType(getType(type.getValueType()));
    typeInfo.setValueTypeName(getTypeName(type.getValueType()));
    return typeInfo;
  }

  private String getTypeName(ScalarTypeLiteral type) {
    Symbol symbol = table.find(type.getTypeName());
    checkNotNull(symbol, "can't find symbol %s.", type.getTypeName());
    SwiftSymbol swiftSymbol = SwiftSymbolFactory.create(symbol, table);
    if (useSimpleName(swiftSymbol)) {
      return swiftSymbol.getSimpleName();
    } else {
      return swiftSymbol.getCanonicalName();
    }
  }

  private Type getType(ScalarTypeLiteral type) {
    if (type.getType() == Type.USER_DEFINED) {
      Symbol symbol = table.find(type.getTypeName());
      checkNotNull(symbol, "can't find symbol %s.", type.getTypeName());
      SwiftSymbol swiftSymbol = SwiftSymbolFactory.create(symbol, table);
      switch(swiftSymbol.getCategory()) {
        case ENUM:
          return Type.ENUM;
        case STRUCT:
          return Type.STRUCT;
        default:
          throw new IllegalStateException("UseDefined type with infeasible category");
      }
    }
    return type.getType();
  }

  /**
   * Use simple name if either of the following is true:
   * <ul>
   * <li>It is of a Swift built-in type</li>
   * <li>It is of one of the predefined types</li>
   * <li>It is from the current module</li>
   * </ul>
   */
  private boolean useSimpleName(SwiftSymbol swiftSymbol) {
    String swiftSymbolModule = swiftSymbol.getModule();
    return "Void".equals(swiftSymbol.getSimpleName())
     || PREDEFINED_MODULES.contains(swiftSymbolModule)
     || "".equals(swiftSymbolModule)
     || module.equals(swiftSymbolModule);
  }
}
