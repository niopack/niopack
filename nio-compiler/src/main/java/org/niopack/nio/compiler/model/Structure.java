package org.niopack.nio.compiler.model;

import static com.google.common.base.Preconditions.checkArgument;

import com.google.common.collect.ImmutableMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.niopack.nio.compiler.model.type.ScalarTypeLiteral;
import org.niopack.nio.compiler.model.type.TypeLiteral.Type;

public final class Structure extends AbstractAnnotatedIdentifier {

  private Map<String, StructureField> fields = new LinkedHashMap<>();

  public Structure(String name) {
    super(name, new ScalarTypeLiteral(Type.USER_DEFINED, name));
  }

  @Override
  public Category getCategory() {
    return Category.STRUCT;
  }

  public Map<String, StructureField> getFields() {
    return ImmutableMap.copyOf(fields);
  }

  public Set<String> getFieldNames() {
    return fields.keySet();
  }

  public StructureField getField(String fieldName) {
    return fields.get(fieldName);
  }

  public void addField(StructureField field) {
    checkArgument(!fields.containsKey(field.getName()), "Duplicate field %s,", field.getName());
    this.fields.put(field.getName(), field);
  }

  @Override
  public String toString() {
    return getName() + "::" + getCategory() + "::" + getAnnotations() + "::" + fields;
  }
}
