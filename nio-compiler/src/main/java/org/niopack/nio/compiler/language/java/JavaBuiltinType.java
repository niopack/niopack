package org.niopack.nio.compiler.language.java;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.collect.Iterables;
import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Path;
import java.util.Map;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.model.Identifier.Category;
import org.niopack.nio.compiler.model.Symbol;
import org.niopack.nio.compiler.model.type.TypeLiteral.Type;

final class JavaBuiltinType implements JavaSymbol {

  private final Symbol symbol;
  private final SymbolTable table;

  public JavaBuiltinType(Symbol symbol, SymbolTable table) {
    Category type = Iterables.getLast(symbol.getIdentifiers()).getCategory();
    checkArgument(type == Category.BUILTIN, "%s is not a built-in symbol.", symbol);
    this.symbol = checkNotNull(symbol, "symbol");
    this.table = checkNotNull(table, "table");
  }

  public SymbolTable getTable() {
    return table;
  }

  @Override
  public String getPackageName() {
    return "java.lang";
  }

  @Override @SuppressWarnings("unchecked")
  public <T> T getDefinition() {
    return (T) SymbolTable.EMPTY_DEFINITION;
  }

  public Type getType() {
    return Iterables.getLast(symbol.getIdentifiers()).getType().getType();
  }

  @Override
  public String getCanonicalName() {
    switch (getType()) {
      case BOOL:
        return "java.lang.Boolean";
      case BYTES:
        return "java.lang.Byte[]";
      case CHAR:
        return "java.lang.Character";
      case STRING:
        return "java.lang.String";
      case FLOAT:
        return "java.lang.Float";
      case DOUBLE:
        return "java.lang.Double";
      case INT8:
        return "java.lang.Byte";
      case INT16:
        return "java.lang.Short";
      case INT32:
        return "java.lang.Integer";
      case INT64:
        return "java.lang.Long";
      case USEC:
        return "java.lang.Long";
      case MSEC:
        return "java.lang.Long";
      case VOID:
        return "void";
      default:
        throw new IllegalStateException("");
    }
  }

  @Override
  public String getSimpleName() {
    switch (getType()) {
      case BOOL:
        return "Boolean";
      case BYTES:
        return "Byte[]";
      case CHAR:
        return "Character";
      case STRING:
        return "String";
      case FLOAT:
        return "Float";
      case DOUBLE:
        return "Double";
      case INT8:
        return "Byte";
      case INT16:
        return "Short";
      case INT32:
        return "Integer";
      case INT64:
        return "Long";
      case USEC:
        return "Long";
      case MSEC:
        return "Long";
      case VOID:
        return "void";
      default:
        throw new IllegalStateException("");
    }
  }

  @Override
  public Symbol getSymbol() {
    return symbol;
  }

  @Override
  public Category getCategory() {
    return Category.BUILTIN;
  }

  @Override
  public Map<String, Object> getDataModel() {
    throw new UnsupportedOperationException("Can't generate source data model");
  }

  @Override
  public void write(Configuration cfg, Writer writer) throws TemplateException, IOException {
    throw new UnsupportedOperationException("Can't write Source file for built-in types.");
  }

  @Override
  public void write(Configuration cfg, Path outputDirectory)
      throws TemplateException, IOException {
    throw new UnsupportedOperationException("Can't write Source file for built-in types.");
  }
}
