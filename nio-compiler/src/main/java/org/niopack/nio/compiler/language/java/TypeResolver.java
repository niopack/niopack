package org.niopack.nio.compiler.language.java;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.collect.ImmutableMap;
import java.util.Map;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.model.Symbol;
import org.niopack.nio.compiler.model.type.ListTypeLiteral;
import org.niopack.nio.compiler.model.type.MapTypeLiteral;
import org.niopack.nio.compiler.model.type.ScalarTypeLiteral;
import org.niopack.nio.compiler.model.type.TypeLiteral;

final class TypeResolver {

  private static final Map<String, String> PRIMITIVES = ImmutableMap.<String, String>builder()
      .put("Boolean", "boolean")
      .put("Char", "char")
      .put("Byte", "byte")
      .put("Short", "short")
      .put("Integer", "int")
      .put("Long", "long")
      .put("Float", "float")
      .put("Double", "double")
      .build();

  // JAX-RS annotation types
  private static final Map<String, String> KNOWN_TYPES = ImmutableMap.<String, String>builder()
      .put("HttpVerb.GET", "javax.ws.rs.GET")
      .put("HttpVerb.POST", "javax.ws.rs.POST")
      .put("HttpVerb.PUT", "javax.ws.rs.PUT")
      .put("HttpVerb.DELETE", "javax.ws.rs.DELETE")
      .put("ParamType.QUERY", "javax.ws.rs.QueryParam")
      .put("ParamType.FORM", "javax.ws.rs.FormParam")
      .put("ParamType.PATH", "javax.ws.rs.PathParam")
      .build();

  private static final String JAVA_LANG_PACKAGE = "java.lang";

  private static final String LIST_SIMPLE_NAME = "List";
  private static final String LIST_CANONICAL_NAME = "java.util.List";

  private static final String MAP_SIMPLE_NAME = "Map";
  private static final String MAP_CANONICAL_NAME = "java.util.Map";

  private final SymbolTable table;
  private final String javaPackageName;
  // SimpleName -> CanonicalName
  private final Map<String, String> imports;

  public TypeResolver(SymbolTable table, String javaPackageName, Map<String, String> imports) {
    this.table = checkNotNull(table, "table");
    this.javaPackageName = checkNotNull(javaPackageName, "javaPackageName");
    this.imports = checkNotNull(imports, "imports");
  }

  public SymbolTable getTable() {
    return table;
  }

  public Map<String, String> getImports() {
    return imports;
  }

  public String resolve(TypeLiteral type) {
    StringBuilder sb = new StringBuilder();
    resolve(type, sb);
    String typeLiteral = sb.toString();
    String primitive = PRIMITIVES.get(typeLiteral);
    return primitive != null ? primitive : typeLiteral;
  }

  public <E extends Enum<E>>  String resolve(Class<E> klass, E value) {
    String key = klass.getSimpleName() + JavaSymbol.DOT + value.name();
    String canonicalName = KNOWN_TYPES.get(key);
    checkNotNull(canonicalName, "Unknown type for key %s.", key);
    String simpleName = canonicalName.substring(canonicalName.lastIndexOf(JavaSymbol.DOT) + 1);
    return addImport(simpleName, canonicalName) ? simpleName : canonicalName;
  }

  private void resolve(TypeLiteral type, StringBuilder sb) {
    if (type instanceof ScalarTypeLiteral) {
      resolve((ScalarTypeLiteral) type, sb);
    } else if (type instanceof ListTypeLiteral) {
      resolve((ListTypeLiteral) type, sb);
    } else if (type instanceof MapTypeLiteral) {
      resovle((MapTypeLiteral) type, sb);
    } else {
      throw new IllegalStateException("Unknown typeLiteral = " + type.getClass());
    }
  }

  private void resolve(ScalarTypeLiteral type, StringBuilder sb) {
    Symbol symbol = table.find(type.getTypeName());
    checkNotNull(symbol, "can't find symbol %s.", type.getTypeName());
    JavaSymbol javaSymbol = JavaSymbolFactory.create(symbol, table);
    if (useSimpleName(javaSymbol.getSimpleName(), javaSymbol.getCanonicalName())) {
      addImport(javaSymbol.getSimpleName(), javaSymbol.getCanonicalName());
      sb.append(javaSymbol.getSimpleName());
    } else {
      sb.append(javaSymbol.getCanonicalName());
    }
  }

  private void resolve(ListTypeLiteral type, StringBuilder sb) {
    if (useSimpleName(LIST_SIMPLE_NAME, LIST_CANONICAL_NAME)) {
      addImport(LIST_SIMPLE_NAME, LIST_CANONICAL_NAME);
      sb.append(LIST_SIMPLE_NAME + "<");
    } else {
      sb.append(LIST_CANONICAL_NAME + "<");
    }
    resolve(type.getElementType(), sb);
    sb.append(">");
  }

  private void resovle(MapTypeLiteral type, StringBuilder sb) {
    if (useSimpleName(MAP_SIMPLE_NAME, MAP_CANONICAL_NAME)) {
      addImport(MAP_SIMPLE_NAME, MAP_CANONICAL_NAME);
      sb.append(MAP_SIMPLE_NAME + "<");
    } else {
      sb.append(MAP_CANONICAL_NAME + "<");
    }
    resolve(type.getKeyType(), sb);
    sb.append(", ");
    resolve(type.getValueType(), sb);
    sb.append(">");
  }

  private boolean useSimpleName(String simpleName, String canonicalName) {
    String existingCanonicalName = imports.get(simpleName);
    if ("void".equals(simpleName)) {
      return false; // so that void is not imported.
    } else if (existingCanonicalName == null) {
      return true;
    } else if (canonicalName.equals(existingCanonicalName)) {
      return true;
    } else {
      // another class with SimpleName List has already been imported,
      // so we need to use canonical name
      return false;
    }
  }

  /**
   * Returns true if import was added.
   */
  public boolean addImport(String simpleName, String canonicalName) {
    checkArgument(canonicalName.endsWith(simpleName),
        "%s is not a simple name for %s", simpleName, canonicalName);
    // canonicalName = containerName + DOT + simpleName
    String containerName = canonicalName.equals(simpleName)
        ? ""
        : canonicalName.substring(0, canonicalName.length() - simpleName.length() - 1);
    if (!JAVA_LANG_PACKAGE.equals(containerName) && !javaPackageName.equals(containerName)) {
      imports.put(simpleName, canonicalName);
      return true;
    }
    return false;
  }
}
