package org.niopack.nio.compiler.language.swift;

import com.google.common.collect.Iterables;
import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.stream.Collectors;
import org.niopack.nio.compiler.annotator.SwiftParser;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.model.Identifier.Category;
import org.niopack.nio.compiler.model.Package;
import org.niopack.nio.compiler.model.Symbol;
import org.niopack.nio.core.Swift;

public interface SwiftSymbol {

  public static final String DOT = ".";

  Symbol getSymbol();
  SymbolTable getTable();
  <T> T getDefinition();

  default String getModule() {
    Package pkg = (Package) Iterables.getFirst(getSymbol().getIdentifiers(), null);
    if (pkg != null) {
      Swift swift = new SwiftParser(pkg, getTable()).getAnnotation();
      if (swift != null) {
        String module = swift.getModule();
        if (module != null) {
          return module;
        }
      }
    }
    return "";
  }

  default String getCanonicalName() {
    String subName = getSymbol().getIdentifiers().stream()
        .skip(1)
        .map(id -> id.getName())
        .collect(Collectors.joining(DOT));
    String packageName = getModule();
    if (packageName.isEmpty()) {
      return subName;
    } else {
      return packageName + DOT + subName;
    }
  }

  default String getSimpleName() {
    return Iterables.getLast(getSymbol().getIdentifiers()).getName();
  }

  Category getCategory();

  default boolean isProvided() {
    return false;
  }

  Map<String, Object> getDataModel();

  void write(Configuration cfg, Writer writer) throws TemplateException, IOException;

  default void write(Configuration cfg, Path outputDirectory)
      throws TemplateException, IOException {
    if (isProvided()) {
      return;
    }
    String relativePath = getCanonicalName().replace(DOT, File.separator) + ".swift";
    Path outputFile = outputDirectory.resolve(relativePath);
    if (Files.notExists(outputFile.getParent())) {
      Files.createDirectories(outputFile.getParent());
    }
    write(cfg, Files.newBufferedWriter(outputFile, StandardCharsets.UTF_8));
  }
}
