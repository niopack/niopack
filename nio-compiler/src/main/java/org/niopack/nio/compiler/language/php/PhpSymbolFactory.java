package org.niopack.nio.compiler.language.php;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import java.util.Set;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.model.Identifier.Category;
import org.niopack.nio.compiler.model.Symbol;

public final class PhpSymbolFactory {

  public static final Set<String> KEYWORDS;

  static {
    // Source: http://php.net/manual/en/reserved.keywords.php
    ImmutableSet.Builder<String> builder = ImmutableSet.builder();
    builder.add("__CLASS__", "__DIR__", "__FILE__", "__FUNCTION__", "__LINE__", "__METHOD__",
        "__NAMESPACE__", "__TRAIT__", "abstract", "and", "as", "break", "callable", "case", "catch",
        "class", "clone", "const", "continue", "declare", "default", "do", "echo", "else", "elseif",
        "enddeclare", "endfor", "endforeach", "endif", "endswitch", "endwhile", "extends", "final",
        "finally", "for", "foreach", "function", "global", "goto", "if", "implements", "include",
        "include_once", "instanceof", "insteadof", "interface", "namespace", "new", "or", "parent",
        "print", "private", "protected", "public", "require", "require_once", "return", "self",
        "static", "static", "switch", "throw", "trait", "try", "use", "var", "while", "xor",
        "yield");
    KEYWORDS = builder.build();
  }

  public static PhpSymbol create(Symbol symbol, SymbolTable table) {
    Category category = Iterables.getLast(symbol.getIdentifiers()).getCategory();
    switch (category) {
      case ENUM:
        return new PhpEnum(symbol, table);
      case STRUCT:
        return new PhpStruct(symbol, table);
      case SERVICE:
        return new PhpService(symbol, table);
      default:
        throw new UnsupportedOperationException(
            String.format("Can't generate PHP code for symbol %s", symbol));
    }
  }

}
