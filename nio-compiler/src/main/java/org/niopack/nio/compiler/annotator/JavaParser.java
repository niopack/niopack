package org.niopack.nio.compiler.annotator;

import static org.niopack.nio.compiler.common.Formatter.unescape;

import com.google.common.collect.ImmutableSet;
import java.util.IllegalFormatException;
import java.util.Set;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.model.AnnotatedIdentifier;
import org.niopack.nio.compiler.model.Identifier.Category;
import org.niopack.nio.compiler.model.value.StructureValue;
import org.niopack.nio.core.Java;
import org.niopack.nio.core.Java_Nio;

public class JavaParser extends AbstractAnnotationParser<Java> {

  public JavaParser(AnnotatedIdentifier identifier, SymbolTable table) {
    super(identifier, table);
  }

  @Override
  protected Set<Category> getSupportedCategories() {
    return ImmutableSet.of(Category.PACKAGE);
  }

  @Override
  public String getCanonicalName() {
    return ".org.niopack.nio.core.Java";
  }

  @Override
  protected Java parse(StructureValue value) throws IllegalFormatException {
    Java annotation = new Java();
    if (value.hasValue(Java_Nio.PACKAGE_NAME)) {
      annotation.setPackageName(unescape(value.scalarValue(Java_Nio.PACKAGE_NAME).toString()));
    }
    return annotation;
  }
}
