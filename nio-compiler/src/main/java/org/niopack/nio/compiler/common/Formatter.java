package org.niopack.nio.compiler.common;

import static com.google.common.base.CaseFormat.LOWER_CAMEL;
import static com.google.common.base.CaseFormat.LOWER_HYPHEN;
import static com.google.common.base.CaseFormat.LOWER_UNDERSCORE;
import static com.google.common.base.CaseFormat.UPPER_CAMEL;
import static com.google.common.base.CaseFormat.UPPER_UNDERSCORE;
import static com.google.common.base.Preconditions.checkNotNull;

import org.apache.commons.lang3.StringEscapeUtils;

public final class Formatter {

  public static String toUpperUnderscore(String name) {
    checkNotNull(name, "name");
    try {
      return LOWER_CAMEL.to(UPPER_UNDERSCORE, name);
    } catch (Exception e1) {
      try {
        return LOWER_HYPHEN.to(UPPER_UNDERSCORE, name);
      } catch (Exception e2) {
        try {
          return LOWER_UNDERSCORE.to(UPPER_UNDERSCORE, name);
        } catch (Exception e3) {
          try {
            return UPPER_CAMEL.to(UPPER_UNDERSCORE, name);
          } catch (Exception e4) {
            if (name.isEmpty()) {
              return "";
            } else {
              return String.valueOf(name.charAt(0)).toUpperCase() + name.substring(1);
            }
          }
        }
      }
    }
  }

  public static String toUpperCamel(String name) {
    checkNotNull(name, "name");
    try {
      return LOWER_CAMEL.to(UPPER_CAMEL, name);
    } catch (Exception e1) {
      try {
        return LOWER_HYPHEN.to(UPPER_CAMEL, name);
      } catch (Exception e2) {
        try {
          return LOWER_UNDERSCORE.to(UPPER_CAMEL, name);
        } catch (Exception e3) {
          try {
            return UPPER_UNDERSCORE.to(UPPER_CAMEL, name);
          } catch (Exception e4) {
            if (name.isEmpty()) {
              return "";
            } else {
              return String.valueOf(name.charAt(0)).toUpperCase() + name.substring(1);
            }
          }
        }
      }
    }
  }

  // TODO(tamal): Needs to be properly unescaped.
  public static String unescape(String value) {
    return StringEscapeUtils.unescapeJava(value);
  }

  private Formatter() {}
}
