package org.niopack.nio.compiler.language.php;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.niopack.nio.compiler.annotator.FieldParser;
import org.niopack.nio.compiler.annotator.ProvidedParser;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.language.php.model.PhpEnumerationField;
import org.niopack.nio.compiler.model.Enumeration;
import org.niopack.nio.compiler.model.Identifier.Category;
import org.niopack.nio.compiler.model.Symbol;
import org.niopack.nio.core.Field;
import org.niopack.nio.core.FieldStatus;
import org.niopack.nio.core.Provided;

final class PhpEnum extends AbstractPhpSymbol {

  private final Symbol symbol;
  private final SymbolTable table;
  private final Enumeration definition;

  public PhpEnum(Symbol symbol, SymbolTable table) {
    Category type = Iterables.getLast(symbol.getIdentifiers()).getCategory();
    checkArgument(type == Category.ENUM, "%s is not an enum.", symbol);
    this.symbol = checkNotNull(symbol, "symbol");
    this.table = checkNotNull(table, "table");
    this.definition = table.getDefinition(symbol);
  }

  @Override
  public Symbol getSymbol() {
    return symbol;
  }

  @Override
  public SymbolTable getTable() {
    return table;
  }

  @Override @SuppressWarnings("unchecked")
  public <T> T getDefinition() {
    return (T) definition;
  }

  @Override
  public Category getCategory() {
    return Category.ENUM;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(symbol);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof PhpEnum)) {
      return false;
    }
    PhpEnum that = (PhpEnum) obj;
    return Objects.equal(this.symbol, that.symbol);
  }

  @Override
  public String toString() {
    return symbol.getCanonicalName();
  }

  @Override
  public boolean isProvided() {
    Provided annotation = new ProvidedParser(definition, table).getAnnotation();
    return annotation != null && annotation.isProvided();
  }

  @Override
  public void write(Configuration cfg, Writer writer) throws TemplateException, IOException {
    if (isProvided()) {
      return;
    }
    Template template = cfg.getTemplate("php/enum.php.ftl");
    template.process(getDataModel(), writer);
  }

  @Override
  public Map<String, Object> getDataModel() {
    Map<String, Object> dataModel = new HashMap<>();
    dataModel.put("namespace", getNamespace());
    dataModel.put("typeName", getSimpleName());

    List<PhpEnumerationField> fields = definition.getFields().values().stream()
        .filter(field -> {
          FieldStatus status = FieldStatus.ACTIVE;
          Field annotation = new FieldParser(field, table).getAnnotation();
          if (annotation != null && annotation.getStatus() != null) {
            status = annotation.getStatus();
          }
          return status != FieldStatus.RETIRED;
        })
        .map(field -> new PhpEnumerationField(field.getName(), field.getValue()))
        .collect(Collectors.toList());
    dataModel.put("fields", fields);
    return dataModel;
  }
}
