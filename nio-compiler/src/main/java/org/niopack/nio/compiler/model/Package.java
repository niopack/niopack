package org.niopack.nio.compiler.model;

import org.niopack.nio.compiler.model.type.ScalarTypeLiteral;
import org.niopack.nio.compiler.model.type.TypeLiteral.Type;

public final class Package extends AbstractAnnotatedIdentifier {

  public Package(String name) {
    super(name, new ScalarTypeLiteral(Type.NONE, name));
  }

  @Override
  public Category getCategory() {
    return Category.PACKAGE;
  }
}
