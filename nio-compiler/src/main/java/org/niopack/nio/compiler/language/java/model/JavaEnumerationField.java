package org.niopack.nio.compiler.language.java.model;

public final class JavaEnumerationField {
  private final String name;
  private final String javaType;
  private final int value;

  public JavaEnumerationField(String name, String javaType, int value) {
    this.name = name;
    this.javaType = javaType;
    this.value = value;
  }

  public String getNameConstant() {
    return name.toUpperCase();
  }

  public String getName() {
    return name;
  }

  public String getJavaType() {
    return javaType;
  }

  public int getValue() {
    return value;
  }
}
