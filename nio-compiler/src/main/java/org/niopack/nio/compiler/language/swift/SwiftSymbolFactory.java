package org.niopack.nio.compiler.language.swift;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import java.util.Set;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.model.Identifier.Category;
import org.niopack.nio.compiler.model.Symbol;

public final class SwiftSymbolFactory {

  public static final Set<String> KEYWORDS;

  static {
    // Source: http://bit.ly/1DTUw08
    ImmutableSet.Builder<String> builder = ImmutableSet.builder();
    builder.add("class", "deinit", "enum", "extension", "func", "import", "init", "internal", "let",
        "operator", "private", "protocol", "public", "static", "struct", "subscript", "typealias",
        "var", "break", "case", "continue", "default", "do", "else", "fallthrough", "for", "if",
        "in", "return", "switch", "where", "while", "as", "dynamicType", "false", "is", "nil",
        "self", "Self", "super", "true", "__COLUMN__", "__FILE__", "__FUNCTION__", "__LINE__",
        "associativity", "convenience", "dynamic", "didSet", "final", "get", "infix", "inout",
        "lazy", "left", "mutating", "none", "nonmutating", "optional", "override", "postfix",
        "precedence", "prefix", "Protocol", "required", "right", "set", "Type", "unowned", "weak",
        "willSet");
    KEYWORDS = builder.build();
  }

  public static SwiftSymbol create(Symbol symbol, SymbolTable table) {
    Category category = Iterables.getLast(symbol.getIdentifiers()).getCategory();
    switch (category) {
      case ENUM:
        return new SwiftEnum(symbol, table);
      case STRUCT:
        return new SwiftStruct(symbol, table);
      case SERVICE:
        return new SwiftService(symbol, table);
      case BUILTIN:
        return new SwiftBuiltinType(symbol, table);
      default:
        throw new UnsupportedOperationException(
            String.format("Can't generate java code for symbol %s", symbol));
    }
  }

}
