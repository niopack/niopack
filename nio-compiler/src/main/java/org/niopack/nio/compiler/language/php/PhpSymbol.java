package org.niopack.nio.compiler.language.php;

import com.google.common.collect.Iterables;
import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.stream.Collectors;
import org.niopack.nio.compiler.annotator.PhpParser;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.model.Identifier.Category;
import org.niopack.nio.compiler.model.Package;
import org.niopack.nio.compiler.model.Symbol;
import org.niopack.nio.core.Php;

public interface PhpSymbol {

  public static final String BS = "\\";

  Symbol getSymbol();
  SymbolTable getTable();
  <T> T getDefinition();

  default String getNamespace() {
    Package pkg = (Package) Iterables.getFirst(getSymbol().getIdentifiers(), null);
    if (pkg != null) {
      Php php = new PhpParser(pkg, getTable()).getAnnotation();
      if (php != null) {
        String namespace = php.getNamespace();
        if (namespace != null) {
          return namespace;
        }
      }
    }
    return "";
  }

  default String getCanonicalName() {
    String subName = getSymbol().getIdentifiers().stream()
        .skip(1)
        .map(id -> id.getName())
        .collect(Collectors.joining(BS));
    String packageName = getNamespace();
    if (packageName.isEmpty()) {
      return BS + subName;
    } else {
      return BS + packageName + BS + subName;
    }
  }

  default String getSimpleName() {
    return Iterables.getLast(getSymbol().getIdentifiers()).getName();
  }

  Category getCategory();

  default boolean isProvided() {
    return false;
  }

  Map<String, Object> getDataModel();

  void write(Configuration cfg, Writer writer) throws TemplateException, IOException;

  default void write(Configuration cfg, Path outputDirectory)
      throws TemplateException, IOException {
    if (isProvided()) {
      return;
    }
    String subName = getSymbol().getIdentifiers().stream()
        .skip(1)
        .map(id -> id.getName())
        .collect(Collectors.joining(File.separator));
    if (getCategory() == Category.SERVICE) {
      subName += "Client";
    }
    String namespace = getNamespace();
    final String relativePath;
    if (namespace.isEmpty()) {
      relativePath = subName + ".php";
    } else {
      relativePath = namespace.replace(BS, File.separator) + File.separator + subName + ".php";
    }
    Path outputFile = outputDirectory.resolve(relativePath);
    if (Files.notExists(outputFile.getParent())) {
      Files.createDirectories(outputFile.getParent());
    }
    write(cfg, Files.newBufferedWriter(outputFile, StandardCharsets.UTF_8));
  }
}
