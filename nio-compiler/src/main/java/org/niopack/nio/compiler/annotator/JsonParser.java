package org.niopack.nio.compiler.annotator;

import com.google.common.collect.ImmutableSet;
import java.util.IllegalFormatException;
import java.util.Set;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.model.AnnotatedIdentifier;
import org.niopack.nio.compiler.model.Identifier.Category;
import org.niopack.nio.compiler.model.value.StructureValue;
import org.niopack.nio.core.Json;
import org.niopack.nio.core.Json_Nio;

public class JsonParser extends AbstractAnnotationParser<Json> {

  public JsonParser(AnnotatedIdentifier identifier, SymbolTable table) {
    super(identifier, table);
  }

  @Override
  protected Set<Category> getSupportedCategories() {
    return ImmutableSet.of(
        Category.SERVICE,
        Category.STRUCT,
        Category.FIELD_STRUCT);
  }

  @Override
  public String getCanonicalName() {
    return ".org.niopack.nio.core.Json";
  }

  @Override
  protected Json parse(StructureValue value) throws IllegalFormatException {
    Json annotation = new Json();
    if (value.hasValue(Json_Nio.NAME)) {
      annotation.setName(value.scalarValue(Json_Nio.NAME).toString());
    }
    return annotation;
  }
}
