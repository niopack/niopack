package org.niopack.nio.compiler.annotator;

import com.google.common.collect.ImmutableSet;
import java.util.IllegalFormatException;
import java.util.Set;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.model.AnnotatedIdentifier;
import org.niopack.nio.compiler.model.Identifier.Category;
import org.niopack.nio.compiler.model.value.StructureValue;
import org.niopack.nio.core.Seq;
import org.niopack.nio.core.Seq_Nio;

public class SeqParser extends AbstractAnnotationParser<Seq> {

  public SeqParser(AnnotatedIdentifier identifier, SymbolTable table) {
    super(identifier, table);
  }

  @Override
  protected Set<Category> getSupportedCategories() {
    return ImmutableSet.of(
        Category.PACKAGE,
        Category.SERVICE,
        Category.STRUCT,
        Category.ENUM,
        Category.FIELD_ENUM,
        Category.FIELD_STRUCT,
        Category.METHOD_SERVICE);
  }

  @Override
  public String getCanonicalName() {
    return ".org.niopack.nio.core.Seq";
  }

  @Override
  protected Seq parse(StructureValue value) throws IllegalFormatException {
    Seq annotation = new Seq();
    if (value.hasValue(Seq_Nio.POS)) {
      annotation.setPos(value.scalarValue(Seq_Nio.POS).toInteger());
    }
    return annotation;
  }
}
