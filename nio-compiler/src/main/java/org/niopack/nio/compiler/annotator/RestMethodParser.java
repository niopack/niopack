package org.niopack.nio.compiler.annotator;

import com.google.common.collect.ImmutableSet;
import java.util.IllegalFormatException;
import java.util.Set;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.model.AnnotatedIdentifier;
import org.niopack.nio.compiler.model.Identifier.Category;
import org.niopack.nio.compiler.model.value.StructureValue;
import org.niopack.nio.core.HttpVerb;
import org.niopack.nio.core.RestMethod;
import org.niopack.nio.core.RestMethod_Nio;

public class RestMethodParser extends AbstractAnnotationParser<RestMethod> {

  public RestMethodParser(AnnotatedIdentifier identifier, SymbolTable table) {
    super(identifier, table);
  }

  @Override
  protected Set<Category> getSupportedCategories() {
    return ImmutableSet.of(Category.METHOD_SERVICE);
  }

  @Override
  public String getCanonicalName() {
    return ".org.niopack.nio.core.RestMethod";
  }

  @Override
  protected RestMethod parse(StructureValue value) throws IllegalFormatException {
    RestMethod annotation = new RestMethod();
    if (value.hasValue(RestMethod_Nio.VERB)) {
      annotation.setVerb(value.scalarValue(RestMethod_Nio.VERB).toEnum(HttpVerb.class));
    }
    if (value.hasValue(RestMethod_Nio.PATH)) {
      annotation.setPath(value.scalarValue(RestMethod_Nio.PATH).toString());
    }
    return annotation;
  }
}
