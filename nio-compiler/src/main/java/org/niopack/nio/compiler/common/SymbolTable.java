package org.niopack.nio.compiler.common;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static org.niopack.nio.compiler.model.Symbol.DOT;

import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.googlecode.concurrenttrees.common.PrettyPrinter;
import com.googlecode.concurrenttrees.radix.node.concrete.DefaultByteArrayNodeFactory;
import com.googlecode.concurrenttrees.radix.node.util.PrettyPrintable;
import com.googlecode.concurrenttrees.radixreversed.ConcurrentReversedRadixTree;
import com.googlecode.concurrenttrees.radixreversed.ReversedRadixTree;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.niopack.nio.compiler.model.BuiltinIdentifier;
import org.niopack.nio.compiler.model.Symbol;
import org.niopack.nio.compiler.model.type.TypeLiteral.Type;

public final class SymbolTable {

  public static final Object EMPTY_DEFINITION = new Object();

  private final Map<Symbol, Object> defBySymbol = new HashMap<>();
  private final ReversedRadixTree<Symbol> symbols = new ConcurrentReversedRadixTree<>(
      new DefaultByteArrayNodeFactory());
  private boolean done = false;
  private final Set<PostGenerationHandler> postGenerationHandlers = new HashSet<>();

  public SymbolTable() {
    put(new Symbol(new BuiltinIdentifier(Type.BOOL), false));
    put(new Symbol(new BuiltinIdentifier(Type.BYTES), false));
    put(new Symbol(new BuiltinIdentifier(Type.CHAR), false));
    put(new Symbol(new BuiltinIdentifier(Type.STRING), false));
    put(new Symbol(new BuiltinIdentifier(Type.FLOAT), false));
    put(new Symbol(new BuiltinIdentifier(Type.DOUBLE), false));
    put(new Symbol(new BuiltinIdentifier(Type.INT8), false));
    put(new Symbol(new BuiltinIdentifier(Type.INT16), false));
    put(new Symbol(new BuiltinIdentifier(Type.INT32), false));
    put(new Symbol(new BuiltinIdentifier(Type.INT64), false));
    put(new Symbol(new BuiltinIdentifier(Type.MSEC), false));
    put(new Symbol(new BuiltinIdentifier(Type.USEC), false));
    put(new Symbol(new BuiltinIdentifier(Type.VOID), false));
  }

  public void registerPostGenerationHandler(PostGenerationHandler handler) {
    postGenerationHandlers.add(handler);
  }

  private void invokePostGenerationHandlers() {
    postGenerationHandlers.forEach(handler -> handler.resolveDefaultField(this));
    postGenerationHandlers.forEach(handler -> handler.checkDuplicateAnnotation(this));
  }

  public void done() {
    done = true;
    invokePostGenerationHandlers();
  }

  private void put(Symbol symbol) {
    put(symbol, EMPTY_DEFINITION);
  }

  public void put(Symbol symbol, Object def) {
    checkNotNull(symbol, "symbol");

    Object currentDefinition = defBySymbol.putIfAbsent(symbol, def);
    checkArgument(currentDefinition == null, "Can't redefine symbol = %s.", symbol);
    symbols.putIfAbsent(symbol.getCanonicalName(), symbol);
  }

  public void merge(SymbolTable imported) {
    checkNotNull(imported, "imported");
    checkState(imported.done, "imported symbol table generation is not complete.");

    for (Entry<Symbol, Object> entry : imported.defBySymbol.entrySet()) {
      Symbol symbol = entry.getKey();
      defBySymbol.putIfAbsent(symbol, entry.getValue());
      symbols.putIfAbsent(symbol.getCanonicalName(), symbol);
    }
  }

  public Symbol find(String symbolName) {
    checkState(done, "symbol table generation is not complete.");

    boolean canonicalName = symbolName.startsWith(DOT);
    if (canonicalName) {
      return symbols.getValueForExactKey(symbolName);
    } else {
      Iterable<Symbol> result = symbols.getValuesForKeysEndingWith(DOT + symbolName);
      return Iterables.isEmpty(result) ? null : Iterables.getOnlyElement(result);
    }
  }

  @SuppressWarnings("unchecked")
  public <T> T getDefinition(Symbol symbol) {
    return (T) defBySymbol.get(symbol);
  }

  public Map<Symbol, Object> outputSymbols() {
    checkState(done, "symbol table generation is not complete.");
    return Maps.filterKeys(defBySymbol, symbol -> symbol.isOutput());
  }

  @Override
  public String toString() {
    return PrettyPrinter.prettyPrint((PrettyPrintable) symbols);
  }

  public static interface PostGenerationHandler {
    default void resolveDefaultField(SymbolTable table) {}
    default void checkDuplicateAnnotation(SymbolTable table) {}
  }
}
