package org.niopack.nio.compiler.language.java;

import static com.google.common.base.CaseFormat.LOWER_CAMEL;
import static com.google.common.base.CaseFormat.UPPER_CAMEL;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.collect.Iterables;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.stream.Collectors;
import org.niopack.nio.compiler.annotator.FieldParser;
import org.niopack.nio.compiler.annotator.ProvidedParser;
import org.niopack.nio.compiler.annotator.RestParamParser;
import org.niopack.nio.compiler.common.Formatter;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.language.java.model.Annotation;
import org.niopack.nio.compiler.language.java.model.JavaStructureField;
import org.niopack.nio.compiler.model.Identifier.Category;
import org.niopack.nio.compiler.model.Structure;
import org.niopack.nio.compiler.model.Symbol;
import org.niopack.nio.compiler.model.value.StructureValue;
import org.niopack.nio.core.Field;
import org.niopack.nio.core.FieldStatus;
import org.niopack.nio.core.ParamType;
import org.niopack.nio.core.Provided;
import org.niopack.nio.core.RestParam;

final class JavaStruct extends AbstractJavaSymbol {

  private final Symbol symbol;
  private final SymbolTable table;
  private final Structure definition;

  public JavaStruct(Symbol symbol, SymbolTable table) {
    Category type = Iterables.getLast(symbol.getIdentifiers()).getCategory();
    checkArgument(type == Category.STRUCT, "%s is not a service.", symbol);
    this.symbol = checkNotNull(symbol, "symbol");
    this.table = checkNotNull(table, "table");
    this.definition = table.getDefinition(symbol);
  }

  @Override
  public Symbol getSymbol() {
    return symbol;
  }

  @Override
  public SymbolTable getTable() {
    return table;
  }

  @Override @SuppressWarnings("unchecked")
  public <T> T getDefinition() {
    return (T) definition;
  }

  @Override
  public Category getCategory() {
    return Category.STRUCT;
  }

  @Override
  public boolean isProvided() {
    Provided annotation = new ProvidedParser(definition, table).getAnnotation();
    return annotation != null && annotation.isProvided();
  }

  @Override
  public void write(Configuration cfg, Writer writer) throws TemplateException, IOException {
    if (isProvided()) {
      return;
    }
    writeSource(cfg, writer);
    writeNio(cfg, writer);
  }

  @Override
  public void write(Configuration cfg, Path outputDirectory)
      throws TemplateException, IOException {
    if (isProvided()) {
      return;
    }
    writeSource(cfg, getWriter(outputDirectory, ".java"));
    writeNio(cfg, getWriter(outputDirectory, "_Nio.java"));
  }

  private void writeSource(Configuration cfg, Writer writer) throws TemplateException, IOException {
    Template template = cfg.getTemplate("java/struct.java.ftl");
    template.process(getSourceDataModel(), writer);
  }

  @Override
  protected Map<String, Object> getSourceDataModel() {
    Map<String, Object> dataModel = new HashMap<>();
    dataModel.put("packageName", getPackageName());
    dataModel.put("typeName", getSimpleName());

    // Static imports will never be affected by user defined nio messages.
    TreeSet<String> staticImports = new TreeSet<>();
    dataModel.put("staticImports", staticImports);

    // Simple class name -> Fully qualified class name
    Map<String, String> imports = new HashMap<>();
    imports.put("Objects", "com.google.common.base.Objects");
    imports.put("MoreObjects", "com.google.common.base.MoreObjects");

    TypeResolver resolver = new TypeResolver(table, getPackageName(), imports);
    ValueGenerator valueGenerator = new ValueGenerator(resolver);
    List<JavaStructureField> fields = definition.getFields().values().stream()
        .filter(field -> {
          FieldStatus status = FieldStatus.ACTIVE;
          Field annotation = new FieldParser(field, table).getAnnotation();
          if (annotation != null && annotation.getStatus() != null) {
            status = annotation.getStatus();
          }
          return status != FieldStatus.RETIRED;
        })
        .map(field -> {
          String fieldName = field.getName();
          String fieldType = resolver.resolve(field.getType());
          String defaultValue = valueGenerator.getDefaultValue(field.getType());
          RestParam restParam = new RestParamParser(field, table).getAnnotation();
          String restParamType = null;
          String restParamName = null;
          if (restParam != null && restParam.getType() != null){
            restParamType = resolver.resolve(ParamType.class, restParam.getType());
            restParamName = restParam.getName() != null ? restParam.getName() : fieldName;
          }
          return new JavaStructureField(
              fieldName, fieldType, defaultValue, restParamType, restParamName);
        })
        .collect(Collectors.toList());
    dataModel.put("fields", fields);
    dataModel.put("imports", new TreeSet<>(imports.values()));
    return dataModel;
  }

  private void writeNio(Configuration cfg, Writer writer) throws IOException, TemplateException {
    Template template = cfg.getTemplate("java/struct_nio.java.ftl");
    template.process(getNioDataModel(cfg), writer);
  }

  private Map<String, Object> getNioDataModel(Configuration cfg) {
    Map<String, Object> dataModel = new HashMap<>();
    dataModel.put("packageName", getPackageName());
    dataModel.put("typeName", definition.getName());

    // Static imports will never be affected by user defined nio messages.
    TreeSet<String> staticImports = new TreeSet<>();
    dataModel.put("staticImports", staticImports);

    // Simple class name -> Fully qualified class name
    Map<String, String> imports = new HashMap<>();
    imports.put("List", "java.util.List");
    imports.put("Map", "java.util.Map");
    imports.put("ImmutableList", "com.google.common.collect.ImmutableList");
    imports.put("ImmutableMap", "com.google.common.collect.ImmutableMap");
    imports.put("ImmutableTable", "com.google.common.collect.ImmutableTable");
    imports.put("Table", "com.google.common.collect.Table");

    TypeResolver resolver = new TypeResolver(table, getPackageName(), imports);
    NameGenerator nameGenerator = new NameGenerator();
    StructureInstantiator structCoder = new StructureInstantiator(resolver, nameGenerator);

    List<Annotation> typeAnnotations = definition.getAnnotations().values().stream()
        .map(annotation -> structCoder.codegen(
            cfg,
            UPPER_CAMEL.to(LOWER_CAMEL, ((StructureValue)annotation).getName()),
            (StructureValue) annotation))
        .collect(Collectors.toList());
    dataModel.put("typeAnnotations", typeAnnotations);

    List<Annotation> fieldAnnotations = new ArrayList<>();
    definition.getFields().values().stream()
        .forEach(field -> field.getAnnotations().values()
            .forEach(annotation ->  {
                Annotation fieldAnnotation = structCoder.codegen(
                  cfg,
                  field.getName().toLowerCase() + ((StructureValue)annotation).getName(),
                  (StructureValue) annotation);
                fieldAnnotation.setNameConstant(Formatter.toUpperUnderscore(field.getName()));
                fieldAnnotations.add(fieldAnnotation);
            })
        );
    dataModel.put("fieldAnnotations", fieldAnnotations);

    List<JavaStructureField> fields = definition.getFields().values().stream()
        .map(field -> new JavaStructureField(field.getName(), null, null, null, null))
        .collect(Collectors.toList());
    dataModel.put("fields", fields);

    dataModel.put("imports", new TreeSet<>(imports.values()));
    return dataModel;
  }
}
