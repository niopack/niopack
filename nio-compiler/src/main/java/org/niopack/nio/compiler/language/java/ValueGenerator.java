package org.niopack.nio.compiler.language.java;

import javax.annotation.Nullable;
import org.niopack.nio.compiler.model.type.TypeLiteral;
import org.niopack.nio.compiler.model.value.StructureValue;

public class ValueGenerator {

  private final TypeResolver resolver;

  public ValueGenerator(TypeResolver resolver) {
    this.resolver = resolver;
  }

  public String generate(TypeLiteral type, @Nullable StructureValue value) {
    throw new UnsupportedOperationException();
  }

  @Nullable
  public String getDefaultValue(TypeLiteral type) {
    switch (type.getType()) {
      case LIST:
        resolver.addImport("ArrayList", "java.util.ArrayList");
        return "new ArrayList<>()";
      case MAP:
        resolver.addImport("HashMap", "java.util.HashMap");
        return "new HashMap<>()";
      default:
        return null;
    }
  }
}
