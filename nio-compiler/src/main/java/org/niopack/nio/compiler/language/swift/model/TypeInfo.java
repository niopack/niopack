package org.niopack.nio.compiler.language.swift.model;

import org.niopack.nio.compiler.model.type.TypeLiteral.Type;

public class TypeInfo {
  private Type fieldType;
  private String fieldTypeName;
  private Type keyType;
  private String keyTypeName;
  private Type valueType;
  private String valueTypeName;

  public Type getFieldType() {
    return fieldType;
  }

  public void setFieldType(Type fieldType) {
    this.fieldType = fieldType;
  }

  public String getFieldTypeName() {
    return fieldTypeName;
  }

  public void setFieldTypeName(String fieldTypeName) {
    this.fieldTypeName = fieldTypeName;
  }

  public Type getKeyType() {
    return keyType;
  }

  public void setKeyType(Type keyType) {
    this.keyType = keyType;
  }

  public String getKeyTypeName() {
    return keyTypeName;
  }

  public void setKeyTypeName(String keyTypeName) {
    this.keyTypeName = keyTypeName;
  }

  public Type getValueType() {
    return valueType;
  }

  public void setValueType(Type valueType) {
    this.valueType = valueType;
  }

  public String getValueTypeName() {
    return valueTypeName;
  }

  public void setValueTypeName(String valueTypeName) {
    this.valueTypeName = valueTypeName;
  }
}
