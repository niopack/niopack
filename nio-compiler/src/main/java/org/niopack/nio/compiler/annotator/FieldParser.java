package org.niopack.nio.compiler.annotator;

import com.google.common.collect.ImmutableSet;
import java.util.IllegalFormatException;
import java.util.Set;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.model.AnnotatedIdentifier;
import org.niopack.nio.compiler.model.Identifier.Category;
import org.niopack.nio.compiler.model.value.StructureValue;
import org.niopack.nio.core.Field;
import org.niopack.nio.core.FieldStatus;
import org.niopack.nio.core.Field_Nio;

public class FieldParser extends AbstractAnnotationParser<Field> {

  public FieldParser(AnnotatedIdentifier identifier, SymbolTable table) {
    super(identifier, table);
  }

  @Override
  protected Set<Category> getSupportedCategories() {
    return ImmutableSet.of(
        Category.FIELD_ENUM,
        Category.FIELD_STRUCT);
  }

  @Override
  public String getCanonicalName() {
    return ".org.niopack.nio.core.Field";
  }

  @Override
  protected Field parse(StructureValue value) throws IllegalFormatException {
    Field annotation = new Field();
    if (value.hasValue(Field_Nio.STATUS)) {
      annotation.setStatus(value.scalarValue(Field_Nio.STATUS).toEnum(FieldStatus.class));
    }
    return annotation;
  }
}
