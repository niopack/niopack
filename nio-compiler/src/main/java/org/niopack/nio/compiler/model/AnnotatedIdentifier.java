package org.niopack.nio.compiler.model;

import java.util.Map;
import java.util.Set;
import org.niopack.nio.compiler.common.SymbolTable.PostGenerationHandler;
import org.niopack.nio.compiler.model.value.Value;

public interface AnnotatedIdentifier extends Identifier, PostGenerationHandler {

  Map<String, Value> getAnnotations();

  Set<String> getAnnotationTypes();

  Value getAnnotation(String structName);

  void addAnnotation(String annotation, Value value);
}
