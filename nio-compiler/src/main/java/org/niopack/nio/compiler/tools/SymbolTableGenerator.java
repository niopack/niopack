package org.niopack.nio.compiler.tools;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.nio.file.Path;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;
import org.antlr.v4.runtime.RuleContext;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.niopack.nio.compiler.antlr4.NioBaseListener;
import org.niopack.nio.compiler.antlr4.NioParser;
import org.niopack.nio.compiler.antlr4.NioParser.AnnotationContext;
import org.niopack.nio.compiler.antlr4.NioParser.EnumerationContext;
import org.niopack.nio.compiler.antlr4.NioParser.EnumerationFieldContext;
import org.niopack.nio.compiler.antlr4.NioParser.ImportDeclContext;
import org.niopack.nio.compiler.antlr4.NioParser.ListTypeContext;
import org.niopack.nio.compiler.antlr4.NioParser.MapTypeContext;
import org.niopack.nio.compiler.antlr4.NioParser.MultiValueFieldContext;
import org.niopack.nio.compiler.antlr4.NioParser.NormalAnnotationContext;
import org.niopack.nio.compiler.antlr4.NioParser.PairContext;
import org.niopack.nio.compiler.antlr4.NioParser.ScalarFieldContext;
import org.niopack.nio.compiler.antlr4.NioParser.ScalarTypesContext;
import org.niopack.nio.compiler.antlr4.NioParser.ServiceContext;
import org.niopack.nio.compiler.antlr4.NioParser.ServiceMethodContext;
import org.niopack.nio.compiler.antlr4.NioParser.ServiceResponseContext;
import org.niopack.nio.compiler.antlr4.NioParser.SingleElementAnnotationContext;
import org.niopack.nio.compiler.antlr4.NioParser.StructureValueContext;
import org.niopack.nio.compiler.antlr4.NioParser.ValueContext;
import org.niopack.nio.compiler.common.IdentifierEntry;
import org.niopack.nio.compiler.common.PathLocator;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.model.Enumeration;
import org.niopack.nio.compiler.model.EnumerationField;
import org.niopack.nio.compiler.model.Identifier;
import org.niopack.nio.compiler.model.Package;
import org.niopack.nio.compiler.model.Service;
import org.niopack.nio.compiler.model.ServiceMethod;
import org.niopack.nio.compiler.model.Structure;
import org.niopack.nio.compiler.model.StructureField;
import org.niopack.nio.compiler.model.Symbol;
import org.niopack.nio.compiler.model.type.ListTypeLiteral;
import org.niopack.nio.compiler.model.type.MapTypeLiteral;
import org.niopack.nio.compiler.model.type.ScalarTypeLiteral;
import org.niopack.nio.compiler.model.type.TypeLiteral;
import org.niopack.nio.compiler.model.type.TypeLiteral.Type;
import org.niopack.nio.compiler.model.value.ScalarValue;
import org.niopack.nio.compiler.model.value.StructureValue;
import org.niopack.nio.compiler.model.value.Value;

public final class SymbolTableGenerator extends NioBaseListener {

  private final PathLocator locator;
  private final boolean output;
  private final SymbolTable table = new SymbolTable();
  private final Stack<IdentifierEntry> identifierStack = new Stack<>();

  public SymbolTableGenerator(PathLocator locator, boolean output) {
    this.locator = checkNotNull(locator, "locator");
    this.output = output;
  }

  public SymbolTable getSymbolTable() {
    return table;
  }

  @Override
  public void enterInit(NioParser.InitContext ctx) {
    Package pkg = new Package(ctx.pkgDecl().packageName().getText());
    table.registerPostGenerationHandler(pkg);
    identifierStack.push(new IdentifierEntry(pkg, ctx.depth()));
  }

  @Override
  public void exitInit(NioParser.InitContext ctx) {
    popAndCheckExitInvariant(ctx);
    table.done();
  }

  @Override
  public void enterImportDecl(ImportDeclContext ctx) {
    Path importedFile = locator.find(toText(ctx.StringLiteral()));
    SymbolTable imported = Tool.loadSymbols(locator.getRootDirectory(), importedFile, false);
    table.merge(imported);
  }

  @Override
  public void enterAnnotation(AnnotationContext ctx) {
    if (ctx.normalAnnotation() != null) {
      StructureValue value = parse(ctx.normalAnnotation());
      identifierStack.peek().getIdentifier().addAnnotation(value.getName(), value);
    } else if (ctx.singleElementAnnotation() != null) {
      StructureValue value = parse(ctx.singleElementAnnotation());
      identifierStack.peek().getIdentifier().addAnnotation(value.getName(), value);
    }
  }

  // ----- Enumeration -----

  @Override
  public void enterEnumeration(EnumerationContext ctx) {
    Enumeration enumeration = new Enumeration(ctx.Identifier().getText());
    table.registerPostGenerationHandler(enumeration);
    identifierStack.push(new IdentifierEntry(enumeration, ctx.depth()));
    table.put(createSymbol(), enumeration);
  }

  @Override
  public void exitEnumeration(NioParser.EnumerationContext ctx) {
    popAndCheckExitInvariant(ctx);
  }

  @Override
  public void enterEnumerationField(EnumerationFieldContext ctx) {
    EnumerationField field = new EnumerationField(
        ctx.Identifier().getText(), Integer.valueOf(ctx.IntegerLiteral().getText()));
    table.registerPostGenerationHandler(field);
    ((Enumeration) identifierStack.peek().getIdentifier()).addValue(field);
    identifierStack.push(new IdentifierEntry(field, ctx.depth()));
  }

  @Override
  public void exitEnumerationField(EnumerationFieldContext ctx) {
    popAndCheckExitInvariant(ctx);
  }

  // ----- Service -----

  @Override
  public void enterService(ServiceContext ctx) {
    Service service = new Service(ctx.Identifier().getText());
    table.registerPostGenerationHandler(service);
    identifierStack.push(new IdentifierEntry(service, ctx.depth()));
    table.put(createSymbol(), service);
  }

  @Override
  public void exitService(ServiceContext ctx) {
    popAndCheckExitInvariant(ctx);
  }

  @Override
  public void enterServiceMethod(ServiceMethodContext ctx) {
    ServiceMethod method = new ServiceMethod(ctx.Identifier().getText());
    table.registerPostGenerationHandler(method);

    TypeLiteral requestType =
        new ScalarTypeLiteral(Type.USER_DEFINED, ctx.serviceRequest().getText());
    method.setRequestType(requestType);

    ServiceResponseContext respCtx = ctx.serviceResponse();
    TypeLiteral responseType = respCtx.VOID() != null
        ? new ScalarTypeLiteral(Type.VOID)
        : parse(respCtx.scalarTypes());
    method.setResponseType(responseType);

    ((Service) identifierStack.peek().getIdentifier()).addMethod(method);
    identifierStack.push(new IdentifierEntry(method, ctx.depth()));
  }

  @Override
  public void exitServiceMethod(ServiceMethodContext ctx) {
    popAndCheckExitInvariant(ctx);
  }

  // ----- Struct -----

  @Override
  public void enterStructure(NioParser.StructureContext ctx) {
    Structure struct = new Structure(ctx.Identifier().getText());
    table.registerPostGenerationHandler(struct);
    identifierStack.push(new IdentifierEntry(struct, ctx.depth()));
    table.put(createSymbol(), struct);
  }

  @Override
  public void exitStructure(NioParser.StructureContext ctx) {
    popAndCheckExitInvariant(ctx);
  }

  @Override
  public void enterScalarField(ScalarFieldContext ctx) {
    StructureField field = new StructureField(ctx.Identifier().getText(), parse(ctx.scalarTypes()));
    table.registerPostGenerationHandler(field);
    if (ctx.value() != null) {
      // capture the value properly with types.
      field.setValue(parse(ctx.value()));
    }
    ((Structure) identifierStack.peek().getIdentifier()).addField(field);
    identifierStack.push(new IdentifierEntry(field, ctx.depth()));
  }

  private TypeLiteral parse(ScalarTypesContext ctx) {
    if (ctx.builtinTypes() != null) {
      return new ScalarTypeLiteral(Type.valueOf(ctx.builtinTypes().getText().toUpperCase()));
    } else if (ctx.fullyQualifiedType() != null) {
      return new ScalarTypeLiteral(Type.USER_DEFINED, ctx.fullyQualifiedType().getText());
    } else {
      return new ScalarTypeLiteral(Type.USER_DEFINED, ctx.Identifier().getText());
    }
  }

  @Override
  public void exitScalarField(ScalarFieldContext ctx) {
    popAndCheckExitInvariant(ctx);
  }

  private Stack<TypeLiteral> typeStack = new Stack<>();

  @Override
  public void enterScalarTypes(ScalarTypesContext ctx) {
    typeStack.push(parse(ctx));
  }

  @Override
  public void exitScalarTypes(ScalarTypesContext ctx) {
    if (typeStack.size() == 1) {
      // not part of a multi type
      typeStack.pop();
    }
  }

  @Override
  public void enterListType(ListTypeContext ctx) {
    typeStack.push(new ListTypeLiteral());
  }

  @Override
  public void exitListType(ListTypeContext ctx) {
    ScalarTypeLiteral elementType = (ScalarTypeLiteral) typeStack.pop();
    ((ListTypeLiteral) typeStack.peek()).setElementType(elementType);
    if (typeStack.size() == 1) {
      ((StructureField) identifierStack.peek().getIdentifier()).setType(typeStack.pop());
    }
  }

  @Override
  public void enterMapType(MapTypeContext ctx) {
    typeStack.push(new MapTypeLiteral());
  }

  @Override
  public void exitMapType(MapTypeContext ctx) {
    ScalarTypeLiteral valueType = (ScalarTypeLiteral) typeStack.pop();
    ScalarTypeLiteral keyType = (ScalarTypeLiteral) typeStack.pop();
    ((MapTypeLiteral) typeStack.peek()).setValueType(valueType);
    ((MapTypeLiteral) typeStack.peek()).setKeyType(keyType);
    if (typeStack.size() == 1) {
      ((StructureField) identifierStack.peek().getIdentifier()).setType(typeStack.pop());
    }
  }

  @Override
  public void enterMultiValueField(MultiValueFieldContext ctx) {
    // TODO(tamal): capture type of list / set / map
    StructureField field = new StructureField(ctx.Identifier().getText());
    ((Structure) identifierStack.peek().getIdentifier()).addField(field);
    identifierStack.push(new IdentifierEntry(field, ctx.depth()));
  }

  @Override
  public void exitMultiValueField(MultiValueFieldContext ctx) {
    popAndCheckExitInvariant(ctx);
  }

  private Symbol createSymbol() {
    List<Identifier> identifiers = identifierStack.stream()
        .map(entry -> entry.getIdentifier())
        .collect(Collectors.toList());
    return new Symbol(identifiers, output);
  }

  private String toText(TerminalNode node) {
    // TODO(tamal): verify that it is a StringLiteral
    return toText(node.getText());
  }

  private String toText(String aString) {
    return aString.substring(1, aString.length() - 1);
  }

  private StructureValue parse(NormalAnnotationContext ctx) {
    return (StructureValue) parse(ctx.Identifier().getText(), ctx.structureValue());
  }

  private StructureValue parse(SingleElementAnnotationContext ctx) {
    StructureValue value = new StructureValue();
    value.setName(ctx.Identifier().getText());
    value.setOnlyFieldValue(parse(ctx.value()), table);
    return value;
  }

  private Value parse(ValueContext ctx) {
    if (ctx.CharacterLiteral() != null) {
      return new ScalarValue(ctx.CharacterLiteral().getText(), Value.Type.CHAR);
    } else if (ctx.StringLiteral() != null) {
      return new ScalarValue(toText(ctx.StringLiteral().getText()), Value.Type.STRING);
    } else if (ctx.IntegerLiteral() != null) {
      return new ScalarValue(ctx.IntegerLiteral().getText(), Value.Type.INT);
    } else if (ctx.FloatingPointLiteral() != null) {
      return new ScalarValue(ctx.FloatingPointLiteral().getText(), Value.Type.FLOAT);
    } else if (ctx.BooleanLiteral() != null) {
      return new ScalarValue(ctx.BooleanLiteral().getText(), Value.Type.BOOL);
    } else if (ctx.Identifier() != null) {
      return new ScalarValue(ctx.Identifier().getText(), Value.Type.ENUM);
    } else {
      return parse(null, ctx.structureValue());
    }
  }

  private Value parse(String typeName, StructureValueContext ctx) {
    StructureValue value = new StructureValue();
    value.setName(typeName);
    for (PairContext pctx : ctx.pair()) {
      value.addValueByField(pctx.Identifier().getText(), parse(pctx.value()));
    }
    return value;
  }

  /**
   * Used for stack push<->pop verification purpose only.
   */
  private void popAndCheckExitInvariant(RuleContext ctx) {
    IdentifierEntry entry = identifierStack.pop();
    checkArgument(entry.getDepth() == ctx.depth(),
        "EntryDepth=%s and ContextDepth=%s", entry.getDepth(), ctx.depth());
  }
}
