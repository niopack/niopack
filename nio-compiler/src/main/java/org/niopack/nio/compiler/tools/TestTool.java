package org.niopack.nio.compiler.tools;

import static java.nio.file.FileVisitResult.CONTINUE;

import com.google.common.base.Preconditions;
import com.google.common.base.Throwables;
import com.google.common.collect.Iterables;
import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.Version;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Locale;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.BailErrorStrategy;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.niopack.nio.compiler.antlr4.NioBaseListener;
import org.niopack.nio.compiler.antlr4.NioLexer;
import org.niopack.nio.compiler.antlr4.NioParser;
import org.niopack.nio.compiler.common.PathLocator;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.language.swift.SwiftSymbol;
import org.niopack.nio.compiler.language.swift.SwiftSymbolFactory;
import org.niopack.nio.compiler.model.Identifier;
import org.niopack.nio.compiler.model.Symbol;

public final class TestTool {

  private static void walkParseTree(Path nioFile, NioBaseListener listener) {
    try {
      ANTLRInputStream input = new ANTLRInputStream(new FileInputStream(nioFile.toFile()));
      NioLexer lexer = new NioLexer(input);
      CommonTokenStream tokens = new CommonTokenStream(lexer);
      NioParser parser = new NioParser(tokens);
      parser.setErrorHandler(new BailErrorStrategy());

      ParseTree tree = parser.init(); // begin parsing at init rule
      ParseTreeWalker walker = new ParseTreeWalker();
      walker.walk(listener, tree);
    } catch (IOException e) {
      Throwables.propagateIfPossible(e);
    }
  }

  public static SymbolTable loadSymbols(Path rootDirectory, Path nioFile, boolean output) {
    PathLocator pathLocator = new PathLocator(rootDirectory, nioFile.getParent());
    SymbolTableGenerator generator = new SymbolTableGenerator(pathLocator, output);
    walkParseTree(nioFile, generator);
    return generator.getSymbolTable();
  }

  public static void main(String[] args) throws IOException, TemplateException {
    Configuration cfg = new Configuration(Configuration.VERSION_2_3_21);
    cfg.setIncompatibleImprovements(new Version(2, 3, 20));
    cfg.setDefaultEncoding("UTF-8");
    cfg.setLocale(Locale.US);
    cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);

    Path rootDirectory = Paths.get("/Users/tamal/Niopack/Source/niopacker/nio-registry");
    Path outDirectory = Paths.get("/tmp/nio");
    deleteDirectory(outDirectory);
    Files.createDirectory(outDirectory);

    Path nioPath =
        // Paths.get("/Users/tamal/Niopack/Source/niopacker/nio-registry/org/niopack/nio/demo/4.nio");
        // Paths.get("/Users/tamal/Niopack/Source/niopacker/nio-registry/org/niopack/d2/device.nio");
        // Paths.get("/Users/tamal/Niopack/Source/niopacker/nio-registry/org/niopack/d2");
        Paths.get("/Users/tamal/TigerWorks/Source/felidae/felidae.server/src/main/nio");
    if (Files.isDirectory(nioPath)) {
      compileFolder(cfg, rootDirectory, nioPath, outDirectory);
    } else {
      compile(cfg, rootDirectory, nioPath, outDirectory);
    }
    System.exit(0);
  }

  private static void compileFolder(Configuration cfg, Path rootDirectory, Path nioPath,
      Path outDirectory) throws IOException {
    PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:*.nio");
    Files.walkFileTree(nioPath, new SimpleFileVisitor<Path>() {
      @Override
      public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
        Path name = file.getFileName();
        if (name != null && matcher.matches(name)) {
          try {
            compile(cfg, rootDirectory, file, outDirectory);
          } catch (IOException | TemplateException e) {
            throw Throwables.propagate(e);
          }
        }
        return CONTINUE;
      }

      @Override
      public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
        return CONTINUE;
      }

      @Override
      public FileVisitResult visitFileFailed(Path file, IOException exc) {
        System.err.println(exc);
        return CONTINUE;
      }
    });
  }

  private static void deleteDirectory(Path outDirectory) throws IOException {
    Files.walkFileTree(outDirectory, new SimpleFileVisitor<Path>() {
      @Override
      public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
          throws IOException {
        Files.delete(file);
        return FileVisitResult.CONTINUE;
      }

      @Override
      public FileVisitResult postVisitDirectory(Path dir, IOException exc)
          throws IOException {
        Files.delete(dir);
        return FileVisitResult.CONTINUE;
      }
    });
  }

  private static void compile(Configuration cfg, Path rootDirectory, Path nioFile,
      Path outDirectory) throws IOException, TemplateException {
    Preconditions.checkArgument(Files.isDirectory(rootDirectory));
    Preconditions.checkArgument(Files.isRegularFile(nioFile));
    Preconditions.checkArgument(Files.isDirectory(outDirectory));

    SymbolTable table = loadSymbols(rootDirectory, nioFile, true);
    for (Symbol symbol : table.outputSymbols().keySet()) {
      System.out.println("----->>>>> " + symbol);

      // Swift
      if (Iterables.getLast(symbol.getIdentifiers()).getCategory() == Identifier.Category.STRUCT
          || Iterables.getLast(symbol.getIdentifiers()).getCategory() == Identifier.Category.ENUM) {
        SwiftSymbol swiftSymbol = SwiftSymbolFactory.create(symbol, table);
        Writer consoleWriter = new OutputStreamWriter(System.out);
        swiftSymbol.write(cfg, consoleWriter);
        swiftSymbol.write(cfg, outDirectory);
      }

      /*
      // PHP
      PhpSymbol phpSymbol = PhpSymbolFactory.create(symbol, table);
      Writer consoleWriter = new OutputStreamWriter(System.out);
      phpSymbol.write(cfg, consoleWriter);
      phpSymbol.write(cfg, outDirectory);
      */

      /*
      // JAVA
      JavaSymbol javaSymbol = JavaSymbolFactory.create(symbol, table);
      javaSymbol.write(cfg, outDirectory);
      javaSymbol.writeNio(cfg, outDirectory);

      Writer consoleWriter = new OutputStreamWriter(System.out);
      javaSymbol.write(cfg, consoleWriter);
      javaSymbol.writeNio(cfg, consoleWriter);
      */
    }
  }

  private TestTool() {
  }
}
