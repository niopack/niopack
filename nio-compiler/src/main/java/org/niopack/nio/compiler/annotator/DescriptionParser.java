package org.niopack.nio.compiler.annotator;

import com.google.common.collect.ImmutableSet;
import java.util.IllegalFormatException;
import java.util.Set;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.model.AnnotatedIdentifier;
import org.niopack.nio.compiler.model.Identifier.Category;
import org.niopack.nio.compiler.model.value.StructureValue;
import org.niopack.nio.core.Description;
import org.niopack.nio.core.Description_Nio;

public class DescriptionParser extends AbstractAnnotationParser<Description> {

  public DescriptionParser(AnnotatedIdentifier identifier, SymbolTable table) {
    super(identifier, table);
  }

  @Override
  protected Set<Category> getSupportedCategories() {
    return ImmutableSet.of(
        Category.PACKAGE,
        Category.SERVICE,
        Category.STRUCT,
        Category.ENUM,
        Category.FIELD_ENUM,
        Category.FIELD_STRUCT,
        Category.METHOD_SERVICE);
  }

  @Override
  public String getCanonicalName() {
    return ".org.niopack.nio.core.Description";
  }

  @Override
  protected Description parse(StructureValue value) throws IllegalFormatException {
    Description annotation = new Description();
    if (value.hasValue(Description_Nio.DESCRIPTION)) {
      annotation.setDescription(value.scalarValue(Description_Nio.DESCRIPTION).toString());
    }
    return annotation;
  }
}
