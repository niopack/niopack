package org.niopack.nio.compiler.model.value;

public interface Value {

  Type getType();

  public enum Type {
    NONE,
    BOOL,
    CHAR,
    STRING,
    FLOAT,
    INT,
    ENUM,
    STRUCT
  }
}
