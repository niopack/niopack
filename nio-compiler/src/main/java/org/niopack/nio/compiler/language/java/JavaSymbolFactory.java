package org.niopack.nio.compiler.language.java;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import java.util.Set;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.model.Identifier.Category;
import org.niopack.nio.compiler.model.Symbol;

public final class JavaSymbolFactory {

  public static final Set<String> KEYWORDS;
  static {
    // Source: http://docs.oracle.com/javase/tutorial/java/nutsandbolts/_keywords.html
    ImmutableSet.Builder<String> builder = ImmutableSet.builder();
    builder.add("abstract", "assert", "boolean", "break", "byte", "case", "catch", "char", "class",
        "const", "continue", "default", "do", "double", "else", "enum", "extends", "final",
        "finally", "float", "for", "goto", "if", "implements", "import", "instanceof", "int",
        "interface", "long", "native", "new", "package", "private", "protected", "public", "return",
        "short", "static", "strictfp", "super", "switch", "synchronized", "this", "throw", "throws",
        "transient", "try", "void", "volatile", "while");
    KEYWORDS = builder.build();
  }

  public static JavaSymbol create(Symbol symbol, SymbolTable table) {
    Category category = Iterables.getLast(symbol.getIdentifiers()).getCategory();
    switch (category) {
      case ENUM:
        return new JavaEnum(symbol, table);
      case STRUCT:
        return new JavaStruct(symbol, table);
      case SERVICE:
        return new JavaService(symbol, table);
      case BUILTIN:
        return new JavaBuiltinType(symbol, table);
      default:
        throw new UnsupportedOperationException(String.format(
            "Can't generate java code for symbol %s", symbol));
    }
  }

}
