package org.niopack.nio.compiler.language.java;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

abstract class AbstractJavaSymbol implements JavaSymbol {

  @Override
  public Map<String, Object> getDataModel() {
    return getSourceDataModel();
  }

  abstract Map<String, Object> getSourceDataModel();

  protected Writer getWriter(Path outputDirectory, String suffix) throws IOException {
    String relativePath = getCanonicalName().replace(DOT, File.separator) + suffix;
    Path outputFile = outputDirectory.resolve(relativePath);
    if (Files.notExists(outputFile.getParent())) {
      Files.createDirectories(outputFile.getParent());
    }
    return Files.newBufferedWriter(outputFile, StandardCharsets.UTF_8);
  }
}
