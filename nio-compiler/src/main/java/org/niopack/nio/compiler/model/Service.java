package org.niopack.nio.compiler.model;

import static com.google.common.base.Preconditions.checkArgument;

import com.google.common.collect.ImmutableMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.niopack.nio.compiler.model.type.ScalarTypeLiteral;
import org.niopack.nio.compiler.model.type.TypeLiteral.Type;

public final class Service extends AbstractAnnotatedIdentifier {

  private Map<String, ServiceMethod> methods = new LinkedHashMap<>();

  public Service(String name) {
    super(name, new ScalarTypeLiteral(Type.USER_DEFINED, name));
  }

  @Override
  public Category getCategory() {
    return Category.SERVICE;
  }

  public Map<String, ServiceMethod> getMethods() {
    return ImmutableMap.copyOf(methods);
  }

  public Set<String> getMethodNames() {
    return methods.keySet();
  }

  public ServiceMethod getMethod(String methodName) {
    return methods.get(methodName);
  }

  public void addMethod(ServiceMethod method) {
    checkArgument(!methods.containsKey(method.getName()), "Duplicate method %s", method.getName());
    this.methods.put(method.getName(), method);
  }

  @Override
  public String toString() {
    return getName() + "::" + getCategory() + "::" + getAnnotations() + "::" + methods;
  }
}
