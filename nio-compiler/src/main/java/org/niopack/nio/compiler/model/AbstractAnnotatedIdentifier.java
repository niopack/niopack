package org.niopack.nio.compiler.model;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

import com.google.common.collect.ImmutableMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.model.type.TypeLiteral;
import org.niopack.nio.compiler.model.value.Value;

public abstract class AbstractAnnotatedIdentifier implements AnnotatedIdentifier {

  private final String name;
  private TypeLiteral type;
  private final Map<String, Value> annotations = new LinkedHashMap<>();

  public AbstractAnnotatedIdentifier(String name) {
    this.name = name;
  }

  public AbstractAnnotatedIdentifier(String name, TypeLiteral type) {
    this.name = name;
    this.type = type;
  }

  public final String getName() {
    return name;
  }

  @Override
  public TypeLiteral getType() {
    return type;
  }

  public void setType(TypeLiteral type) {
    this.type = type;
  }

  public Map<String, Value> getAnnotations() {
    return ImmutableMap.copyOf(annotations);
  }

  public Set<String> getAnnotationTypes() {
    return annotations.keySet();
  }

  public Value getAnnotation(String structName) {
    return annotations.get(structName);
  }

  public void addAnnotation(String annotation, Value value) {
    checkArgument(!annotations.containsKey(annotation), "Duplicate annotation %s", annotation);
    this.annotations.put(annotation, value);
  }

  @Override
  public String toString() {
    return getName() + "::" + getCategory() + "::" + getAnnotations();
  }

  @Override
  public void checkDuplicateAnnotation(SymbolTable table) {
    Set<Symbol> annotationSymbols = new HashSet<>();
    for (String annotation : annotations.keySet()) {
      Symbol symbol = table.find(annotation);
      checkState(!annotationSymbols.contains(symbol), "Duplicate annotation %s.", annotation);
      annotationSymbols.add(symbol);
    }
  }
}
