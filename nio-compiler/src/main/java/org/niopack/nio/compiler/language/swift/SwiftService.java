package org.niopack.nio.compiler.language.swift;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.base.Strings;
import com.google.common.collect.Iterables;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;
import org.niopack.nio.compiler.annotator.RestMethodParser;
import org.niopack.nio.compiler.annotator.RestServiceParser;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.language.swift.model.SwiftRestMethod;
import org.niopack.nio.compiler.language.swift.model.TypeInfo;
import org.niopack.nio.compiler.model.Identifier.Category;
import org.niopack.nio.compiler.model.Service;
import org.niopack.nio.compiler.model.Symbol;
import org.niopack.nio.core.RestMethod;
import org.niopack.nio.core.RestService;

final class SwiftService extends AbstractSwiftSymbol {

  private final Symbol symbol;
  private final SymbolTable table;
  private final Service definition;

  public SwiftService(Symbol symbol, SymbolTable table) {
    Category type = Iterables.getLast(symbol.getIdentifiers()).getCategory();
    checkArgument(type == Category.SERVICE, "%s is not a service.", symbol);
    this.symbol = checkNotNull(symbol, "symbol");
    this.table = checkNotNull(table, "table");
    this.definition = table.getDefinition(symbol);
  }

  @Override
  public Symbol getSymbol() {
    return symbol;
  }

  @Override
  public SymbolTable getTable() {
    return table;
  }

  @Override @SuppressWarnings("unchecked")
  public <T> T getDefinition() {
    return (T) definition;
  }

  @Override
  public Category getCategory() {
    return Category.SERVICE;
  }

  @Override
  public void write(Configuration cfg, Writer writer) throws TemplateException, IOException {
    Template template = cfg.getTemplate("swift/service_client.swift.ftl");
    template.process(getDataModel(), writer);
  }

  @Override
  public Map<String, Object> getDataModel() {
    Map<String, Object> dataModel = new HashMap<>();
    dataModel.put("module", getModule());
    dataModel.put("typeName", getSimpleName());

    // Module name
    Set<String> imports = new TreeSet<>();
    imports.addAll(TypeResolver.PREDEFINED_MODULES);

    // TODO(tamal): verify that request has fields annotated with a query param
    TypeResolver resolver = new TypeResolver(table, getModule());

    RestService restService = new RestServiceParser(definition, table).getAnnotation();
    checkNotNull(restService, "%s is missing @RestService", definition.getName());

    List<SwiftRestMethod> methods = definition.getMethods().values().stream()
        .map(method -> {
          String name = method.getName();
          TypeInfo requestType = resolver.resolve(method.getRequestType());
          TypeInfo responseType = resolver.resolve(method.getResponseType());
          RestMethod restMethod = new RestMethodParser(method, table).getAnnotation();
          checkNotNull(restMethod, "%s is missing @RestMethod", method.getName());
          String pathTemplate = restService.getPath() + Strings.nullToEmpty(restMethod.getPath());
          return new SwiftRestMethod(
              name,
              requestType,
              responseType,
              restMethod.getVerb(),
              pathTemplate.replace("{", "${"));
        })
        .collect(Collectors.toList());
    dataModel.put("methods", methods);
    dataModel.put("imports", imports);
    return dataModel;
  }
}
