package org.niopack.nio.compiler.language.java.model;

public class Annotation {

  private String nameConstant;
  private String klass;
  private String variable;
  private String instantiation;

  public Annotation(String klass, String variable, String instantiation) {
    this.klass = klass;
    this.variable = variable;
    this.instantiation = instantiation;
  }

  public String getNameConstant() {
    return nameConstant;
  }

  public void setNameConstant(String name) {
    this.nameConstant = name.toUpperCase();
  }

  public String getKlass() {
    return klass;
  }

  public void setKlass(String klass) {
    this.klass = klass;
  }

  public String getVariable() {
    return variable;
  }

  public void setVariable(String variable) {
    this.variable = variable;
  }

  public String getInstantiation() {
    return instantiation;
  }

  public void setInstantiation(String instantiation) {
    this.instantiation = instantiation;
  }
}
