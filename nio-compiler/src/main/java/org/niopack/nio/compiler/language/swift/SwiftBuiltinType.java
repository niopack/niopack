package org.niopack.nio.compiler.language.swift;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.collect.Iterables;
import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Path;
import java.util.Map;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.model.Identifier.Category;
import org.niopack.nio.compiler.model.Symbol;
import org.niopack.nio.compiler.model.type.TypeLiteral.Type;

final class SwiftBuiltinType extends AbstractSwiftSymbol {

  private final Symbol symbol;
  private final SymbolTable table;

  public SwiftBuiltinType(Symbol symbol, SymbolTable table) {
    Category type = Iterables.getLast(symbol.getIdentifiers()).getCategory();
    checkArgument(type == Category.BUILTIN, "%s is not a built-in symbol.", symbol);
    this.symbol = checkNotNull(symbol, "symbol");
    this.table = checkNotNull(table, "table");
  }

  public SymbolTable getTable() {
    return table;
  }

  @Override
  public String getModule() {
    Type type = getType();
    switch (type) {
      case BYTES:
        return "Foundation";
      case BOOL:
      case CHAR:
      case STRING:
      case FLOAT:
      case DOUBLE:
      case INT8:
      case INT16:
      case INT32:
      case INT64:
      case USEC:
      case MSEC:
      case VOID:
        return "";
      default:
        throw new IllegalStateException("");
    }
  }

  @Override @SuppressWarnings("unchecked")
  public <T> T getDefinition() {
    return (T) SymbolTable.EMPTY_DEFINITION;
  }

  public Type getType() {
    return Iterables.getLast(symbol.getIdentifiers()).getType().getType();
  }

  @Override
  public String getCanonicalName() {
    switch (getType()) {
      case BOOL:
        return "Bool";
      case BYTES:
        return "Foundation.NSData";
      case CHAR:
        return "Character";
      case STRING:
        return "String";
      case FLOAT:
        return "Float";
      case DOUBLE:
        return "Double";
      case INT8:
      case INT16:
      case INT32:
      case INT64:
      case USEC:
      case MSEC:
        return "Int";
      case VOID:
        return "Void";
      default:
        throw new IllegalStateException("");
    }
  }

  @Override
  public String getSimpleName() {
    switch (getType()) {
      case BOOL:
        return "Bool";
      case BYTES:
        return "NSData";
      case CHAR:
        return "Character";
      case STRING:
        return "String";
      case FLOAT:
        return "Float";
      case DOUBLE:
        return "Double";
      case INT8:
      case INT16:
      case INT32:
      case INT64:
      case USEC:
      case MSEC:
        return "Int";
      case VOID:
        return "Void";
      default:
        throw new IllegalStateException("");
    }
  }

  @Override
  public Symbol getSymbol() {
    return symbol;
  }

  @Override
  public Category getCategory() {
    return Category.BUILTIN;
  }

  @Override
  public Map<String, Object> getDataModel() {
    throw new IllegalStateException("Can't generate source data model.");
  }

  @Override
  public void write(Configuration cfg, Writer writer) throws TemplateException, IOException {
    throw new UnsupportedOperationException("Can't write Source file for built-in types.");
  }

  @Override
  public void write(Configuration cfg, Path outputDirectory)
      throws TemplateException, IOException {
    throw new UnsupportedOperationException("Can't write Source file for built-in types.");
  }
}
