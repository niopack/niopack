package org.niopack.nio.compiler.language.java;

import static com.google.common.base.CaseFormat.LOWER_CAMEL;
import static com.google.common.base.CaseFormat.UPPER_CAMEL;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import com.google.common.collect.Iterables;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.stream.Collectors;
import org.niopack.nio.compiler.annotator.RestMethodParser;
import org.niopack.nio.compiler.annotator.RestServiceParser;
import org.niopack.nio.compiler.common.Formatter;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.language.java.model.Annotation;
import org.niopack.nio.compiler.language.java.model.JavaRestMethod;
import org.niopack.nio.compiler.language.java.model.JavaRestService;
import org.niopack.nio.compiler.model.Identifier.Category;
import org.niopack.nio.compiler.model.Service;
import org.niopack.nio.compiler.model.Symbol;
import org.niopack.nio.compiler.model.value.StructureValue;
import org.niopack.nio.core.HttpVerb;
import org.niopack.nio.core.RestMethod;
import org.niopack.nio.core.RestService;

final class JavaService extends AbstractJavaSymbol {

  private final Symbol symbol;
  private final SymbolTable table;
  private final Service definition;

  public JavaService(Symbol symbol, SymbolTable table) {
    Category type = Iterables.getLast(symbol.getIdentifiers()).getCategory();
    checkArgument(type == Category.SERVICE, "%s is not a service.", symbol);
    this.symbol = checkNotNull(symbol, "symbol");
    this.table = checkNotNull(table, "table");
    this.definition = table.getDefinition(symbol);
  }

  @Override
  public Symbol getSymbol() {
    return symbol;
  }

  @Override
  public SymbolTable getTable() {
    return table;
  }

  @Override @SuppressWarnings("unchecked")
  public <T> T getDefinition() {
    return (T) definition;
  }

  @Override
  public Category getCategory() {
    return Category.SERVICE;
  }

  @Override
  public void write(Configuration cfg, Writer writer) throws TemplateException, IOException {
    if (isProvided()) {
      return;
    }
    writeSource(cfg, writer);
    writeNio(cfg, writer);
  }

  @Override
  public void write(Configuration cfg, Path outputDirectory)
      throws TemplateException, IOException {
    if (isProvided()) {
      return;
    }
    writeSource(cfg, getWriter(outputDirectory, ".java"));
    writeNio(cfg, getWriter(outputDirectory, "_Nio.java"));
  }

  private void writeSource(Configuration cfg, Writer writer) throws TemplateException, IOException {
    Template template = cfg.getTemplate("java/service.java.ftl");
    template.process(getSourceDataModel(), writer);
  }

  @Override
  protected Map<String, Object> getSourceDataModel() {
    Map<String, Object> dataModel = new HashMap<>();
    dataModel.put("packageName", getPackageName());
    dataModel.put("typeName", getSimpleName());

    // Static imports will never be affected by user defined nio messages.
    TreeSet<String> staticImports = new TreeSet<>();
    staticImports.add("java.lang.annotation.RetentionPolicy.RUNTIME");
    dataModel.put("staticImports", staticImports);

    // Simple class name -> Fully qualified class name
    Map<String, String> imports = new HashMap<>();
    imports.put("Response", "javax.ws.rs.core.Response");
    imports.put("BeanParam", "javax.ws.rs.BeanParam");
    imports.put("Path", "javax.ws.rs.Path");
    imports.put("Produces", "javax.ws.rs.Produces");
    imports.put("MediaType", "javax.ws.rs.core.MediaType");
    imports.put("Documented", "java.lang.annotation.Documented");
    imports.put("Retention", "java.lang.annotation.Retention");
    imports.put("Qualifier", "javax.inject.Qualifier");

    // TODO(tamal): verify that request has fields annotated with a query param
    TypeResolver resolver = new TypeResolver(table, getPackageName(), imports);

    RestService restService = new RestServiceParser(definition, table).getAnnotation();
    checkNotNull(restService, "%s is missing @RestService", definition.getName());
    dataModel.put("restService", new JavaRestService(
        // TODO(tamal): Use java value resolver
        restService.getPath(),
        "MediaType" + DOT + restService.getProduces()));

    List<JavaRestMethod> methods = definition.getMethods().values().stream()
        .map(method -> {
          String name = method.getName();
          String requestType = resolver.resolve(method.getRequestType());
          String responseType = resolver.resolve(method.getResponseType());
          RestMethod restMethod = new RestMethodParser(method, table).getAnnotation();
          checkNotNull(restMethod, "%s is missing @RestMethod", method.getName());
          String verb = resolver.resolve(HttpVerb.class, restMethod.getVerb());
          // TODO(tamal): Fix java string value type
          String path = restMethod.getPath();
          return new JavaRestMethod(name, requestType, responseType, verb, path);
        })
        .collect(Collectors.toList());
    dataModel.put("methods", methods);
    dataModel.put("imports", new TreeSet<>(imports.values()));
    return dataModel;
  }

  private void writeNio(Configuration cfg, Writer writer) throws IOException, TemplateException {
    Template template = cfg.getTemplate("java/service_nio.java.ftl");
    template.process(getNioDataModel(cfg), writer);
  }

  private Map<String, Object> getNioDataModel(Configuration cfg) {
    Map<String, Object> dataModel = new HashMap<>();
    dataModel.put("packageName", getPackageName());
    dataModel.put("typeName", definition.getName());

    // Static imports will never be affected by user defined nio messages.
    TreeSet<String> staticImports = new TreeSet<>();
    dataModel.put("staticImports", staticImports);

    // Simple class name -> Fully qualified class name
    Map<String, String> imports = new HashMap<>();
    imports.put("List", "java.util.List");
    imports.put("Map", "java.util.Map");
    imports.put("ImmutableList", "com.google.common.collect.ImmutableList");
    imports.put("ImmutableMap", "com.google.common.collect.ImmutableMap");
    imports.put("ImmutableTable", "com.google.common.collect.ImmutableTable");
    imports.put("Table", "com.google.common.collect.Table");

    TypeResolver resolver = new TypeResolver(table, getPackageName(), imports);
    NameGenerator nameGenerator = new NameGenerator();
    StructureInstantiator structCoder = new StructureInstantiator(resolver, nameGenerator);

    List<Annotation> typeAnnotations = definition.getAnnotations().values().stream()
        .map(annotation -> structCoder.codegen(
            cfg,
            UPPER_CAMEL.to(LOWER_CAMEL, ((StructureValue)annotation).getName()),
            (StructureValue) annotation))
        .collect(Collectors.toList());
    dataModel.put("typeAnnotations", typeAnnotations);

    List<Annotation> methodAnnotations = new ArrayList<>();
    definition.getMethods().values().stream()
        .forEach(method -> method.getAnnotations().values()
            .forEach(annotation ->  {
                Annotation methodAnnotation = structCoder.codegen(
                  cfg,
                  method.getName().toLowerCase() + ((StructureValue)annotation).getName(),
                  (StructureValue) annotation);
                methodAnnotation.setNameConstant(Formatter.toUpperUnderscore(method.getName()));
                methodAnnotations.add(methodAnnotation);
            })
        );
    dataModel.put("methodAnnotations", methodAnnotations);

    List<JavaRestMethod> methods = definition.getMethodNames().stream()
        .map(methodName -> new JavaRestMethod(methodName, null, null, null, null))
        .collect(Collectors.toList());
    dataModel.put("methods", methods);

    dataModel.put("imports", new TreeSet<>(imports.values()));
    return dataModel;
  }
}
