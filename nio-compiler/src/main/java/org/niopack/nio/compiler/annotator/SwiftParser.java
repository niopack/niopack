package org.niopack.nio.compiler.annotator;

import static org.niopack.nio.compiler.common.Formatter.unescape;

import com.google.common.collect.ImmutableSet;
import java.util.IllegalFormatException;
import java.util.Set;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.model.AnnotatedIdentifier;
import org.niopack.nio.compiler.model.Identifier.Category;
import org.niopack.nio.compiler.model.value.StructureValue;
import org.niopack.nio.core.Swift;
import org.niopack.nio.core.Swift_Nio;

public class SwiftParser extends AbstractAnnotationParser<Swift> {

  public SwiftParser(AnnotatedIdentifier identifier, SymbolTable table) {
    super(identifier, table);
  }

  @Override
  protected Set<Category> getSupportedCategories() {
    return ImmutableSet.of(Category.PACKAGE);
  }

  @Override
  public String getCanonicalName() {
    return ".org.niopack.nio.core.Swift";
  }

  @Override
  protected Swift parse(StructureValue value) throws IllegalFormatException {
    Swift annotation = new Swift();
    if (value.hasValue(Swift_Nio.MODULE)) {
      annotation.setModule(unescape(value.scalarValue(Swift_Nio.MODULE).toString()));
    }
    return annotation;
  }
}
