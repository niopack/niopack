package org.niopack.nio.compiler.annotator;

import com.google.common.collect.ImmutableSet;
import java.util.IllegalFormatException;
import java.util.Set;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.model.AnnotatedIdentifier;
import org.niopack.nio.compiler.model.Identifier.Category;
import org.niopack.nio.compiler.model.value.StructureValue;
import org.niopack.nio.core.Provided;
import org.niopack.nio.core.Provided_Nio;

public class ProvidedParser extends AbstractAnnotationParser<Provided> {

  public ProvidedParser(AnnotatedIdentifier identifier, SymbolTable table) {
    super(identifier, table);
  }

  @Override
  protected Set<Category> getSupportedCategories() {
    return ImmutableSet.of(Category.STRUCT, Category.ENUM);
  }

  @Override
  public String getCanonicalName() {
    return ".org.niopack.nio.core.Provided";
  }

  @Override
  protected Provided parse(StructureValue value) throws IllegalFormatException {
    Provided annotation = new Provided();
    if (value.hasValue(Provided_Nio.PROVIDED)) {
      annotation.setProvided(value.scalarValue(Provided_Nio.PROVIDED).toBool());
    } else {
      annotation.setProvided(false);
    }
    return annotation;
  }
}
