package org.niopack.nio.compiler.model.type;

public interface TypeLiteral {
  Type getType();

  public enum Type {
    /* Category: PACKAGE, FIELD_ENUM, FIELD_STRUCT, METHOD_SERVICE  */
    NONE(null),
    /* Category: BUILT-IN */
    BOOL("bool"),
    BYTES("bytes"),
    CHAR("char"),
    STRING("string"),
    FLOAT("float"),
    DOUBLE("double"),
    INT8("int8"),
    INT16("int16"),
    INT32("int32"),
    INT64("int64"),
    USEC("usec"),
    MSEC("msec"),
    VOID("void"),
    /* Category: ENUM | SERVICE | STRUCT */
    USER_DEFINED(null),
    /* ENUM | SERVICE should never be used by SymbolTable */
    ENUM(null),
    STRUCT(null),
    /* Composite Type */
    LIST(null),
    MAP(null);

    private String concreteType;

    private Type(String concreteType) {
      this.concreteType = concreteType;
    }

    public String getConcreteType() {
      return concreteType;
    }
  }
}
