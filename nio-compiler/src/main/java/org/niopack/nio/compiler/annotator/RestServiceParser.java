package org.niopack.nio.compiler.annotator;

import com.google.common.collect.ImmutableSet;
import java.util.IllegalFormatException;
import java.util.Set;
import org.niopack.nio.compiler.common.SymbolTable;
import org.niopack.nio.compiler.model.AnnotatedIdentifier;
import org.niopack.nio.compiler.model.Identifier.Category;
import org.niopack.nio.compiler.model.value.StructureValue;
import org.niopack.nio.core.MediaType;
import org.niopack.nio.core.RestService;
import org.niopack.nio.core.RestService_Nio;

public class RestServiceParser extends AbstractAnnotationParser<RestService> {

  public RestServiceParser(AnnotatedIdentifier identifier, SymbolTable table) {
    super(identifier, table);
  }

  @Override
  protected Set<Category> getSupportedCategories() {
    return ImmutableSet.of(Category.SERVICE);
  }

  @Override
  public String getCanonicalName() {
    return ".org.niopack.nio.core.RestService";
  }

  @Override
  protected RestService parse(StructureValue value) throws IllegalFormatException {
    RestService annotation = new RestService();
    if (value.hasValue(RestService_Nio.PATH)) {
      annotation.setPath(value.scalarValue(RestService_Nio.PATH).toString());
    }
    if (value.hasValue(RestService_Nio.PRODUCES)) {
      annotation.setProduces(value.scalarValue(RestService_Nio.PRODUCES).toEnum(MediaType.class));
    }
    return annotation;
  }
}
