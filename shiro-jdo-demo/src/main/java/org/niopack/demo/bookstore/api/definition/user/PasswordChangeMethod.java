package org.niopack.demo.bookstore.api.definition.user;

import com.fasterxml.jackson.annotation.JsonTypeName;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

public final class PasswordChangeMethod {

  public static final class Request {
    @PathParam("user_id") private long userId;
    @QueryParam("old_password") private String oldPassword;
    @QueryParam("new_password") private String newPassword;

    public long getUserId() {
      return userId;
    }

    public void setUserId(long userId) {
      this.userId = userId;
    }

    public String getOldPassword() {
      return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
      this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
      return newPassword;
    }

    public void setNewPassword(String newPassword) {
      this.newPassword = newPassword;
    }
  }

  @JsonTypeName("response")
  public static final class Response {
    private boolean passwordChanged;

    public boolean isPasswordChanged() {
      return passwordChanged;
    }

    public void setPasswordChanged(boolean passwordChanged) {
      this.passwordChanged = passwordChanged;
    }
  }
}
