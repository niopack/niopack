package org.niopack.demo.bookstore.dao;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import org.datanucleus.api.jdo.JDOPersistenceManager;
import org.datanucleus.query.typesafe.TypesafeQuery;
import org.niopack.demo.bookstore.model.store.Book;
import org.niopack.demo.bookstore.model.store.QBook;

public final class BookDao {

  public Book get(JDOPersistenceManager pm, long bookId) {
    checkNotNull(pm, "pm");

    TypesafeQuery<Book> tq = pm.newTypesafeQuery(Book.class);
    QBook cand = QBook.candidate();
    return tq.filter(cand.id.eq(bookId)).executeUnique();
  }

  public List<Book> list(JDOPersistenceManager pm) {
    checkNotNull(pm, "pm");

    TypesafeQuery<Book> tq = pm.newTypesafeQuery(Book.class);
    return tq.executeList();
  }
}
