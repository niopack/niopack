package org.niopack.demo.bookstore.model.store;

import java.util.Objects;
import javax.annotation.Nullable;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
import javax.jdo.annotations.Version;
import javax.jdo.annotations.VersionStrategy;

/**
 * The persistent class for book.
 */
@PersistenceCapable(table = "BOOK", detachable = "true")
@Version(strategy = VersionStrategy.DATE_TIME, column = "LAST_UPDATE_TIMESTAMP_IN_MILLIS")
public class Book {

  @PrimaryKey
  @Persistent(column = "BOOK_ID", valueStrategy = IdGeneratorStrategy.IDENTITY)
  private long id;

  @Persistent(column = "BOOK_NAME")
  private String name;

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(@Nullable String name) {
    this.name = name;
  }

  @Override
  public int hashCode() {
    return Objects.hash(this.id);
  }

  @Override
  public boolean equals(@Nullable Object obj) {
    if (!(obj instanceof Book)) {
      return false;
    }
    Book that = (Book) obj;
    return Objects.equals(this.id, that.id);
  }

  @Override
  public String toString() {
    return id + ":" + name;
  }
}
