package org.niopack.demo.bookstore.api.resource.user;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.inject.Inject;
import javax.ws.rs.BeanParam;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresGuest;
import org.niopack.demo.bookstore.api.definition.user.PasswordChangeMethod;
import org.niopack.demo.bookstore.api.definition.user.SeedMethod;
import org.niopack.demo.bookstore.api.definition.user.UserService;

public class UserServiceImpl implements UserService {

  private final PasswordChangeController passwordChange;
  private final SeedController seed;

  @Inject
  public UserServiceImpl(PasswordChangeController passwordChange, SeedController seed) {
    this.passwordChange = checkNotNull(passwordChange, "passwordChange");
    this.seed = checkNotNull(seed, "seed");
  }

  @Override
  @RequiresGuest
  public SeedMethod.Response seed(@BeanParam SeedMethod.Request request) {
    return seed.execute(request);
  }

  @Override
  @RequiresAuthentication
  public PasswordChangeMethod.Response changePassword(
      @BeanParam PasswordChangeMethod.Request request) {
    return passwordChange.execute(request);
  }
}
