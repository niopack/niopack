package org.niopack.demo.bookstore.api;

import com.google.common.collect.ImmutableList;
import com.google.inject.Module;
import org.apache.shiro.guice.aop.ShiroAopModule;
import org.apache.shiro.guice.web.ShiroWebModule;
import org.niopack.Application;
import org.niopack.guice.ResteasyGuiceServletContextListener;

public class Program {

  public static void main(String[] args) throws Exception {
    new Application().run(ApiServerContextListener.class);
  }

  private static class ApiServerContextListener extends ResteasyGuiceServletContextListener {
    @Override
    public Iterable<Module> getModules() {
      return ImmutableList.of(
          ShiroWebModule.guiceFilterModule(),
          new ShiroAopModule(),
          new SecurityModule(getServletContext()),
          new ResourceModule());
    }
  }
}
