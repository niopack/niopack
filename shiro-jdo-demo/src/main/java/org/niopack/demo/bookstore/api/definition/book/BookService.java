package org.niopack.demo.bookstore.api.definition.book;

import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/api/books")
@Produces(MediaType.APPLICATION_JSON)
public interface BookService {

  @POST
  CreateMethod.Response create(@BeanParam CreateMethod.Request request);

  @GET
  ListMethod.Response list(@BeanParam ListMethod.Request request);

  @GET
  @Path("{id}")
  GetMethod.Response get(@BeanParam GetMethod.Request request);
}
