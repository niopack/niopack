package org.niopack.demo.bookstore.api.resource.user;

import static com.google.common.base.Preconditions.checkNotNull;

import javax.inject.Inject;
import org.apache.shiro.authc.credential.PasswordService;
import org.niopack.db.jdo.Transactor;
import org.niopack.demo.bookstore.api.definition.user.SeedMethod.Request;
import org.niopack.demo.bookstore.api.definition.user.SeedMethod.Response;
import org.niopack.shiro.realm.jdo.model.Permission;
import org.niopack.shiro.realm.jdo.model.Role;
import org.niopack.shiro.realm.jdo.model.User;


public class SeedController {

  private final Transactor transactor;
  private final PasswordService passwordService;

  @Inject
  public SeedController(Transactor transactor, PasswordService passwordService) {
    this.transactor = checkNotNull(transactor, "transactor");
    this.passwordService = checkNotNull(passwordService, "passwordService");
  }

  public Response execute(Request request) {
    transactor.run(
        pm -> {
          // ADMIN
          User admin = createUser("admin", "secret");
          pm.makePersistent(admin);

          Role adminRole = new Role();
          adminRole.setRoleName("admin");
          adminRole.getUsers().add(admin);
          admin.getRoles().add(adminRole);

          Permission allPermission = new Permission();
          allPermission.setPermissionName("*");
          allPermission.getRoles().add(adminRole);
          adminRole.getPermissions().add(allPermission);

          // JOHN
          User john = createUser("john", "secret");
          pm.makePersistent(john);

          Role writerRole = new Role();
          writerRole.setRoleName("bookwriter");
          writerRole.getUsers().add(john);
          john.getRoles().add(writerRole);

          Permission writePermission = new Permission();
          writePermission.setPermissionName("book:write");
          writePermission.getRoles().add(writerRole);
          writerRole.getPermissions().add(writePermission);

          // JILL
          User jill = createUser("jill", "secret");
          pm.makePersistent(jill);

          Role readerRole = new Role();
          readerRole.setRoleName("bookreader");
          readerRole.getUsers().add(jill);
          jill.getRoles().add(readerRole);

          Permission readPermission = new Permission();
          readPermission.setPermissionName("book:read");
          readPermission.getRoles().add(readerRole);
          readerRole.getPermissions().add(readPermission);
        });
    Response response = new Response();
    response.getUsers().add("admin");
    response.getUsers().add("john");
    response.getUsers().add("jill");
    return response;
  }

  private User createUser(String userName, String password) {
    User user = new User();
    user.setUserName(userName);
    user.setCredential(passwordService.encryptPassword(password));
    return user;
  }
}
