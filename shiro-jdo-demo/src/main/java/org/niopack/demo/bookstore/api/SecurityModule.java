package org.niopack.demo.bookstore.api;

import com.google.inject.Key;
import com.google.inject.name.Names;
import javax.servlet.ServletContext;
import org.apache.shiro.authc.credential.PasswordMatcher;
import org.apache.shiro.authc.credential.PasswordService;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.cache.MemoryConstrainedCacheManager;
import org.apache.shiro.crypto.RandomNumberGenerator;
import org.apache.shiro.crypto.hash.HashService;
import org.apache.shiro.guice.web.ShiroWebModule;
import org.niopack.shiro.module.RealmModule;
import org.niopack.shiro.realm.jdo.JdoRealm;

class SecurityModule extends ShiroWebModule {

  public SecurityModule(ServletContext servletContext) {
    super(servletContext);
  }

  @SuppressWarnings("unchecked")
  @Override
  protected void configureShiroWeb() {
    install(new RealmModule());
    expose(RandomNumberGenerator.class);
    expose(HashService.class);
    expose(PasswordService.class);
    expose(PasswordMatcher.class);
    expose(JdoRealm.class);
    bindRealm().to(JdoRealm.class);

    bindConstant().annotatedWith(Names.named("shiro.permissionsLookupEnabled")).to(true);
    expose(Key.get(Boolean.class, Names.named("shiro.permissionsLookupEnabled")));

    // Use redis backed cache manager in production.
    bind(CacheManager.class).to(MemoryConstrainedCacheManager.class);
    expose(CacheManager.class);

    addFilterChain("/api/books/**", AUTHC_BASIC);
    addFilterChain("/api/users/*/change_password", AUTHC_BASIC);
    addFilterChain("/api/users/", ANON);
    // addFilterChain("/api/users/", config(REST, "POST"), ANON);

    addFilterChain("/admin/**", AUTHC_BASIC);
    addFilterChain("/tasks/**", AUTHC_BASIC);
  }
}
