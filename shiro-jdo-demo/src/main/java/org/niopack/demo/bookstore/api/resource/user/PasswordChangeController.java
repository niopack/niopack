package org.niopack.demo.bookstore.api.resource.user;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import javax.inject.Inject;
import org.apache.shiro.authc.credential.PasswordService;
import org.niopack.db.jdo.Transactor;
import org.niopack.demo.bookstore.api.definition.user.PasswordChangeMethod.Request;
import org.niopack.demo.bookstore.api.definition.user.PasswordChangeMethod.Response;
import org.niopack.logging.ContextualLogger;
import org.niopack.shiro.realm.jdo.dao.UserDao;
import org.niopack.shiro.realm.jdo.model.User;
import org.slf4j.Logger;

public class PasswordChangeController {

  public static final Logger logger = ContextualLogger.create();

  private final Transactor transactor;
  private final PasswordService passwordService;

  @Inject
  public PasswordChangeController(Transactor transactor, PasswordService passwordService) {
    this.transactor = checkNotNull(transactor, "transactor");
    this.passwordService = checkNotNull(passwordService, "passwordService");
  }

  public Response execute(Request request) {
    String oldPassword = checkNotNull(request.getOldPassword(), "oldPassword");
    String newPassword = checkNotNull(request.getNewPassword(), "newPassword");

    transactor.run(pm -> {
      UserDao dao = new UserDao();
      User user = dao.getByUserId(pm, request.getUserId());
      checkNotNull(user, "Can't find User for userId=%s.", request.getUserId());
      checkState(passwordService.passwordsMatch(oldPassword, user.getCredential()),
          "Old password does not match. Please, try again!");
      user.setCredential(passwordService.encryptPassword(newPassword));
    });
    Response response = new Response();
    response.setPasswordChanged(true);
    return response;
  }
}
