package org.niopack.demo.bookstore.api.definition.user;

import javax.ws.rs.BeanParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/api/users")
@Produces(MediaType.APPLICATION_JSON)
public interface UserService {

  @PUT
  SeedMethod.Response seed(@BeanParam SeedMethod.Request request);

  @PUT
  @Path("{user_id}/change_password")
  PasswordChangeMethod.Response changePassword(@BeanParam PasswordChangeMethod.Request request);
}
