package org.niopack.demo.bookstore.api.definition.user;

import com.fasterxml.jackson.annotation.JsonTypeName;
import java.util.ArrayList;
import java.util.List;

public final class SeedMethod {

  public static final class Request {
  }

  @JsonTypeName("response")
  public static final class Response {
    private List<String> users = new ArrayList<>();

    public List<String> getUsers() {
      return users;
    }

    public void setUsers(List<String> users) {
      this.users = users;
    }
  }
}
