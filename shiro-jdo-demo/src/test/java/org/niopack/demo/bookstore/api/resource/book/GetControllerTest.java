package org.niopack.demo.bookstore.api.resource.book;

import static com.google.common.truth.Truth.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;

import com.google.common.testing.NullPointerTester;
import javax.ws.rs.core.Response.Status;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.niopack.db.testing.MockDatabase;
import org.niopack.demo.bookstore.api.definition.book.GetMethod.Request;
import org.niopack.demo.bookstore.api.definition.book.GetMethod.Response;
import org.niopack.demo.bookstore.common.BookConverter;
import org.niopack.demo.bookstore.dao.BookDao;
import org.niopack.demo.bookstore.model.store.Book;
import org.niopack.resteasy.errors.ApplicationErrorException;

@RunWith(JUnit4.class)
public class GetControllerTest {

  @Rule public MockDatabase mockDb = new MockDatabase();
  @Rule public ExpectedException thrown = ExpectedException.none();

  @Test
  public void testAllPublicConstructors() {
    new NullPointerTester().testAllPublicConstructors(GetController.class);
  }

  @Test
  public void testAllPublicInstanceMethods() {
    GetController controller = createController();
    new NullPointerTester().testAllPublicInstanceMethods(controller);
  }

  @Test
  public void shouldFailOnAbsentBook() {
    GetController controller = createController();

    Request request = new Request();
    request.setId(1L);
    thrown.expect(ApplicationErrorException.class);
    thrown.expect(hasProperty("status", is(Status.NOT_FOUND)));
    controller.execute(request);
  }

  @Test
  public void shouldGetBook() {
    Book b1 = new Book();
    b1.setName("b1");
    Book b2 = new Book();
    b2.setName("b2");
    mockDb.getTransactor().run(pm -> {
      pm.makePersistent(b1);
      pm.makePersistent(b2);
    });

    Request request = new Request();
    request.setId(b1.getId());
    GetController controller = createController();
    Response response = controller.execute(request);
    BookConverter converter = new BookConverter();
    assertThat(response.getBook()).isEqualTo(converter.convert(b1));
  }

  private GetController createController() {
    return new GetController(mockDb.getTransactor(), new BookDao());
  }
}
