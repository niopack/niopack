package org.niopack.jetty;

import static com.google.common.truth.Truth.assertThat;

import org.junit.Test;
import org.niopack.jackson.DiscoverableSubtypeResolver;

public class HttpConnectorFactoryTest {
  @Test
  public void isDiscoverable() throws Exception {
    assertThat(new DiscoverableSubtypeResolver().getDiscoveredSubtypes())
        .contains(HttpConnectorFactory.class);
  }
}
