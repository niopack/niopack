package org.niopack.jetty;

import static com.google.common.truth.Truth.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableSet;
import com.google.common.io.Resources;
import org.junit.Before;
import org.junit.Test;
import org.niopack.jackson.ObjectMappers;
import org.niopack.util.Size;

public class GzipFilterFactoryTest {
  private GzipFilterFactory gzip;

  @Before
  public void setUp() throws Exception {
    ObjectMapper mapper = ObjectMappers.forJson();
    this.gzip = mapper.readValue(Resources.getResource("json/gzip.json"), GzipFilterFactory.class);
  }

  @Test
  public void canBeEnabled() throws Exception {
    assertThat(gzip.isEnabled()).isFalse();
  }

  @Test
  public void hasAMinimumEntitySize() throws Exception {
    assertThat(gzip.getMinimumEntitySize()).isEqualTo(Size.kilobytes(12));
  }

  @Test
  public void hasABufferSize() throws Exception {
    assertThat(gzip.getBufferSize()).isEqualTo(Size.kilobytes(32));
  }

  @Test
  public void hasExcludedUserAgents() throws Exception {
    assertThat(gzip.getExcludedUserAgents()).isEqualTo(ImmutableSet.of("IE"));
  }

  @Test
  public void hasCompressedMimeTypes() throws Exception {
    assertThat(gzip.getCompressedMimeTypes()).isEqualTo(ImmutableSet.of("text/plain"));
  }

  @Test
  public void varyIsOnlyForAcceptEncoding() throws Exception {
    assertThat(gzip.getVary()).isEqualTo("Accept-Encoding");
  }
}
