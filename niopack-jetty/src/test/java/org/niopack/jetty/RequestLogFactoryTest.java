package org.niopack.jetty;

import static com.google.common.truth.Truth.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Resources;
import java.util.TimeZone;
import org.junit.Before;
import org.junit.Test;
import org.niopack.jackson.ObjectMappers;
import org.niopack.logging.ConsoleAppenderFactory;
import org.niopack.logging.FileAppenderFactory;
import org.niopack.logging.SyslogAppenderFactory;

public class RequestLogFactoryTest {
  private RequestLogFactory requestLog;

  @Before
  public void setUp() throws Exception {
    final ObjectMapper objectMapper = ObjectMappers.forJson();
    objectMapper.getSubtypeResolver().registerSubtypes(ConsoleAppenderFactory.class,
        FileAppenderFactory.class,
        SyslogAppenderFactory.class);
    this.requestLog = objectMapper.readValue(
        Resources.getResource("json/requestLog.json"), RequestLogFactory.class);
  }

  @Test
  public void defaultTimeZoneIsUTC() {
    assertThat(requestLog.getTimeZone()).isEqualTo(TimeZone.getTimeZone("UTC"));
  }
}
