package org.niopack.demo.bookstore.dao;

import static com.google.common.truth.Truth.assertThat;

import java.util.List;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.niopack.db.testing.MockDatabase;
import org.niopack.demo.bookstore.model.store.Book;

@RunWith(JUnit4.class)
public class BookDaoTest {

  @Rule public MockDatabase mockDb = new MockDatabase();

  @Test
  public void shouldGetByIdSuccessfully() {
    Book book = new Book();
    book.setName("War & Peace");
    mockDb.getTransactor().run(pm -> pm.makePersistent(book));

    BookDao bookDao = new BookDao();
    Book storedBook = mockDb.getTransactor().retrieve(pm -> bookDao.get(pm, book.getId()));
    assertThat(storedBook).isEqualTo(book);
  }

  @Test
  public void shouldListSuccessfully() {
    Book b1 = new Book();
    b1.setName("War & Peace");
    Book b2 = new Book();
    b2.setName("The C Programming Language");

    BookDao bookDao = new BookDao();
    List<Book> books = mockDb.getTransactor().retrieve(pm -> {
      pm.makePersistent(b1);
      pm.makePersistent(b2);
      return bookDao.list(pm);
    });
    assertThat(books).containsExactly(b1, b2);
  }
}
