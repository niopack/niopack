package org.niopack.demo.bookstore.api.resource.book;

import static com.google.common.truth.Truth.assertThat;

import com.google.common.testing.NullPointerTester;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.niopack.db.testing.MockDatabase;
import org.niopack.demo.bookstore.api.definition.book.ListMethod.Request;
import org.niopack.demo.bookstore.api.definition.book.ListMethod.Response;
import org.niopack.demo.bookstore.common.BookConverter;
import org.niopack.demo.bookstore.dao.BookDao;
import org.niopack.demo.bookstore.model.store.Book;

@RunWith(JUnit4.class)
public class ListControllerTest {

  @Rule public MockDatabase mockDb = new MockDatabase();

  @Test
  public void testAllPublicConstructors() {
    new NullPointerTester().testAllPublicConstructors(ListController.class);
  }

  @Test
  public void testAllPublicInstanceMethods() {
    ListController controller = createController();
    new NullPointerTester().testAllPublicInstanceMethods(controller);
  }

  @Test
  public void shouldReturnEmptyResponse() {
    ListController controller = createController();

    Response response = controller.execute(new Request());
    assertThat(response.getBooks()).isEmpty();
  }

  @Test
  public void shouldReturnAllBooks() {
    Book b1 = new Book();
    b1.setName("b1");
    Book b2 = new Book();
    b2.setName("b2");
    mockDb.getTransactor().run(pm -> {
      pm.makePersistent(b1);
      pm.makePersistent(b2);
    });

    ListController controller = createController();
    Response response = controller.execute(new Request());
    BookConverter converter = new BookConverter();
    assertThat(response.getBooks()).containsExactly(converter.convert(b1), converter.convert(b2));
  }

  private ListController createController() {
    return new ListController(mockDb.getTransactor(), new BookDao());
  }
}
