package org.niopack.demo.bookstore.api.definition.book;

import com.fasterxml.jackson.annotation.JsonTypeName;
import javax.ws.rs.PathParam;
import org.niopack.demo.bookstore.model.service.Book;

public final class GetMethod {

  public static final class Request {
    @PathParam("id") private long id;

    public long getId() {
      return id;
    }

    public void setId(long id) {
      this.id = id;
    }
  }

  @JsonTypeName("response")
  public static final class Response {
    private Book book;

    public Book getBook() {
      return book;
    }

    public void setBook(Book book) {
      this.book = book;
    }
  }
}
