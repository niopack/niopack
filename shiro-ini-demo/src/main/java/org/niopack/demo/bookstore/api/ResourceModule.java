package org.niopack.demo.bookstore.api;

import com.codahale.metrics.health.HealthCheck;
import com.codahale.metrics.health.jvm.ThreadDeadlockHealthCheck;
import com.google.inject.multibindings.MapBinder;
import javax.inject.Singleton;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManagerFactory;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.niopack.db.jdo.Transactor;
import org.niopack.db.jdo.TransactorImpl;
import org.niopack.demo.bookstore.api.Qualifiers.MaxListLength;
import org.niopack.demo.bookstore.api.definition.book.BookService;
import org.niopack.demo.bookstore.api.resource.book.BookServiceImpl;
import org.niopack.guice.ResteasyServletModule;

class ResourceModule extends ResteasyServletModule {
  @Override
  protected void configureResources() {
    bind(PersistenceManagerFactory.class).toInstance(JDOHelper.getPersistenceManagerFactory("dev"));
    bind(Transactor.class).to(TransactorImpl.class).in(Singleton.class);

    // Resources
    bind(BookService.class).to(BookServiceImpl.class);

    // TODO(admin): Only allow CORS from webui.
    // CORS: http://www.eclipse.org/jetty/documentation/current/cross-origin-filter.html
    bind(CrossOriginFilter.class).in(Singleton.class);
    filter("/api/*").through(CrossOriginFilter.class);

    // TODO(admin): Use flag to set MaxListLength
    bindConstant().annotatedWith(MaxListLength.class).to(100L);

    MapBinder<String, HealthCheck> healthChecksBinder =
        MapBinder.newMapBinder(binder(), String.class, HealthCheck.class);
    healthChecksBinder.addBinding("myapp").to(ThreadDeadlockHealthCheck.class);
  }
}
