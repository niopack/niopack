package org.niopack.demo.bookstore.common;

import org.niopack.util.guava.Converter;

public class BookConverter extends Converter<
    org.niopack.demo.bookstore.model.store.Book,
    org.niopack.demo.bookstore.model.service.Book> {

  @Override
  protected org.niopack.demo.bookstore.model.service.Book doForward(
      org.niopack.demo.bookstore.model.store.Book in) {
    org.niopack.demo.bookstore.model.service.Book out =
        new org.niopack.demo.bookstore.model.service.Book();
    out.setId(in.getId());
    out.setName(in.getName());
    return out;
  }

  @Override
  protected org.niopack.demo.bookstore.model.store.Book doBackward(
      org.niopack.demo.bookstore.model.service.Book in) {
    org.niopack.demo.bookstore.model.store.Book out =
        new org.niopack.demo.bookstore.model.store.Book();
    out.setId(in.getId());
    out.setName(in.getName());
    return out;
  }
}
