```
#!shell

$ http -a john:secret -f POST 'http://localhost:8080/api/books' name='War and Peace'

$ http -a john:secret -f GET http://localhost:8080/api/books
$ http -a jill:secret -f GET http://localhost:8080/api/books

$ http -a john:secret -f GET 'http://localhost:8080/api/books/<book_id>'
$ http -a jill:secret -f GET 'http://localhost:8080/api/books/<book_id>'

$ http://localhost:8080/admin/
```
