package org.niopack.io;

import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.InflaterInputStream;
import org.iq80.snappy.SnappyInputStream;
import org.iq80.snappy.SnappyOutputStream;

public enum CompressionStrategy {
  NONE {
    @Override
    public Output getOutput(ByteArrayOutputStream out) throws IOException {
      return new Output(Base64.getEncoder().wrap(out));
    }

    @Override
    public Input getInput(ByteArrayInputStream in) throws IOException {
      return new Input(Base64.getDecoder().wrap(in));
    }
  },
  DEFLATE {
    @Override
    public Output getOutput(ByteArrayOutputStream out) throws IOException {
      return new Output(new DeflaterOutputStream(Base64.getEncoder().wrap(out)));
    }

    @Override
    public Input getInput(ByteArrayInputStream in) throws IOException {
      return new Input(new InflaterInputStream(Base64.getDecoder().wrap(in)));
    }
  },
  GZIP {
    @Override
    public Output getOutput(ByteArrayOutputStream out) throws IOException {
      return new Output(new GZIPOutputStream(Base64.getEncoder().wrap(out)));
    }

    @Override
    public Input getInput(ByteArrayInputStream in) throws IOException {
      return new Input(new GZIPInputStream(Base64.getDecoder().wrap(in)));
    }
  },
  SNAPPY {
    @Override
    public Output getOutput(ByteArrayOutputStream out) throws IOException {
      return new Output(new SnappyOutputStream(out));
    }

    @Override
    public Input getInput(ByteArrayInputStream in) throws IOException {
      return new Input(new SnappyInputStream(in));
    }
  };

  public abstract Output getOutput(ByteArrayOutputStream out) throws IOException;

  public abstract Input getInput(ByteArrayInputStream in) throws IOException;
}
