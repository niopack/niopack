package org.niopack.io;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.pool.KryoFactory;
import com.esotericsoftware.kryo.pool.KryoPool;
import com.google.common.base.Throwables;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

public final class SerDerUtils {

  private static final KryoFactory factory = new KryoFactory() {
    public Kryo create () {
      Kryo kryo = new Kryo();
      // configure kryo instance, customize settings
      return kryo;
    }
  };
  // Build pool with SoftReferences enabled (optional)
  private static final KryoPool pool = new KryoPool.Builder(factory).softReferences().build();

  public static byte[] serialize(Object obj, CompressionStrategy strategy) {
    if (obj == null) {
      return new byte[0];
    }
    ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
    try (Output output = strategy.getOutput(byteStream)) {
      pool.run(kryo -> {
        kryo.writeClassAndObject(output, obj);
        return null;
      });
    } catch (Throwable t) {
      throw Throwables.propagate(t);
    }
    return byteStream.toByteArray();
  }

  public static Object deserialize(byte[] data, CompressionStrategy strategy) {
    if (isEmpty(data)) {
      return null;
    }
    try (Input input = strategy.getInput(new ByteArrayInputStream(data))) {
      return pool.run(kryo -> kryo.readClassAndObject(input));
    } catch (Throwable t) {
      throw Throwables.propagate(t);
    }
  }

  private static boolean isEmpty(byte[] data) {
    return data == null || data.length == 0;
  }

  private SerDerUtils() {}
}
