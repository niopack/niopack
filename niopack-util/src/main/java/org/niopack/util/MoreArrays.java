package org.niopack.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MoreArrays {

  @SafeVarargs
  public static <T> List<T> asList(T... a) {
  return a == null? new ArrayList<>() :  Arrays.asList(a);
  }

  private MoreArrays(){
  }
}
