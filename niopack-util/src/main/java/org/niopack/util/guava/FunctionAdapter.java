package org.niopack.util.guava;

import com.google.common.base.Converter;

public final class FunctionAdapter<A, B> extends
    Converter<com.google.common.base.Function<A, B>, java.util.function.Function<A, B>> {

  @Override
  protected java.util.function.Function<A, B> doForward(
      final com.google.common.base.Function<A, B> a) {
    return new java.util.function.Function<A, B>() {
      @Override
      public B apply(A t) {
        return a.apply(t);
      }
    };
  }

  @Override
  protected com.google.common.base.Function<A, B> doBackward(
      final java.util.function.Function<A, B> b) {
    return new com.google.common.base.Function<A, B>() {
      @Override
      public B apply(A t) {
        return b.apply(t);
      }
    };
  }
}
