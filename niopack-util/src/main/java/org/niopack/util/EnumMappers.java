package org.niopack.util;

import com.google.common.base.Converter;
import com.google.common.base.Enums;
import com.google.common.collect.BiMap;
import com.google.common.collect.EnumBiMap;

public class EnumMappers {

  public static <A extends Enum<A>, B extends Enum<B>> BiMap<A, B> mapByName(Class<A> klassA,
    Class<B> klassB) {
  Converter<String, A> nameToA = Enums.<A>stringConverter(klassA);
  Converter<String, B> nameToB = Enums.<B>stringConverter(klassB);
  BiMap<A, B> result = EnumBiMap.create(klassA, klassB);
  for (A a : klassA.getEnumConstants()) {
    result.put(a, nameToB.convert(nameToA.reverse().convert(a)));
  }
  for (B b : klassB.getEnumConstants()) {
    nameToA.convert(nameToB.reverse().convert(b));
  }
  return result;
  }

  private EnumMappers() {
  }
}
