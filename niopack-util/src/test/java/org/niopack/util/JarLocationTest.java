package org.niopack.util;

import static com.google.common.truth.Truth.assertThat;

import java.util.Optional;
import org.junit.Test;

public class JarLocationTest {
  @Test
  public void isHumanReadable() throws Exception {
    assertThat(new JarLocation(JarLocationTest.class).toString())
        .isEqualTo("project.jar");
  }

  @Test
  public void hasAVersion() throws Exception {
    assertThat(new JarLocation(JarLocationTest.class).getVersion())
        .isEqualTo(Optional.<String>empty());
  }
}
