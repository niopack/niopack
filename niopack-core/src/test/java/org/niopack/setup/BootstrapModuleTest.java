package org.niopack.setup;

import static com.google.common.truth.Truth.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.TypeLiteral;
import org.junit.Test;
import org.niopack.Application;

public class BootstrapModuleTest {

  @Test
  public void hasAnApplication() throws Exception {
    Application application = new Application();
    Injector injector = Guice.createInjector(new BootstrapModule(application));
    assertThat(injector.getInstance(Key.get(new TypeLiteral<Application>() {
    }))).isEqualTo(application);
  }

  @Test
  public void hasAnObjectMapper() throws Exception {
    Injector injector = Guice.createInjector(new BootstrapModule(new Application()));
    assertThat(injector.getInstance(ObjectMapper.class)).isNotNull();
  }
}
