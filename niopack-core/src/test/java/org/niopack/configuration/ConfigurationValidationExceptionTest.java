package org.niopack.configuration;

import static com.google.common.truth.Truth.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assume.assumeThat;

import java.util.Locale;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;
import org.junit.Before;
import org.junit.Test;

public class ConfigurationValidationExceptionTest {
  private static class Example {
    @NotNull
    String woo;
  }

  private ConfigurationValidationException e;

  @Before
  public void setUp() throws Exception {
    assumeThat(Locale.getDefault().getLanguage(), is("en"));

    final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
    final Set<ConstraintViolation<Example>> violations = validator.validate(new Example());
    this.e = new ConfigurationValidationException("config.json", violations);
  }

  @Test
  public void formatsTheViolationsIntoAHumanReadableMessage() throws Exception {
    assertThat(e.getMessage())
        .isEqualTo(String.format(
            "config.json has an error:%n" +
                "  * woo may not be null (was null)%n"
        ));
  }

  @Test
  public void retainsTheSetOfExceptions() throws Exception {
    assertThat(e.getConstraintViolations())
        .isNotEmpty();
  }
}
