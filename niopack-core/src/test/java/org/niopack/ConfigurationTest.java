package org.niopack;

import static com.google.common.truth.Truth.assertThat;

import org.junit.Test;
import org.niopack.configuration.Configuration;

public class ConfigurationTest {
  private final Configuration configuration = new Configuration();

  @Test
  public void hasAnHttpConfiguration() throws Exception {
    assertThat(configuration.getServerFactory())
        .isNotNull();
  }

  @Test
  public void hasALoggingConfiguration() throws Exception {
    assertThat(configuration.getLoggingFactory())
        .isNotNull();
  }

  //TODO(codefx): Fix test
  /*
  @Test
  public void ensureConfigSerializable() throws Exception {
    final ObjectMapper mapper = ObjectMappers.forJson();
    mapper.getSubtypeResolver()
        .registerSubtypes(ServiceFinder.find(AppenderFactory.class).toClassArray());
    mapper.getSubtypeResolver()
        .registerSubtypes(ServiceFinder.find(ConnectorFactory.class).toClassArray());

    // Issue-96: some types were not serializable
    final String json = mapper.writeValueAsString(configuration);
    assertThat(json)
        .isNotNull();

    // and as an added bonus, let's see we can also read it back:
    final Configuration cfg = mapper.readValue(json, Configuration.class);
    assertThat(cfg)
        .isNotNull();
  }
  */
}
