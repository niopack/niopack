package org.niopack.server;

import static com.google.common.truth.Truth.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Resources;
import java.util.concurrent.CountDownLatch;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import org.junit.Before;
import org.junit.Test;
import org.niopack.jackson.ObjectMappers;
import org.niopack.jetty.HttpConnectorFactory;
import org.niopack.logging.ConsoleAppenderFactory;
import org.niopack.logging.FileAppenderFactory;
import org.niopack.logging.SyslogAppenderFactory;

public class ServerFactoryTest {
  private ServerFactory http;

  @Before
  public void setUp() throws Exception {
    final ObjectMapper objectMapper = ObjectMappers.forJson();
    objectMapper.getSubtypeResolver().registerSubtypes(ConsoleAppenderFactory.class,
        FileAppenderFactory.class,
        SyslogAppenderFactory.class,
        HttpConnectorFactory.class);
    this.http = objectMapper.readValue(
        Resources.getResource("json/server.json"), ServerFactory.class);
  }

  @Test
  public void loadsGzipConfig() throws Exception {
    assertThat(http.getGzipFilterFactory().isEnabled())
        .isFalse();
  }

  @Test
  public void hasAMaximumNumberOfThreads() throws Exception {
    assertThat(http.getMaxThreads())
        .isEqualTo(101);
  }

  @Test
  public void hasAMinimumNumberOfThreads() throws Exception {
    assertThat(http.getMinThreads())
        .isEqualTo(89);
  }

  /*
  @Test @Ignore 
  public void testGracefulShutdown() throws Exception {
    ObjectMapper objectMapper = ObjectMappers.forJson();
    Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
    MetricRegistry metricRegistry = new MetricRegistry();
    Environment environment = new Environment("test", objectMapper, validator, metricRegistry,
        ClassLoader.getSystemClassLoader());

    CountDownLatch requestReceived = new CountDownLatch(1);
    CountDownLatch shutdownInvoked = new CountDownLatch(1);

    environment.jersey().register(new TestResource(requestReceived, shutdownInvoked));

    final ScheduledExecutorService executor = Executors.newScheduledThreadPool(3);
    final Server server = http.build(environment);

    ((AbstractNetworkConnector)server.getConnectors()[0]).setPort(0);

    ScheduledFuture<Void> cleanup = executor.schedule(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        if (!server.isStopped()) {
          server.stop();
        }
        executor.shutdownNow();
        return null;
      }
    }, 5, TimeUnit.SECONDS);


    server.start();

    final int port = ((AbstractNetworkConnector) server.getConnectors()[0]).getLocalPort();

    Future<String> futureResult = executor.submit(new Callable<String>() {
      @Override
      public String call() throws Exception {
        URL url = new URL("http://localhost:" + port + "/test");
        URLConnection connection = url.openConnection();
        connection.connect();
        return CharStreams.toString(new InputStreamReader(connection.getInputStream()));
      }
    });

    requestReceived.await();

    Future<Void> serverStopped = executor.submit(new Callable<Void>() {
      @Override
      public Void call() throws Exception {
        server.stop();
        return null;
      }
    });

    Connector[] connectors = server.getConnectors();
    assertThat(connectors).isNotEmpty();
    assertThat(connectors[0]).isInstanceOf(NetworkConnector.class);
    NetworkConnector connector = (NetworkConnector) connectors[0];

    // wait for server to close the connectors
    while (true) {
      if (!connector.isOpen()) {
        shutdownInvoked.countDown();
        break;
      }
      Thread.sleep(5);
    }

    String result = futureResult.get();
    assertThat(result).isEqualTo("test");

    serverStopped.get();

    // cancel the cleanup future since everything succeeded
    cleanup.cancel(false);
    executor.shutdownNow();
  }
  */

  @Path("/test")
  @Produces("text/plain")
  public static class TestResource {

    private final CountDownLatch requestReceived;
    private final CountDownLatch shutdownInvoked;

    public TestResource(CountDownLatch requestReceived, CountDownLatch shutdownInvoked) {
      this.requestReceived = requestReceived;
      this.shutdownInvoked = shutdownInvoked;
    }

    @GET
    public String get() throws Exception {
      requestReceived.countDown();
      shutdownInvoked.await();
      return "test";
    }
  }
}
