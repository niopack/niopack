package org.niopack.server;

import static com.codahale.metrics.servlets.HealthCheckServlet.HEALTH_CHECK_REGISTRY;
import static com.codahale.metrics.servlets.MetricsServlet.METRICS_REGISTRY;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.health.HealthCheckRegistry;
import com.codahale.metrics.jetty9.InstrumentedHandler;
import com.codahale.metrics.jetty9.InstrumentedQueuedThreadPool;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.regex.Pattern;
import javax.servlet.DispatcherType;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ErrorHandler;
import org.eclipse.jetty.server.handler.RequestLogHandler;
import org.eclipse.jetty.server.handler.StatisticsHandler;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.setuid.RLimit;
import org.eclipse.jetty.setuid.SetUIDListener;
import org.eclipse.jetty.util.BlockingArrayQueue;
import org.eclipse.jetty.util.thread.ThreadPool;
import org.jboss.resteasy.plugins.server.servlet.ResteasyBootstrap;
import org.niopack.Application;
import org.niopack.configuration.Configuration;
import org.niopack.guice.ResteasyGuiceServletContextListener;
import org.niopack.jetty.ConnectorFactory;
import org.niopack.jetty.GzipFilterFactory;
import org.niopack.jetty.HttpConnectorFactory;
import org.niopack.jetty.RequestLogFactory;
import org.niopack.logging.ContextualLogger;
import org.niopack.servlets.ThreadNameFilter;
import org.niopack.util.Duration;
import org.niopack.validation.MinDuration;
import org.niopack.validation.ValidationMethod;
import org.slf4j.Logger;

// TODO: 5/15/13 <coda> -- add tests for DefaultServerFactory

/**
 * A factory for building {@link Server} instances for Niopack applications. This allows for
 * multiple sets of connectors, all running on separate ports.
 * <p>
 * <b>Configuration Parameters:</b>
 * <table summary="Configuration Parameters">
 * <tr>
 * <td>Name</td>
 * <td>Default</td>
 * <td>Description</td>
 * </tr>
 * <tr>
 * <td>{@code connectors}</td>
 * <td>An {@link HttpConnectorFactory HTTP connector} listening on port 8080.</td>
 * <td>A set of {@link ConnectorFactory connectors} which will handle application requests.</td>
 * </tr>
 * <tr>
 * <td>{@code requestLog}</td>
 * <td></td>
 * <td>The {@link RequestLogFactory request log} configuration.</td>
 * </tr>
 * <tr>
 * <td>{@code gzip}</td>
 * <td></td>
 * <td>The {@link GzipFilterFactory GZIP} configuration.</td>
 * </tr>
 * <tr>
 * <td>{@code maxThreads}</td>
 * <td>1024</td>
 * <td>The maximum number of threads to use for requests.</td>
 * </tr>
 * <tr>
 * <td>{@code minThreads}</td>
 * <td>8</td>
 * <td>The minimum number of threads to use for requests.</td>
 * </tr>
 * <tr>
 * <td>{@code maxQueuedRequests}</td>
 * <td>1024</td>
 * <td>The maximum number of requests to queue before blocking the acceptors.</td>
 * </tr>
 * <tr>
 * <td>{@code idleThreadTimeout}</td>
 * <td>1 minute</td>
 * <td>The amount of time a worker thread can be idle before being stopped.</td>
 * </tr>
 * <tr>
 * <td>{@code nofileSoftLimit}</td>
 * <td>(none)</td>
 * <td>
 * The number of open file descriptors before a soft error is issued. <b>Requires Jetty's
 * {@code libsetuid.so} on {@code java.library.path}.</b></td>
 * </tr>
 * <tr>
 * <td>{@code nofileHardLimit}</td>
 * <td>(none)</td>
 * <td>
 * The number of open file descriptors before a hard error is issued. <b>Requires Jetty's
 * {@code libsetuid.so} on {@code java.library.path}.</b></td>
 * </tr>
 * <tr>
 * <td>{@code gid}</td>
 * <td>(none)</td>
 * <td>
 * The group ID to switch to once the connectors have started. <b>Requires Jetty's
 * {@code libsetuid.so} on {@code java.library.path}.</b></td>
 * </tr>
 * <tr>
 * <td>{@code uid}</td>
 * <td>(none)</td>
 * <td>
 * The user ID to switch to once the connectors have started. <b>Requires Jetty's
 * {@code libsetuid.so} on {@code java.library.path}.</b></td>
 * </tr>
 * <tr>
 * <td>{@code user}</td>
 * <td>(none)</td>
 * <td>
 * The username to switch to once the connectors have started. <b>Requires Jetty's
 * {@code libsetuid.so} on {@code java.library.path}.</b></td>
 * </tr>
 * <tr>
 * <td>{@code group}</td>
 * <td>(none)</td>
 * <td>
 * The group to switch to once the connectors have started. <b>Requires Jetty's {@code libsetuid.so}
 * on {@code java.library.path}.</b></td>
 * </tr>
 * <tr>
 * <td>{@code umask}</td>
 * <td>(none)</td>
 * <td>
 * The umask to switch to once the connectors have started. <b>Requires Jetty's {@code libsetuid.so}
 * on {@code java.library.path}.</b></td>
 * </tr>
 * <tr>
 * <td>{@code startsAsRoot}</td>
 * <td>(none)</td>
 * <td>
 * Whether or not the Niopack application is started as a root user. <b>Requires Jetty's
 * {@code libsetuid.so} on {@code java.library.path}.</b></td>
 * </tr>
 * <tr>
 * <td>{@code shutdownGracePeriod}</td>
 * <td>30 seconds</td>
 * <td>
 * The maximum time to wait for Jetty, and all Managed instances, to cleanly shutdown before
 * forcibly terminating them.</td>
 * </tr>
 * </table>
 */
public class ServerFactory {

  private static final Pattern WINDOWS_NEWLINE = Pattern.compile("\\r\\n?");
  private static final Logger logger = ContextualLogger.create();

  @Valid
  @NotNull
  private List<ConnectorFactory> connectors = new ArrayList<>(
      Collections.singletonList(HttpConnectorFactory.standard()));

  @Valid
  @NotNull
  private RequestLogFactory requestLog = new RequestLogFactory();

  @Valid
  @NotNull
  private GzipFilterFactory gzip = new GzipFilterFactory();

  @Min(2)
  private int maxThreads = 1024;

  @Min(1)
  private int minThreads = 8;

  private int maxQueuedRequests = 1024;

  @MinDuration(1)
  private Duration idleThreadTimeout = Duration.minutes(1);

  @Min(1)
  private Integer nofileSoftLimit;

  @Min(1)
  private Integer nofileHardLimit;

  private Integer gid;

  private Integer uid;

  private String user;

  private String group;

  private String umask;

  private Boolean startsAsRoot;

  private Duration shutdownGracePeriod = Duration.seconds(30);

  @JsonProperty
  public List<ConnectorFactory> getConnectors() {
    return connectors;
  }

  @JsonProperty
  public void setApplicationConnectors(List<ConnectorFactory> connectors) {
    this.connectors = connectors;
  }

  @JsonIgnore
  @ValidationMethod(message = "must have a smaller minThreads than maxThreads")
  public boolean isThreadPoolSizedCorrectly() {
    return minThreads <= maxThreads;
  }

  @JsonProperty("requestLog")
  public RequestLogFactory getRequestLogFactory() {
    return requestLog;
  }

  @JsonProperty("requestLog")
  public void setRequestLogFactory(RequestLogFactory requestLog) {
    this.requestLog = requestLog;
  }

  @JsonProperty("gzip")
  public GzipFilterFactory getGzipFilterFactory() {
    return gzip;
  }

  @JsonProperty("gzip")
  public void setGzipFilterFactory(GzipFilterFactory gzip) {
    this.gzip = gzip;
  }

  @JsonProperty
  public int getMaxThreads() {
    return maxThreads;
  }

  @JsonProperty
  public void setMaxThreads(int count) {
    this.maxThreads = count;
  }

  @JsonProperty
  public int getMinThreads() {
    return minThreads;
  }

  @JsonProperty
  public void setMinThreads(int count) {
    this.minThreads = count;
  }

  @JsonProperty
  public int getMaxQueuedRequests() {
    return maxQueuedRequests;
  }

  @JsonProperty
  public void setMaxQueuedRequests(int maxQueuedRequests) {
    this.maxQueuedRequests = maxQueuedRequests;
  }

  @JsonProperty
  public Duration getIdleThreadTimeout() {
    return idleThreadTimeout;
  }

  @JsonProperty
  public void setIdleThreadTimeout(Duration idleThreadTimeout) {
    this.idleThreadTimeout = idleThreadTimeout;
  }

  @JsonProperty
  public Integer getNofileSoftLimit() {
    return nofileSoftLimit;
  }

  @JsonProperty
  public void setNofileSoftLimit(Integer nofileSoftLimit) {
    this.nofileSoftLimit = nofileSoftLimit;
  }

  @JsonProperty
  public Integer getNofileHardLimit() {
    return nofileHardLimit;
  }

  @JsonProperty
  public void setNofileHardLimit(Integer nofileHardLimit) {
    this.nofileHardLimit = nofileHardLimit;
  }

  @JsonProperty
  public Integer getGid() {
    return gid;
  }

  @JsonProperty
  public void setGid(Integer gid) {
    this.gid = gid;
  }

  @JsonProperty
  public Integer getUid() {
    return uid;
  }

  @JsonProperty
  public void setUid(Integer uid) {
    this.uid = uid;
  }

  @JsonProperty
  public String getUser() {
    return user;
  }

  @JsonProperty
  public void setUser(String user) {
    this.user = user;
  }

  @JsonProperty
  public String getGroup() {
    return group;
  }

  @JsonProperty
  public void setGroup(String group) {
    this.group = group;
  }

  @JsonProperty
  public String getUmask() {
    return umask;
  }

  @JsonProperty
  public void setUmask(String umask) {
    this.umask = umask;
  }

  @JsonProperty
  public Boolean getStartsAsRoot() {
    return startsAsRoot;
  }

  @JsonProperty
  public void setStartsAsRoot(Boolean startsAsRoot) {
    this.startsAsRoot = startsAsRoot;
  }

  @JsonProperty
  public Duration getShutdownGracePeriod() {
    return shutdownGracePeriod;
  }

  @JsonProperty
  public void setShutdownGracePeriod(Duration shutdownGracePeriod) {
    this.shutdownGracePeriod = shutdownGracePeriod;
  }

  /**
   * Creates a server for the given Niopack application.
   *
   * @param bootstrapInjector the application's bootstrap Guice injector
   * @param configuration the application's configuration
   * @return a {@link Server} running the Niopack application
   */
  public <T extends Configuration> Server create(Injector bootstrapInjector, T configuration,
      ResteasyGuiceServletContextListener contextListener) {
    Application application = bootstrapInjector.getInstance(Application.class);
    MetricRegistry metricRegistry = bootstrapInjector.getInstance(MetricRegistry.class);
    HealthCheckRegistry healthCheckRegistry =
        bootstrapInjector.getInstance(HealthCheckRegistry.class);

    printBanner(application.getName());
    ThreadPool threadPool = createThreadPool(metricRegistry);
    Server server = buildServer(threadPool);
    connectors.forEach(factory -> {
      Connector connector = factory.build(server, metricRegistry, application.getName(), null);
      server.addConnector(connector);
    });

    ServletContextHandler handler =
        createHandler(contextListener, metricRegistry, healthCheckRegistry, server);
    server.setHandler(addStatsHandler(addRequestLog(
        addInstrumentation(handler, server, metricRegistry), server, application.getName())));
    return server;
  }

  protected void printBanner(String name) {
    try {
      String banner = WINDOWS_NEWLINE
          .matcher(Resources.toString(Resources.getResource("banner.txt"), Charsets.UTF_8))
          .replaceAll("\n")
          .replace("\n", String.format("%n"));
      logger.info(String.format("Starting {}%n{}"), name, banner);
    } catch (IllegalArgumentException | IOException ignored) {
      // don't display the banner if there isn't one
      logger.info("Starting {}", name);
    }
  }

  protected ThreadPool createThreadPool(MetricRegistry metricRegistry) {
    final BlockingQueue<Runnable> queue =
        new BlockingArrayQueue<>(minThreads, maxThreads, maxQueuedRequests);
    final InstrumentedQueuedThreadPool threadPool =
        new InstrumentedQueuedThreadPool(metricRegistry, maxThreads, minThreads,
            (int) idleThreadTimeout.toMilliseconds(), queue);
    threadPool.setName("niopack");
    return threadPool;
  }

  protected Server buildServer(/* LifecycleEnvironment lifecycle, */ThreadPool threadPool) {
    final Server server = new Server(threadPool);
    server.addLifeCycleListener(buildSetUIDListener());
    // lifecycle.attach(server);
    final ErrorHandler errorHandler = new ErrorHandler();
    errorHandler.setServer(server);
    errorHandler.setShowStacks(false);
    server.addBean(errorHandler);
    server.setStopAtShutdown(true);
    server.setStopTimeout(shutdownGracePeriod.toMilliseconds());
    return server;
  }

  protected SetUIDListener buildSetUIDListener() {
    final SetUIDListener listener = new SetUIDListener();

    if (startsAsRoot != null) {
      listener.setStartServerAsPrivileged(startsAsRoot);
    }

    if (gid != null) {
      listener.setGid(gid);
    }

    if (uid != null) {
      listener.setUid(uid);
    }

    if (user != null) {
      listener.setUsername(user);
    }

    if (group != null) {
      listener.setGroupname(group);
    }

    if (nofileHardLimit != null || nofileSoftLimit != null) {
      final RLimit rlimit = new RLimit();
      if (nofileHardLimit != null) {
        rlimit.setHard(nofileHardLimit);
      }

      if (nofileSoftLimit != null) {
        rlimit.setSoft(nofileSoftLimit);
      }

      listener.setRLimitNoFiles(rlimit);
    }

    if (umask != null) {
      listener.setUmaskOctal(umask);
    }

    return listener;
  }

  private ServletContextHandler createHandler(ResteasyGuiceServletContextListener contextListener,
      MetricRegistry metricRegistry, HealthCheckRegistry healthCheckRegistry, Server server) {
    // No Security support, as these will be handled by Shiro
    ServletContextHandler handler = new ServletContextHandler(ServletContextHandler.SESSIONS);
    handler.setClassLoader(Thread.currentThread().getContextClassLoader());
    handler.setServer(server);
    handler.getServletContext().setAttribute(METRICS_REGISTRY, metricRegistry);
    handler.getServletContext().setAttribute(HEALTH_CHECK_REGISTRY, healthCheckRegistry);
    handler.addFilter(GuiceFilter.class, "/*", EnumSet.allOf(DispatcherType.class));
    handler.addFilter(ThreadNameFilter.class, "/*", EnumSet.of(DispatcherType.REQUEST));
    if (gzip.isEnabled()) {
      final FilterHolder holder = new FilterHolder(gzip.build());
      handler.addFilter(holder, "/*", EnumSet.allOf(DispatcherType.class));
    }
    handler.setContextPath("/");
    // Bootstrap RestEasy. Must occur before Guice is initialized.
    handler.addEventListener(new ResteasyBootstrap());
    handler.addEventListener(contextListener);
    return handler;
  }

  protected Handler addInstrumentation(Handler handler, Server server, MetricRegistry metricRegistry) {
    InstrumentedHandler instrumented = new InstrumentedHandler(metricRegistry);
    instrumented.setServer(server);
    instrumented.setHandler(handler);
    return instrumented;
  }

  protected Handler addRequestLog(Handler handler, Server server, String name) {
    if (requestLog.isEnabled()) {
      final RequestLogHandler requestLogHandler = new RequestLogHandler();
      requestLogHandler.setRequestLog(requestLog.build(name));
      // server should own the request log's lifecycle since it's already started,
      // the handler might not become managed in case of an error which would leave
      // the request log stranded
      server.addBean(requestLogHandler.getRequestLog(), true);
      requestLogHandler.setHandler(handler);
      return requestLogHandler;
    }
    return handler;
  }

  protected Handler addStatsHandler(Handler handler) {
    // Graceful shutdown is implemented via the statistics handler,
    // see https://bugs.eclipse.org/bugs/show_bug.cgi?id=420142
    StatisticsHandler statisticsHandler = new StatisticsHandler();
    statisticsHandler.setHandler(handler);
    return statisticsHandler;
  }
}
