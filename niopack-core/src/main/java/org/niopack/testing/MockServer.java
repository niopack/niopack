package org.niopack.testing;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.jboss.resteasy.core.Dispatcher;
import org.jboss.resteasy.mock.MockDispatcherFactory;
import org.jboss.resteasy.plugins.guice.ModuleProcessor;
import org.jboss.resteasy.spi.HttpRequest;
import org.jboss.resteasy.spi.HttpResponse;
import org.niopack.guice.ResteasyServletModule;

public class MockServer {

  private final Dispatcher dispatcher;
  private final Injector injector;

  private MockServer(Dispatcher dispatcher, Injector injector) {
    super();
    this.dispatcher = dispatcher;
    this.injector = injector;
  }

  public Dispatcher getDispatcher() {
    return dispatcher;
  }

  public Injector getInjector() {
    return injector;
  }

  public void invoke(HttpRequest in, HttpResponse response) {
    dispatcher.invoke(in, response);
  }

  public static MockServer create(ResteasyServletModule module) {
    Dispatcher dispatcher = MockDispatcherFactory.createDispatcher();
    ModuleProcessor processor =
        new ModuleProcessor(dispatcher.getRegistry(), dispatcher.getProviderFactory());
    Injector injector = Guice.createInjector(module);
    processor.processInjector(injector);
    return new MockServer(dispatcher, injector);
  }
}
