package org.niopack;

import static com.google.common.base.Preconditions.checkNotNull;

import com.codahale.metrics.MetricRegistry;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.eclipse.jetty.server.Server;
import org.niopack.configuration.Configuration;
import org.niopack.guice.ResteasyGuiceServletContextListener;
import org.niopack.logging.ContextualLogger;
import org.niopack.logging.LoggingFactory;
import org.niopack.setup.BootstrapModule;
import org.slf4j.Logger;

/**
 * The base class for Niopack applications.
 */
public final class Application {
  static {
    // make sure spinning up Hibernate Validator doesn't yell at us
    LoggingFactory.bootstrap();
  }

  private static final Logger logger = ContextualLogger.create();

  /**
   * Returns the name of the application.
   *
   * @return the application's name
   */
  public String getName() {
    return getClass().getSimpleName();
  }

  /**
   * Runs the application. Call this method from a {@code public static void main} entry point in
   * your application.
   *
   * @param contextListenerClass the Resteasy Guice context listener
   * @throws Exception if something goes wrong
   */
  public void run(Class<? extends ResteasyGuiceServletContextListener> contextListenerClass)
      throws Exception {
    checkNotNull(contextListenerClass, "contextListener");

    Injector bootstrapInjector = Guice.createInjector(new BootstrapModule(this));
    Configuration configuration = bootstrapInjector.getInstance(Configuration.class);
    Server server = null;
    try {
      configuration.getLoggingFactory().configure(
          bootstrapInjector.getInstance(MetricRegistry.class), this.getName());

      ResteasyGuiceServletContextListener contextListener =
          bootstrapInjector.getInstance(contextListenerClass);
      server = configuration.getServerFactory()
          .create(bootstrapInjector, configuration, contextListener);
      server.start();
      server.join();

      // configuration.getMetricsFactory().configure(environment.lifecycle(),
      // bootstrap.getMetricRegistry());
    } catch (Exception e) {
      logger.error(e.getMessage(), e);
      if (server != null) {
        server.stop();
      }
      throw e;
    } finally {
      if (configuration != null) {
        configuration.getLoggingFactory().stop();
      }
    }
  }
}
