package org.niopack.setup;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.health.HealthCheckRegistry;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Throwables;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import java.net.URL;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import org.niopack.Application;
import org.niopack.configuration.Configuration;
import org.niopack.configuration.ConfigurationValidationException;
import org.niopack.configuration.ServerConfig;
import org.niopack.flag.Flag;
import org.niopack.jackson.ObjectMappers;

public class BootstrapModule extends AbstractModule {

  @Flag(help = "Path to configuration file")
  private static final String configFile = new String("web.json");

  private final Application application;

  public BootstrapModule(Application application) {
    this.application = checkNotNull(application, "application");
  }

  @Override
  protected void configure() {
    bind(Application.class).toInstance(application);
    bindConstant().annotatedWith(ServerConfig.class).to(configFile);
    bind(ObjectMapper.class).toInstance(ObjectMappers.forJson());

    bind(MetricRegistry.class).in(Singleton.class);
    bind(HealthCheckRegistry.class).in(Singleton.class);
    bind(Validator.class).toInstance(Validation.buildDefaultValidatorFactory().getValidator());
  }

  @Provides
  @Singleton
  public Configuration provideConfiguration(@ServerConfig String configurationFilePath,
      ObjectMapper objectMapper, Validator validator) {
    URL url = null;
    try {
      url = Thread.currentThread().getContextClassLoader().getResource(configurationFilePath);
      checkState(url != null, "Can't find server configuration file.");

      Configuration config = objectMapper.readValue(url.openStream(), Configuration.class);
      final Set<ConstraintViolation<Configuration>> violations = validator.validate(config);
      if (!violations.isEmpty()) {
        throw new ConfigurationValidationException(configurationFilePath, violations);
      }
      return config;
    } catch (Exception e) {
      Throwables.propagateIfPossible(e);
      throw Throwables.propagate(e);
    }
  }

//  @Provides
//  @Singleton
//  public LifecycleEnvironment provideLifecycleEnvironment() {
//    // How is this used?
//    return new LifecycleEnvironment();
//  }
}
