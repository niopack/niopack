  package org.niopack.guice;

  import com.codahale.metrics.Metric;
  import com.codahale.metrics.health.HealthCheck;
  import com.codahale.metrics.health.jvm.ThreadDeadlockHealthCheck;
  import com.codahale.metrics.jvm.BufferPoolMetricSet;
  import com.codahale.metrics.jvm.GarbageCollectorMetricSet;
  import com.codahale.metrics.jvm.MemoryUsageGaugeSet;
  import com.codahale.metrics.jvm.ThreadStatesGaugeSet;
  import com.codahale.metrics.servlets.AdminServlet;
  import com.google.inject.AbstractModule;
  import com.google.inject.Provides;
  import com.google.inject.multibindings.MapBinder;
  import com.google.inject.multibindings.Multibinder;
  import com.google.inject.servlet.RequestScoped;
  import com.google.inject.servlet.ServletModule;
  import java.lang.management.ManagementFactory;
  import javax.inject.Singleton;
  import javax.ws.rs.core.HttpHeaders;
  import javax.ws.rs.core.Request;
  import javax.ws.rs.core.SecurityContext;
  import javax.ws.rs.core.UriInfo;
  import org.jboss.resteasy.plugins.guice.ext.JaxrsModule;
  import org.jboss.resteasy.plugins.providers.jackson.Jackson2JsonpInterceptor;
  import org.jboss.resteasy.plugins.server.servlet.HttpServletDispatcher;
  import org.jboss.resteasy.spi.ResteasyProviderFactory;
  import org.niopack.resteasy.errors.RootExceptionMapper;
  import org.niopack.resteasy.filter.HttpMethodOverrideFilter;
  import org.niopack.resteasy.jackson.JsonProcessingExceptionMapper;
  import org.niopack.resteasy.params.OffsetDateTimeParamConverterProvider;
  import org.niopack.resteasy.validation.ConstraintViolationExceptionMapper;
  import org.niopack.servlets.tasks.GarbageCollectionTask;
  import org.niopack.servlets.tasks.Task;
  import org.niopack.servlets.tasks.TaskServlet;

/**
 * A Resteasy specific extension of {@link ServletModule}. Extend this class instead of
 * {@link ServletModule} to bind your resource POJOs and {@link javax.ws.rs.ext.Provider} classes.
 * This module binds the following JAX-RS specific objects:
 * <p>
 * <em>Regular scoped</em> JAX-RS bindings:
 * </p>
 * <ul>
 * <li>{@link javax.ws.rs.ext.RuntimeDelegate}</li>
 * <li>{@link javax.ws.rs.core.Response.ResponseBuilder}</li>
 * <li>{@link javax.ws.rs.core.UriBuilder}</li>
 * <li>{@link javax.ws.rs.core.Variant.VariantListBuilder}</li>
 * </ul>
 * <p>
 * <em>RequestScoped</em> JAX-RS bindings:
 * </p>
 * <ul>
 * <li>{@link javax.ws.rs.core.Request}</li>
 * <li>{@link javax.ws.rs.core.HttpHeaders}</li>
 * <li>{@link javax.ws.rs.core.UriInfo}</li>
 * <li>{@link javax.ws.rs.core.SecurityContext}</li>
 * </ul>
 */
public class ResteasyServletModule extends ServletModule {

  @Override
  protected final void configureServlets() {
    // Install common bindings (skipped if already installed).
    install(new InternalResteasyServletModule());

    // Install local filter, servlet and resource bindings.
    configureResources();
  }

  protected void configureResources() {}

  /**
   * This is a left-factoring of all ServletModules installed in the system. In other words, this
   * module contains the bindings common to all ResteasyModules, and is bound exactly once per
   * injector.
   */
  private static final class InternalResteasyServletModule extends AbstractModule {

    @Override
    protected void configure() {
      install(new JaxrsModule());
      bind(HttpMethodOverrideFilter.class).in(Singleton.class);
      install(new ServletModule() {
        @Override
        protected void configureServlets() {
          // Admin stuff
          MapBinder<String, Metric> metricsBinder =
              MapBinder.newMapBinder(binder(), String.class, Metric.class);
          metricsBinder.addBinding("jvm.buffers").toInstance(
              new BufferPoolMetricSet(ManagementFactory.getPlatformMBeanServer()));
          metricsBinder.addBinding("jvm.gc").toInstance(new GarbageCollectorMetricSet());
          metricsBinder.addBinding("jvm.memory").toInstance(new MemoryUsageGaugeSet());
          metricsBinder.addBinding("jvm.threads").toInstance(new ThreadStatesGaugeSet());

          MapBinder<String, HealthCheck> healthChecksBinder =
              MapBinder.newMapBinder(binder(), String.class, HealthCheck.class);
          healthChecksBinder.addBinding("deadlocks").toInstance(new ThreadDeadlockHealthCheck());

          bind(AdminServlet.class).in(Singleton.class);
          serve("/admin/*").with(AdminServlet.class);

          // Tasks
          Multibinder<Task> tasksBinder = Multibinder.newSetBinder(binder(), Task.class);
          tasksBinder.addBinding().toInstance(new GarbageCollectionTask());

          bind(TaskServlet.class).in(Singleton.class);
          serve("/tasks/*").with(TaskServlet.class);

          bind(OffsetDateTimeParamConverterProvider.class).in(Singleton.class);
          bind(RootExceptionMapper.class).in(Singleton.class);
          bind(ConstraintViolationExceptionMapper.class).in(Singleton.class);
          bind(JsonProcessingExceptionMapper.class).in(Singleton.class);
          bind(Jackson2JsonpInterceptor.class).in(Singleton.class);
          
          bind(HttpServletDispatcher.class).in(Singleton.class);
          serve("/api/*").with(HttpServletDispatcher.class);
        }
      });
    }

    @Provides
    @RequestScoped
    Request provideRequest() {
      return ResteasyProviderFactory.getContextData(Request.class);
    }

    @Provides
    @RequestScoped
    HttpHeaders provideHttpHeaders() {
      return ResteasyProviderFactory.getContextData(HttpHeaders.class);
    }

    @Provides
    @RequestScoped
    UriInfo provideUriInfo() {
      return ResteasyProviderFactory.getContextData(UriInfo.class);
    }

    @Provides
    @RequestScoped
    SecurityContext provideSecurityContext() {
      return ResteasyProviderFactory.getContextData(SecurityContext.class);
    }

    @Override
    public boolean equals(Object o) {
      // Is only ever installed internally, so we don't need to check state.
      return o instanceof InternalResteasyServletModule;
    }

    @Override
    public int hashCode() {
      return InternalResteasyServletModule.class.hashCode();
    }
  }
}
