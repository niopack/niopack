package org.niopack.guice;

import static com.google.common.base.Preconditions.checkNotNull;

import com.codahale.metrics.Metric;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.health.HealthCheck;
import com.codahale.metrics.health.HealthCheckRegistry;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.Module;
import com.google.inject.TypeLiteral;
import com.google.inject.servlet.GuiceServletContextListener;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import org.jboss.resteasy.plugins.guice.ModuleProcessor;
import org.jboss.resteasy.spi.Registry;
import org.jboss.resteasy.spi.ResteasyProviderFactory;
import org.niopack.logging.ContextualLogger;
import org.niopack.servlets.tasks.Task;
import org.slf4j.Logger;

/**
 * A subclass of {@link GuiceServletContextListener}. Extend this class to supply {@link Module}
 * instances required for your service. This will ensure the injector is created when the web
 * application is deployed. This also scans for JAX-RS resource and {@link javax.ws.rs.ext.Provider}
 * bindings and registers them with Resteasy.
 */
public abstract class ResteasyGuiceServletContextListener extends GuiceServletContextListener {

  private static final Logger logger = ContextualLogger.create();

  @Inject private Injector bootstrapInjector = null;
  private ServletContext servletContext;

  public ServletContext getServletContext() {
    return servletContext;
  }

  @Override
  protected Injector getInjector() {
    Registry registry = (Registry) servletContext.getAttribute(Registry.class.getName());
    ResteasyProviderFactory providerFactory = (ResteasyProviderFactory)
        servletContext.getAttribute(ResteasyProviderFactory.class.getName());
    ModuleProcessor processor = new ModuleProcessor(registry, providerFactory);
    Injector applicationInjector = bootstrapInjector == null
        ? Guice.createInjector(getModules())
        : bootstrapInjector.createChildInjector(getModules());
    // Scan for resources and providers
    Injector injector = applicationInjector;
    do {
      processor.processInjector(injector);
      injector = injector.getParent();
    } while (injector != null);
    withInjector(applicationInjector);
    return applicationInjector;
  }

  protected void withInjector(Injector injector){
    registerMetrics(injector);
    registerHealthChecks(injector);
    logTasks(injector);
    logHealthChecks(injector);
  }

  /**
   * Returns the collection of modules used to create application Guice injector.
   */
  protected abstract Iterable<Module> getModules();

  @Override
  public void contextInitialized(ServletContextEvent servletContextEvent) {
    this.servletContext = checkNotNull(servletContextEvent.getServletContext(), "servletContext");
    super.contextInitialized(servletContextEvent);
  }

  private void registerMetrics(Injector injector) {
    Map<String, Metric> metrics =
        injector.getInstance(Key.get(new TypeLiteral<Map<String, Metric>>() {}));
    MetricRegistry registry = injector.getInstance(MetricRegistry.class);
    for (Entry<String, Metric> entry : metrics.entrySet()) {
      registry.register(entry.getKey(), entry.getValue());
    }
  }

  private void registerHealthChecks(Injector injector) {
    Map<String, HealthCheck> healthChecks =
        injector.getInstance(Key.get(new TypeLiteral<Map<String, HealthCheck>>() {}));
    HealthCheckRegistry registry = injector.getInstance(HealthCheckRegistry.class);
    for (Entry<String, HealthCheck> entry : healthChecks.entrySet()) {
      registry.register(entry.getKey(), entry.getValue());
    }
  }

  private void logTasks(Injector injector) {
    Set<Task> tasks = injector.getInstance(Key.get(new TypeLiteral<Set<Task>>() {}));
    StringBuilder stringBuilder = new StringBuilder(1024).append(String.format("%n%n"));
    for (Task task : tasks) {
      stringBuilder.append(String.format("    %-7s /tasks/%s (%s)%n", "POST",
          task.getName(),
          task.getClass().getCanonicalName()));
    }
    logger.info("tasks = {}", stringBuilder.toString());
  }

  private void logHealthChecks(Injector injector) {
    HealthCheckRegistry registry = injector.getInstance(HealthCheckRegistry.class);
    if (registry.getNames().size() <= 1) {
      logger.warn(String.format("%n"
          + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%n"
          + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%n"
          + "!    THIS APPLICATION HAS NO HEALTHCHECKS. THIS MEANS YOU WILL NEVER KNOW      !%n"
          + "!     IF IT DIES IN PRODUCTION, WHICH MEANS YOU WILL NEVER KNOW IF YOU'RE      !%n"
          + "!    LETTING YOUR USERS DOWN. YOU SHOULD ADD A HEALTHCHECK FOR EACH OF YOUR    !%n"
          + "!         APPLICATION'S DEPENDENCIES WHICH FULLY (BUT LIGHTLY) TESTS IT.       !%n"
          + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!%n"
          + "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"));
    }
    logger.debug("health checks = {}", registry.getNames());
  }
}
