package org.niopack.nio.core;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public final class RestService {
  private String path;
  private MediaType produces = MediaType.APPLICATION_JSON;

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public MediaType getProduces() {
    return produces;
  }

  public void setProduces(MediaType produces) {
    this.produces = produces;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(path, produces);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof RestService)) {
      return false;
    }
    RestService that = (RestService) obj;
    return Objects.equal(this.path, that.path)
        && Objects.equal(this.produces, that.produces);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("path", path)
        .add("produces", produces)
        .toString();
  }
}
