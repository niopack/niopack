package org.niopack.nio.core;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public final class Field {

  private FieldStatus status = FieldStatus.ACTIVE;

  public FieldStatus getStatus() {
    return status;
  }

  public void setStatus(FieldStatus status) {
    this.status = status;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(status);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Field)) {
      return false;
    }
    Field that = (Field) obj;
    return Objects.equal(this.status, that.status);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("status", status)
        .toString();
  }
}
