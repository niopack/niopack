package org.niopack.nio.core;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableTable;
import com.google.common.collect.Table;
import java.util.List;
import java.util.Map;

public final class ParamType_Nio {
  public static final String QUERY = "QUERY";
  public static final String FORM = "FORM";
  public static final String PATH = "PATH";

  public static final List<String> _FIELDS;
  public static final Map<Class<?>, Object> _TYPE_ANNOTATIONS;
  public static final Table<String, Class<?>, Object> _FIELD_ANNOTATIONS;

  static {
    ImmutableList.Builder<String> fields = ImmutableList.<String>builder();
    fields.add(QUERY);
    fields.add(FORM);
    fields.add(PATH);
    _FIELDS = fields.build();

    _TYPE_ANNOTATIONS = ImmutableMap.<Class<?>, Object>of();

    ImmutableTable.Builder<String, Class<?>, Object> fieldAnnotations =
        ImmutableTable.<String, Class<?>, Object>builder();
    Seq querySeq = new Seq();
    querySeq.setPos(1);
    fieldAnnotations.put(QUERY, Seq.class, querySeq);
    Seq formSeq = new Seq();
    formSeq.setPos(2);
    fieldAnnotations.put(FORM, Seq.class, formSeq);
    Seq pathSeq = new Seq();
    pathSeq.setPos(3);
    fieldAnnotations.put(PATH, Seq.class, pathSeq);
    _FIELD_ANNOTATIONS = fieldAnnotations.build();
  }

  private ParamType_Nio() {}
}
