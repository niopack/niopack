package org.niopack.nio.core;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableTable;
import com.google.common.collect.Table;
import java.util.List;
import java.util.Map;

public final class Error_Nio {
  public static final String ERROR_CODE = "errorCode";
  public static final String LOG_ID = "logId";
  public static final String DEBUG_MESSAGE = "debugMessage";
  public static final String STACK_TRACE = "stackTrace";

  public static final List<String> _FIELDS;
  public static final Map<Class<?>, Object> _TYPE_ANNOTATIONS;
  public static final Table<String, Class<?>, Object> _FIELD_ANNOTATIONS;

  static {
    ImmutableList.Builder<String> fields = ImmutableList.<String>builder();
    fields.add(ERROR_CODE);
    fields.add(LOG_ID);
    fields.add(DEBUG_MESSAGE);
    fields.add(STACK_TRACE);
    _FIELDS = fields.build();

    _TYPE_ANNOTATIONS = ImmutableMap.<Class<?>, Object>of();

    _FIELD_ANNOTATIONS = ImmutableTable.<String, Class<?>, Object>of();
  }

  private Error_Nio() {}
}
