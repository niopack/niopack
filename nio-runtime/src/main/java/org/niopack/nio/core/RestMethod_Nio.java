package org.niopack.nio.core;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableTable;
import com.google.common.collect.Table;
import java.util.List;
import java.util.Map;

public final class RestMethod_Nio {
  public static final String VERB = "verb";
  public static final String PATH = "path";

  public static final List<String> _FIELDS;
  public static final Map<Class<?>, Object> _TYPE_ANNOTATIONS;
  public static final Table<String, Class<?>, Object> _FIELD_ANNOTATIONS;

  static {
    ImmutableList.Builder<String> fields = ImmutableList.<String>builder();
    fields.add(VERB);
    fields.add(PATH);
    _FIELDS = fields.build();

    _TYPE_ANNOTATIONS = ImmutableMap.<Class<?>, Object>of();

    _FIELD_ANNOTATIONS = ImmutableTable.<String, Class<?>, Object>of();
  }

  private RestMethod_Nio() {}
}
