package org.niopack.nio.core;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public final class Error {
  private String errorCode;
  private String logId;
  private String debugMessage;
  private String stackTrace;

  public String getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }

  public String getLogId() {
    return logId;
  }

  public void setLogId(String logId) {
    this.logId = logId;
  }

  public String getDebugMessage() {
    return debugMessage;
  }

  public void setDebugMessage(String debugMessage) {
    this.debugMessage = debugMessage;
  }

  public String getStackTrace() {
    return stackTrace;
  }

  public void setStackTrace(String stackTrace) {
    this.stackTrace = stackTrace;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(
        errorCode,
        logId,
        debugMessage,
        stackTrace);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Error)) {
      return false;
    }
    Error that = (Error) obj;
    return Objects.equal(this.errorCode, that.errorCode)
        && Objects.equal(this.logId, that.logId)
        && Objects.equal(this.debugMessage, that.debugMessage)
        && Objects.equal(this.stackTrace, that.stackTrace);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("errorCode", errorCode)
        .add("logId", logId)
        .add("debugMessage", debugMessage)
        .add("stackTrace", stackTrace)
        .toString();
  }
}
