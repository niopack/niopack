package org.niopack.nio.core;

public enum FieldStatus {
  ACTIVE(1),
  DEPRECATED(2),
  RETIRED(3);

  private final int value;

  private FieldStatus(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }
}
