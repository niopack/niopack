package org.niopack.nio.core;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableTable;
import com.google.common.collect.Table;
import java.util.List;
import java.util.Map;

public final class MediaType_Nio {
  public static final String APPLICATION_JSON = "APPLICATION_JSON";

  public static final List<String> _FIELDS;
  public static final Map<Class<?>, Object> _TYPE_ANNOTATIONS;
  public static final Table<String, Class<?>, Object> _FIELD_ANNOTATIONS;

  static {
    ImmutableList.Builder<String> fields = ImmutableList.<String>builder();
    fields.add(APPLICATION_JSON);
    _FIELDS = fields.build();

    ImmutableTable.Builder<String, Class<?>, Object> fieldAnnotations =
        ImmutableTable.<String, Class<?>, Object>builder();
    Seq applicationJsonSeq = new Seq();
    applicationJsonSeq.setPos(1);
    fieldAnnotations.put(APPLICATION_JSON, Seq.class, applicationJsonSeq);
    _FIELD_ANNOTATIONS = fieldAnnotations.build();

    _TYPE_ANNOTATIONS = ImmutableMap.<Class<?>, Object>of();
  }

  private MediaType_Nio() {}
}
