package org.niopack.nio.core;

public enum MediaType {
  APPLICATION_JSON(1);

  private final int value;

  private MediaType(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }
}
