package org.niopack.nio.core;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableTable;
import com.google.common.collect.Table;
import java.util.List;
import java.util.Map;

public final class HttpVerb_Nio {
  public static final String GET = "GET";
  public static final String POST = "POST";
  public static final String PUT = "PUT";
  public static final String DELETE = "DELETE";

  public static final List<String> _FIELDS;
  public static final Map<Class<?>, Object> _TYPE_ANNOTATIONS;
  public static final Table<String, Class<?>, Object> _FIELD_ANNOTATIONS;

  static {
    ImmutableList.Builder<String> fields = ImmutableList.<String>builder();
    fields.add(GET);
    fields.add(POST);
    fields.add(PUT);
    fields.add(DELETE);
    _FIELDS = fields.build();

    ImmutableTable.Builder<String, Class<?>, Object> fieldAnnotations =
        ImmutableTable.<String, Class<?>, Object>builder();
    Seq getSeq = new Seq();
    getSeq.setPos(1);
    fieldAnnotations.put(GET, Seq.class, getSeq);
    Seq postSeq = new Seq();
    postSeq.setPos(2);
    fieldAnnotations.put(POST, Seq.class, postSeq);
    Seq putSeq = new Seq();
    putSeq.setPos(3);
    fieldAnnotations.put(PUT, Seq.class, putSeq);
    Seq deleteSeq = new Seq();
    deleteSeq.setPos(4);
    fieldAnnotations.put(DELETE, Seq.class, deleteSeq);
    _FIELD_ANNOTATIONS = fieldAnnotations.build();

    _TYPE_ANNOTATIONS = ImmutableMap.<Class<?>, Object>of();
  }

  private HttpVerb_Nio() {}
}
