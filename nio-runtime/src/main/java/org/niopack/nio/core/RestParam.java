package org.niopack.nio.core;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public final class RestParam {
  private ParamType type;
  private String name;

  public ParamType getType() {
    return type;
  }

  public void setType(ParamType type) {
    this.type = type;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public int hashCode() {
    return Objects.hashCode(type, name);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof RestParam)) {
      return false;
    }
    RestParam that = (RestParam) obj;
    return Objects.equal(this.type, that.type)
        && Objects.equal(this.name, that.name);
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this)
        .add("type", type)
        .add("name", name)
        .toString();
  }
}
