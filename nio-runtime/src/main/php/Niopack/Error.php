<?php namespace Niopack;

use JsonSerializable;

final class Error implements JsonSerializable
{
    public $errorCode;
    public $logId;
    public $debugMessage;
    public $stackTrace;

    /**
     * @param array $json
     */
    public function __construct(array $json = [])
    {
        if (isset($json['errorCode'])) {
            $this->errorCode = $json['errorCode'];
        }
        if (isset($json['logId'])) {
            $this->logId = $json['logId'];
        }
        if (isset($json['debugMessage'])) {
            $this->debugMessage = $json['debugMessage'];
        }
        if (isset($json['stackTrace'])) {
            $this->stackTrace = $json['stackTrace'];
        }
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
      $json = [];
      if (isset($this->errorCode)) {
          $json['errorCode'] = $this->errorCode;
      }
      if (isset($this->logId)) {
          $json['logId'] = $this->logId;
      }
      if (isset($this->debugMessage)) {
          $json['debugMessage'] = $this->debugMessage;
      }
      if (isset($this->stackTrace)) {
          $json['stackTrace'] = $this->stackTrace;
      }
      return $json;
    }
}
