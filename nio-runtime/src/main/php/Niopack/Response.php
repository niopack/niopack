<?php namespace Niopack;

use Exception;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Message\ResponseInterface;

final class Response
{

    private $rawResponse;
    private $responseEntityClass;

    private function __construct()
    {
    }

    /**
     * @param Exception $e
     * @return Response
     */
    public static function fromException(Exception $e)
    {
        $response = new Response();
        $response->responseEntityClass = '\Niopack\Error';
        if ($e instanceof RequestException && $e->hasResponse()) {
            $response->rawResponse = $e->getResponse();
        } else {
            $response->rawResponse = new ExceptionResponse($e->getMessage());
        }
        return $response;
    }

    /**
     * @param ResponseInterface $rawResponse
     * @param $responseEntityClass
     * @return Response
     */
    public static function of(ResponseInterface $rawResponse, $responseEntityClass)
    {
        $response = new Response();
        $response->rawResponse = $rawResponse;
        $response->responseEntityClass = $responseEntityClass;
        return $response;
    }

    /**
     * @return bool
     */
    public function hasRawResponse()
    {
        return isset($this->rawResponse);
    }

    /**
     * @return bool
     */
    public function isSuccess()
    {
        return isset($this->rawResponse)
        && $this->rawResponse->getStatusCode() >= 200
        && $this->rawResponse->getStatusCode() < 300;
    }

    /**
     * @return mixed
     */
    public function getResponseEntity()
    {
        if ($this->responseEntityClass === '') {
            return $this->rawResponse->getBody();
        }
        return new $this->responseEntityClass($this->rawResponse->json());
    }

    /**
     * @return mixed
     */
    public function getRawResponse()
    {
        return $this->rawResponse;
    }
}
