<?php namespace Niopack;

final class ExceptionResponse
{
    /**
     * @var \Niopack\Error
     */
    public $error;

    /**
     * @param $debugMessage string exception message
     */
    public function __construct($debugMessage)
    {
        $this->error = new Error();
        $this->error->debugMessage = $debugMessage;
    }

    /**
     * Gets the response Status-Code. This is set to a non-overlapping error code with HTTP status code.
     *
     * @return int Status code.
     */
    public function getStatusCode()
    {
        return 0;
    }

    /**
     * Parse the JSON response body and return the JSON decoded data.
     *
     * @return array Returns the JSON decoded data.
     */
    public function json()
    {
        return $this->error->jsonSerialize();
    }
}
