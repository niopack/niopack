package org.niopack.metrics;

import static com.google.common.truth.Truth.assertThat;

import org.junit.Test;
import org.niopack.jackson.DiscoverableSubtypeResolver;

public class ConsoleReporterFactoryTest {
  @Test
  public void isDiscoverable() throws Exception {
    assertThat(new DiscoverableSubtypeResolver().getDiscoveredSubtypes())
        .contains(ConsoleReporterFactory.class);
  }
}
