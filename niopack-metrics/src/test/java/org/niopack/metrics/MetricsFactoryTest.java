package org.niopack.metrics;

import static com.google.common.truth.Truth.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Resources;
import java.io.File;
import org.junit.Before;
import org.junit.Test;
import org.niopack.jackson.ObjectMappers;
import org.niopack.logging.LoggingFactory;
import org.niopack.util.Duration;

public class MetricsFactoryTest {
  static {
    LoggingFactory.bootstrap();
  }

  private final ObjectMapper objectMapper = ObjectMappers.forJson();
  private MetricsFactory config;

  @Before
  public void setUp() throws Exception {
    objectMapper.getSubtypeResolver().registerSubtypes(ConsoleReporterFactory.class,
        CsvReporterFactory.class,
        Slf4jReporterFactory.class);

    this.config =
        objectMapper.readValue(Resources.getResource("json/metrics.json"), MetricsFactory.class);
  }

  @Test
  public void hasADefaultFrequency() throws Exception {
    assertThat(config.getFrequency()).isEqualTo(Duration.seconds(10));
  }

  @Test
  public void hasReporters() throws Exception {
    CsvReporterFactory csvReporter = new CsvReporterFactory();
    csvReporter.setFile(new File("metrics.csv"));
    assertThat(config.getReporters()).hasSize(3);
  }
}
