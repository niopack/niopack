package org.niopack.metrics;

import static com.google.common.truth.Truth.assertThat;

import org.junit.Test;
import org.niopack.jackson.DiscoverableSubtypeResolver;

public class Slf4jReporterFactoryTest {
  @Test
  public void isDiscoverable() throws Exception {
    assertThat(new DiscoverableSubtypeResolver().getDiscoveredSubtypes())
        .contains(Slf4jReporterFactory.class);
  }
}
