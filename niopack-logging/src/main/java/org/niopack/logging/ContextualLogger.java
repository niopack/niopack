package org.niopack.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A factory for creating {@link Logger} whose name is set to the containing class.
 */
public final class ContextualLogger {

  public static Logger create() {
  return LoggerFactory.getLogger(new ClassLocator().getCallerClass());
  }

  private static class ClassLocator extends SecurityManager {

  private Class<?> getCallerClass() {
    return getClassContext()[3];
  }
  }
}
