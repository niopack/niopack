package org.niopack.logging;

import com.google.common.base.MoreObjects;
import java.net.URL;
import java.util.logging.LogManager;

/**
 * A java.util.logging.config.class for loading configuration from jar file. To use set,
 * {@code -Djava.util.logging.config.class=org.niopack.logging.ConfigLoader}.
 * <p>
 * By default, it will look for the {@code logging.properties} file inside the running jar. The file
 * path can be changed by setting {@code java.util.logging.config.file} system property.
 *
 * @see <a
 * href="http://docs.oracle.com/javase/7/docs/api/java/util/logging/LogManager.html">
 * java.util.logging.LogManager</a>
 */
public class ConfigLoader {

  public ConfigLoader() {
    String configFile = MoreObjects.firstNonNull(
      System.getProperty("java.util.logging.config.file"), "logging.properties");
    URL url = null;
    try {
      url = Thread.currentThread().getContextClassLoader().getResource(configFile);
      if (url == null) {
        System.err.println("Cannot find logging.properties.");
      } else {
        LogManager.getLogManager().readConfiguration(url.openStream());
      }
    } catch (Exception e) {
      System.err.println("Error reading logging.properties from '" + url + "': " + e);
    }
  }
}
