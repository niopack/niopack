package org.niopack.logging;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.slf4j.Logger;

@RunWith(JUnit4.class)
public final class ContextualLoggerTest {

  private static final Logger staticLogger = ContextualLogger.create();

  @Test
  public void testStaticLoggerName() {
  assertEquals("org.niopack.logging.ContextualLoggerTest", staticLogger.getName());
  }

  @Test
  public void testLocalLoggerName() {
  Logger localLogger = ContextualLogger.create();
  assertEquals("org.niopack.logging.ContextualLoggerTest", localLogger.getName());
  }
}
