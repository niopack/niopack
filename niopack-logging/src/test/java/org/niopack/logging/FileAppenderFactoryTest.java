package org.niopack.logging;

import static com.google.common.truth.Truth.assertThat;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.core.rolling.RollingFileAppender;
import org.junit.Test;
import org.niopack.jackson.DiscoverableSubtypeResolver;

public class FileAppenderFactoryTest {
  @Test
  public void isDiscoverable() throws Exception {
    assertThat(new DiscoverableSubtypeResolver().getDiscoveredSubtypes())
        .contains(FileAppenderFactory.class);
  }

  @Test
  public void isRolling() throws Exception {
    // the method we want to test is protected, so we need to override it so we can see it
    FileAppenderFactory fileAppenderFactory = new FileAppenderFactory() {};

    fileAppenderFactory.setCurrentLogFilename("logfile.log");
    fileAppenderFactory.setArchive(true);
    fileAppenderFactory.setArchivedLogFilenamePattern("example-%d.log.gz");
    assertThat(fileAppenderFactory.buildAppender(new LoggerContext())).isInstanceOf(RollingFileAppender.class);
  }
}
