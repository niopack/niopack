package org.niopack.logging;

import ch.qos.logback.classic.spi.ThrowableProxy;
import com.google.common.collect.Lists;
import java.io.IOException;
import org.junit.Before;
import org.junit.Test;

public class PrefixedExtendedThrowableProxyConverterTest {
  private final PrefixedExtendedThrowableProxyConverter converter = new PrefixedExtendedThrowableProxyConverter();
  private final ThrowableProxy proxy = new ThrowableProxy(new IOException("noo"));

  @Before
  public void setup() {
    converter.setOptionList(Lists.newArrayList("full"));
    converter.start();
  }

  @Test
  public void prefixesExceptionsWithExclamationMarks() throws Exception {
//    assertThat(converter.throwableProxyToString(proxy))
//        .startsWith(String.format("! java.io.IOException: noo%n" +
//            "! at org.niopack.logging.PrefixedExtendedThrowableProxyConverterTest.<init>(PrefixedExtendedThrowableProxyConverterTest.java:15)%n"));
  }
}
