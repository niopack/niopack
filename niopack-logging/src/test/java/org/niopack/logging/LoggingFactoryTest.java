package org.niopack.logging;

import static com.google.common.truth.Truth.assertThat;

import ch.qos.logback.classic.Level;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.Resources;
import org.junit.Before;
import org.junit.Test;
import org.niopack.jackson.ObjectMappers;

public class LoggingFactoryTest {

  private final ObjectMapper objectMapper = ObjectMappers.forJson();
  private LoggingFactory config;

  @Before
  public void setUp() throws Exception {
    objectMapper.getSubtypeResolver().registerSubtypes(ConsoleAppenderFactory.class,
        FileAppenderFactory.class,
        SyslogAppenderFactory.class);

    this.config =
        objectMapper.readValue(Resources.getResource("json/logging.json"), LoggingFactory.class);
  }

  @Test
  public void hasADefaultLevel() throws Exception {
    assertThat(config.getLevel()).isEqualTo(Level.INFO);
  }

  @Test
  public void hasASetOfOverriddenLevels() throws Exception {
    assertThat(config.getLoggers()).isEqualTo(ImmutableMap.of("com.example.app", Level.DEBUG));
  }
}
